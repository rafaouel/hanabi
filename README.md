# Hanabi

## Version compatibilities

### Data model v1 vs data model v2

A refactor of the model of the data have been conducted, you will find a summary of the images version to use before and after this changes:

| image				| version using data model v1	| version using data model v2	|
|:-----------------:|:-----------------------------:|:-----------------------------:|
| hanabi_server 	| <= 1.10.2						| >= 2.0.0						|
| hanabi_viewer		| <= 1.5.0						| >= 2.0.0						|
| hanabi_api		| <= 1.23.3						| >= 2.0.0						|
| optimal			| <= 1.0.2						| >= 2.0.0						|
| hanabi_client		| < 2.0.0						| >= 2.0.0						|

In the ConvertDB project, the script 'data_v1_to_v2.py' is responsible for this conversion

### Pagination

A refactor of the API (add the pagination) leads to compatibility breakdown. Please use the following versions of the stack:

| image				| version using pagination	| Remarks	|
|:-----------------:|:-------------------------:|:----------|
| hanabi_server 	| >= 2.2.1					|
| hanabi_viewer		| >= 2.2.0  				|
| hanabi_api		| >= 2.5.1					|
| optimal			| >= 2.1.2					|
| hanabi_ai_random	| -							| This software is not impacted by those changes	|
| hanabi_client		| -							| This software is not impacted by those changes	|

### Minor changes to the data model v2

 - An attribute 'reactsTo' is introduced to 'gameOption.colorOption'.
 - An attribute 'callableColors' is introduced to 'gameOption'
Please use the following versions of the stack to ensure using this modification:

| image				| version using model change	| Remarks	|
|:-----------------:|:-----------------------------:|:----------|
| hanabi_server		| >= 2.3.1						|
| hanabi_viewer		| >= 2.3.0						|
| hanabi_api		| >= 2.9.0						|
| optimal			| >= 2.1.2						| This software is not impacted by those changes	|
| hanabi_ai_random	| >= 1.13.0						| 
| hanabi_client		| >= 2.2.1						|

### Minor changes to the data model v2

- Two attributes 'score', 'maxScore', 'errorTokenCount', 'maxErrorTokenCount', 'elapsedGameTurnCount' have been added to game end
- An array of statistics have been added to summarize the game actions
Please use the following version of the stack to ennsure using this modification:

| image				| versionn using model change	| Remarks	|
|:-----------------:|:-----------------------------:|:----------|
| hanabi_server		| >= 2.5.1						|
| hanabi_viewer		| >= 2.3.1						|
| hanabi_api		| >= 2.10.2						|
| optimal			| >= 2.1.2						| This software is not impacted by those changes	|
| hanabi_ai_random	| >= 1.13.0						| This software is not impacted by those changes	|
| hanabi_client		| >= 2.3.1							| 


## Deploy the database

### Docker

First we need to create volumes for the database. Those lines have to be run once, before the first docker compose run.

The volume can be build in a different but they have to exist before the next step

``` bash
docker volume create db_data
docker volume create db_config
```

Then we can start the docker compose from the repository root:

``` bash
docker compose up -d
```

### Kubernetes

Create a mongo Statefullset

Then enter the pod and...

``` bash
# Connect to the database
mongosh -u "root"

# Enter the root password
# ...

# Create a database for hanabi
use hanabi

# Create a user for the API
db.createUser({"user":<login>, "pwd":<password>, "roles":["readWrite"]})
```
