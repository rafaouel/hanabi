import datetime
import requests


class ApiGateway:
    def __init__(self, host, port, login, password):
        self.__host = host
        self.__port = port
        self.__login = login
        self.__password = password
        self.__access_token = ""
        self.__access_token_expiration = datetime.datetime.min
        self.__refresh_token = ""
        self.__refresh_tokenn_expiration = datetime.datetime.min
        self.__expiration_margin = datetime.timedelta(seconds=30)

    def __get_access_token(self):

        now = datetime.datetime.now()

        # Is an access token present ?
        if len(self.__access_token) > 0:

            # Is the access token expired ?
            if now + self.__expiration_margin < self.__access_token_expiration:
                return self.__access_token

        # Is a refresh token present ?
        if len(self.__refresh_token) > 0:

            # Is the refresh tocken expired ?
            if now + self.__expiration_margin < self.__refresh_tokenn_expiration:

                self.__refresh_tokens()

                return self.__access_token

        # At this point there is neither a valid access token nor a valid refresh token
        # Lets aks for a token from login + password
        self.__create_tokens()
        return self.__access_token


    def __parse_tokens(self, body):
        self.__access_token = body["accessToken"]
        self.__access_token_expiration = datetime.datetime.fromisoformat(body["accessTokenExpirationDate"].replace("Z", ""))
        self.__refresh_token = body["refreshToken"]
        self.__refresh_tokenn_expiration = datetime.datetime.fromisoformat(body["refreshTokenExpirationDate"].replace("Z", ""))


    def __refresh_tokens(self):
        
        url = "http://{}:{}/api/v1/tokens".format(self.__host, self.__port)
        headers = {"Authorization": "Bearer {}".format(self.__refresh_token)}
        res = requests.put(url, headers=headers)

        if not res.ok:
            raise Exception("Unable to refresh tokens")

        self.__parse_tokens(res.json())


    def __create_tokens(self):
        
        url = "http://{}:{}/api/v1/tokens".format(self.__host, self.__port)
        res = requests.post(url, auth=(self.__login, self.__password))

        if not res.ok:
            raise Exception("Unable to retrieve tokens")

        self.__parse_tokens(res.json())

    def get_game_overview(self, game_uuid):

        url = "http://{}:{}/api/v2/hanabi/game/overview".format(self.__host, self.__port)
        params = { "gameUuid": game_uuid }
        token = self.__get_access_token()
        headers = { "Authorization": "Bearer {}".format(token) }
        res = requests.get(url, headers=headers, params=params)
    
        if not res.ok:
            raise Exception("Unable to get the game overview")

        data = res.json()["data"]
        if len(data) == 0:
            raise Exception("No game overview returned")

        return data[0]


    def get_player_action(self, game_uuid, player_turn_count):

        url = "http://{}:{}/api/v1/hanabi/playerAction".format(self.__host, self.__port)
        params = { 
            "gameUuid": game_uuid,
            "playerTurnCount": player_turn_count }
        token = self.__get_access_token()
        headers = { "Authorization": "Bearer {}".format(token) }
        res = requests.get(url, headers=headers, params=params)
    
        if not res.ok:
            raise Exception("Unable to get the player action")

        data = res.json()["data"]
        if len(data) == 0:
            raise Exception("No player action returned")

        return data[0]
