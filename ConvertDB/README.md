# ConvertDB

This project is responsible to convert the database from data model v1 to data model v2.
Just define the database host, port and root credentials then run the program:

``` bash
# define DB_HOST, the hostname or IP of the database
# define DB_PORT, the port of the database
# define DB_LOGIN, the login of the database

python data_v1_to_v2.py

# The program will ask for the database root password
```


## data_v1_to_v2.py

This script converts from data model from v1 to v2

## add_reactsat.py

This script adds 'reactsTo' and 'callableColors' into game options from standard values

## add_stats.py

This script completes the game ends with the following informations:

- add 'elapsedGameTurnCount', 'score' and 'maxScore', 'errorTokenCount', 'maxErrorTokenCount' from associated game data
- add 'statistics' array computed from the associated game data

