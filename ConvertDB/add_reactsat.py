# This script adds a 'reactsAt' in each 'gameOption.colorOption' and the corresponding value

if __name__ == "__main__":

    try:
        import getpass
        import pymongo
        import os

        host = os.environ["DB_HOST"]
        port = os.environ["DB_PORT"]
        login = os.environ["DB_LOGIN"]
    
        print("host=", host)
        print("port=", port)
        print("login=", login)
        password = getpass.getpass()

        # Define the conversion mapping
        def get_reaction(color, color_options):
            if color == "blue":
                return ["blue"]
            elif color == "white":
                return ["white"]
            elif color == "red":
                return ["red"]
            elif color == "yellow":
                return ["yellow"]
            elif color == "green":
                return ["green"]
            elif color == "black":
                return []
            elif color == "multicolor":
                colors = []
                for option in color_options:
                    if option["color"] != "multicolor" and option["color"] != "black":
                        colors.append(option["color"])
                return colors
            else:
                raise Exception("Unknown color '{}'".format(color))

        # Create client
        my_client = pymongo.MongoClient("mongodb://{}:{}@{}:{}/".format(login, password, host, port))

        # Check if the database 'hanabi' exists
        db_list = my_client.list_database_names()
        database_name = "hanabi"
        if not database_name in db_list:
            raise Exception("'{}' database do not exists !".format(database_name))
        print("'{}' database exits".format(database_name))
        hanabi_database = my_client[database_name]

        # Check if the collection 'gameOption' exits
        col_list = hanabi_database.list_collection_names()
        game_option_collection_name = "gameOption"
        if not game_option_collection_name in col_list:
            raise Exception("'{}' collection do not exists !".format(game_option_collection_name))
        print("'{}' collection exists".format(game_option_collection_name))

        # For each 'gameOption' document...
        reaction_total_count = 0
        callable_color_total_count = 0
        for game_option in hanabi_database[game_option_collection_name].find():

            print("Converting gameOption '{}'...".format(game_option["_id"]))

            filter = { "_id": game_option["_id"]}

            # For each color option...
            color_id = 0
            for color_option in game_option["colorOption"]:

                reaction_count = 0
                callable_color_count = 0

                color = color_option["color"]
                
                # Add the new field 'reactsTo'
                update = { "$set": {"colorOption.{}.reactsTo".format(color_id) : [] }}
                reactions = get_reaction(color, game_option["colorOption"])
                for reaction in reactions:
                    update["$set"]["colorOption.{}.reactsTo".format(color_id)].append(reaction);
                result = hanabi_database[game_option_collection_name].update_one(filter, update);
                if result.matched_count == 1 and result.modified_count == 1:
                    print("'{}' gameOption reaction updated".format(color))
                    reaction_total_count += 1

                # Add to 'callableColor'
                if color != "black" and color != "multicolor":
                    update = { "$addToSet": {"callableColors": color}}
                    result = hanabi_database[game_option_collection_name].update_one(filter, update)
                    if result.matched_count == 1 and result.modified_count == 1:
                        print("'{}' gameOption callableColors updated".format(color))
                        callable_color_total_count += 1

                color_id += 1

        print()
        print("Summary:")
        print("{} gameOption reaction updated".format(reaction_total_count))
        print("{} gameOption callableColors updated".format(callable_color_total_count))


    except Exception as error:
        print(error)

    except:
        print("Unknown error")