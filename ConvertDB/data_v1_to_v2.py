# This script converts database from data v1 model to data v2

if __name__ == "__main__":

    try:
        import getpass
        import pymongo
        import os
    
        host = os.environ["DB_HOST"]
        port = os.environ["DB_PORT"]
        login = os.environ["DB_LOGIN"]
    
        print("host=", host)
        print("port=", port)
        print("login=", login)
        password = getpass.getpass()

        # Create client
        my_client = pymongo.MongoClient("mongodb://{}:{}@{}:{}/".format(login, password, host, port))

        # Check if the database 'hanabi' exists
        db_list = my_client.list_database_names()
        database_name = "hanabi"
        if not database_name in db_list:
            raise Exception("'{}' database do not exists !".format(database_name))
        print("'{}' database exits".format(database_name))
        hanabi_database = my_client[database_name]

        # Check if the collection 'gameOption' exits
        col_list = hanabi_database.list_collection_names()
        game_option_collection_name = "gameOption"
        if not game_option_collection_name in col_list:
            raise Exception("'{}' collection do not exists !".format(game_option_collection_name))
        print("'{}' collection exists".format(game_option_collection_name))

        # Create the collection 'gameStart'
        game_start_collection_name = "gameStart"
        hanabi_database[game_start_collection_name]
        print("'{}' collection created".format(game_start_collection_name))

        # Rename the 'gameOption' collection into 'gameOptionV1'
        game_option_collection_name_save = game_option_collection_name + "_save"
        hanabi_database[game_option_collection_name].rename(game_option_collection_name_save)
        print("'{}' renamed into '{}'".format(game_option_collection_name, game_option_collection_name_save))

        # Create the collection 'gameOption'
        hanabi_database[game_option_collection_name]
        print("'{}' collection created".format(game_option_collection_name))

        # For each 'gameOption' document not yet converted, create the associated 'gameStart' and update the 'gameOption'
        filter = { "gameUuid": { "$exists": True } }
        count = 0
        for game_option in hanabi_database[game_option_collection_name_save].find(filter):
            
            print("Converting gameUuid '{}'...".format(game_option["gameUuid"]))

            new_stack = [ {"color": card["color"], "value": card["value"] } for card in game_option["stack"] ]
            
            new_color_option = []
            for option in game_option["colorOption"]:

                new_distribution = [ {"value": distribution["value"], "count": distribution["count"] } for distribution in option["distribution"]]
                
                new_option = {
                    "color": option["color"],
                    "isAscending": option["isAscending"],
                    "distribution": new_distribution
                    }

                new_color_option.append(new_option)
            
            new_game_option = {
                "timestamp": game_option["timestamp"],
                "playerCount": game_option["playerCount"],
                "playerHandMaxSize": game_option["playerHandMaxSize"],
                "startPlayerId": game_option["startPlayerId"],
                "maxErrorTokenCount": game_option["maxErrorTokenCount"],
                "maxSpeakTokenCount": game_option["maxSpeakTokenCount"],
                "startErrorTokenCount": game_option["startErrorTokenCount"],
                "startSpeakTokenCount": game_option["startSpeakTokenCount"],
                "minCardValue": game_option["minCardValue"],
                "maxCardValue": game_option["maxCardValue"],
                "maxScore": len(game_option["colorOption"]) * 5,                # This value is a bit hard coded
                "colorOption" : new_color_option,
                "stack" : new_stack,
                "reserved": True
                }
            inserted_data = hanabi_database[game_option_collection_name].insert_one(new_game_option)

            new_game_option_id = inserted_data.inserted_id

            new_game_start = {
                "gameUuid": game_option["gameUuid"],
                "tags": game_option["tags"],
                "gameOptionId": new_game_option_id,
                "timestamp": game_option["timestamp"]
                }
            hanabi_database[game_start_collection_name].insert_one(new_game_start)

            count += 1

        print("{} gameOption converted".format(count))

    except Exception as error:
        print(error)

    except:
        print("Unknown error")
