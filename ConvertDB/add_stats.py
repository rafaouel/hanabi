import requests


def compute_statistics(api, game_uuid):
    
    # Get overview info
    overview = api.get_game_overview(game_uuid)
    elapsed_player_turn_count = overview["playerTurnCount"]
    player_count = overview["maxPlayerCount"]

    # Initialize the statistics container
    stats = []
    for player_id in range(0, player_count):
        stats.append({
            "playerId": player_id,
            "dropCount": 0,
            "placeCount": 0,
            "speakCount": 0,
            "dropDuration": 0,
            "placeDuration": 0,
            "speakDuration": 0,
            "totalDuration": 0
            })

    # For each player turn count get the player action
    for turn_count in range(0, elapsed_player_turn_count):
        action = api.get_player_action(game_uuid, turn_count)

        player_id = action["currentPlayerId"]
        action_type = action["actionType"]
        action_duration = action["roundtripDuration"]

        if action_type == "place":
            stats[player_id]["placeCount"] += 1
            stats[player_id]["placeDuration"] += action_duration
            stats[player_id]["totalDuration"] += action_duration
        elif action_type == "drop":
            stats[player_id]["dropCount"] += 1
            stats[player_id]["dropDuration"] += action_duration
            stats[player_id]["totalDuration"] += action_duration
        elif action_type == "speak":
            stats[player_id]["speakCount"] += 1
            stats[player_id]["speakDuration"] += action_duration
            stats[player_id]["totalDuration"] += action_duration
        else:
            raise Exception("Invalid actionn type {}".format(action_type))

    return stats


if __name__ == "__main__":

    try:
        import getpass
        import pymongo
        import os

        import api_gateway


        db_host = os.environ["DB_HOST"]
        db_port = os.environ["DB_PORT"]
        db_login = os.environ["DB_LOGIN"]
    
        print("db host=", db_host)
        print("db port=", db_port)
        print("db login=", db_login)
        print("db password=", end="")
        db_password = getpass.getpass("")

        print("Connecting to the database...")
        # Create client
        my_client = pymongo.MongoClient("mongodb://{}:{}@{}:{}/".format(db_login, db_password, db_host, db_port))

        # Check if the database 'hanabi' exists
        db_list = my_client.list_database_names()
        database_name = "hanabi"
        if not database_name in db_list:
            raise Exception("'{}' database do not exists !".format(database_name))
        print("'{}' database exits".format(database_name))
        hanabi_database = my_client[database_name]

        # Check if the collection 'gameOption' exits
        col_list = hanabi_database.list_collection_names()
        game_end_collection_name = "gameEnd"
        if not game_end_collection_name in col_list:
            raise Exception("'{}' collection do not exists !".format(game_end_collection_name))
        print("'{}' collection exists".format(game_end_collection_name))


        api_host = os.environ["API_HOST"]
        api_port = os.environ["API_PORT"]
        api_login = os.environ["API_LOGIN"]

        print("api host=", api_host)
        print("api port=", api_port)
        print("api login=", api_login)
        print("api password=", end="")
        api_password = getpass.getpass("")

        api = api_gateway.ApiGateway(api_host, api_port, api_login, api_password)


        for game_end in hanabi_database[game_end_collection_name].find():

            game_uuid = game_end["gameUuid"]
            print("Process game UUID={}".format(game_uuid))
                        
            stats = compute_statistics(api, game_uuid)

            overview = api.get_game_overview(game_uuid)
            elapsed_game_turn_count = overview["gameTurnCount"]
            score = overview["currentScore"]
            max_score = overview["currentMaxScore"]
            error_token_count = overview["errorTokenCount"]
            max_error_token_count = overview["maxErrorTokenCount"]

            filter = {"gameUuid": game_uuid}
            update = {
                "$set": {
                    "statistics": stats,
                    "elapsedGameTurnCount": elapsed_game_turn_count,
                    "score": score,
                    "maxScore": max_score,
                    "errorTokenCount": error_token_count,
                    "maxErrorTokenCount": max_error_token_count,
                    }
                }
            result = hanabi_database[game_end_collection_name].find_one_and_update(filter, update)
            pass



    except Exception as error:
        print(error)

    except:
        print("Unknown error")