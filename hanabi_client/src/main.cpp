#include "pch.h"
#include "stdoutgameclient.h"

using namespace hanabi;


int main(int argc, const char* argv[])
{
	try
	{
		BOOST_LOG_TRIVIAL(trace) << "Program starts";

		const std::string default_host = "ironknee.ovh";
		std::string host = default_host;

		const std::size_t default_port = 32000;
		std::size_t port = default_port;

		const std::string default_name("Noname");
		std::string my_name = default_name;

		bool advanced_rendering = true;

		boost::program_options::options_description description("Allowed options");
		description.add_options()
			("help,h", "produce help message")
			("host", boost::program_options::value<std::string>(&host)->default_value(default_host), "IP address of the server")
			("port", boost::program_options::value<std::size_t>(&port)->default_value(default_port), "Port of the server")
			("name", boost::program_options::value<std::string>(&my_name)->default_value(default_name), "Name of the player in the game")
			;

		boost::program_options::variables_map variables_map;
		boost::program_options::store(boost::program_options::parse_command_line(argc, argv, description), variables_map);
		boost::program_options::notify(variables_map);

		if (variables_map.count("help"))
		{
			std::cout << description << std::endl;
			return 1;
		}

		// Version to log
		BOOST_LOG_TRIVIAL(info) << "Versions:";
		BOOST_LOG_TRIVIAL(info) << " - boost: " << common::to_string(common::to_version(BOOST_VERSION));
		BOOST_LOG_TRIVIAL(info) << " - hanabi lib: " << common::to_string(common::to_version(HANABI_LIB_VERSION));

		// Command line to log
		std::ostringstream oss;
		for (int i = 0; i < argc; ++i)
		{
			if (i != 0)
				oss << " ";
			oss << argv[i];
		}
		BOOST_LOG_TRIVIAL(debug) << "Command line: " << oss.str();

		// Arguments to log
		BOOST_LOG_TRIVIAL(debug) << "Arguments: ";
		BOOST_LOG_TRIVIAL(debug) << " - host=" << host << " (default=" << default_host << ")";
		BOOST_LOG_TRIVIAL(debug) << " - port=" << port << " (default=" << default_port<< ")";
		BOOST_LOG_TRIVIAL(debug) << " - name=" << my_name << " (default=" << default_name << ")";
		
		// Create client
		BOOST_LOG_TRIVIAL(info) << "Creating client...";
		BOOST_LOG_TRIVIAL(debug) << "Server : host=" << host << " port=" << port;

		boost::asio::io_service io_service;

		boost::asio::ip::tcp::resolver resolver(io_service);

		const boost::asio::ip::address host_address = resolver.resolve(host, "").begin()->endpoint().address();
		CStdOutGameClient game_client(io_service, host_address, boost::asio::ip::port_type(port), my_name, advanced_rendering);
		game_client.set_callback_on_receiving_action_accepted(std::bind(&CStdOutGameClient::on_receiving_action_accepted, &game_client));
		game_client.set_callback_on_receiving_action_drop_update(std::bind(&CStdOutGameClient::on_receiving_action_drop_update, &game_client, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
		game_client.set_callback_on_receiving_action_place_update(std::bind(&CStdOutGameClient::on_receiving_action_place_update, &game_client, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
		game_client.set_callback_on_receiving_action_rejected(std::bind(&CStdOutGameClient::on_receiving_action_rejected, &game_client, std::placeholders::_1));
		game_client.set_callback_on_receiving_action_speak_color_update(std::bind(&CStdOutGameClient::on_receiving_action_speak_color_update, &game_client, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));
		game_client.set_callback_on_receiving_action_speak_value_update(std::bind(&CStdOutGameClient::on_receiving_action_speak_value_update, &game_client, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));
		game_client.set_callback_on_receiving_bonus_event(std::bind(&CStdOutGameClient::on_receiving_bonus_event, &game_client, std::placeholders::_1, std::placeholders::_2));
		game_client.set_callback_on_receiving_error_event(std::bind(&CStdOutGameClient::on_receiving_error_event, &game_client, std::placeholders::_1, std::placeholders::_2));
		game_client.set_callback_on_receiving_game_end_update(std::bind(&CStdOutGameClient::on_receiving_game_end_update, &game_client, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
		game_client.set_callback_on_receiving_game_option_update(std::bind(&CStdOutGameClient::on_receiving_game_option_update, &game_client, std::placeholders::_1));
		game_client.set_callback_on_receiving_game_state_update(std::bind(&CStdOutGameClient::on_receiving_game_state_update, &game_client, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
		game_client.set_callback_on_receiving_hello(std::bind(&CStdOutGameClient::on_receiving_hello, &game_client, std::placeholders::_1));
		game_client.set_callback_on_receiving_peer_info_request(std::bind(&CStdOutGameClient::on_receiving_peer_info_request, &game_client, std::placeholders::_1));
		game_client.set_callback_on_receiving_peer_info_update(std::bind(&CStdOutGameClient::on_receiving_peer_info_update, &game_client, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));

		// Run client
		game_client.run();

		BOOST_LOG_TRIVIAL(trace) << "Program ends normally";
		return 0;
	}
	catch (std::exception& e)
	{
		BOOST_LOG_TRIVIAL(fatal) << "A standard exception has been catch.";
		BOOST_LOG_TRIVIAL(fatal) << e.what();
		return 1;
	}
	catch (...)
	{
		BOOST_LOG_TRIVIAL(fatal) << "An unknown error occured.";
		return 1;
	}
}