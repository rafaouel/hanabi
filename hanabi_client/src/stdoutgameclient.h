#ifndef STDOUTGAMECLIENT_H_97337831_A32C_47E6_8451_0FE186F378FB
#define STDOUTGAMECLIENT_H_97337831_A32C_47E6_8451_0FE186F378FB
#pragma once


class CStdOutGameClient : public hanabi::client_side::IGameClient
{
public:
	CStdOutGameClient(boost::asio::io_service& io_service, const boost::asio::ip::address& server_address, const boost::asio::ip::port_type& server_port, const std::string& my_name, bool advanced_rendering);
	virtual ~CStdOutGameClient() noexcept;


	const std::string& my_name() const noexcept;

	hanabi::common::TVersionArray version_array() const noexcept;

	bool debug() const noexcept;
	const hanabi::common::TTagList& tags() const noexcept;

	bool advanced_rendering() const noexcept;
	void advanced_rendering(bool enabled);

	void on_receiving_hello(const std::string& server_hello_message) const;
	void on_receiving_game_end_update(const hanabi::client_side::CGameEnd& game_end, const hanabi::client_side::CGameState& last_game_state, const hanabi::client_side::TPeerInfoMap& peer_info) const;
	void on_receiving_game_state_update(const hanabi::client_side::CGameState& game_state, hanabi::common::TIndex my_id, const hanabi::client_side::TPeerInfoMap& peer_info) const;
	void on_receiving_game_option_update(const hanabi::client_side::CGameOption& game_option) const;
	void on_receiving_action_speak_value_update(hanabi::common::TIndex source_player_id, hanabi::common::TIndex target_player_id, const hanabi::client_side::TPeerInfoMap& peer_info, hanabi::common::TValue value_info) const;
	void on_receiving_action_speak_color_update(hanabi::common::TIndex source_player_id, hanabi::common::TIndex target_player_id, const hanabi::client_side::TPeerInfoMap& peer_info, hanabi::common::CardColor color_info) const;
	void on_receiving_action_drop_update(hanabi::common::TIndex source_player_id, const hanabi::client_side::TPeerInfoMap& peer_info, const hanabi::common::CCard& card) const;
	void on_receiving_action_place_update(hanabi::common::TIndex source_player_id, const hanabi::client_side::TPeerInfoMap& peer_info, const hanabi::common::CCard& card) const;
	void on_receiving_peer_info_request(const std::string& server_version) const;
	void on_receiving_peer_info_update(hanabi::common::TIndex new_peer_id, const hanabi::client_side::CPeerInfo& new_peer, const hanabi::client_side::TPeerInfoMap& peer_info) const;
	void on_receiving_action_accepted() const;
	void on_receiving_action_rejected(const std::string& reason) const;
	void on_receiving_bonus_event(hanabi::common::TIndex current_player_id, const hanabi::common::CardColor& color) const;
	void on_receiving_error_event(hanabi::common::TIndex current_player_id, hanabi::common::TCount error_token_count) const;

private:
	virtual std::shared_ptr<hanabi::common::IAction> process_action_request(const hanabi::client_side::CGameState& game_state, const hanabi::client_side::TPeerInfoMap& peer_info, hanabi::common::TIndex my_id);
	virtual hanabi::client_side::CPeerInfoResponse process_peer_info_request(hanabi::common::TIndex my_id, const std::string server_verison);

	const std::string m_my_name;
	const hanabi::common::TTagList m_tags;
	bool m_advanced_rendering;
};

#endif