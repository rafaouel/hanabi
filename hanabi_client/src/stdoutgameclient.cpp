#include "pch.h"
#include "stdoutgameclient.h"


CStdOutGameClient::CStdOutGameClient(boost::asio::io_service& io_service, const boost::asio::ip::address& server_address, const boost::asio::ip::port_type& server_port, const std::string& my_name, bool advanced_rendering) :
	hanabi::client_side::IGameClient(io_service, server_address, server_port),
	m_my_name(my_name),
	m_tags({}),
	m_advanced_rendering(advanced_rendering)
{

}

CStdOutGameClient::~CStdOutGameClient() noexcept
{

}

const std::string& CStdOutGameClient::my_name() const noexcept
{
	return this->m_my_name;
}

hanabi::common::TVersionArray CStdOutGameClient::version_array() const noexcept
{
	return hanabi::common::to_version(HANABI_LIB_VERSION);
}

bool CStdOutGameClient::debug() const noexcept
{
	return compiled_with_debug_option;
}

const hanabi::common::TTagList& CStdOutGameClient::tags() const noexcept
{
	return this->m_tags;
}

bool CStdOutGameClient::advanced_rendering() const noexcept
{
	return this->m_advanced_rendering;
}

void CStdOutGameClient::advanced_rendering(bool enabled)
{
	this->m_advanced_rendering = enabled;
}

void CStdOutGameClient::on_receiving_hello(const std::string& server_hello_message) const
{
	std::cout << "The server says \"" << server_hello_message << "\"" << std::endl;
}

void CStdOutGameClient::on_receiving_game_end_update(const hanabi::client_side::CGameEnd& game_end, const hanabi::client_side::CGameState& last_game_state, const hanabi::client_side::TPeerInfoMap& peer_info) const
{
	std::cout << "End of the game !" << std::endl;
	if (game_end.victory())
	{
		std::cout << "You have won this game :)" << std::endl;
	}
	else
	{
		std::cout << "You have lost this game :(" << std::endl;
	}

	std::cout << "Statistics:" << std::endl;

	std::cout << "\tStart: " << boost::posix_time::to_iso_extended_string(game_end.game_start_timestamp()) << std::endl;
	std::cout << "\tEnd: " << boost::posix_time::to_iso_extended_string(game_end.game_end_timestamp()) << std::endl;

	const double duration = double((game_end.game_end_timestamp() - game_end.game_start_timestamp()).total_milliseconds()) / 1000.;
	std::cout << "\tDuration [s]: " << duration << std::endl;

	std::cout << "\t#Turn: " << game_end.elapsed_player_turn_count() << " - " << game_end.elapsed_game_turn_count() << std::endl;
	std::cout << "\tScore: " << game_end.score() << "/" << game_end.max_score() << std::endl;
	std::cout << "\t#Error: " << game_end.error_token_count() << "/" << game_end.max_error_token_count() << std::endl;

	std::cout << std::format("\t{0:9}  {1:15}  {2:12}  {3:12}  {4:12}  {5:12}", "player_id", "name", "drop", "place", "speak", "playing time") << std::endl;
	for (const auto& stat_pair : game_end.player_statistics())
	{
		const hanabi::common::CPlayerStatistics& stat = stat_pair.second;
		std::cout << std::format("\t{0:^9}  {1:<15}  {2:<3} {3:<8}  {4:<3} {5:<8}  {6:<3} {7:<8}  {8:<8}",
			stat.player_id(),
			peer_info.at(stat.player_id()).name(),
			stat.drop_count(), stat.drop_duration().total_milliseconds() / 1000.,
			stat.place_count(), stat.place_duration().total_milliseconds() / 1000.,
			stat.speak_count(),  stat.speak_duration().total_milliseconds() / 1000.,
			stat.total_duration().total_milliseconds() / 1000.) << std::endl;
	}
}

void CStdOutGameClient::on_receiving_game_state_update(const hanabi::client_side::CGameState& game_state, hanabi::common::TIndex my_id, const hanabi::client_side::TPeerInfoMap& peer_info) const
{
	std::cout << std::endl;
	std::cout << "#Turn: " << game_state.player_turn_count() << " - " << game_state.game_turn_count() << std::endl;
	std::cout << "Score: " << game_state.board().score() << "/" << game_state.trash().current_max_score() << "/" << game_state.game_option().game_max_score() << std::endl;
	std::cout << "Speak: " << game_state.speak_token_count() << "/" << game_state.game_option().max_speak_token_count() << std::endl;
	std::cout << "Error: " << game_state.error_token_count() << "/" << game_state.game_option().max_error_token_count() << std::endl;
	std::cout << "Stack: " << game_state.stack().stack_count() << " cards remaining" << std::endl;
	std::cout << std::endl;

	// Board and trash
	const std::size_t color_width = 15;
	const std::size_t board_width = 15;
	const std::size_t trash_width = 15;

	// Title
	std::cout << std::setw(color_width) << " " << std::setw(board_width) << std::left << "Board" << std::setw(trash_width) << std::left << "Trash" << std::endl;

	// Reorder the trash data
	std::map<hanabi::common::CardColor, std::vector<hanabi::common::CCard>> trash_map;
	for (const auto& card : game_state.trash().stack())
	{
		if (!trash_map.contains(card.color()))
			trash_map.insert(std::pair(card.color(), std::vector<hanabi::common::CCard>()));

		assert(trash_map.contains(card.color()));
		trash_map.at(card.color()).push_back(card);
	}

	const auto& game_option = game_state.game_option();
	for (const auto& pair : game_state.board().board())
	{
		const hanabi::common::CardColor& color = pair.first;
		const hanabi::common::TValue& board_value = pair.second;

		// Title
		std::cout << std::setw(color_width) << std::right << hanabi::common::card_color_to_string(color) + " ";

		// Board
		if (board_value > 0)
		{
			assert(game_option.color_option().contains(color));
			const bool is_ascending = game_option.color_option().at(color).is_ascending();
			std::ostringstream board_display;
			if (is_ascending)
			{
				for (hanabi::common::TValue i = game_option.min_card_value(); i <= board_value; ++i)
					board_display << i << " ";
			}
			else
			{
				for (hanabi::common::TValue i = game_option.max_card_value(); i >= board_value; --i)
					board_display << i << " ";
			}
			std::cout << std::setw(board_width) << std::left << board_display.str();
		}
		else
		{
			std::cout << std::setw(board_width) << std::left << ".";
		}

		// Trash
		if (trash_map.contains(color))
		{
			assert(trash_map.contains(color));
			const auto& trash_color = trash_map.at(color);
			std::ostringstream oss;
			for (const auto& trash_card : trash_color)
			{
				oss << trash_card.value() << " ";
			}
			std::cout << std::setw(trash_width) << std::left << oss.str();
		}
		else
		{
			std::cout << std::setw(trash_width) << std::left << ".";
		}
		std::cout << std::endl;
	}
	std::cout << std::right << std::endl;

	std::cout << "\tHands: " << std::endl;

	const std::string turn_indicator(">>> ");
	const int turn_indicator_width = int(turn_indicator.size());
	const int player_name_width = 16;
	const int card_width = 10;

	// Title
	std::cout << std::setw(turn_indicator_width + player_name_width) << "player names";
	for (hanabi::common::TValue i = 0; i < game_option.player_hand_max_size(); ++i)
	{
		std::ostringstream oss;
		oss << "Card # " << i + 1;
		std::cout << std::setw(card_width) << oss.str();
	}
	std::cout << std::endl;
	std::cout << std::endl;

	for (hanabi::common::TIndex player_id = 0; player_id < game_state.game_option().player_count(); ++player_id)
	{
		// Display the turn indicator
		std::cout << std::setw(turn_indicator_width);
		if (player_id == game_state.current_player_id())
			std::cout << turn_indicator;
		else
			std::cout << " ";

		// Display each player
		if (player_id != my_id)
		{
			// Not me

			const hanabi::client_side::CPlayerHand::THand& hand = game_state.other_hands().hand(player_id);

			assert(peer_info.contains(player_id));
			const std::string& player_name = peer_info.at(player_id).name();

			// Display the hand
			std::cout << std::setw(player_name_width) << player_name;
			for (const auto& card : hand)
			{
				std::cout << std::setw(card_width) << card;
			}
			std::cout << std::endl;
		}
		else
		{
			// me

			std::cout << std::setw(player_name_width) << "My hand";

			// No hand to display
			for (std::size_t i = 0; i < game_state.information_collection().information(my_id).size(); ++i)
			{
				std::cout << std::setw(card_width) << "?";
			}
			std::cout << std::endl;
		}

		const hanabi::client_side::CCardInformation::TInformation hand_info = game_state.information_collection().information(player_id);

		// Display the info on the cards

		// Values
		// Display the turn indicator
		std::cout << std::setw(turn_indicator_width);
		if (player_id == game_state.current_player_id())
			std::cout << turn_indicator;
		else
			std::cout << " ";

		std::cout << std::setw(player_name_width) << "Infos +values:";
		for (const auto& card_info : hand_info)
		{
			const hanabi::common::TValue positive_value = card_info.positive_value();
			if (positive_value > 0)
			{
				std::cout << std::setw(card_width) << positive_value;
			}
			else
			{
				std::cout << std::setw(card_width) << ".";
			}
		}
		std::cout << std::endl;

		// Colors
		// Display the turn indicator
		std::cout << std::setw(turn_indicator_width);
		if (player_id == game_state.current_player_id())
			std::cout << turn_indicator;
		else
			std::cout << " ";

		std::cout << std::setw(player_name_width) << "+colors:";
		for (const auto& card_info : hand_info)
		{
			const auto& positive_color = card_info.positive_color();
			if (positive_color.size() > 0)
			{
				std::ostringstream col;
				for (const auto& color : positive_color)
				{
					col << " " << hanabi::common::card_color_to_char(color);
				}
				std::cout << std::setw(card_width) << col.str();
			}
			else
			{
				std::cout << std::setw(card_width) << ".";
			}
		}
		std::cout << std::endl;

		// Negative infos
		if (this->advanced_rendering())
		{
			// Display the turn indicator
			std::cout << std::setw(turn_indicator_width);
			if (player_id == game_state.current_player_id())
				std::cout << turn_indicator;
			else
				std::cout << " ";

			std::cout << std::setw(player_name_width) << "-values:";
			for (const auto& card_info : hand_info)
			{
				const auto& negative_value = card_info.negative_value();
				if (negative_value.size() > 0)
				{
					std::ostringstream val;
					for (const auto& value : negative_value)
					{
						val << " " << value;
					}
					std::cout << std::setw(card_width) << val.str();
				}
				else
				{
					std::cout << std::setw(card_width) << ".";
				}
			}
			std::cout << std::endl;
			// Display the turn indicator
			std::cout << std::setw(turn_indicator_width);
			if (player_id == game_state.current_player_id())
				std::cout << turn_indicator;
			else
				std::cout << " ";

			std::cout << std::setw(player_name_width) << "-colors:";
			for (const auto& card_info : hand_info)
			{
				const auto& negative_color = card_info.negative_color();
				if (negative_color.size() > 0)
				{
					std::ostringstream col;
					for (const auto color : negative_color)
					{
						col << " " << hanabi::common::card_color_to_char(color);
					}
					std::cout << std::setw(card_width) << col.str();
				}
				else
				{
					std::cout << std::setw(card_width) << ".";
				}
			}
			std::cout << std::endl;
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
	std::cout << std::endl;
}

void CStdOutGameClient::on_receiving_game_option_update(const hanabi::client_side::CGameOption& game_option) const
{
	std::cout << "Game options:" << std::endl;
	std::cout << " - game option id: " << game_option.game_option_id() << std::endl;
	std::cout << " - color options: " << std::endl;
	for (const auto& pair : game_option.color_option())
	{
		const hanabi::common::CardColor& color = pair.first;
		std::cout << "   - " << color << std::endl;

		const bool& is_ascending = pair.second.is_ascending();
		std::cout << "     - " << (is_ascending ? "ascending order" : "descending order") << std::endl;

		std::cout << "     - reacts to: ";
		bool first = true;
		for (const auto& reaction : pair.second.reacts_to())
		{
			if (first)
			{
				first = false;
			}
			else
			{
				std::cout << ", ";
			}
			std::cout << hanabi::common::card_color_to_string(reaction);
		}
		std::cout << std::endl;

		std::cout << "     - distribution:";
		for (const auto& value_count : pair.second.distribution())
		{
			const hanabi::common::TValue& value = value_count.first;
			const hanabi::common::TCount& count = value_count.second;
			std::cout << " " << value << "x" << count;
		}
		std::cout << std::endl;
	}

	std::cout << " - callable colors: ";
	bool first = true;
	for (const auto& color : game_option.callable_colors())
	{
		if (first)
		{
			first = false;
		}
		else
		{
			std::cout << ", ";
		}
		std::cout << hanabi::common::card_color_to_string(color);
	}
	std::cout << std::endl;

	std::cout << " - game max score: " << game_option.game_max_score() << std::endl;
	std::cout << " - max error token count: " << game_option.max_error_token_count() << std::endl;
	std::cout << " - max speak token count: " << game_option.max_speak_token_count() << std::endl;
	std::cout << " - start error token count: " << game_option.start_error_token_count() << std::endl;
	std::cout << " - start speak token count: " << game_option.start_speak_token_count() << std::endl;
	std::cout << " - min value: " << game_option.min_card_value() << std::endl;
	std::cout << " - max value: " << game_option.max_card_value() << std::endl;
	std::cout << " - player count: " << game_option.player_count() << std::endl;
}

void CStdOutGameClient::on_receiving_action_speak_value_update(hanabi::common::TIndex source_player_id, hanabi::common::TIndex target_player_id, const hanabi::client_side::TPeerInfoMap& peer_info, hanabi::common::TValue value_info) const
{
	assert(peer_info.contains(source_player_id));
	const std::string& source_name = peer_info.at(source_player_id).name();

	assert(peer_info.contains(target_player_id));
	const std::string& target_name = peer_info.at(target_player_id).name();

	std::cout << source_name << " (id=" << source_player_id << ") used 'speak' to " << target_name << " (id=" << target_player_id << ") ";
	std::cout << " with 'value is " << value_info << "'";
}

void CStdOutGameClient::on_receiving_action_speak_color_update(hanabi::common::TIndex source_player_id, hanabi::common::TIndex target_player_id, const hanabi::client_side::TPeerInfoMap& peer_info, hanabi::common::CardColor color_info) const
{
	assert(peer_info.contains(source_player_id));
	const std::string& source_name = peer_info.at(source_player_id).name();

	assert(peer_info.contains(target_player_id));
	const std::string& target_name = peer_info.at(target_player_id).name();
	std::cout << source_name << " (id=" << source_player_id << ") used 'speak' to " << target_name << " (id=" << target_player_id << ") ";
	std::cout << " with 'color is " << hanabi::common::card_color_to_string(color_info) << "'";
}

void CStdOutGameClient::on_receiving_action_drop_update(hanabi::common::TIndex source_player_id, const hanabi::client_side::TPeerInfoMap& peer_info, const hanabi::common::CCard& card) const
{
	assert(peer_info.contains(source_player_id));
	const std::string& name = peer_info.at(source_player_id).name();

	std::cout << name << " used 'drop' on card '" << card << "'";
	std::cout << std::endl;
}

void CStdOutGameClient::on_receiving_action_place_update(hanabi::common::TIndex source_player_id, const hanabi::client_side::TPeerInfoMap& peer_info, const hanabi::common::CCard& card) const
{
	assert(peer_info.contains(source_player_id));
	const std::string& name = peer_info.at(source_player_id).name();

	std::cout << name << " used 'place' on card '" << card << "'";
	std::cout << std::endl;
}

void CStdOutGameClient::on_receiving_peer_info_request(const std::string& server_version) const
{
	std::cout << "Server version is '" << server_version << "'" << std::endl;
}

void CStdOutGameClient::on_receiving_peer_info_update(hanabi::common::TIndex new_peer_id, const hanabi::client_side::CPeerInfo& new_peer, const hanabi::client_side::TPeerInfoMap& /*peer_info*/) const
{
	std::cout << "Player #" << new_peer_id << " is named '" << new_peer.name() << "'" << std::endl;
}

void CStdOutGameClient::on_receiving_action_accepted() const
{
	std::cout << "Action accepted by the server" << std::endl;
}

void CStdOutGameClient::on_receiving_action_rejected(const std::string& reason) const
{
	std::cout << "Action rejected by the server" << std::endl;
	std::cout << "Reason:" << std::endl;
	std::cout << reason << std::endl;
}

void CStdOutGameClient::on_receiving_bonus_event(hanabi::common::TIndex current_player_id, const hanabi::common::CardColor& color) const
{
	std::cout << "\\o/" << std::endl;
	std::cout << "The " << hanabi::common::card_color_to_string(color) << " firework has been completed !" << std::endl;
	std::cout << "You received a bonus for this completion" << std::endl;
}

void CStdOutGameClient::on_receiving_error_event(hanabi::common::TIndex current_player_id, hanabi::common::TCount error_token_count) const
{
	std::cout << ":(" << std::endl;
	std::cout << "An error has been made !" << std::endl;
	std::cout << "You received an error token, the total is now " << error_token_count << std::endl;
}

std::shared_ptr<hanabi::common::IAction> CStdOutGameClient::process_action_request(const hanabi::client_side::CGameState& game_state, const hanabi::client_side::TPeerInfoMap& peer_info, hanabi::common::TIndex my_id)
{
	while (true)
	{
		try
		{
			std::cout << "--------------------" << std::endl;
			std::cout << "Choose your action :" << std::endl;
			std::cout << "1 : Drop" << std::endl;
			std::cout << "2 : Place" << std::endl;
			std::cout << "3 : Speak" << std::endl;
			std::cout << "> ";

			int answer = 0;
			std::cin >> answer;
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			switch (answer)
			{
			case 1:
			{
				// Drop
				std::cout << "index of the card to drop :" << std::endl;
				std::cout << "(0 to go back to the action selection menu)" << std::endl;

				hanabi::common::TIndex hand_index;
				while (true)
				{
					std::cout << "> ";
					std::cin >> hand_index;
					std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
					if (std::cin.fail())
					{
						std::cin.clear();
						std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
					}
					else
						break;
				}
				if (hand_index == 0)
					// If the player enters '0' it cancel the choice and go back to the main menu
					continue;
				std::cout << std::endl;
			
				return std::shared_ptr<hanabi::common::CDrop>(new hanabi::common::CDrop(hand_index - 1));
			}

			case 2:
			{
				// Place
				std::cout << "index of the card to place :" << std::endl;
				std::cout << "(0 to go back to the action selection menu)" << std::endl;

				hanabi::common::TIndex hand_index;
				while (true)
				{
					std::cout << "> ";
					std::cin >> hand_index;
					std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
					if (std::cin.fail())
					{
						std::cin.clear();
						std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
					}
					else
						break;
				}
				if (hand_index == 0)
					// If the player enters '0' it cancel the choice and go back to the main menu
					continue;
				std::cout << std::endl;
				
				return std::shared_ptr<hanabi::common::CPlace>(new hanabi::common::CPlace(hand_index - 1));
			}

			case 3:
			{
				// Speak
				hanabi::common::TIndex destination_player_index;
				// If this is a game with only two players?
				if (game_state.other_hands().other_hands().size() == 1)
				{
					// ... so you can speak to only one player, the target player is automatically selected
					destination_player_index = game_state.other_hands().other_hands().begin()->first + 1;
					std::cout << std::endl;

					assert(peer_info.contains(destination_player_index - 1));
					const std::string& player_name = peer_info.at(destination_player_index - 1).name();

					std::cout << "Speak to " << player_name << std::endl;
				}
				else
				{
					std::cout << "index of the player to speak :" << std::endl;
					for (const auto& pair : peer_info)
					{
						if (pair.first != my_id)
						{
							std::cout << pair.first + 1 << " : " << pair.second.name() << std::endl;
						}
					}
					std::cout << "(0 to go back to the action selection menu)" << std::endl;

					while (true)
					{
						std::cout << "> ";
						std::cin >> destination_player_index;
						std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
						if (std::cin.fail())
						{
							// Error while reading the player input
							std::cin.clear();
							std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
						}
						else
						{
							// Able to read the player input ...
							if (!game_state.other_hands().other_hands().contains(destination_player_index - 1))
							{
								// ... but this is an invalid player id
								std::cout << "The selected player does not exists, try another one." << std::endl;
								continue;
							}

							break;
						}
					}
					if (destination_player_index == 0)
						// If player enters '0' it cancel the choice and go back to the main menu
						continue;
				}
				std::cout << "information to give :" << std::endl;
				std::cout << game_state.game_option().min_card_value() << "-" << game_state.game_option().max_card_value() << " : value" << std::endl;
				hanabi::common::TValue cpt = game_state.game_option().max_card_value();
				for (const auto& pair : game_state.game_option().color_option())
				{
					const hanabi::common::CardColor color = pair.first;
					if (color != hanabi::common::CardColor::multicolor && color != hanabi::common::CardColor::black)
					{
						++cpt;
						std::cout << cpt << " : " << color << std::endl;
					}
				}
				std::cout << "(0 to go back to the action selection menu)" << std::endl;
				hanabi::common::TIndex information_index;
				while (true)
				{
					std::cout << "> ";
					std::cin >> information_index;
					std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
					if (std::cin.fail())
					{
						std::cin.clear();
						std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
					}
					else
						break;
				}
				if (information_index == 0)
					// If the player enters '0' it cancel the choice and go bakc to the menu
					continue;
				std::cout << std::endl;
				if (information_index >= game_state.game_option().min_card_value() && information_index <= game_state.game_option().max_card_value())
				{
					const hanabi::common::TValue value = information_index;
					std::shared_ptr<hanabi::common::CSpeak> p_action(new hanabi::common::CSpeak(destination_player_index - 1));
					p_action->value_info(value);
					return p_action;
				}
				else
				{
					const hanabi::common::CardColor color = hanabi::common::CardColor(information_index - game_state.game_option().max_card_value() - 1);
					std::shared_ptr<hanabi::common::CSpeak> p_action(new hanabi::common::CSpeak(destination_player_index - 1));
					p_action->color_info(color);
					return p_action;
				}
			}

			default:
				if (std::cin.fail())
				{
					std::cin.clear();
					std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
				}
				continue;
			}
		}
		catch (...)
		{
			continue;
		}
	}
}

hanabi::client_side::CPeerInfoResponse CStdOutGameClient::process_peer_info_request(hanabi::common::TIndex my_id, const std::string server_verison)
{
	std::cout << "Our client is using id #" << my_id << std::endl;
	std::cout << "Connected to the server (version = " << server_verison << ")" << std::endl;
	
	hanabi::client_side::CPeerInfoResponse response(this->my_name(), this->version_array(), hanabi::common::PeerType::HUMAN, this->debug(), this->tags());

	return response;
}