#include "pch.h"
#include "callbacks.h"


void on_player_hello(common::TIndex player_id)
{
	BOOST_LOG_TRIVIAL(info) << "Waving peer id '" << player_id << "'";
}

void on_peer_connecting(common::TIndex player_id)
{
	BOOST_LOG_TRIVIAL(info) << "A new peer is connecting, id '" << player_id << "' has been assigned";
	BOOST_LOG_TRIVIAL(info) << "Start the hand check...";
}

void on_peer_rejected(common::TIndex player_id, const server_side::CPeerInfo& peer_info)
{
	BOOST_LOG_TRIVIAL(warning) << "Peer with id '" << player_id << "' rejected (version=" << peer_info.version() << ").";
}

void on_peer_connected(common::TIndex player_id, const server_side::CPeerInfo& peer_info)
{
	BOOST_LOG_TRIVIAL(info) << "Peer with id '" << player_id << "' pass the hand check.";
}

void on_setup_peer_ends(common::TIndex player_id, const server_side::CPeerInfo& peer_info, const server_side::CGameOption& /*game_option*/, const server_side::CGameStart& game_start)
{
	BOOST_LOG_TRIVIAL(info) << "Peer #" << player_id << " info :";
	BOOST_LOG_TRIVIAL(info) << " - id=" << player_id;
	BOOST_LOG_TRIVIAL(info) << " - name=" << peer_info.name();
	BOOST_LOG_TRIVIAL(debug) << " - ip=" << peer_info.address();
	BOOST_LOG_TRIVIAL(debug) << " - type=" << common::to_string(peer_info.type());
	BOOST_LOG_TRIVIAL(debug) << " - version=" << peer_info.version();
	BOOST_LOG_TRIVIAL(debug) << " - debug=" << peer_info.debug();

	std::ostringstream tags;
	bool first = true;
	for (const auto& tag : peer_info.tags())
	{
		if (first)
			first = false;
		else
			tags << ", ";
		tags << tag;
	}
	BOOST_LOG_TRIVIAL(debug) << " - tags=" << tags.str();
}

void on_lobby_starts()
{
	BOOST_LOG_TRIVIAL(info) << "Lobby is started...";
}

void on_lobby_ends(const server_side::CGameServer::TPlayerMap& players)
{
	BOOST_LOG_TRIVIAL(info) << "End of the lobby, " << players.size() << " players enrolled";
}

void on_game_starts(const server_side::CGameOption& /*game_option*/, const boost::posix_time::ptime& /*start_timestamp*/)
{
	BOOST_LOG_TRIVIAL(info) << "Game starts...";
}

void on_game_ends(const boost::posix_time::ptime& start_timestamp, const boost::posix_time::ptime& end_timestamp)
{
	const boost::posix_time::time_duration up_time = end_timestamp - start_timestamp;

	BOOST_LOG_TRIVIAL(trace) << "Running ends after " << double(up_time.total_milliseconds()) / 1000. << "s";
}

void on_setup_game_starts(const server_side::CGameOption& /*game_option*/)
{
	BOOST_LOG_TRIVIAL(debug) << "Game setup starts...";
}

void on_setup_game_ends(const server_side::CGameState& /*game_state*/, const server_side::CGameOption& game_option)
{
	BOOST_LOG_TRIVIAL(info) << "Game options:";
	BOOST_LOG_TRIVIAL(info) << " - Color options:";
	for (const auto& option_pair : game_option.color_option())
	{
		const common::CardColor& color = option_pair.first;

		BOOST_LOG_TRIVIAL(info) << "   - " << common::card_color_to_string(color) << ": ";

		const bool& is_ascending = option_pair.second.is_ascending();
		BOOST_LOG_TRIVIAL(info) << "     - " << "ascending order=" << is_ascending;

		std::ostringstream reactions;
		bool first = true;
		for (const auto& reaction : option_pair.second.reacts_to())
		{
			if (first)
			{
				first = false;
			}
			else
			{
				reactions << ", ";
			}
			reactions << common::card_color_to_string(reaction);
		}
		BOOST_LOG_TRIVIAL(info) << "     - reacts to=[" << reactions.str() << "]";
		
		std::ostringstream values_and_count;
		for (const auto& value_count : option_pair.second.distribution())
		{
			const common::TValue& value = value_count.first;
			const common::TCount& count = value_count.second;
			values_and_count << value << "x" << count << "; ";
		}
		BOOST_LOG_TRIVIAL(info) << "     - distribution=" << values_and_count.str();
	}
	std::ostringstream callable_colors;
	bool first = true;
	for (const auto& color : game_option.callable_colors())
	{
		if (first)
		{
			first = false;
		}
		else
		{
			callable_colors << ", ";
		}
		callable_colors << color;
	}
	BOOST_LOG_TRIVIAL(info) << " - Callable colors=" << callable_colors.str();

	std::ostringstream tags;
	first = false;
	for (const auto& tag : game_option.tags())
	{ 
		if (first)
			first = false;
		else
			tags << ", ";
		tags << tag;
	}
	BOOST_LOG_TRIVIAL(info) << " - Tags=" << tags.str();

	BOOST_LOG_TRIVIAL(info) << " - Max error token count=" << game_option.max_error_token_count();
	BOOST_LOG_TRIVIAL(info) << " - Start error token count=" << game_option.start_error_token_count();
	BOOST_LOG_TRIVIAL(info) << " - Max speak otken count=" << game_option.max_speak_token_count();
	BOOST_LOG_TRIVIAL(info) << " - Start speak token count=" << game_option.start_speak_token_count();
	BOOST_LOG_TRIVIAL(info) << " - Max card value=" << game_option.max_card_value();
	BOOST_LOG_TRIVIAL(info) << " - Min card value=" << game_option.min_card_value();
	BOOST_LOG_TRIVIAL(info) << " - Player count=" << game_option.player_count();
	BOOST_LOG_TRIVIAL(info) << " - Player hand max size=" << game_option.player_hand_max_size();
	BOOST_LOG_TRIVIAL(info) << " - Game max score=" << game_option.game_max_score();
	BOOST_LOG_TRIVIAL(info) << " - Start player id=" << game_option.start_player_id();
	BOOST_LOG_TRIVIAL(info) << " - Timestamp=" << boost::posix_time::to_iso_extended_string(game_option.timestamp());
	BOOST_LOG_TRIVIAL(info) << " - Card stack count=" << game_option.stack().size();
}

void on_closing_connection_starts()
{
	BOOST_LOG_TRIVIAL(debug) << "Closing connections...";
}

void on_closing_connection_ends()
{
	BOOST_LOG_TRIVIAL(trace) << "Connections closed.";
}

void on_invalid_action(common::TIndex player_index, const std::shared_ptr<common::IAction> & p_action, const EHanabiException& error, const server_side::CGameServer::TPlayerMap& players)
{
	std::string action_name;
	if (p_action->type() == common::ActionType::DROP)
		action_name = "drop";
	else if (p_action->type() == common::ActionType::SPEAK)
		action_name = "speak";
	else if (p_action->type() == common::ActionType::PLACE)
		action_name = "place";
	else
		THROW_ERROR(EImpossibleError);

	const std::string player_name = players.at(player_index).peer_info().name();

	BOOST_LOG_TRIVIAL(warning) << "The player '" << player_name << "' (id=" << player_index << ") sent '" << action_name << "' but is invalid and causes an error : " << error.what();
	BOOST_LOG_TRIVIAL(warning) << "A new roundtrip will be initiated...";
}

void on_turn_ends(common::TCount player_turn_count, common::TCount game_turn_count, const server_side::CGameState& game_state, const server_side::CGameServer::TPlayerMap& players)
{
	const common::TIndex player_index = game_state.current_player_id();
	const std::string player_name = players.at(player_index).peer_info().name();

	BOOST_LOG_TRIVIAL(trace) << "The player '" << player_name << "' (id=" << player_index << ") played.";
	BOOST_LOG_TRIVIAL(trace) << "Turn #" << player_turn_count << " - " << game_turn_count << " ends.";
}

void on_turn_starts(common::TCount player_turn_count, common::TCount game_turn_count, const server_side::CGameState& game_state, const server_side::CGameServer::TPlayerMap& players)
{
	const common::TIndex player_index = game_state.current_player_id();
	const std::string player_name = players.at(player_index).peer_info().name();

	BOOST_LOG_TRIVIAL(info) << "Turn #" << player_turn_count << " - " << game_turn_count << " starts.";
	BOOST_LOG_TRIVIAL(debug) << "The player '" << player_name << "' (id = " << player_index << ") has to play.";
}

void on_end_of_game_loop(const server_side::CGameEnd& game_end, const server_side::CGameState& game_state)
{
	BOOST_LOG_TRIVIAL(info) << "A condition has been reached to end the game";
	const boost::posix_time::ptime& begin = game_end.game_start_timestamp();
	const boost::posix_time::ptime& end = game_end.game_end_timestamp();
	BOOST_LOG_TRIVIAL(trace) << "The game started at " << boost::posix_time::to_iso_extended_string(begin) << "Z";
	BOOST_LOG_TRIVIAL(trace) << "The game ended at " << boost::posix_time::to_iso_extended_string(end) << "Z";
	BOOST_LOG_TRIVIAL(info) << "The game lasts " << double((end - begin).total_milliseconds()) / 1000. << "s";

	if (game_end.victory())
		BOOST_LOG_TRIVIAL(info) << "This is a win";
	else
		BOOST_LOG_TRIVIAL(info) << "This is a game over";

	BOOST_LOG_TRIVIAL(info) << "The final score is " << game_end.score() << "/" << game_end.max_score();
	BOOST_LOG_TRIVIAL(info) << "The turn count is " << game_end.elapsed_player_turn_count() << "-" << game_end.elapsed_game_turn_count();
	BOOST_LOG_TRIVIAL(info) << "The error count is " << game_end.error_token_count() << "/" << game_end.max_error_token_count();

#ifdef BOOST_COMP_MSVC_AVAILABLE
	// This feature is not available using GCC version < 13
	BOOST_LOG_TRIVIAL(info) << std::format("{0:9}  {1:12}  {2:12}  {3:12}  {4:12}", "player_id", "drop", "place", "speak", "playing time");
	for (const auto& stat_pair : game_end.player_statistics())
	{
		const common::CPlayerStatistics& stat = stat_pair.second;
		BOOST_LOG_TRIVIAL(info) << std::format("{0:^9}  {1:<3} {2:<8}  {3:<3} {4:<8}  {5:<3} {6:<8}  {7:<8}",
			stat.player_id(),
			stat.drop_count(), stat.drop_duration().total_milliseconds() / 1000.,
			stat.place_count(), stat.place_duration().total_milliseconds() / 1000.,
			stat.speak_count(), stat.speak_duration().total_milliseconds() / 1000.,
			stat.total_duration().total_milliseconds() / 1000.);
	}
#else
	BOOST_LOG_TRIVIAL(warning) << "The player statistics cannot be displayed, stay tunned in futur version";
#endif
}

void on_start_of_game_loop(const boost::posix_time::ptime& game_start_timestamp)
{
	BOOST_LOG_TRIVIAL(info) << "The game started at " << boost::posix_time::to_iso_extended_string(game_start_timestamp) << "Z";
}

void on_player_stuck(common::TIndex player_id, const server_side::CGameEnd& /*game_end*/, const server_side::CGameState& /*game_state*/, const server_side::CGameServer::TPlayerMap& players)
{
	BOOST_LOG_TRIVIAL(warning) << players.at(player_id).peer_info().name() << " (id=" << player_id << ") is stuck !";
	BOOST_LOG_TRIVIAL(info) << "The game will end";
}

void on_max_error(common::TCount max_error, const server_side::CGameEnd& /*game_end*/, const server_side::CGameState& /*game_state*/, const server_side::CGameServer::TPlayerMap& /*players*/)
{
	BOOST_LOG_TRIVIAL(warning) << max_error << " error have been reached";
	BOOST_LOG_TRIVIAL(info) << "The game will end";
}

void on_max_score(common::TCount max_score, const server_side::CGameEnd& /*game_end*/, const server_side::CGameState& /*game_state*/, const server_side::CGameServer::TPlayerMap& /*players*/)
{
	BOOST_LOG_TRIVIAL(warning) << "The maximum score have been reached";
	BOOST_LOG_TRIVIAL(info) << "The game will end";
}

void on_process_action_starts(common::TIndex player_index, const std::shared_ptr<common::IAction>& p_action, const server_side::CGameState& game_state, const server_side::CGameServer::TPlayerMap& players)
{
	const std::string player_name = players.at(player_index).peer_info().name();
	std::string action_type;
	std::ostringstream action_description;
	if (p_action->type() == common::ActionType::DROP)
	{
		action_type = "Drop";
		const common::CDrop drop_action = *static_cast<common::CDrop*>(p_action.get());
		action_description << "hand index = " << drop_action.hand_index();
	}
	else if (p_action->type() == common::ActionType::SPEAK)
	{
		action_type = "Speak";
		const common::CSpeak speak_action = *static_cast<common::CSpeak*>(p_action.get());
		action_description << "destination player index = " << speak_action.player_index() << "; ";
		if (speak_action.is_color_info())
		{
			action_description << "color = " << speak_action.color_info();
		}
		else if (speak_action.is_value_info())
		{
			action_description << "value = " << speak_action.value_info();
		}
		else
		{
			THROW_ERROR(EImpossibleError);
		}

	}
	else if (p_action->type() == common::ActionType::PLACE)
	{
		action_type = "Place";
		const common::CPlace place_action = *static_cast<common::CPlace*>(p_action.get());
		action_description << "hand index = " << place_action.hand_index();
	}
	else
	{
		THROW_ERROR(EImpossibleError);
	}
	BOOST_LOG_TRIVIAL(debug) << "The player '" << player_name << "' (id = " << player_index << ") sent a '" << action_type << "' action";
	BOOST_LOG_TRIVIAL(debug) << "The associated description is : " << action_description.str();
}

void on_retrieving_action(common::TIndex player_index, const boost::posix_time::ptime& /*request_timestamp*/, const server_side::CGameState& game_state, const server_side::CGameServer::TPlayerMap& players)
{
	const std::string player_name = players.at(player_index).peer_info().name();
	BOOST_LOG_TRIVIAL(trace) << "Retrieving action from player '" << player_name << "' (id=" << player_index << ").";
}

void on_giving_bonus(const common::CardColor& completed_color, const server_side::CGameState& /*game_state*/)
{
	std::ostringstream oss;
	oss << "A bonus has been given for the completion of the color '" << completed_color << "' (id = " << int(completed_color) << ")";
	BOOST_LOG_TRIVIAL(info) << oss.str();
}

void on_player_draw(common::TIndex /*player_id*/, const common::CCard& drawn_card, const server_side::CGameState& /*game_state*/)
{
	std::ostringstream oss;
	oss << "A card has been drawn: " << drawn_card;
	BOOST_LOG_TRIVIAL(debug) << oss.str();
}

void on_player_place(common::TIndex /*player_id*/, const common::CCard& card_to_place, const std::shared_ptr<common::CardColor>& p_completed_color, const server_side::CGameState& /*game_state*/)
{
	std::ostringstream oss;
	oss << "A card has been placed: " << card_to_place;
	BOOST_LOG_TRIVIAL(debug) << oss.str();
	if (p_completed_color)
	{
		oss.str("");
		oss << "The color '" << *p_completed_color << "' has been completed";
		BOOST_LOG_TRIVIAL(debug) << oss.str();
	}
}

void on_player_drop(common::TIndex /*player_id*/, const common::CCard& card_to_trash, const server_side::CGameState& /*game_state*/)
{
	std::ostringstream oss;
	oss << "A card has been dropped: " << card_to_trash;
	BOOST_LOG_TRIVIAL(debug) << oss.str();
}

void on_save_game_start_starts(const server_side::CGameStart& game_start)
{
	BOOST_LOG_TRIVIAL(info) << "Saving game start in database...";
	BOOST_LOG_TRIVIAL(debug) << " - game uuid=" << boost::uuids::to_string(game_start.game_uuid());
	BOOST_LOG_TRIVIAL(debug) << " - game option id=" << game_start.game_option_id();
	BOOST_LOG_TRIVIAL(debug) << " - timestamp=" << boost::posix_time::to_iso_extended_string(game_start.timestamp());

	std::ostringstream tags;
	bool first = true;
	for (const auto& tag : game_start.tags())
	{
		if (first)
			first = false;
		else
			tags << ", ";
		tags << tag;
	}
	BOOST_LOG_TRIVIAL(debug) << " - tags=" << tags.str();
}

void on_save_game_starts_ends(const server_side::CGameStart& /*game_start*/)
{
	BOOST_LOG_TRIVIAL(info) << "Game start saved";
}

void on_save_peer_info_starts(const server_side::CGameStart& /*game_start*/, const server_side::CPeerInfo& peer_info)
{
	BOOST_LOG_TRIVIAL(info) << "Saving peer info in database...";
	BOOST_LOG_TRIVIAL(debug) << " - peer id=" << peer_info.id();
	BOOST_LOG_TRIVIAL(debug) << " - name=" << peer_info.name();
	BOOST_LOG_TRIVIAL(debug) << " - type=" << common::to_string(peer_info.type());
	BOOST_LOG_TRIVIAL(debug) << " - address=" << peer_info.address();
	BOOST_LOG_TRIVIAL(debug) << " - version=" << peer_info.version();
	BOOST_LOG_TRIVIAL(debug) << " - debug=" << peer_info.debug();

	std::ostringstream tags;
	bool first = true;
	for (const auto& tag : peer_info.tags())
	{
		if (first)
			first = false;
		else
			tags << ", ";
		tags << tag;
	}
	BOOST_LOG_TRIVIAL(debug) << " - tags=" << tags.str();
}

void on_save_peer_info_ends(const server_side::CGameStart& /*game_start*/, const server_side::CPeerInfo& /*peer_info*/)
{
	BOOST_LOG_TRIVIAL(info) << "Peer info saved";
}

void on_save_game_state_starts(const server_side::CGameStart& /*game_start*/, const server_side::CGameState& game_state)
{
	BOOST_LOG_TRIVIAL(info) << "Saving game state in database...";
	BOOST_LOG_TRIVIAL(debug) << " - turn count=" << game_state.player_turn_count() << "-" << game_state.game_turn_count();
	BOOST_LOG_TRIVIAL(debug) << " - error token count=" << game_state.error_token_count() << "/" << game_state.game_option().max_error_token_count();
	BOOST_LOG_TRIVIAL(debug) << " - speak token count=" << game_state.speak_token_count() << "/" << game_state.game_option().max_speak_token_count();
	BOOST_LOG_TRIVIAL(debug) << " - score=" << game_state.board().score() << "/" << game_state.trash().current_max_score() << "/" << game_state.game_option().game_max_score();
	BOOST_LOG_TRIVIAL(debug) << " - stack=" << game_state.stack().size() << " card remaining";
}

void on_save_game_state_ends(const server_side::CGameStart& /*game_start*/, const server_side::CGameState& /*game_state*/)
{
	BOOST_LOG_TRIVIAL(info) << "Game state saved";
}

void on_save_player_action_starts(const server_side::CGameStart& game_start, const common::IAction& action, common::TCount player_turn_count, common::TCount game_turn_count, common::TIndex current_player_id, const boost::posix_time::ptime& request_timestamp, const boost::posix_time::ptime& response_timestamp)
{
	BOOST_LOG_TRIVIAL(info) << "Saving player action in database...";
	BOOST_LOG_TRIVIAL(debug) << " - player id=" << current_player_id;
	BOOST_LOG_TRIVIAL(debug) << " - type=" << common::to_string(action.type());
	BOOST_LOG_TRIVIAL(debug) << " - turn count=" << player_turn_count << "-" << game_turn_count;
	BOOST_LOG_TRIVIAL(debug) << " - request timestamp=" << boost::posix_time::to_iso_extended_string(request_timestamp);
	BOOST_LOG_TRIVIAL(debug) << " - response timestamp=" << boost::posix_time::to_iso_extended_string(response_timestamp);
	BOOST_LOG_TRIVIAL(debug) << " - roundtrip duration=" << response_timestamp - request_timestamp;
}

void on_save_player_action_ends(const server_side::CGameStart& game_start, const common::IAction& action, common::TCount player_turn_count, common::TCount game_turn_count, common::TIndex current_player_id, const boost::posix_time::ptime& request_timestamp, const boost::posix_time::ptime& response_timestamp)
{
	BOOST_LOG_TRIVIAL(info) << "Player action saved";
}

void on_save_game_end_starts(const server_side::CGameStart& game_start, const server_side::CGameEnd& game_end)
{
	BOOST_LOG_TRIVIAL(info) << "Saving game end in database...";
	BOOST_LOG_TRIVIAL(debug) << " - victory=" << game_end.victory();
	BOOST_LOG_TRIVIAL(debug) << " - score=" << game_end.score() << "/" << game_end.max_score();
	BOOST_LOG_TRIVIAL(debug) << " - turn count=" << game_end.elapsed_player_turn_count() << "-" << game_end.elapsed_game_turn_count();
	BOOST_LOG_TRIVIAL(debug) << " - error=" << game_end.error_token_count() << "/" << game_end.max_error_token_count();
	BOOST_LOG_TRIVIAL(debug) << " - game duration=" << game_end.compute_game_duration();
	// TODO Add the statistics per players
}

void on_save_game_end_ends(const server_side::CGameStart& game_start, const server_side::CGameEnd& game_end)
{
	BOOST_LOG_TRIVIAL(info) << "Game end saved";
}

void on_api_connecting(std::size_t tentative, std::size_t max_tentative, const common::TRequest& /*req*/, const common::CAPIGateway::TTimings& /*timings*/)
{
	BOOST_LOG_TRIVIAL(trace) << "Connecting to API (tentative " << tentative + 1 << "/" << max_tentative << ")";
}

void on_api_connected(std::size_t tentative, std::size_t max_tentative, const common::TRequest& /*req*/, const common::CAPIGateway::TTimings& /*timings*/, const boost::asio::ip::basic_resolver_results<boost::asio::ip::tcp>& /*resolver_results*/)
{
	BOOST_LOG_TRIVIAL(trace) << "Connected to API (tentative " << tentative + 1 << "/" << max_tentative << ")";
}

void on_api_request_sent(std::size_t tentative, std::size_t max_tentative, const common::TRequest& /*req*/, const common::CAPIGateway::TTimings& timings, std::size_t write_bytes_count)
{
	const boost::posix_time::time_duration write_duration = timings.at("sent") - timings.at("connected");
	BOOST_LOG_TRIVIAL(debug) << "API request sent (tentative " << tentative + 1 << "/" << max_tentative << ")"; 
	BOOST_LOG_TRIVIAL(trace) << "sending duration=" << write_duration.total_microseconds() / 1000. << "ms";
	BOOST_LOG_TRIVIAL(trace) << "bytes sent=" << write_bytes_count;
}

void on_api_response_received(std::size_t tentative, std::size_t max_tentative, const common::TRequest& /*req*/, const common::TResponse& res, const common::CAPIGateway::TTimings& timings, std::size_t read_bytes_count)
{
	const boost::posix_time::time_duration roundtrip_duration = timings.at("response") - timings.at("connected");
	const boost::posix_time::time_duration reading_duration = timings.at("response") - timings.at("sent");
	BOOST_LOG_TRIVIAL(debug) << "API response received (tentative " << tentative + 1 << "/" << max_tentative << ")";
	BOOST_LOG_TRIVIAL(trace) << "reading duration=" << reading_duration.total_microseconds() / 1000. << "ms";
	BOOST_LOG_TRIVIAL(trace) << "roundtrip duration=" << roundtrip_duration.total_microseconds() / 1000. << "ms";
	BOOST_LOG_TRIVIAL(trace) << "status code=" << res.result_int();
	BOOST_LOG_TRIVIAL(trace) << "bytes received=" << read_bytes_count;
}

void on_api_disconnected(std::size_t tentative, std::size_t max_tentative, const common::TRequest& /*req*/, const common::TResponse& /*res*/, const common::CAPIGateway::TTimings& /*timings*/)
{
	BOOST_LOG_TRIVIAL(debug) << "Disconnected from API (tentative " << tentative + 1 << "/" << max_tentative << ")";
}

void on_api_sending_failure(std::size_t tentative, std::size_t max_tentative, const common::TRequest& req, const common::CAPIGateway::TTimings& timings, std::size_t tentative_delay, const std::exception& error)
{
	BOOST_LOG_TRIVIAL(error) << "Error while sending request to API (tentative " << tentative + 1 << "/" << max_tentative << ")";
	BOOST_LOG_TRIVIAL(error) << error.what();
	if (tentative < max_tentative)
	{
		BOOST_LOG_TRIVIAL(error) << "Another attempt will start in " << tentative_delay << "ms...";
	}
}