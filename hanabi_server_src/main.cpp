#include "pch.h"

#include "apicontext.h"
#include "apigateway.h"
#include "callbacks.h"


using namespace hanabi;


int main(int argc, const char* argv[])
{
	try
	{
		BOOST_LOG_TRIVIAL(trace) << "Program starts";

		// Program options variables
		const boost::asio::ip::port_type default_server_port(32000);
		boost::asio::ip::port_type server_port = default_server_port;

		std::string game_option_id = "";

		const common::TCount default_player_count = 2;
		common::TCount player_count = default_player_count;

		const std::string default_preset = to_string(common::Preset::STD);
		std::string preset = default_preset;

		const std::string default_api_host("localhost");
		std::string api_host = default_api_host;

		const std::string default_api_port("3000");
		std::string api_port = default_api_port;

		const std::string default_api_login("game_server");
		std::string api_login = default_api_login;

		std::string api_password("");

		std::string tags_string("");

		bool loop_server_run = false;


		// Program options define and loading
		boost::program_options::options_description description("Allowed options");
		description.add_options()
			("help,h", "Produce help message\n")
			("server_port", boost::program_options::value<boost::asio::ip::port_type>(&server_port)->default_value(default_server_port), "Port used by the server\n")
			("game_option_id", boost::program_options::value<std::string>(&game_option_id), "Identifier of the seed to play with\n")
			("player_count", boost::program_options::value<common::TCount>(&player_count)->default_value(default_player_count), "Number of players the server is waiting for\n")
			("preset", boost::program_options::value<std::string>(&preset)->default_value(default_preset), "game preset:\n  std: Only standard colors\n  hard: Standard colors + black + multicolors\n  black: Standard colors + black\n  multi: Standard colors + multicolors\n  easy: 3 colors\n  cake: 1 color\n")
			("api_host", boost::program_options::value<std::string>(&api_host)->default_value(default_api_host), "Host of the game data API\n")
			("api_port", boost::program_options::value<std::string>(&api_port)->default_value(default_api_port), "Port of the game data API\n")
			("api_login", boost::program_options::value<std::string>(&api_login)->default_value(default_api_login), "Login of the game data API\n")
			("api_password", boost::program_options::value<std::string>(&api_password), "Password of the game data API\n")
			("tags,t", boost::program_options::value<std::string>(&tags_string), "Tags about the game. If several tags are given, use the following syntax : -t \"tag1 tag2\"")
			("loop", boost::program_options::value<bool>(&loop_server_run)->implicit_value(true), "If present the server restarts at the ennd of a game")
			;

		boost::program_options::variables_map variables_map;
		boost::program_options::store(boost::program_options::parse_command_line(argc, argv, description), variables_map);
		boost::program_options::notify(variables_map);

		if (variables_map.count("help"))
		{
			std::cout << description << std::endl;
			return 1;
		}

		// Process the tags
		common::TTagList tag_list;
		std::istringstream iss(tags_string);
		common::TTag tmp;
		while (iss >> tmp)
			tag_list.insert(tmp);

		// Add tags associated to server version
		tag_list.insert(std::string("v") + common::to_string(common::to_version(HANABI_LIB_VERSION)));
#ifdef _DEBUG
		tag_list.insert("debug");
#else
		tag_list.insert("release");
#endif
#if BOOST_OS_WINDOWS > 0
		tag_list.insert("windows");
#elif BOOST_OS_LINUX > 0
		tag_list.insert("linux");
#else
		tag_list.insert("unknown_os");
#endif

		while (true)
		{
			// Versions to log
			BOOST_LOG_TRIVIAL(info) << "Versions:";
			BOOST_LOG_TRIVIAL(info) << " - boost: " << common::to_string(common::to_version(BOOST_VERSION));
			BOOST_LOG_TRIVIAL(info) << " - hanabi lib: " << common::to_string(common::to_version(HANABI_LIB_VERSION));

			// Command line to log
			std::ostringstream oss;
			for (int i = 0; i < argc; ++i)
			{
				if (i != 0)
					oss << " ";
				oss << argv[i];
			}
			BOOST_LOG_TRIVIAL(debug) << "Command line: '" << oss.str() << "'";

			// Arguments to log
			std::string game_option_identifier = game_option_id;
			BOOST_LOG_TRIVIAL(debug) << "Arguments: ";
			BOOST_LOG_TRIVIAL(debug) << " - server_port=" << server_port << " (default=" << default_server_port << ")";
			BOOST_LOG_TRIVIAL(debug) << " - game_option_id=" << game_option_identifier;
			BOOST_LOG_TRIVIAL(debug) << " - player_count=" << player_count << " (default=" << default_player_count << ")";
			BOOST_LOG_TRIVIAL(debug) << " - preset=" << preset << " (default=" << default_preset << ")";
			BOOST_LOG_TRIVIAL(debug) << " - api_host=" << api_host << " (default=" << default_api_host << ")";
			BOOST_LOG_TRIVIAL(debug) << " - api_port=" << api_port << " (default=" << default_api_port << ")";
			BOOST_LOG_TRIVIAL(debug) << " - api_login=" << api_login << " (default=" << default_api_login << ")";
			BOOST_LOG_TRIVIAL(debug) << " - api_password=" << "***";
			std::ostringstream tags;
			for (const common::TTag& tag : tag_list)
			{
				tags << "\"" << tag << "\" ";
			}
			BOOST_LOG_TRIVIAL(debug) << " - tags=" << tags.str();

			BOOST_LOG_TRIVIAL(debug) << "Creating api gateway...";
			const std::size_t api_max_tentative = 5;
			const std::size_t api_tentative_delay = 1000;
			const std::shared_ptr<common::CAPIContext> p_api_context(new common::CAPIContext(api_host, api_port, api_login, api_password, api_max_tentative, api_tentative_delay));
			const std::shared_ptr<common::CAPIGateway> p_api_gateway(new common::CAPIGateway(p_api_context));
			p_api_gateway->set_callback_on_connecting(on_api_connecting);
			p_api_gateway->set_callback_on_connected(on_api_connected);
			p_api_gateway->set_callback_on_request_sent(on_api_request_sent);
			p_api_gateway->set_callback_on_response_received(on_api_response_received);
			p_api_gateway->set_callback_on_disconnected(on_api_disconnected);
			p_api_gateway->set_callback_on_sending_failure(on_api_sending_failure);


			if (game_option_identifier.length() > 0)
			{
				BOOST_LOG_TRIVIAL(debug) << "Retrieving game option (from game option identifier)...";
				BOOST_LOG_TRIVIAL(trace) << "Reserving...";
				p_api_gateway->reserve_game_option(game_option_identifier);
			}
			else
			{
				BOOST_LOG_TRIVIAL(debug) << "Retrieving game option (from player count + preset)...";
				BOOST_LOG_TRIVIAL(trace) << "Reserving...";
				game_option_identifier = p_api_gateway->reserve_game_option(player_count, common::preset_from_string(preset));
			}
			BOOST_LOG_TRIVIAL(trace) << "Retrieving (game option id=" << game_option_identifier << ")...";
			const boost::json::value parsed_value = p_api_gateway->load_game_option(game_option_identifier);
			const std::size_t documentCount = parsed_value.at("metadata").at("documentCount").as_int64();
			BOOST_LOG_TRIVIAL(trace) << documentCount << " game option found";
			server_side::CGameOption game_option;
			game_option.from_json(parsed_value.at("data").as_array()[0]);

			BOOST_LOG_TRIVIAL(debug) << "Updating tags...";
			for (const common::TTag& tag : game_option.tags())
			{
				tag_list.insert(tag);
			}
			tags.str("");
			for (const common::TTag& tag : tag_list)
			{
				tags << "\"" << tag << "\" ";
			}
			BOOST_LOG_TRIVIAL(debug) << " - tags=" << tags.str();

			////////////////////////
			// Create game server //
			BOOST_LOG_TRIVIAL(debug) << "Creating game server...";
			boost::asio::io_context io_context;
			const bool shuffle_peers = true;
			server_side::CGameServer game_server(game_option, p_api_gateway, io_context, server_port, shuffle_peers, tag_list);

			// Pushing callbacks
			BOOST_LOG_TRIVIAL(debug) << "Pushing the callbacks...";
			game_server.set_callback_on_player_hello(on_player_hello);
			game_server.set_callback_on_peer_connecting(on_peer_connecting);
			game_server.set_callback_on_peer_rejected(on_peer_rejected);
			game_server.set_callback_on_peer_connected(on_peer_connected);
			game_server.set_callback_on_setup_peer_ends(on_setup_peer_ends);
			game_server.set_callback_on_lobby_starts(on_lobby_starts);
			game_server.set_callback_on_lobby_ends(on_lobby_ends);
			game_server.set_callback_on_game_starts(on_game_starts);
			game_server.set_callback_on_game_ends(on_game_ends);
			game_server.set_callback_on_setup_game_starts(on_setup_game_starts);
			game_server.set_callback_on_setup_game_ends(on_setup_game_ends);
			game_server.set_callback_on_closing_connection_starts(on_closing_connection_starts);
			game_server.set_callback_on_closing_connection_ends(on_closing_connection_ends);
			game_server.set_callback_on_invalid_action(on_invalid_action);
			game_server.set_callback_on_turn_starts(on_turn_starts);
			game_server.set_callback_on_turn_ends(on_turn_ends);
			game_server.set_callback_on_start_of_game_loop(on_start_of_game_loop);
			game_server.set_callback_on_end_of_game_loop(on_end_of_game_loop);
			game_server.set_callback_on_process_player_action_starts(on_process_action_starts);
			game_server.set_callback_on_player_action_roundtrip_starts(on_retrieving_action);
			game_server.set_callback_on_giving_bonus_starts(on_giving_bonus);
			game_server.set_callback_on_player_draw(on_player_draw);
			game_server.set_callback_on_player_place(on_player_place);
			game_server.set_callback_on_player_drop(on_player_drop);
			game_server.set_callback_on_game_end_condition_player_stuck(on_player_stuck);
			game_server.set_callback_on_game_end_condition_max_error(on_max_error);
			game_server.set_callback_on_save_game_start_starts(on_save_game_start_starts);
			game_server.set_callback_on_save_game_start_ends(on_save_game_starts_ends);
			game_server.set_callback_on_save_peer_info_starts(on_save_peer_info_starts);
			game_server.set_callback_on_save_peer_info_ends(on_save_peer_info_ends);
			game_server.set_callback_on_save_game_state_starts(on_save_game_state_starts);
			game_server.set_callback_on_save_game_state_ends(on_save_game_state_ends);
			game_server.set_callback_on_save_player_action_starts(on_save_player_action_starts);
			game_server.set_callback_on_save_player_action_ends(on_save_player_action_ends);
			game_server.set_callback_on_save_game_end_starts(on_save_game_end_starts);
			game_server.set_callback_on_save_game_end_ends(on_save_game_end_ends);



			//////////////////////////
			// Run the game server //
			BOOST_LOG_TRIVIAL(info) << "Running...";

			game_server.run();

			BOOST_LOG_TRIVIAL(warning) << "Server is down";

			if (!loop_server_run)
			{
				break;
			}
			BOOST_LOG_TRIVIAL(info) << "Restarting server...";
		}

		return 0;
	}
	catch (std::exception& e)
	{
		BOOST_LOG_TRIVIAL(fatal) << "A standard exception has been catch.";
		BOOST_LOG_TRIVIAL(fatal) << e.what();

		boost::stacktrace::stacktrace trace = boost::stacktrace::stacktrace::from_current_exception();
		std::vector<std::string> trace_by_line;
		std::ostringstream oss;
		oss << trace;
		
		boost::split(trace_by_line, oss.str(), boost::is_any_of(" "), boost::token_compress_on);
		if (trace_by_line.empty() || trace_by_line[0].empty())
		{
			BOOST_LOG_TRIVIAL(fatal) << "No callstack available";
		}
		else
		{
			BOOST_LOG_TRIVIAL(fatal) << "Call stack: ";
			for (const auto& line : trace_by_line)
				BOOST_LOG_TRIVIAL(fatal) << line;
		}
		return 1;
	}
	catch (...)
	{
		BOOST_LOG_TRIVIAL(fatal) << "An unknown error occured.";

		boost::stacktrace::stacktrace trace = boost::stacktrace::stacktrace::from_current_exception();
		std::vector<std::string> trace_by_line;
		std::ostringstream oss;
		oss << trace;

		boost::split(trace_by_line, oss.str(), boost::is_any_of(" "), boost::token_compress_on);
		if (trace_by_line.empty() || trace_by_line[0].empty())
		{
			BOOST_LOG_TRIVIAL(fatal) << "No callstack available";
		}
		else
		{
			BOOST_LOG_TRIVIAL(fatal) << "Call stack: ";
			for (const auto& line : trace_by_line)
				BOOST_LOG_TRIVIAL(fatal) << line;
		}

		return 1;
	}
}