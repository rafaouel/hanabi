#ifndef CALLBACKS_H_2607FA2B_7850_4215_A2E6_F0E403B0D85C
#define CALLBACKS_H_2607FA2B_7850_4215_A2E6_F0E403B0D85C
#pragma once


using namespace hanabi;


void on_player_hello(common::TIndex player_id);

void on_peer_connecting(common::TIndex player_id);
void on_peer_rejected(common::TIndex player_id, const server_side::CPeerInfo& peer_info);
void on_peer_connected(common::TIndex player_id, const server_side::CPeerInfo& peer_info);

void on_setup_peer_ends(common::TIndex player_id, const server_side::CPeerInfo& peer_info, const server_side::CGameOption& /*game_option*/, const server_side::CGameStart& game_start);

void on_lobby_starts();

void on_lobby_ends(const server_side::CGameServer::TPlayerMap& players);

void on_game_starts(const server_side::CGameOption& game_option, const boost::posix_time::ptime& start_timestamp);

void on_game_ends(const boost::posix_time::ptime& start_timestamp, const boost::posix_time::ptime& end_timestamp);

void on_setup_game_starts(const server_side::CGameOption& game_option);

void on_setup_game_ends(const server_side::CGameState& game_state, const server_side::CGameOption& game_option);

void on_closing_connection_starts();
void on_closing_connection_ends();

void on_invalid_action(common::TIndex player_index, const std::shared_ptr<common::IAction>& p_action, const EHanabiException& error, const server_side::CGameServer::TPlayerMap& players);

void on_turn_ends(common::TCount player_turn_count, common::TCount game_turn_count, const server_side::CGameState& game_state, const server_side::CGameServer::TPlayerMap& players);

void on_turn_starts(common::TCount player_turn_count, common::TCount game_turn_count, const server_side::CGameState& game_state, const server_side::CGameServer::TPlayerMap& players);

void on_start_of_game_loop(const boost::posix_time::ptime& game_start_timestamp);

void on_end_of_game_loop(const server_side::CGameEnd& game_end, const server_side::CGameState& game_state);

void on_player_stuck(common::TIndex player_id, const server_side::CGameEnd& game_end, const server_side::CGameState& game_state, const server_side::CGameServer::TPlayerMap& players);

void on_max_error(common::TCount max_error, const server_side::CGameEnd& game_end, const server_side::CGameState& game_state, const server_side::CGameServer::TPlayerMap& players);

void on_max_score(common::TCount max_score, const server_side::CGameEnd& game_end, const server_side::CGameState& game_state, const server_side::CGameServer::TPlayerMap& players);

void on_process_action_starts(common::TIndex player_index, const std::shared_ptr<common::IAction>& p_action, const server_side::CGameState& game_state, const server_side::CGameServer::TPlayerMap& players);

void on_retrieving_action(common::TIndex player_index, const boost::posix_time::ptime& request_timestamp, const server_side::CGameState& game_state, const server_side::CGameServer::TPlayerMap& players);

void on_giving_bonus(const common::CardColor& completed_color, const server_side::CGameState& game_state);

void on_player_draw(common::TIndex player_id, const common::CCard& drawn_card, const server_side::CGameState& game_state);
void on_player_place(common::TIndex player_id, const common::CCard& card_to_place, const std::shared_ptr<common::CardColor>& p_completed_color, const server_side::CGameState& game_state);
void on_player_drop(common::TIndex player_id, const common::CCard& card_to_trash, const server_side::CGameState& game_state);

void on_save_game_start_starts(const server_side::CGameStart& game_start);
void on_save_game_starts_ends(const server_side::CGameStart& game_start);
void on_save_peer_info_starts(const server_side::CGameStart& game_start, const server_side::CPeerInfo& peer_info);
void on_save_peer_info_ends(const server_side::CGameStart& game_start, const server_side::CPeerInfo& peer_info);
void on_save_game_state_starts(const server_side::CGameStart& game_start, const server_side::CGameState& game_state);
void on_save_game_state_ends(const server_side::CGameStart& game_start, const server_side::CGameState& game_state);
void on_save_player_action_starts(const server_side::CGameStart& game_start, const common::IAction& action, common::TCount player_turn_count, common::TCount game_turn_count, common::TIndex current_player_id, const boost::posix_time::ptime& request_timestamp, const boost::posix_time::ptime& response_timestamp);
void on_save_player_action_ends(const server_side::CGameStart& game_start, const common::IAction& action, common::TCount player_turn_count, common::TCount game_turn_count, common::TIndex current_player_id, const boost::posix_time::ptime& request_timestamp, const boost::posix_time::ptime& response_timestamp);
void on_save_game_end_starts(const server_side::CGameStart& game_start, const server_side::CGameEnd& game_end);
void on_save_game_end_ends(const server_side::CGameStart& game_start, const server_side::CGameEnd& game_end);

void on_api_connecting(std::size_t tentative, std::size_t max_tentative, const common::TRequest& req, const common::CAPIGateway::TTimings& timings);
void on_api_connected(std::size_t tentative, std::size_t max_tentative, const common::TRequest& req, const common::CAPIGateway::TTimings& timings, const boost::asio::ip::basic_resolver_results<boost::asio::ip::tcp>& resolver_results);
void on_api_request_sent(std::size_t tentative, std::size_t max_tentative, const common::TRequest& req, const common::CAPIGateway::TTimings& timings, std::size_t write_bytes_count);
void on_api_response_received(std::size_t tentative, std::size_t max_tentative, const common::TRequest& req, const common::TResponse& res, const common::CAPIGateway::TTimings& timings, std::size_t read_bytes_count);
void on_api_disconnected(std::size_t tentative, std::size_t max_tentative, const common::TRequest& req, const common::TResponse& res, const common::CAPIGateway::TTimings& timings);
void on_api_sending_failure(std::size_t tentative, std::size_t max_tentative, const common::TRequest& req, const common::CAPIGateway::TTimings& timings, std::size_t tentative_delay, const std::exception& error);

#endif