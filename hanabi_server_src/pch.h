#ifndef PCH_H_D84C3010_266C_40B8_87CB_A8ABC4BEFE9B
#define PCH_H_D84C3010_266C_40B8_87CB_A8ABC4BEFE9B
#pragma once

#include <boost/predef.h>
#if BOOST_OS_WINDOWS > 0
#	ifndef _WIN32_WINNT
#		define _WIN32_WINNT 0x0A00
#	endif
#elif BOOST_OS_LINUX > 0
#
#else
#	error "Not eligible platform target"
#endif

#ifdef BOOST_COMP_MSVC_AVAILABLE
// This feature is only available with the next GCC version (GCC 13)
#include <format>
#else
#pragma message("Check if the GCC version now supports 'std::format'")
#endif
#include <fstream>
#include <iostream>

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/program_options.hpp>
#include <boost/log/trivial.hpp>
#include <boost/version.hpp>
 
#include "hanabi_lib.h"

#endif