const mongoose = require('mongoose');


const cardSchema = new mongoose.Schema({
    color: {
        type: String,
        required: true
    },

    value: {
        type: Number,
        min: 1,
        required: true
    }
}, { _id: false });


module.exports = cardSchema;