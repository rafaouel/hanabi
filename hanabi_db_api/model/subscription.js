const mongoose = require('mongoose');


const subjectSchema = new mongoose.Schema({
    type: {
        $type: String,
        enum: ["gameOption", "gameStart", "peerInfo", "gameState", "playerAction", "gameEnd"],
        required: true
    },

    gameUuid: {
        $type: String
    },

    action: {
        $type: String,
        enum: ["insertion", "update", "deletion"],
        required: true
    }
}, { typeKey: '$type', _id: false });


const notificationSchema = new mongoose.Schema({
    method: {
        type: String,
        enum: ["GET", "POST", "PUT", "UPDATE", "DELETE", "PATCH"],
        required: true
    },

    host: {
        type: String,
        required: true
    },

    port: {
        type: String,
        required: true
    },

    target: {
        type: String,
        required: true
    }
}, { _id: false });


const subscriptionSchema = new mongoose.Schema({
    since: {
        type: Date,
        required: true
    },

    expire: {
        type: Date,
        required: true
    },

    subject: {
        type: subjectSchema,
        required: true,
    },

    notification: {
        type: notificationSchema,
        required: true,
    }
});

// Define index
subscriptionSchema.index({ "expire": 1 }, { expireAfterSeconds: 0 });

module.exports = mongoose.model("subscription", subscriptionSchema, "subscription");