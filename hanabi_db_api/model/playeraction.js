const mongoose = require('mongoose');


const rawCardInfoSchema = new mongoose.Schema({
    value: {
        type: Number,
        min: 1,
        required: function () {
            return !this.color;
        }
    },

    color: {
        type: String,
        required: function () {
            return !this.value;
        }
    }
}, { _id: false });


const playerActionSchema = new mongoose.Schema({
    gameUuid: {
        type: String,
        required: true
    },

    playerTurnCount: {
        type: Number,
        min: 0,
        required: true
    },

    gameTurnCount: {
        type: Number,
        min: 0,
        required: true
    },

    requestTimestamp: {
        type: Date,
        required: true
    },

    responseTimestamp: {
        type: Date,
        required: true
    },

    roundtripDuration: {
        type: Number,
        required: true
    },

    currentPlayerId: {
        type: Number,
        required: true
    },

    actionType: {
        type: String,
        enum: ["speak", "drop", "place"],
        required: true
    },

    handIndex: {
        type: Number,
        min: 0,
        required: function () {
            return this.action_type == "drop" || this.action_type == "place";
        }
    },

    targetPlayerId: {
        type: Number,
        min: 0,
        required: function () {
            return this.action_type == "speak";
        }
    },

    cardInfo: {
        type: rawCardInfoSchema,
        required: function () {
            return this.action_type == "speak";
        }
    }
});

// Define index
playerActionSchema.index({ "gameUuid": 1, "playerTurnCount": 1 }, { unique: true });


module.exports = mongoose.model("playerAction", playerActionSchema, "playerAction");