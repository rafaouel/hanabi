const mongoose = require('mongoose');
const cardSchema = require('./cardschema');


const boardSchema = new mongoose.Schema({
    score: {
        type: Number,
        min: 0,
        required: true
    },

    board: [
        {
            color: {
                type: String,
                required: true
            },

            value: {
                type: Number,
                min: 0,
                required: true
            }
        }
    ]
}, { _id: false });


const trashSchema = new mongoose.Schema({
    currentMaxScore: {
        type: Number,
        min: 0,
        required: true
    },

    trash: [ cardSchema ]
}, { _id: false });


const cardInfoSchema = new mongoose.Schema({
    positiveValue: {
        type: Number
    },

    negativeValue: {
        type: [Number]
    },

    positiveColor: {
        type: [String]
    },

    negativeColor: {
        type: [String]
    }
}, { _id: false });


const gameStateSchema = new mongoose.Schema({
    gameUuid: {
        type: String,
        required: true
    },

    speakTokenCount: {
        type: Number,
        min: 0,
        required: true
    },

    errorTokenCount: {
        type: Number,
        min: 0,
        required: true
    },

    playerTurnCount: {
        type: Number,
        min: 0,
        required: true
    },

    gameTurnCount: {
        type: Number,
        min: 0,
        required: true
    },

    currentPlayerId: {
        type: Number,
        min: 0,
        required: true
    },

    playerTurnTimestamp: {
        type: Date,
        required: true
    },

    board: {
        type: boardSchema,
        required: true
    },

    trash: {
        type: trashSchema,
        required: true
    },

    stack: {
        type: [cardSchema],
        required: true
    },

    playerHand: {
        type: [[cardSchema]],
        required: true
    },

    playerCardInfo: {
        type: [[cardInfoSchema]],
        required: true 
    }
});

// Define index
gameStateSchema.index({ "gameUuid": 1, "playerTurnCount": 1 }, { unique: true });

module.exports = mongoose.model("gameState", gameStateSchema, "gameState");