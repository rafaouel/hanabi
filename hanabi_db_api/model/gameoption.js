const mongoose = require('mongoose');
const card_schema = require('./cardschema');


const valueDistributionSchema = new mongoose.Schema({
    value: {
        type: Number,
        min: 1,
        required: true
    },

    count: {
        type: Number,
        min: 1,
        required: true
    }
}, { _id: false });


const colorOptionSchema = new mongoose.Schema({
    color: {
        type: String,
        required: true
    },

    isAscending: {
        type: Boolean,
        required: true
    },

    reactsTo: {
        type: [String],
        required: true,
    },

    distribution: {
        type: [valueDistributionSchema],
        required: true
    }
}, { _id: false });


const gameOptionSchema = new mongoose.Schema({

    timestamp: {
        type: Date,
        required: true
    },

    playerCount: {
        type: Number,
        min: 2,
        max: 5,
        required: true
    },

    playerHandMaxSize: {
        type: Number,
        min: 1,
        required: true
    },

    startPlayerId: {
        type: Number,
        min: 0,
        required: true
    },

    maxErrorTokenCount: {
        type: Number,
        min: 1,
        required: true
    },

    maxSpeakTokenCount: {
        type: Number,
        min: 1,
        required: true
    },

    startErrorTokenCount: {
        type: Number,
        min: 0,
        required: true
    },

    startSpeakTokenCount: {
        type: Number,
        min: 0,
        required: true
    },

    // Computed by the API
    maxScore: {
        type: Number,
        min: 0,
        required: true
    },

    reserved: {
        type: Boolean,
        required: true,
        default: false
    },

    // Computed by the API
    minCardValue: {
        type: Number,
        min: 1,
        required: true
    },

    // Computed by the API
    maxCardValue: {
        type: Number,
        min: 1,
        required: true
    },

    colorOption: {
        type: [colorOptionSchema],
        required: true
    },

    callableColors: {
        type: [String],
        required: true
    },

    stack: {
        type: [card_schema],
        required: true
    },

    tags: {
        type: [String],
        required: true
    }
});

module.exports = mongoose.model("gameOption", gameOptionSchema, "gameOption");