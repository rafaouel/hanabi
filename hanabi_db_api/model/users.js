const constants = require('../constants');
const mongoose = require('mongoose');


const userSchema = new mongoose.Schema({
    login: {
        type: String,
        required: true
    },
    encryptedPassword: {
        type: String,
        required: true
    },
    accessTokenTTL: {
        type: Number,
        required: true,
        default: constants.defaultAccessTokenTTL
    },
    refreshTokenTTL: {
        type: Number,
        required: true,
        default: constants.defaultRefreshTokenTTL
    },
    description: {
        type: String,
        required: false
    },
    enabled: {
        type: Boolean,
        required: true,
        default: false
    }
});

// Define index
userSchema.index({ "login": 1 }, { unique: true });

module.exports = mongoose.model("users", userSchema, "users");