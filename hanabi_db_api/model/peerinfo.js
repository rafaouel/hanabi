const mongoose = require('mongoose');


const peerInfoSchema = new mongoose.Schema({
    gameUuid: {
        type: String,
        required: true
    },

    playerId: {
        type: Number,
        required: true
    },

    name: {
        type: String,
        required: true
    },

    type: {
        type: String,
        required: true,
        enum: ['human', 'ai']
    },

    ip: {
        type: String,
        required: true,
        validate: {
            validator: function (v) {
                const re = /^((25[0-5]|(2[0-4]|1\d|[1-9]|)\d)\.?\b){4}$/;
                return (!v || !v.trim().length) || re.test(v);
            },
            message: "Provided IP v4 address is invalid"
        }
    },

    version: {
        type: String,
        required: true
    },

    debug: {
        type: Boolean,
        required: true
    },

    versionArray: {
        type: [Number],
        required: true,
        validate: [arrayLimit, "Provided version array is invalid"]
    },

    tags: {
        type: [String],
        required: true,
    }
});

function arrayLimit(val) {
    return val.length == 3;
}

// Define index
peerInfoSchema.index({ "gameUuid": 1, "playerId": 1 }, { unique: true });


module.exports = mongoose.model("peerInfo", peerInfoSchema, "peerInfo");