const mongoose = require('mongoose');


const gameStartSchema = new mongoose.Schema({
    gameUuid: {
        type: String,
        required: true
    },

    timestamp: {
        type: Date,
        required: true
    },

    gameOptionId: {
        type: mongoose.ObjectId,
        required: true
    },

    tags: {
        type: [String],
        required: true
    }
});

// Define index
gameStartSchema.index({ "gameUuid": 1 }, { unique: true });

module.exports = mongoose.model("gameStart", gameStartSchema, "gameStart");