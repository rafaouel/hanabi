const mongoose = require('mongoose');

const gameStatisticsSchema = new mongoose.Schema({

    playerId: {
        type: Number,
        required: true
    },

    dropCount: {
        type: Number,
        required: true,
        default: 0,
    },

    dropDuration: {
        type: Number,
        required: true,
        default: 0.,
    },

    placeCount: {
        type: Number,
        required: true,
        default: 0,
    },

    placeDuration: {
        type: Number,
        required: true,
        default: 0.,
    },

    speakCount: {
        type: Number,
        required: true,
        default: 0,
    },

    speakDuration: {
        type: Number,
        required: true,
        default: 0.,
    },

    totalDuration: {
        type: Number,
        required: true,
    }

}, { _id: false });


const gameEndSchema = new mongoose.Schema({
    gameUuid: {
        type: String,
        required: true
    },

    gameStartTimestamp: {
        type: Date,
        required: true
    },

    gameEndTimestamp: {
        type: Date,
        required: true
    },

    gameDuration: {
        type: Number,
        min: 0,
        required: true
    },

    elapsedPlayerTurnCount: {
        type: Number,
        min: 0,
        required: true
    },

    elapsedGameTurnCount: {
        type: Number,
        min: 0,
        required: true
    },

    errorTokenCount: {
        type: Number,
        min: 0,
        required: true
    },

    maxErrorTokenCount: {
        type: Number,
        min: 0,
        required: true
    },

    victory: {
        type: Boolean,
        required: true
    },

    maxScore: {
        type: Number,
        required: true
    },

    score: {
        type: Number,
        required: true
    },

    statistics: {
        type: [gameStatisticsSchema],
        required: true,
        validate: (array) => {
            return array.length >= 2;
        }
    }
});

// Define index
gameEndSchema.index({ "gameUuid": 1 }, {unique: true});

module.exports = mongoose.model("gameEnd", gameEndSchema, "gameEnd");