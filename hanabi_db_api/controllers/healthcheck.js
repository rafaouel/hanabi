const mongoose = require('mongoose');
const os = require('os');


const readiness_probe = (req, res, next) => {

    try {
        // If this probe fail, the API will not receive traffic
        // A database connectivity check needs to be done
        // We can be exaustive in the checks and a return a body with lots of info
        const databaseState = mongoose.connection.readyState;
        const connectionStateString = {
            0: "disconnected",
            1: "connected",
            2: "connecting",
            3: "disconnecting"
        };
        const totalMemory = os.totalmem();
        const availableMemory = process.availableMemory();
        const availablePerCent = 100. * availableMemory / totalMemory;
        const usedMemory = process.memoryUsage.rss();
        const usedPerCent = 100. * usedMemory / totalMemory;
        const load = os.loadavg();
        const returned_status = (databaseState == 1 || usedPerCent >= 95. || availablePerCent <= 5.) ? 200 : 503;
        const returned_info = {
            apiStatus: returned_status == 200 ? "ok" : "ko",
            databaseConnection: connectionStateString[databaseState],
            uptime: process.uptime(),
            memory: {
                total: totalMemory,
                available: availableMemory,
                availablePerCent: availablePerCent,
                used: usedMemory,
                usedPerCent: usedPerCent
            },
            cpus: os.cpus(),
            load: {
                "1min": load[0],
                "5min": load[1],
                "15min": load[2]
            },
            versions: process.versions,
            resourceUsage: process.resourceUsage()
        };

        res.status(returned_status).json(returned_info);
    }
    catch (err) {
        next(err);
    }
}

const liveness_probe = (req, res, next) => {

    try {
        // If this probe fail, the API needs to be killed and restarted
        res.status(200).send();
    }
    catch (err) {
        next(err);
    }
}

const startup_probe = (req, res, next) => {

    try {
        // If this probe fail, the API is not ready to receive requests
        const databaseState = mongoose.connection.readyState;
        if (databaseState == 1) {
            res.status(200).send();
        }
        else {
            res.status(503).send();
        }
    }
    catch (err) {
        next(err);
    }
}

module.exports = {
    readiness_probe,
    liveness_probe,
    startup_probe
}