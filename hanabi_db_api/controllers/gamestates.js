const constants = require('../constants');
const errors = require('../middleware/errors');
const gameStateModel = require('../model/gamestates');
const subscription = require('./subscription');

const notifyInsertion = subscription.notifyInsertion;
const notifyDeletion = subscription.notifyDeletion;


const gameStateFilter = (query => {
    let filter = {};
    if ("gameUuid" in query) {
        filter.gameUuid = query.gameUuid;
    }
    if ("playerTurnCount" in query) {
        filter.playerTurnCount = parseInt(query.playerTurnCount);
    }
    if ("gameTurnCount" in query) {
        filter.gameTurnCount = parseInt(query.gameTurnCount);
    }

    let playerTurnTimestamp = undefined;
    if ("after" in query) {
        if (playerTurnTimestamp === undefined) {
            playerTurnTimestamp = {};
        }
        playerTurnTimestamp["$gte"] = new Date(query.after);
    }
    if ("before" in query) {
        if (playerTurnTimestamp === undefined) {
            playerTurnTimestamp = {};
        }
        playerTurnTimestamp["$lte"] = new Date(query.before);
    }
    if (playerTurnTimestamp !== undefined) {
        filter.playerTurnTimestamp = playerTurnTimestamp;
    }

    if ("speakTokenCount" in query) {
        filter.speakTokenCount = parseInt(query.speakTokenCount);
    }
    if ("errorTokenCount" in query) {
        filter.errorTokenCount = parseInt(query.errorTokenCount);
    }
    if ("score" in query) {
        filter["board.score"] = parseInt(query.score);
    }
    if ("maxScore" in query) {
        filter["trash.currentMaxScore"] = parseInt(query.maxScore);
    }
    if ("currentPlayerId" in query) {
        filter.currentPlayerId = parseInt(query.currentPlayerId);
    }
    return filter;
})


const createGameState = ((req, res, next) => {

    try {
        const newState = new gameStateModel(req.body);
        newState.save().then(createdDocument => {

            createdDocument._id = undefined;
            createdDocument.__v = undefined;

            res.status(201).send();

            // Notification
            notifyInsertion(createdDocument, "game_state");

        }).catch(err => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
});


const getGameStates = ((req, res, next) => {

    try {
        const pageNumber = parseInt((req.query.page !== undefined) ? req.query.page : 0);
        const limit = parseInt((req.query.limit !== undefined) ? req.query.limit : constants.paginationMaxDocumentPerPage);
        let queryParams = "";
        for (const [key, value] of Object.entries(req.query)) {
            if (key !== "limit" && key !== "page") {
                queryParams += `&${key}=${value}`;
            }
        }
        const baseUrl = req.originalUrl.split("?")[0] + `?limit=${limit}` + queryParams;

        let pipeline = constants.paginationPipeline(pageNumber, limit, baseUrl);

        // Add projection
        pipeline.unshift({
            $project: {
                _id: 0,
                __v: 0,
            }
        });

        // Add sorting
        pipeline.unshift({
            $sort: {
                gameUuid: 1,
                playerTurnCount: 1
            } });

        // Add filtering
        const filter = gameStateFilter(req.query);
        pipeline.unshift({ $match: filter, });

        gameStateModel.aggregate(pipeline)
            .then(result => {

                if (result.length == 1) {

                    if (result[0].error) {
                        if (result[0].error.type === "currentPageOutOfRange") {
                            throw new errors.PageNumberOutOfRangeError()
                        }
                    }

                    const returnedResult = {
                        metadata: result[0].metadata,
                        data: result[0].data,
                    }
                    return res.status(200).json(returnedResult);
                }
                throw new Error("Unknown aggregation error");
            })
            .catch(err => {
                next(err);
            });
    }
    catch (err) {
        next(err);
    }
});


const deleteGameStateByGameUuid = ((req, res, next) => {

    try {
        const filter = { gameUuid: req.params.gameUuid };
        gameStateModel.find(filter).then(gameStateArray => {

            var deletedGameUuid = [];
            var processedGameState = 0;

            if (gameStateArray.length > 0) {

                gameStateArray.forEach(gameState => {

                    gameStateModel.deleteOne(gameState._id).then(summary => {

                        processedGameState += 1;

                        if (summary.deletedCount == 1) {

                            deletedGameUuid.push({
                                gameUuid: gameState.gameUuid,
                                playerTurnCount: gameState.playerTurnCount
                            });

                            // Notification
                            notifyDeletion(gameState, "gameState");
                        }

                        if (processedGameState == gameStateArray.length) {

                            if (deletedGameUuid.length == gameStateArray.length) {
                                res.status(200).send(deletedGameUuid);
                            }
                            else {
                                res.status(400).send(deletedGameUuid);
                            }
                        }

                    }).catch(err => {
                        processedGameState += 1;

                        console.log("Unable to delete game_state '" + gameState.gameUuid + "' / '" + gameState.playerTurnCount + "'");
                        console.log(err);
                    });
                });

            }
            else {
                res.status(404).send();
            }

        }).catch(err => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
});


module.exports = {
    createGameState,
    getGameStates,
    deleteGameStateByGameUuid,
    gameStateFilter,
}