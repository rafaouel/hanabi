const constants = require('../constants');
const errors = require('../middleware/errors');
const gameOptionModel = require('../model/gameoption');
const gameStartModel = require('../model/gamestart');
const { gameStartFilter } = require('./gamestart');
const peerInfoModel = require('../model/peerinfo');
const { peerInfoFilter } = require('./peerinfo');
const gameStateModel = require('../model/gamestates');
const { gameStateFilter } = require('./gamestates');
const playerActionModel = require('../model/playeraction');
const { playerActionFilter } = require('./playeraction');
const gameEndModel = require('../model/gameend');
const { gameEndFilter } = require('./gameend');


const listGameUuidFromGameStart = (req, res, next) => {

    try {
        const filter = gameStartFilter(req.query);

        gameStartModel.find(filter).distinct('gameUuid').then(data => {

            res.status(200).send(data);

        }).catch(err => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
}


const listGameUuidFromPeerInfo = (req, res, next) => {

    try {
        const filter = peerInfoFilter(req.query);

        peerInfoModel.find(filter).distinct('gameUuid').then(data => {
            return res.status(200).send(data);
        }).catch(err => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};


const listGameUuidFromGameState = (req, res, next) => {

    try {
        const filter = gameStateFilter(req.query);

        gameStateModel.find(filter).distinct('gameUuid').then(data => {
            res.status(200).send(data);
        }).catch(err => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};


const listGameUuidFromPlayerAction = (req, res, next) => {

    try {
        // We get the filter
        const filter = playerActionFilter(req.query);

        // We send the request to the database
        playerActionModel.find(filter).distinct('gameUuid').then(data => {
            res.status(200).send(data);
        }).catch(err => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};


const listGameUuidFromGameEnd = (req, res, next) => {

    try {
        const filter = gameEndFilter(req.query);

        gameEndModel.find(filter).distinct('gameUuid').then(data => {
            res.status(200).send(data);
        }).catch(err => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};


// From https://stackoverflow.com/questions/1129216/sort-array-of-objects-by-string-property-value
const dynamicSort = (property, order) => {

    return (a, b) => {
        const result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * order;
    }
}

const dynamicSortMultiple = (sortOptions) => {

    return (a, b) => {
        
        for (let key of Object.keys(sortOptions)) {

            const order = sortOptions[key];
            const result = dynamicSort(key, order)(a, b);

            if (result !== 0)
                return result;
        }
        
    }
}


const gameOverviewSort = (query) => {

    let sort = {};
    let errorArray = [];
    const sortItem = [
        "gameUuid",
        "gameOptionId",
        "playerCount",
        "maxPlayerCount",
        "playerTurnCount",
        "gameTurnCount",
        "currentScore",
        "currentMaxScore",
        "maxScore",
        "errorTokenCount",
        "speakTokenCount",
        "maxErrorTokenCount",
        "maxSpeakTokenCount",
        "gameCreation",
        "gameStartTimestamp",
        "gameEndTimestamp",
        "duration",
        "peerInfoCount",    // TODO this argument is strange: what is the difference with 'playerCount' ?
        "gameStateCount",
        "playerActionCount",
        "hasGameEnd",
        "victory"
    ]

    if ("sort_by" in query) {

        query.sort_by.split(",").forEach((item) => {
            try {

                const regex = /^(?<key>[a-zA-Z0-9]*)((?<asc>\[asc\])?|(?<desc>\[desc\])?)$/gm;
                const result = regex.exec(item);
                if (result) {

                    const key = result.groups.key;

                    if (!sortItem.includes(key))
                        throw new Error(`Unable to sort on '${key}'`);

                    let order = 1;
                    if (result.groups.desc)
                        order = -1;

                    sort[key] = order;
                }
                else
                    throw new Error(`Unable to parse the argument '${item}'`);
            }
            catch (err) {
                errorArray.push(err);
            }
        });

        if (errorArray.length != 0) {
            // TODO change this behaviour
            console.error(`There are ${errorArray.length} error(s) while parsing 'sort_by' argument`);
            throw errorArray[0];
        }
    }
    else {
        // Default sorting
        sort.gameCreation = -1;
    }

    return sort;
}


const gameOverview = (req, res, next) => {

    try {
        const sort = {
            timestamp: -1
        };

        const pageNumber = parseInt((req.query.page !== undefined) ? req.query.page : 0);
        const limit = parseInt((req.query.limit !== undefined) ? req.query.limit : constants.paginationMaxDocumentPerPage);
        let queryParams = "";
        for (const [key, value] of Object.entries(req.query)) {
            if (key !== "limit" && key !== "page") {
                queryParams += `&${key}=${value}`;
            }
        }
        const baseUrl = req.originalUrl.split("?")[0] + `?limit=${limit}` + queryParams;

        let pipeline = constants.paginationPipeline(pageNumber, limit, baseUrl);

        // Add sorting
        pipeline.unshift({ $sort: sort });

        // Add filtering
        const filter = gameStartFilter(req.query);
        pipeline.unshift({ $match: filter });

        gameStartModel.aggregate(pipeline)
            .then(async result => {

                if (result.length != 1) {
                    throw new Error("Aggregation error");
                }

                const starts = result[0].data;

                // For each game UUID...
                const overview = await Promise.all(starts.map(async (gameStart) => {

                    // Find the game option
                    const gameOptionId = gameStart.gameOptionId.toString();
                    const gameOption = await gameOptionModel.findById(gameOptionId);
                    if (gameOption == null) {
                        throw new errors.NotFoundError("gameOption", gameOptionId);
                    }

                    const gameUuid = gameStart.gameUuid;
                    const gameFilter = { gameUuid };

                    // Lets find the peer infos
                    const peerInfoCount = await peerInfoModel.countDocuments(gameFilter).exec();

                    // Lets find the game states
                    const gameStateCount = await gameStateModel.countDocuments(gameFilter).exec();
                    let lastGameState = undefined;
                    if (gameStateCount > 0) {
                        const gameStateFilter = {
                            gameUuid,
                            playerTurnCount: gameStateCount - 1,
                        }
                        const gameStateList = await gameStateModel.find(gameStateFilter);
                        if (gameStateList.length != 1) {
                            throw new errors.DatabaseInconsistencyError();
                        }
                        lastGameState = gameStateList[0];
                    }

                    // Lets find the player actions
                    const playerActionCount = await playerActionModel.countDocuments(gameFilter).exec();

                    // Lets find the game end
                    const gameEndList = await gameEndModel.find(gameFilter);
                    let gameEnd = undefined;
                    if (gameEndList.length == 1) {
                        gameEnd = gameEndList[0];
                    }

                    const summary = {
                        gameUuid,
                        gameOptionId: gameOptionId,
                        playerCount: peerInfoCount,
                        maxPlayerCount: gameOption.playerCount,
                        playerTurnCount: lastGameState && lastGameState.playerTurnCount,
                        gameTurnCount: lastGameState && lastGameState.gameTurnCount,
                        currentScore: lastGameState && lastGameState.board.score,
                        currentMaxScore: lastGameState && lastGameState.trash.currentMaxScore,
                        maxScore: gameOption.maxScore,
                        errorTokenCount: lastGameState && lastGameState.errorTokenCount,
                        speakTokenCount: lastGameState && lastGameState.speakTokenCount,
                        maxErrorTokenCount: gameOption.maxErrorTokenCount,
                        maxSpeakTokenCount: gameOption.maxSpeakTokenCount,
                        gameCreation: gameStart.timestamp,
                        gameStartTimestamp: gameEnd && gameEnd.gameStartTimestamp,
                        gameEndTimestamp: gameEnd && gameEnd.gameEndTimestamp,
                        duration: gameEnd && gameEnd.gameDuration,
                        tags: gameStart.tags,
                        peerInfoCount,
                        gameStateCount,
                        playerActionCount,
                        hasGameEnd: !!(gameEnd),
                        victory: gameEnd && gameEnd.victory,
                    };

                    return summary;
                }));

                const metadata = result[0].metadata;

                const sort = gameOverviewSort(req.query);
                overview.sort(dynamicSortMultiple(sort));

                const returnedData = {
                    metadata,
                    data: overview,
                };
                return res.status(200).json(returnedData);

            }).catch(err => {
                next(err);
            })
    }
    catch (err) {
        next(err);
    }
};


module.exports = {
    listGameUuidFromGameStart,
    listGameUuidFromPeerInfo,
    listGameUuidFromGameState,
    listGameUuidFromPlayerAction,
    listGameUuidFromGameEnd,
    gameOverview,
}