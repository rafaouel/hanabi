const bcrypt = require('bcrypt');
const constants = require('../constants');
const jwt = require('jsonwebtoken');


const encryptPassword = (password) => {
    return bcrypt.hashSync(password, constants.saltRounds);
}

const isPasswordValid = (passwordToTest, encryptedPassword) => {
    return bcrypt.compareSync(passwordToTest, encryptedPassword);
}

const generateAccessToken = (userLogin, TTL) => {

    const options = {
        expiresIn: TTL
    };

    const payload = {
        tokenType: constants.accessTokenType,
        login: userLogin
    };

    const token = jwt.sign(payload, constants.apiPrivateKey, options);
    return token;
}

const generateRefreshToken = (userLogin, TTL) => {

    const options = {
        expiresIn: TTL
    };

    const payload = {
        tokenType: constants.refreshTokenType,
        login: userLogin
    }

    const token = jwt.sign(payload, constants.apiPrivateKey, options);
    return token;
}

const extractFromToken = (token) => {

    const payload = jwt.verify(token, constants.apiPrivateKey);
    payload.expirationDate = new Date(payload.exp * 1000);
    payload.issuedDate = new Date(payload.iat * 1000);

    delete payload.iat;
    delete payload.exp;

    return payload;
}

const getRandomInt = (max) => {
    return Math.floor(Math.random() * max);
}

module.exports = {
    encryptPassword,
    isPasswordValid,
    generateAccessToken,
    generateRefreshToken,
    extractFromToken,
    getRandomInt,
}