const constants = require('../constants');
const errors = require('../middleware/errors');
const playerActionModel = require('../model/playeraction');
const subscription = require('./subscription');

const notifyInsertion = subscription.notifyInsertion;
const notifyDeletion = subscription.notifyDeletion;


const playerActionFilter = (query => {
    let filter = {};
    if ("gameUuid" in query) {
        filter.gameUuid = query.gameUuid;
    }
    if ("playerTurnCount" in query) {
        filter.playerTurnCount = parseInt(query.playerTurnCount);
    }
    if ("gameTurnCount" in query) {
        filter.gameTurnCount = parseInt(query.gameTurnCount);
    }
    if ("currentPlayerId" in query) {
        filter.currentPlayerId = parseInt(query.currentPlayerId);
    }
    if ("actionType" in query) {
        filter.actionType = query.actionType;
    }
    if ("targetPlayerId" in query) {
        filter.targetPlayerId = parseInt(query.targetPlayerId);
    }
    if ("handIndex" in query) {
        filter.handIndex = parseInt(query.handIndex);
    }
    if ("cardInfo.value" in query) {
        filter["cardInfo.value"] = parseInt(query["cardInfo.value"]);
    }
    if ("cardInfo.color" in query) {
        filter["cardInfo.color"] = query["cardInfo.color"];
    }

    let roundtripDuration = undefined;
    if ("maxRoundtripDuration" in query) {
        if (roundtripDuration === undefined) {
            roundtripDuration = {};
        }
        roundtripDuration["$lte"] = parseFloat(query.maxRoundtripDuration);
    }
    if ("minRoundtripDuration" in query) {
        if (roundtripDuration === undefined) {
            roundtripDuration = {};
        }
        roundtripDuration["$gte"] = parseFloat(query.minRoundtripDuration);
    }
    if (roundtripDuration !== undefined) {
        filter.roundtripDuration = roundtripDuration;
    }

    return filter;
})


const createPlayerAction = ((req, res, next) => {

    try {
        const newPlayerAction = new playerActionModel(req.body);
        newPlayerAction.save().then(createdDocument => {

            createdDocument._id = undefined;
            createdDocument.__v = undefined;

            res.status(201).send();

            // Notification
            notifyInsertion(createdDocument, "playerAction");

        }).catch(err => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
})


const getPlayerAction = ((req, res, next) => {

    try {
        const pageNumber = parseInt((req.query.page !== undefined) ? req.query.page : 0);
        const limit = parseInt((req.query.limit !== undefined) ? req.query.limit : constants.paginationMaxDocumentPerPage);
        let queryParams = "";
        for (const [key, value] of Object.entries(req.query)) {
            if (key !== "limit" && key !== "page") {
                queryParams += `&${key}=${value}`;
            }
        }
        const baseUrl = req.originalUrl.split("?")[0] + `?limit=${limit}` + queryParams;

        let pipeline = constants.paginationPipeline(pageNumber, limit, baseUrl);

        // Add projection
        pipeline.unshift({
            $project: {
                _id: 0,
                __v: 0,
            }
        });

        // Add sorting
        pipeline.unshift({
            $sort: {
                gameUuid: 1,
                playerTurnCount: 1
            }
        });

        // Add filtering
        const filter = playerActionFilter(req.query);
        pipeline.unshift({ $match: filter, });

        playerActionModel.aggregate(pipeline)
            .then(result => {

                if (result.length == 1) {

                    if (result[0].error) {
                        if (result[0].error.type === "currentPageOutOfRange") {
                            throw new errors.PageNumberOutOfRangeError()
                        }
                    }

                    const returnedResult = {
                        metadata: result[0].metadata,
                        data: result[0].data,
                    }
                    return res.status(200).json(returnedResult);
                }
                throw new Error("Unknown aggregation error");
            })
            .catch(err => {
                next(err);
            });
    }
    catch (err) {
        next(err);
    }
})


const deletePlayerActionByGameUuid = ((req, res, next) => {

    try {
        const filter = { gameUuid: req.params.gameUuid };

        playerActionModel.find(filter).then(playerActionArray => {

            var deletedGameUuid = [];
            var processedPlayerAction = 0;

            if (playerActionArray.length > 0) {

                playerActionArray.forEach(playerAction => {
                    playerActionModel.deleteOne(playerAction._id).then(summary => {

                        processedPlayerAction += 1;

                        if (summary.deletedCount == 1) {

                            deletedGameUuid.push({
                                game_uuid: playerAction.gameUuid,
                                player_turn_count: playerAction.playerTurnCount
                            });

                            // Notification
                            notifyDeletion(playerAction, "player_action");
                        }

                        if (processedPlayerAction == playerActionArray.length) {

                            if (deletedGameUuid.length == playerActionArray.length) {
                                res.status(200).send(deletedGameUuid);
                            }
                            else {
                                res.status(400).send(deletedGameUuid);
                            }
                        }

                    }).catch(err => {
                        processedPlayerAction += 1;

                        console.log("Unable to delete player action '" + playerAction.gameUuid + "' / '" + playerAction.playerTurnCount + "'");
                        console.log(err);
                    });
                });

            }
            else {
                res.status(404).send();
            }

        }).catch(err => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
});


module.exports = {
    createPlayerAction,
    getPlayerAction,
    deletePlayerActionByGameUuid,
    playerActionFilter,
}