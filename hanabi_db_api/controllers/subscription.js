const subscriptionModel = require('../model/subscription');
const http = require("http");
const constants = require("../constants");


const subscribe = ((req, res, next) => {

    try {
        const since = new Date(Date.now());
        const expire = new Date(since.getTime() + 1000. * constants.subscriptionExpireAfterSeconds);

        const notification = {
            method: req.body.notification.method,
            host: req.body.notification.host,
            port: req.body.notification.port,
            target: req.body.notification.target,
        };

        const subject = {
            type: req.body.subject.type,
            gameUuid: req.body.subject.gameUuid,
            action: req.body.subject.action
        };

        const data = {
            since: since,
            expire: expire,
            subject: subject,
            notification: notification
        };

        subscriptionModel.create(data).then(savedData => {
            return res.status(201).send(savedData);
        }).catch(err => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
});

const getFilter = (query => {
    let filter = {};
    if ("method" in query) {
        filter["notification.method"] = query["notification.method"].toUpperCase();
    }
    if ("host" in query) {
        filter["notification.host"] = query["notification.host"];
    }
    if ("port" in query) {
        filter["notification.port"] = query["notification.port"];
    }
    if ("subject.gameUuid" in query) {
        filter["subject.gameUuid"] = query["subject.gameUuid"];
    }
    if ("subject.type" in query) {
        filter["subject.type"] = query["subject.type"];
    }
    if ("subject.action" in query) {
        filter["subject.action"] = query["subject.action"];
    }

    return filter;
});


const unsubscribe = ((req, res, next) => {

    try {
        const filter = {
            _id: req.params._id
        };

        subscriptionModel.deleteOne(filter).then(data => {
            if (data.deletedCount == 0) {
                res.status(404).send(data);
            }
            else {
                res.status(200).send(data);
            }
        }).catch(err => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
});


const unsubscribeAll = ((req, res, next) => {

    try {
        subscriptionModel.deleteMany().then(data => {
            if (data.deletedCount == 0) {
                res.status(404).send(data);
            }
            else {
                res.status(200).send(data);
            }
        }).catch(err => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
});


const getSubscription = ((req, res, next) => {

    try {
        // Get the filter
        const filter = getFilter(req.query);

        // Send the request to the database
        subscriptionModel.find(filter).then(data => {

            res.status(200).send(data);
        }).catch(err => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
});


const updateSubscription = ((req, res, next) => {

    try {
        const filter = { _id: req.params._id };
        const since = new Date(Date.now());
        const expire = new Date(since.getTime() + 1000 * constants.subscription_expire_after_seconds);
        const update_data = {
            $set: {
                since: since,
                expire: expire
            }
        }
        subscriptionModel.findOneAndUpdate(filter, update_data, { new: true }).then(data => {
            if (data) {
                res.status(200).send(data);
            }
            else {
                res.status(404).send(data);
            }
        }).catch(err => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
});


const notifyInsertion = ((subjectDocument, subjectType) => {
    notify(subjectDocument, subjectType, "insertion");
});


const notifyUpdate = ((subjectDocument, subjectType) => {
    notify(subjectDocument, subjectType, "update");
});


const notifyDeletion = ((subjectDocument, subjectType) => {
    notify(subjectDocument, subjectType, "deletion");
});


const notify = ((subjectData, subjectType, action) => {

    // Build a filter on the subscriptions
    let filter = {
        "subject.type": subjectType,
        "subject.action": action
    };

    // Look for notification info in the database
    subscriptionModel.find(filter).then(subscriptionArray => {       

        // For each observer registered in the database...
        subscriptionArray.forEach((subscription) => {
            // Check if subscription is linked with the subject
            if (subscription.subject.hasOwnProperty("gameUuid")) {
                if (subjectData.gameUuid != subscription.subject.gameUuid) {
                    return;
                }
            }
            
            // Body
            // Subscripton info
            const subscriptionInfo = {
                _id: subscription._id,
                expire: subscription.expire
            };

            // Subject info
            let subjectInfo = {
                type: subjectType,
                gameUuid: subjectData.gameUuid
            };
            if ("playerTurnCount" in subjectData) {
                subjectInfo.playerTurnCount = subjectData.playerTurnCount;
            }
            if ("playerId" in subjectData) {
                subjectInfo.playerId = subjectData.playerId;
            }

            // Building body request
            const body = JSON.stringify({
                subscription: subscriptionInfo,
                subject: subjectInfo,
                action: action
            });

            // Headers
            const headers = {
                'content-type': 'application/json',
                'content-length': Buffer.byteLength(body),
                'host': "localhost"
            };

            const options = {
                method: subscription.notification.method,
                hostname: subscription.notification.host,
                port: subscription.notification.port,
                path: subscription.notification.target,
                headers: headers
            };

            const req = subscription.notification.method + " " + subscription.notification.host + ":" + subscription.notification.port + subscription.notification.target;
            console.log("Sending notification '" + req + "'");

            // Builing request
            const request = http.request(options, res => {
                if (res.statusCode < 300) {
                    console.log("Notification ok");
                }
                else {
                    console.log("Notification not ok (status code=" + res.statusCode + "; " + res.statusMessage + ")");
                    if (res.body)
                        console.log(res.body);
                }
                // Close the connection
                request.destroy();
            }).on("error", (err => {
                console.log("An error occured in notification request creation!");
                console.log(err);
                if (constants.subscription_remove_after_error) {
                    subscriptionModel.deleteOne(subscription._id).then(summary => {
                        if (summary.deletedCount == 1) {
                            console.log("Subscription successfully removed");
                        }
                        else {
                            console.log("Error while removing subscription");
                        }
                    }).catch(err => {
                        console.log("Error while removing subscription");
                    });
                }
            }));

            // Adding the body
            request.write(body);

            // Sending it
            request.end();
        });
    }).catch(err => {
        console.log("An error occured while subscription retrieval");
        console.log(err);
    });
});


module.exports = {
    subscribe,
    unsubscribe,
    unsubscribeAll,
    getSubscription,
    updateSubscription,
    notifyInsertion,
    notifyUpdate,
    notifyDeletion
}