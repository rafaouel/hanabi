const constants = require('../constants');
const errors = require('../middleware/errors');
const gameOptionModel = require('../model/gameoption');
const mongoose = require('mongoose');
const subscription = require('./subscription');
const tools = require('./tools');

const notifyInsertion = subscription.notifyInsertion;
const notifyDeletion = subscription.notifyDeletion;


const gameOptionFilter = query => {
    let filter = {};

    if ("gameOptionId" in query) {
        filter._id = new mongoose.Types.ObjectId(query.gameOptionId);
    }

    let timestamp = undefined;
    if ("after" in query) {
        if (timestamp === undefined) {
            timestamp = {};
        }
        timestamp["$gte"] = new Date(query.after);
    }
    if ("before" in query) {
        if (timestamp === undefined) {
            timestamp = {};
        }
        timestamp["$lte"] = new Date(query.before);
    }
    if (timestamp !== undefined) {
        filter.timestamp = timestamp;
    }

    if ("playerCount" in query) {
        filter.playerCount = parseInt(query.playerCount);
    }
    if ("playerHandMaxSize" in query) {
        filter.playerHandMaxSize = parseInt(query.playerHandMaxSize);
    }
    if ("startPlayerId" in query) {
        filter.startPlayerId = parseInt(query.startPlayerId);
    }
    if ("maxErrorTokenCount" in query) {
        filter.maxErrorTokenCount = parseInt(query.maxErrorTokenCount);
    }
    if ("maxSpeakTokenCount" in query) {
        filter.maxSpeakTokenCount = parseInt(query.maxSpeakTokenCount);
    }
    if ("startErrorTokenCount" in query) {
        filter.startErrorTokenCount = parseInt(query.startErrorTokenCount);
    }
    if ("startSpeakTokenCount" in query) {
        filter.startSpeakTokenCount = parseInt(query.startSpeakTokenCount);
    }
    if ("minCardValue" in query) {
        filter.minCardValue = parseInt(query.minCardValue);
    }
    if ("maxCardValue" in query) {
        filter.maxCardValue = parseInt(query.maxCardValue);
    }
    if ("reserved" in query) {
        filter.reserved = (query.reserved === 'true');
    }
    if ("maxScore" in query) {
        filter.maxScore = parseInt(query.maxScore);
    }
    if ("callableColor" in query) {
        filter["callableColors"] = { $all: [query.callableColor] };
    }
    if ("color" in query) {
        filter["colorOption.color"] = { $all: [query.color] };
    }
    if ("tags" in query) {
        filter["tags"] = { $all: [].concat(query.tags) };
    }

    return filter;
}

const gameOptionSort = query => {

    let sort = {};
    let errorArray = [];
    const sortItem = [
        "timestamp",
        "playerCount",
        "playerHandMaxSize",
        "startPlayerId",
        "maxErrorTokenCount",
        "maxSpeakTokenCount",
        "startErrorTokenCount",
        "startSpeakTokenCount",
        "minCardValue",
        "maxCardValue",
        "maxScore",
        "reserved",
        "gameOptionId",
    ]

    if ("sort_by" in query) {

        query.sort_by.split(",").forEach((item) => {
            try {

                const regex = /^(?<key>[a-zA-Z0-9]*)((?<asc>\[asc\])?|(?<desc>\[desc\])?)$/gm;
                const result = regex.exec(item);
                if (result) {

                    const key = result.groups.key;

                    if (!sortItem.includes(key))
                        throw new Error(`Unable to sort on '${key}'`);

                    let order = 1;
                    if (result.groups.desc)
                        order = -1;

                    sort[key] = order;
                }
                else
                    throw new Error(`Unable to parse the argument '${item}'`);
            }
            catch (err) {
                errorArray.push(err);
            }
        });

        if (errorArray.length != 0) {
            // TODO change this behaviour
            console.error(`There are ${errorArray.length} error(s) while parsing 'sort_by' argument`);
            throw errorArray[0];
        }
    }
    else {
        // Default sorting
        sort.timestamp = -1;
    }

    return sort;
}


const handMaxSizeFromPlayerCount = (playerCount) => {
    if (playerCount <= 3) {
        return 5;
    }
    else {
        return 4;
    }
}

const getStandardReaction = (color) => {

    switch (color) {
        case "blue":
            return ["blue"];
        case "white":
            return ["white"];
        case "red":
            return ["red"];
        case "yellow":
            return ["yellow"];
        case "green":
            return ["green"];
        case "black":
            return [];
        case "multicolor":
            return ["blue", "white", "red", "yellow", "green"];
        default:
            throw new Error(`Unknown color '${color}'`);
    }
}

const colorOptionFromPreset = (preset) => {

    let colorOption = [];

    for (const color of colorListFromPreset(preset)) {

        const isAscending = color != "black";

        colorOption.push({
            color,
            isAscending,
            reactsTo: getStandardReaction(color),
            distribution: getClassicDistribution(isAscending),
        });
    }

    return colorOption;
}

const colorListFromPreset = (preset) => {

    switch (preset) {
        case "std":
            return ["blue", "white", "red", "yellow", "green"];
        case "easy":
            return ["blue", "white", "red"];
        case "cake":
            return ["blue"];
        case "hard":
            return ["blue", "white", "red", "yellow", "green", "multicolor", "black"];
        case "black":
            return ["blue", "white", "red", "yellow", "green", "black"];
        case "multi":
            return ["blue", "white", "red", "yellow", "green", "multicolor"];
        default:
            throw new Error(`Unknown preset '${preset}'`);
    }
}

const callableColorsFromPreset = (preset) => {

    switch (preset) {
        case "std":
            return ["blue", "white", "red", "yellow", "green"];
        case "easy":
            return ["blue", "white", "red"];
        case "cake":
            return ["blue"];
        case "hard":
            return ["blue", "white", "red", "yellow", "green"];
        case "black":
            return ["blue", "white", "red", "yellow", "green"];
        case "multi":
            return ["blue", "white", "red", "yellow", "green"];
        default:
            throw new Error(`Unknown preset '${preset}'`);
    }
}

const getClassicDistribution = (isAscending = true) => {

    let distribution = [];

    for (let value = 1; value <= 5; ++value) {

        count = 2;

        if (value == 1) {
            isAscending ? count += 1 : count -= 1;
        }

        if (value == 5) {
            isAscending ? count -= 1 : count += 1;
        }

        distribution.push({
            value,
            count,
        });
    }

    return distribution;
}

const getParamFromPreset = (preset, playerCount) => {

    const param = {
        playerCount,
        //startPlayerId: tools.getRandomInt(playerCount),
        startPlayerId: 0,
        playerHandMaxSize: handMaxSizeFromPlayerCount(playerCount),
        maxErrorTokenCount: 3,
        maxSpeakTokenCount: 8,
        startErrorTokenCount: 0,
        startSpeakTokenCount: 8,
        colorOption: colorOptionFromPreset(preset),
        callableColors: callableColorsFromPreset(preset),
    };

    return param;
}

const reserveGameOptionById = (req, res, next) => {

    const filter = {
        _id: req.params.gameOptionId
    };

    const updateOperation = {
        reserved: true,
    };

    gameOptionModel.updateOne(filter, updateOperation)
        .then(result => {

            if (result.matchedCount == 1) {
                if (result.modifiedCount == 1) {
                    return res.status(200).send();
                }
                return res.status(204).send();
            }
            return res.status(404).send();
        }).catch(err => {
            next(err);
        });
}


const reserveGameOptionFromPreset = (req, res, next) => {

    try {

        if (req.body.playerCount === undefined) {
            throw new Error("'playerCount' is missing");
        }

        if (req.body.preset === undefined) {
            throw new Error("'preset' is missing");
        }

        // Get the filter to find a game option
        let filter = {
            reserved: false,
            tags: { $all: [req.body.preset] },
            playerCount: req.body.playerCount,
        };

        // Look for a random game option that match the criterion (if there is one)
        gameOptionModel.aggregate([
            {
                $match: filter,
            },
            {
                $sample: { size: 1 },
            },
        ]).then(result => {
            // No game option available => lets build one
            if (result.length == 0) {

                const param = {
                    preset: req.body.preset,
                    playerCount: req.body.playerCount,
                }
                const newGameOption = gameOptionFromDistribution(param);
                newGameOption.reserved = true;

                gameOptionModel.create(newGameOption).then(result => {

                    let returnedValue = {
                        gameOptionId: result._id,
                    }

                    return res.status(201).json(returnedValue);

                }).catch(err => {

                    console.log(err);
                    return res.status(500).send(err);
                });
                return;
            }
            // At least one game option is available => reserve this one
            else if (result.length == 1) {

                const updateFilter = {
                    _id: result[0]._id,
                };
                const updateOperation = {
                    reserved: true
                };
                const options = {
                    new: true,
                    upsert: false
                };
                gameOptionModel.updateOne(updateFilter, updateOperation, options)
                    .then(updateResult => {

                        if (updateResult.modifiedCount == 1) {
                            // Return the game option id
                            const returnedResult = {
                                gameOptionId: result[0]._id,
                            };

                            return res.status(200).json(returnedResult);
                        }
                        throw new Error("Unable to update the gameOption");
                    }).catch(err => {

                        console.log(err);
                        return res.status(500).send(err);
                    });
                return;
            }

            console.log(filter);
            return res.status(500).send();
        }).catch(next);
    }
    catch (err) {
        next(err);
    }
}


const reserveGameOptionFromParam = (req, res, next) => {

    try {

        if (req.body.playerCount === undefined) {
            throw new Error("'playerCount' is missing");
        }

        let filter = {
            reserved: false,
        };

        filter["playerCount"] = parseInt(req.body.playerCount);

        if (req.body.startPlayerId !== undefined) {
            filter["startPlayerId"] = parseInt(req.body.startPlayerId);
        }
        else {
            filter["startPlayerId"] = tools.getRandomInt(filter.playerCount);
        }

        if (!req.body.playerHandMaxSize) {
            throw new Error("'playerHandMaxSize' is missing");
        }
        filter["playerHandMaxSize"] = parseInt(req.body.playerHandMaxSize);

        if (!req.body.maxErrorTokenCount) {
            throw new Error("'maxErrorTokenCount' is missing");
        }
        filter["maxErrorTokenCount"] = parseInt(req.body.maxErrorTokenCount);

        if (!req.body.maxSpeakTokenCount) {
            throw new Error("'maxSpeakTokenCount' is missing");
        }
        filter["maxSpeakTokenCount"] = parseInt(req.body.maxSpeakTokenCount);

        if (req.body.startErrorTokenCount === undefined) {
            // Choose a random value [0; maxErrorTokenCount[
            filter["startErrorTokenCount"] = tools.getRandomInt(filter.maxErrorTokenCount);
        }
        else {
            const proposedStartTokenCount = parseInt(req.body.startErrorTokenCount);
            if (proposedStartTokenCount >= filter.maxErrorTokenCount) {
                throw new Error(`'startErrorTokenCount' is bigger than 'maxErrorTokenCount' (${proposedStartTokenCount} >= ${filter.maxErrorTokenCount})`);
            }
            if (proposedStartTokenCount < 0) {
                throw new Error(`'startErrorTokenCount' should be positive or null`);
            }
            filter["startErrorTokenCount"] = proposedStartTokenCount;
        }

        if (req.body.startSpeakTokenCount === undefined) {
            // Choose a random value [0; maxSpeakTokenCount]
            filter["startSpeakTokenCount"] = tools.getRandomInt(filter.maxSpeakTokenCount + 1);
        }
        else {
            const proposedStartTokenCount = parseInt(req.body.startSpeakTokenCount);
            if (proposedStartTokenCount > filter.maxSpeakTokenCount) {
                throw new Error(`'startSpeakTokenCount' is bigger than 'maxSpeakTokenCount' (${proposedStartTokenCount} > ${filter.maxSpeakTokenCount})`);
            }
            if (proposedStartTokenCount < 0) {
                throw new Error(`'startSpeakTokenCount' should be positive or null`);
            }
        }

        if (!req.body.colorOption) {
            throw new Error("'colorOption' is missing");
        }
        if (!Array.isArray(req.body.colorOption)) {
            throw new Error("'colorOption' is not an array");
        }
        filter["colorOption"] = req.body.colorOption;

        if (req.body.callableColors === undefined) {
            throw new Error("'callableColors' is missing");
        }
        if (!Array.isArray(req.body.callableColors)) {
            throw new Error("'callableColors' should be an array");
        }
        filter["callableColors"] = req.body.callableColors;

        // Look for a random 'unreserved' game option that match the params
        gameOptionModel.aggregate([
            {
                $match: filter,
            },
            {
                $sample: { size: 1 },
            },
        ]).then(result => {

            // No game option available => lets build one
            if (result.length == 0) {

                const newGameOption = gameOptionFromDistribution(filter);
                newGameOption.reserved = true;

                gameOptionModel.create(newGameOption).then(result => {

                    let returnedValue = {
                        gameOptionId: result._id,
                    }

                    return res.status(201).json(returnedValue);

                }).catch(err => {

                    console.log(err);
                    return res.status(500).send(err);
                });
                return;
            }
            // At least one game option is available => reserve this one
            else if (result.length == 1) {

                const updateFilter = {
                    _id: result[0]._id,
                };
                const updateOperation = {
                    reserved: true
                };
                const options = {
                    new: true,
                    upsert: false
                };
                gameOptionModel.updateOne(updateFilter, updateOperation, options)
                    .then(updateResult => {

                        if (updateResult.modifiedCount == 1) {
                            // Return the game option id
                            const returnedResult = {
                                gameOptionId: result[0]._id,
                            };

                            return res.status(200).json(returnedResult);
                        }
                        throw new Error("Unable to update the gameOption");
                    }).catch(err => {

                        console.log(err);
                        return res.status(500).send(err);
                    });
                return;
            }
            
            console.log(filter);
            return res.status(500).send();

        }).catch(err => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }

}

const shuffle = (array) => {
    // From 'https://bost.ocks.org/mike/shuffle/'

    var m = array.length, t, i;

    // While there remain elements to shuffle�
    while (m) {

        // Pick a remaining element�
        i = Math.floor(Math.random() * m--);

        // And swap it with the current element.
        t = array[m];
        array[m] = array[i];
        array[i] = t;
    }

    return array;
}

const generateStackFromColorOption = (colorOption) => {

    let stack = [];

    // Add cards
    for (const option of colorOption) {
        for (const distribution of option.distribution) {

            const card = {
                value: distribution.value,
                color: option.color
            };

            for (let i = 0; i < distribution.count; ++i) {
                stack.push(card);
            }

        }
    }

    // Shuffle
    shuffle(stack);

    return stack;
}

const getCardValueFromColorOption = (colorOption) => {

    let first = true;
    let min = undefined;
    let max = undefined;

    for (const option of colorOption) {
        for (const distribution of option.distribution) {
            if (first) {
                min = distribution.value;
                max = distribution.value;
                first = false;
            }

            if (distribution.value < min) {
                min = distribution.value;
            }

            if (distribution.value > max) {
                max = distribution.value;
            }
        }
    }

    return {
        minCardValue: min,
        maxCardValue: max
    };
}

const getMaxScoreFromColorOption = (colorOption) => {

    let score = 0;

    for (const option of colorOption) {
        score += option.distribution.length;
    }

    return score;
}

const getColorOptionFromStack = (partialColorOption, stack) => {

    let colorOption = [...partialColorOption];

    for (const card of stack) {

        // If color is in color option...
        const colorIndex = colorOption.findIndex((option) => {
            return option.color == card.color;
        });
        if (colorIndex == -1) {
            throw new Error(`The color of the card (${card.color}) is not present in the 'colorOption'`);
        }

        // If there is not a distribution
        if (colorOption[colorIndex].distribution === undefined) {
            colorOption[colorIndex].distribution = [{
                value: card.value,
                count: 1
            }];
        }
        else {
            // Add the value to the distribution
            const distributionIndex = colorOption[colorIndex].distribution.findIndex((pair) => {
                return pair.value == card.value;
            });
            if (distributionIndex == -1) {
                colorOption[colorIndex].distribution.push({
                    value: card.value,
                    count: 1
                });
            }
            else {
                colorOption[colorIndex].distribution[distributionIndex].count += 1;
            }
        }
    }

    const result = colorOption.filter((option, index) => {
        return option.distribution !== undefined;
    });

    return result;
}

const getCallableColorsFromStack = (stack, excludedColors = ["multicolor", "black"]) => {

    colors = [];

    for (const card of stack) {
        color = card.color;

        // The color of the card is not in exclusion list
        if (!excludedColors.includes(color)) {

            // The color of card is not yet in returned colors
            if (!colors.includes(color)) {

                colors.push(color)
            }
        }
    }

    return colors;
}

const gameOptionFromStack = (param) => {
    let newGameOption = {};

    newGameOption["tags"] = [
        "fromStack",
    ]

    if (param.playerCount === undefined) {
        throw new Error("'playerCount' is missing");
    }
    newGameOption["playerCount"] = parseInt(param.playerCount);

    if (param.playerHandMaxSize === undefined) {
        throw new Error("'playerHandMaxSize' is missing");
    }
    newGameOption["playerHandMaxSize"] = parseInt(param.playerHandMaxSize);

    if (param.startPlayerId === undefined) {
        newGameOption["startPlayerId"] = tools.getRandomInt(newGameOption.playerCount);
    }
    else {
        newGameOption["startPlayerId"] = parseInt(param.startPlayerId) % newGameOption.playerCount;
    }

    if (param.maxErrorTokenCount === undefined) {
        throw new Error("'maxErrorTokenCount' is missing");
    }
    newGameOption["maxErrorTokenCount"] = parseInt(param.maxErrorTokenCount);

    if (param.maxSpeakTokenCount === undefined) {
        throw new Error("'maxSpeakTokenCount' is missing");
    }
    newGameOption["maxSpeakTokenCount"] = parseInt(param.maxSpeakTokenCount);

    if (param.startErrorTokenCount === undefined) {
        // Choose a random value [0; maxErrorTokenCount[
        newGameOption["startErrorTokenCount"] = tools.getRandomInt(newGameOption.maxErrorTokenCount);
    }
    else {
        const proposedStartTokenCount = parseInt(param.startErrorTokenCount);
        if (proposedStartTokenCount >= newGameOption.maxErrorTokenCount) {
            throw new Error(`'startErrorTokenCount' is bigger than 'maxErrorTokenCount' (${proposedStartTokenCount} >= ${newGameOption.maxErrorTokenCount})`);
        }
        if (proposedStartTokenCount < 0) {
            throw new Error(`'startErrorTokenCount' should be positive or null`);
        }
        newGameOption["startErrorTokenCount"] = proposedStartTokenCount;
    }

    if (param.startSpeakTokenCount === undefined) {
        // Choose a random value [0; maxSpeakTokenCount]
        newGameOption["startSpeakTokenCount"] = tools.getRandomInt(newGameOption.maxSpeakTokenCount + 1);
    }
    else {
        const proposedStartTokenCount = parseInt(param.startSpeakTokenCount);
        if (proposedStartTokenCount > newGameOption.maxSpeakTokenCount) {
            throw new Error(`'startSpeakTokenCount' is bigger than 'maxSpeakTokenCount' (${proposedStartTokenCount} > ${newGameOption.maxSpeakTokenCount})`);
        }
        if (proposedStartTokenCount < 0) {
            throw new Error(`'startSpeakTokenCount' should be positive or null`);
        }
        newGameOption["startSpeakTokenCount"] = proposedStartTokenCount;
    }

    if (param.colorOption === undefined) {
        throw new Error("'colorOption' is missing");
    }
    for (const color of param.colorOption) {
        if (color.isAscending === undefined) {
            throw new Error(`'isAscending' is missing for colorOption.${color.color}`);
        }
        if (color.reactsTo === undefined) {
            throw new Error(`'reactsTo' is missing for colorOption.${color.color}`);
        }
    }
    const partialColorOption = param.colorOption;

    if (param.stack === undefined) {
        throw new Error("'stack' is missing");
    }
    newGameOption["stack"] = param.stack;

    newGameOption.colorOption = getColorOptionFromStack(partialColorOption, newGameOption.stack);
    newGameOption.callableColors = getCallableColorsFromStack(newGameOption.stack);

    newGameOption.maxScore = getMaxScoreFromColorOption(newGameOption.colorOption);
    const { minCardValue, maxCardValue } = getCardValueFromColorOption(newGameOption.colorOption);
    newGameOption.minCardValue = minCardValue;
    newGameOption.maxCardValue = maxCardValue;
    newGameOption.timestamp = new Date(Date.now());

    return newGameOption;
}


const createGameOptionFromStack = (req, res, next) => {

    try {

        const newGameOption = gameOptionFromStack(req.body);

        gameOptionModel.create(newGameOption).then(result => {

            const returnedValue = {
                gameOptionId: result._id,
            };

            return res.status(201).json(returnedValue);

        }).catch(err => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
}

const gameOptionFromDistribution = (param) => {

    if (param.playerCount === undefined) {
        throw new Error("'playerCount' is missing");
    }

    let newGameOption = {};

    // Preset is defined => convertion from preset to param
    if (param.preset) {
        newGameOption = getParamFromPreset(param.preset, param.playerCount);
        newGameOption["tags"] = [
            param.preset,
            "fromPreset",
        ];
    }
    else {
        // Preset is not defined => the params are given into the body
        newGameOption["playerCount"] = parseInt(param.playerCount);
        newGameOption["tags"] = [
            "fromParam",
        ];

        if (param.startPlayerId !== undefined) {
            newGameOption["startPlayerId"] = parseInt(param.startPlayerId) % newGameOption.playerCount;
        }
        else {
            newGameOption["startPlayerId"] = tools.getRandomInt(newGameOption.playerCount);
        }

        if (!param.playerHandMaxSize) {
            throw new Error("'playerHandMaxSize' is missing");
        }
        newGameOption["playerHandMaxSize"] = parseInt(param.playerHandMaxSize);

        if (!param.maxErrorTokenCount) {
            throw new Error("'maxErrorTokenCount' is missing");
        }
        newGameOption["maxErrorTokenCount"] = parseInt(param.maxErrorTokenCount);

        if (!param.maxSpeakTokenCount) {
            throw new Error("'maxSpeakTokenCount' is missing");
        }
        newGameOption["maxSpeakTokenCount"] = parseInt(param.maxSpeakTokenCount);

        if (param.startErrorTokenCount === undefined) {
            // Choose a random value [0; maxErrorTokenCount[
            newGameOption["startErrorTokenCount"] = tools.getRandomInt(newGameOption.maxErrorTokenCount);
        }
        else {
            const proposedStartTokenCount = parseInt(param.startErrorTokenCount);
            if (proposedStartTokenCount >= newGameOption.maxErrorTokenCount) {
                throw new Error(`'startErrorTokenCount' is bigger than 'maxErrorTokenCount' (${proposedStartTokenCount} >= ${newGameOption.maxErrorTokenCount})`);
            }
            if (proposedStartTokenCount < 0) {
                throw new Error(`'startErrorTokenCount' should be positive or null`);
            }
            newGameOption["startErrorTokenCount"] = proposedStartTokenCount;
        }

        if (param.startSpeakTokenCount === undefined) {
            // Choose a random value [0; maxSpeakTokenCount]
            newGameOption["startSpeakTokenCount"] = tools.getRandomInt(newGameOption.maxSpeakTokenCount + 1);
        }
        else {
            const proposedStartTokenCount = parseInt(param.startSpeakTokenCount);
            if (proposedStartTokenCount > newGameOption.maxSpeakTokenCount) {
                throw new Error(`'startSpeakTokenCount' is bigger than 'maxSpeakTokenCount' (${proposedStartTokenCount} > ${newGameOption.maxSpeakTokenCount})`);
            }
            if (proposedStartTokenCount < 0) {
                throw new Error(`'startSpeakTokenCount' should be positive or null`);
            }
            newGameOption["startSpeakTokenCount"] = proposedStartTokenCount;
        }

        if (!param.colorOption) {
            throw new Error("'colorOption' is missing");
        }
        if (!Array.isArray(param.colorOption)) {
            throw new Error("'colorOption' is not an array");
        }
        for (const color of param.colorOption) {
            if (color.isAscending === undefined) {
                throw new Error(`'isAscending' is missing from colorOption.${color.color}`);
            }
            if (color.reactsTo === undefined) {
                throw new Error(`'reactsTo' is missing from colorOption.${color.color}`);
            }
        }
        newGameOption["colorOption"] = param.colorOption;

        if (param.callableColors === undefined) {
            throw new Error("'callableColors' is missing");
        }
        if (!Array.isArray(param.callableColors)) {
            throw new Error("'callableColors' should be an array");
        }
        newGameOption["callableColors"] = param.callableColors;
    }

    newGameOption.stack = generateStackFromColorOption(newGameOption.colorOption);
    newGameOption.maxScore = getMaxScoreFromColorOption(newGameOption.colorOption);
    const { minCardValue, maxCardValue } = getCardValueFromColorOption(newGameOption.colorOption);
    newGameOption.minCardValue = minCardValue;
    newGameOption.maxCardValue = maxCardValue;
    newGameOption.timestamp = new Date(Date.now());

    return newGameOption;
}


const createRandomGameOption = (req, res, next) => {

    try {

        const newGameOption = gameOptionFromDistribution(req.body);

        gameOptionModel.create(newGameOption).then(result => {

            let returnedValue = {
                gameOptionId: result._id,
            }

            return res.status(201).json(returnedValue);

        }).catch(err => {
            next(err);
        });

    }
    catch (err) {
        next(err);
    }
}


const getGameOption = async (req, res, next) => {

    try {
        const pageNumber = parseInt((req.query.page !== undefined) ? req.query.page : 0);
        const limit = parseInt((req.query.limit !== undefined) ? req.query.limit : constants.paginationMaxDocumentPerPage);
        let queryParams = "";
        for (const [key, value] of Object.entries(req.query)) {
            if (key !== "limit" && key !== "page") {
                queryParams += `&${key}=${value}`;
            }
        }
        const baseUrl = req.originalUrl.split("?")[0] + `?limit=${limit}` + queryParams;

        let pipeline = constants.paginationPipeline(pageNumber, limit, baseUrl);

        // Add projection
        pipeline.unshift({
            $project: {
                _id: 0,
                __v: 0,
            }
        });
        pipeline.unshift({
            $addFields: {
                gameOptionId: "$_id"
            }
        });

        // Add sorting
        const sort = gameOptionSort(req.query);
        if (Object.keys(sort).length != 0) {
            pipeline.unshift({ $sort: sort });
        }

        // Add filtering
        const filter = gameOptionFilter(req.query);
        if (Object.keys(filter).length != 0){
            pipeline.unshift({ $match: filter });
        }

        gameOptionModel.aggregate(pipeline)
            .then(result => {

                if (result.length == 1) {

                    if (result[0].error) {
                        if (result[0].error.type === "currentPageOutOfRange") {
                            throw new errors.PageNumberOutOfRangeError()
                        }
                    }

                    const returnedResult = {
                        metadata: result[0].metadata,
                        data: result[0].data,
                    }
                    return res.status(200).json(returnedResult);
                }
                throw new Error("Unknown aggregation error");
            })
            .catch(err => {
                next(err);
            });
    }
    catch (err) {
        next(err);
    }
};



const replaceTags = (req, res, next) => {

    try {

        const filter = {
            _id: req.params.gameOptionId,
        };

        const tags = req.body.tags;
        if (tags === undefined)
            throw new errors.MissingArgumentError('request.body.tags');

        const update = {
            $set: {
                tags: tags
            }
        };

        const options = {
            new: true,
            upsert: false,
        }

        gameOptionModel.findOneAndUpdate(filter, update, options).then(updatedDocument => {

            if (!updatedDocument)
                throw new errors.NotFoundError("gameOption", `${filter.gameOptionId}`);

            return res.status(200).send();

        }).catch(next);
    }
    catch (err) {
        next(err);
    }
}


const appendTags = (req, res, next) => {

    try {
        const filter = {
            _id: req.params.gameOptionId,
        };

        const tags = req.body.tags;
        if (tags === undefined)
            throw new errors.MissingArgumentError('request.body.tags');

        const update = {
            $addToSet: {
                tags: {
                    $each: tags
                }
            }
        };

        const options = {
            new: true,
            upsert: false,
        };

        gameOptionModel.findOneAndUpdate(filter, update, options).then(updatedDocument => {

            if (!updatedDocument)
                throw new errors.NotFoundError("gameOption", filter._id);

            return res.status(200).send();

        }).catch(next);
    }
    catch (err) {
        next(err);
    }
}


const deleteGameOptionById = (req, res, next) => {

    try {
        const filter = {
            _id: req.params.gameOptionId
        };

        gameOptionModel.findOneAndDelete(filter).then(deletedDocument => {
            if (deletedDocument) {

                deletedDocument.__v = undefined;
                deletedDocument.gameOptionId = deletedDocument._id;
                deletedDocument._id = undefined;
                // TODO check if that mecanic is working

                res.status(200).send();

                // Notification
                notifyDeletion(deletedDocument, "gameOption");
            }
            else {

                res.status(204).send();
            }
        }).catch(err => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};


module.exports = {
    reserveGameOptionFromPreset,
    reserveGameOptionFromParam,
    reserveGameOptionById,
    createGameOptionFromStack,
    createRandomGameOption,
    getGameOption,
    deleteGameOptionById,
    gameOptionFilter,
    replaceTags,
    appendTags,
}