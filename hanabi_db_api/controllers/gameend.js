const constants = require('../constants');
const errors = require('../middleware/errors');
const gameEndModel = require('../model/gameend');
const subscription = require('./subscription');

const notifyInsertion = subscription.notifyInsertion;
const notifyDeletion = subscription.notifyDeletion;


const gameEndFilter = (query => {

    let filter = {};
    if ("gameUuid" in query) {
        filter.gameUuid = query.gameUuid;
    }

    let gameStartTimestamp = undefined;
    if ("gameStartAfter" in query) {
        if (gameStartTimestamp === undefined) {
            gameStartTimestamp = {};
        }
        gameStartTimestamp["$gte"] = new Date(query.gameStartAfter);
    }
    if ("gameStartBefore" in query) {
        if (gameStartTimestamp === undefined) {
            gameStartTimestamp = {};
        }
        gameStartTimestamp["$lte"] = new Date(query.gameStartBefore);
    }
    if (gameStartTimestamp !== undefined) {
        filter.gameStartTimestamp = gameStartTimestamp;
    }

    let gameEndTimestamp = undefined;
    if ("gameEndAfter" in query) {
        if (gameEndTimestamp === undefined) {
            gameEndTimestamp = {};
        }
        gameEndTimestamp["$gte"] = new Date(query.gameEndAfter);
    }
    if ("gameEndBefore" in query) {
        if (gameEndTimestamp === undefined) {
            gameEndTimestamp = {};
        }
        gameEndTimestamp["$lte"] = new Date(query.gameEndBefore);
    }
    if (gameEndTimestamp !== undefined) {
        filter.gameEndTimestamp = gameEndTimestamp;
    }

    let gameDuration = undefined;
    if ("minGameDuration" in query) {
        if (gameDuration === undefined) {
            gameDuration = {};
        }
        gameDuration["$gte"] = parseFloat(query.minGameDuration);
    }
    if ("maxGameDuration" in query) {
        if (gameDuration === undefined) {
            gameDuration = {};
        }
        gameDuration["$lte"] = parseFloat(query.maxGameDuration);
    }
    if (gameDuration !== undefined) {
        filter.gameDuration = gameDuration;
    }

    let elapsedPlayerTurnCount = undefined;
    if ("elapsedPlayerTurnCount" in query) {
        if (elapsedPlayerTurnCount === undefined) {
            elapsedPlayerTurnCount = {};
        }
        elapsedPlayerTurnCount["$eq"] = parseInt(query.elapsedPlayerTurnCount);
    }
    if ("minElapsedPlayerTurnCount" in query) {
        if (elapsedPlayerTurnCount === undefined) {
            elapsedPlayerTurnCount = {};
        }
        elapsedPlayerTurnCount["$gte"] = parseInt(query.minElapsedPlayerTurnCount);
    }
    if ("maxElapsedPlayerTurnCount" in query) {
        if (elapsedPlayerTurnCount === undefined) {
            elapsedPlayerTurnCount = {};
        }
        elapsedPlayerTurnCount["$lte"] = parseInt(query.maxElapsedPlayerTurnCount);
    }
    if (elapsedPlayerTurnCount !== undefined) {
        filter.elapsedPlayerTurnCount = elapsedPlayerTurnCount;
    }

    let elapsedGameTurnCount = undefined;
    if ("elapsedGameTurnCount" in query) {
        if (elapsedGameTurnCount === undefined) {
            elapsedGameTurnCount = {};
        }
        elapsedGameTurnCount["$eq"] = parseInt(query.elapsedGameTurnCount);
    }
    if ("minElapsedGameTurnCount" in query) {
        if (elapsedGameTurnCount === undefined) {
            elapsedGameTurnCount = {};
        }
        elapsedGameTurnCount["$gte"] = parseInt(query.minElapsedGameTurnCount);
    }
    if ("maxElapsedGameTurnCount" in query) {
        if (elapsedGameTurnCount === undefined) {
            elapsedGameTurnCount = {};
        }
        elapsedGameTurnCount["$lte"] = parseInt(query.maxElapsedGameTurnCount);
    }
    if (elapsedGameTurnCount !== undefined) {
        filter.elapsedGameTurnCount = elapsedGameTurnCount;
    }

    if ("errorTokenCount" in query) {
        filter.errorTokenCount = parseInt(query.errorTokenCount);
    }

    if ("maxErrorTokenCount" in query) {
        filter.maxErrorTokenCount = parseInt(query.maxErrorTokenCount);
    }

    if ("victory" in query) {
        filter.victory = (query.victory === 'true');
    }

    if ("score" in query) {
        filter.score = parseInt(query.score);
    }

    if ("maxScore" in query) {
        filter.maxScore = parseInt(query.maxScore);
    }

    return filter;
});

const gameEndSort = (query) => {

    let sort = {};
    let errorArray = [];
    const sortItem = [
        "gameUuid",
        "gameStartTimestamp",
        "gameEndTimestamp",
        "gameDuration",
        "elapsedPlayerTurnCount",
        "elapsedGameTurnCount",
        "errorTokenCount",
        "maxErrorTokenCount",
        "victory",
        "score",
        "maxScore",
    ]

    if ("sort_by" in query) {

        query.sort_by.split(",").forEach((item) => {
            try {

                const regex = /^(?<key>[a-zA-Z0-9]*)((?<asc>\[asc\])?|(?<desc>\[desc\])?)$/gm;
                const result = regex.exec(item);
                if (result) {

                    const key = result.groups.key;

                    if (!sortItem.includes(key))
                        throw new Error(`Unable to sort on '${key}'`);

                    let order = 1;
                    if (result.groups.desc)
                        order = -1;

                    sort[key] = order;
                }
                else
                    throw new Error(`Unable to parse the argument '${item}'`);
            }
            catch (err) {
                errorArray.push(err);
            }
        });

        if (errorArray.length != 0) {
            // TODO change this behaviour
            console.error(`There are ${errorArray.length} error(s) while parsing 'sort_by' argument`);
            throw errorArray[0];
        }
    }
    else {
        // Default sorting
        sort.gameStartTimestamp = -1;
    }

    return sort;
}

const computeTotalDuration = (gameEnd) => {

    // Loop on 'playerId'
    for (const playerData of gameEnd.statistics) {
        playerData.totalDuration = 0;
        playerData.totalDuration += playerData.dropDuration;
        playerData.totalDuration += playerData.placeDuration;
        playerData.totalDuration += playerData.speakDuration;
    }
}

const createGameEnd = ((req, res, next) => {

    try {
        let gameEndData = req.body;

        computeTotalDuration(gameEndData);

        const newGameEnd = new gameEndModel(gameEndData);

        newGameEnd.save().then(createdDocument => {

            delete createdDocument._id;
            delete createdDocument.__v;

            res.status(201).send();

            // Notification
            notifyInsertion(createdDocument, "gameEnd");

        }).catch(err => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
})


const getGameEnd = ((req, res, next) => {

    try {
        const pageNumber = parseInt((req.query.page !== undefined) ? req.query.page : 0);
        const limit = parseInt((req.query.limit !== undefined) ? req.query.limit : constants.paginationMaxDocumentPerPage);
        let queryParams = "";
        for (const [key, value] of Object.entries(req.query)) {
            if (key !== "limit" && key !== "page") {
                queryParams += `&${key}=${value}`;
            }
        }
        const baseUrl = req.originalUrl.split("?")[0] + `?limit=${limit}` + queryParams;

        let pipeline = constants.paginationPipeline(pageNumber, limit, baseUrl);

        // Add projection
        pipeline.unshift({
            $project: {
                _id: 0,
                __v: 0,
            }
        });

        // Add sorting
        const sort = gameEndSort(req.query);
        pipeline.unshift({ $sort: sort });

        // Add filtering
        const filter = gameEndFilter(req.query);
        pipeline.unshift({ $match: filter });

        gameEndModel.aggregate(pipeline)
            .then(result => {

                if (result.length == 1) {

                    if (result[0].error) {
                        if (result[0].error.type === "currentPageOutOfRange") {
                            throw new errors.PageNumberOutOfRangeError()
                        }
                    }

                    const returnedResult = {
                        metadata: result[0].metadata,
                        data: result[0].data,
                    }
                    return res.status(200).json(returnedResult);
                }
                throw new Error("Unknown aggregation error");
            })
            .catch(err => {
                next(err);
            });
    }
    catch (err) {
        next(err);
    }
})


const deleteGameEndByGameUuid = ((req, res, next) => {

    try {
        const filter = { gameUuid: req.params.gameUuid };
        gameEndModel.findOneAndDelete(filter).then(deletedDocument => {
            if (deletedDocument) {
                res.status(200).send();

                // Notification
                notifyDeletion(deletedDocument, "gameEnd");
            }
            else {
                res.status(204).send();
            }
        }).catch(err => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
})


module.exports = {
    createGameEnd,
    getGameEnd,
    deleteGameEndByGameUuid,
    gameEndFilter,
}