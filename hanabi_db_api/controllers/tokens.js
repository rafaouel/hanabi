const errors = require('../middleware/errors');
const tools = require('./tools');
const user_model = require('../model/users');


const check_token = (req, res, next) => {

    try {
        if (res.locals.token === undefined || res.locals.token.payload === undefined) {
            throw new errors.TokenPayloadError();
        }
        else {
            return res.status(200).json(res.locals.token.payload);
        }
    }
    catch (err) {
        next(err);
    }
}

const ask_token = (req, res, next) => {

    try {
        const filter = {
            login: res.locals.user.login
        };

        // Look for the user in the database
        user_model.findOne(filter).then(data => {

            // Check if the user exists
            if (data == null) {
                throw new errors.UserNotFoundError();
            }

            // Check if the user is enabled
            if (!data.enabled) {
                throw new errors.DisabledUserError();
            }

            // Check the password
            if (tools.isPasswordValid(res.locals.user.password, data.encryptedPassword)) {

                // Prepare an access token
                const accessToken = tools.generateAccessToken(data.login, data.accessTokenTTL);
                const accessTokenExpirationDate = new Date(Date.now() + data.accessTokenTTL * 1000);

                // Prepare a refresh token
                const refreshToken = tools.generateRefreshToken(data.login, data.refreshTokenTTL);
                const refreshTokenExpirationDate = new Date(Date.now() + data.refreshTokenTTL * 1000);

                const response = {
                    accessToken,
                    accessTokenExpirationDate,
                    refreshToken,
                    refreshTokenExpirationDate
                }

                // Send the response
                return res.status(201).json(response);
            }

            // The password is invalid 
            throw new errors.InvalidPasswordError();

        }).catch(err => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
}

const refresh_token = (req, res, next) => {

    try {
        if (res.locals.token === undefined || res.locals.token.payload === undefined) {
            throw new errors.TokenPayloadError();
        }

        const payload = res.locals.token.payload;

        const filter = {
            login: res.locals.token.payload.login
        }

        user_model.findOne(filter).then(data => {

            // Check if the user is still enabled
            if (data.enabled) {
                // Prepare an access token
                const accessToken = tools.generateAccessToken(data.login, data.accessTokenTTL);
                const accessTokenExpirationDate = new Date(Date.now() + data.accessTokenTTL * 1000);

                // Prepare a refresh token
                const refreshToken = tools.generateRefreshToken(data.login, data.refreshTokenTTL);
                const refreshTokenExpirationDate = new Date(Date.now() + data.refreshTokenTTL * 1000);

                const response = {
                    accessToken,
                    accessTokenExpirationDate,
                    refreshToken,
                    refreshTokenExpirationDate
                }

                // Send the response
                return res.status(201).json(response);
            }

            // The user is not enabled
            throw new errors.DisabledUserError();

        }).catch(error => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
}

module.exports = {
    check_token,
    ask_token,
    refresh_token
}