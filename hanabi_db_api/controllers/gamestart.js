const constants = require('../constants');
const errors = require('../middleware/errors');
const gameStartModel = require('../model/gamestart');
const mongoose = require('mongoose');
const subscription = require('./subscription');

const notifyInsertion = subscription.notifyInsertion;
const notifyUpdate = subscription.notifyUpdate;
const notifyDeletion = subscription.notifyDeletion;


const gameStartFilter = (query => {

    let filter = {};
    if ("gameUuid" in query) {
        filter.gameUuid = query.gameUuid;
    }
    if ("gameOptionId" in query) {
        filter.gameOptionId = new mongoose.Types.ObjectId(query.gameOptionId);
    }

    let timestamp = undefined;
    if ("after" in query) {
        if (timestamp === undefined) {
            timestamp = {};
        }
        timestamp["$gte"] = new Date(query.after);
    }
    if ("before" in query) {
        if (timestamp === undefined) {
            timestamp = {};
        }
        timestamp["$lte"] = new Date(query.before);
    }
    if (timestamp !== undefined) {
        filter.timestamp = timestamp;
    }

    if ("tags" in query) {
        filter.tags = {
            $all: [query.tags]
        }
    }

    return filter;
})


const createGameStart = (req, res, next) => {

    try {
        const newGameStart = req.body;

        gameStartModel.create(newGameStart).then(createdDocument => {

            createdDocument._id = undefined;
            createdDocument.__v = undefined;

            res.status(201).send();

            // Notification
            notifyInsertion(createdDocument, "gameStart");

        }).catch(err => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
}

const getGameStart = (req, res, next) => {

    try {
        const pageNumber = parseInt((req.query.page !== undefined) ? req.query.page : 0);
        const limit = parseInt((req.query.limit !== undefined) ? req.query.limit : constants.paginationMaxDocumentPerPage);
        let queryParams = "";
        for (const [key, value] of Object.entries(req.query)) {
            if (key !== "limit" && key !== "page") {
                queryParams += `&${key}=${value}`;
            }
        }
        const baseUrl = req.originalUrl.split("?")[0] + `?limit=${limit}` + queryParams;

        let pipeline = constants.paginationPipeline(pageNumber, limit, baseUrl);

        // Add projection
        pipeline.unshift({
            $project: {
                _id: 0,
                __v: 0,
            }
        });

        // Add sorting
        pipeline.unshift({ $sort: { timestamp: -1 } });

        // Add filtering
        const filter = gameStartFilter(req.query);
        pipeline.unshift({ $match: filter, });

        gameStartModel.aggregate(pipeline)
            .then(result => {

                if (result.length == 1) {

                    if (result[0].error) {
                        if (result[0].error.type === "currentPageOutOfRange") {
                            throw new errors.PageNumberOutOfRangeError()
                        }
                    }

                    const returnedResult = {
                        metadata: result[0].metadata,
                        data: result[0].data,
                    }
                    return res.status(200).json(returnedResult);
                }
                throw new Error("Unknown aggregation error");
            })
            .catch(err => {
                next(err);
            });
    }
    catch (err) {
        next(err);
    }
}

const replaceTags = (req, res, next) => {

    try {
        const filter = {
            gameUuid: req.params.gameUuid
        };

        const tags = req.body.tags;
        if (tags === undefined)
            throw new errors.MissingArgumentError('request.body.tags');

        const update = {
            $set: {
                tags: tags
            }
        };

        const options = {
            new: true,
            upsert: false,
        }

        gameStartModel.findOneAndUpdate(filter, update, options).then(updatedDocument => {

            if (updatedDocument) {

                updatedDocument._id = undefined;
                updatedDocument.__v = undefined;

                res.status(200).send();

                // Notification
                notifyUpdate(updatedDocument, "gameStart");
            }
            else {

                res.status(404).send();
            }
        }).catch(err => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
}

const appendTags = (req, res, next) => {

    try {
        const filter = {
            gameUuid: req.params.gameUuid
        };

        const tags = req.body.tags;
        if (tags === undefined)
            throw new errors.MissingArgumentError('request.body.tags');

        const update = {
            $addToSet: {
                tags: {
                    $each: tags
                }
            }
        };

        const options = {
            new: true,
            upsert: false,
        };

        gameStartModel.findOneAndUpdate(filter, update, options).then(updatedDocument => {

            if (updatedDocument) {

                updatedDocument._id = undefined;
                updatedDocument.__v = undefined;

                res.status(200).send();

                // Notification
                notifyUpdate(updatedDocument, "gameStart");
            }
            else {

                res.status(404).send();
            }
        }).catch(err => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
}

const deleteGameStartByGameUuid = (req, res, next) => {

    try {
        const filter = {
            gameUuid: req.params.gameUuid
        };

        gameStartModel.findOneAndDelete(filter).then(deletedDocument => {

            if (deletedDocument) {

                deletedDocument._id = undefined;
                deletedDocument.__v = undefined;

                res.status(200).send();

                // Notification
                notifyDeletion(deletedDocument, "gameStart");
            }
            else {

                res.status(404).send(deletedDocument);
            }
        }).catch(err => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
}

module.exports = {
    createGameStart,
    getGameStart,
    replaceTags,
    appendTags,
    deleteGameStartByGameUuid,
    gameStartFilter,
}