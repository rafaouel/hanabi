const constants = require('../constants');
const errors = require('../middleware/errors');
const peerInfoModel = require('../model/peerinfo');
const subscription = require('./subscription');

const notifyInsertion = subscription.notifyInsertion;
const notifyDeletion = subscription.notifyDeletion;


const peerInfoFilter = query => {

    let filter = {};

    if ("gameUuid" in query) {
        filter.gameUuid = query.gameUuid;
    }
    if ("playerId" in query) {
        filter.playerId = parseInt(query.playerId);
    }
    if ("name" in query) {
        filter.name = query.name;
    }
    if ("ip" in query) {
        filter.ip = query.ip;
    }
    if ("version" in query) {
        filter.version = query.version;
    }
    if ("debug" in query) {
        filter.debug = (query.debug === 'true');
    }
    if ("type" in query) {
        filter.type = query.type;
    }
    return filter;
};

const peerInfoSort = query => {

    let sort = {};
    let errorArray = [];
    const sortItem = [
        "gameUuid",
        "playerId",
        "name",
        "ip",
        "version",
        "debug",
        "type",
    ]
    if ("sort_by" in query) {
            
        query.sort_by.split(",").forEach((item) => {
            try {

                const regex = /^(?<key>[a-zA-Z0-9]*)((?<asc>\[asc\])?|(?<desc>\[desc\])?)$/gm;
                const result = regex.exec(item);
                if (result) {

                    const key = result.groups.key;

                    if (!sortItem.includes(key))
                        throw new Error(`Unable to sort on '${key}'`);

                    let order = 1;
                    if (result.groups.desc)
                        order = -1;

                    sort[key] = order;
                }
                else
                    throw new Error(`Unable to parse the argument '${item}'`);
            }
            catch (err) {
                errorArray.push(err);
            }
        });

        if (errorArray.length != 0) {
            // TODO change this behaviour
            console.error(`There are ${errorArray.length} error(s) while parsing 'sort_by' argument`);
            throw errorArray[0];
        }
    }
    else {
        // Default sorting
        sort.gameUuid = 1;
        sort.playerTurnCount = 1;
    }

    return sort;
}


const createPeerInfo = (req, res, next) => {

    try {
        const newPeerInfo = new peerInfoModel(req.body);
        newPeerInfo.save().then(createdDocument => {

            createdDocument._id = undefined;
            createdDocument.__v = undefined;

            res.status(201).send();

            // Notification
            notifyInsertion(createdDocument, "peerInfo");

        }).catch(err => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};


const getPeerInfo = ((req, res, next) => {

    try {
        const pageNumber = parseInt((req.query.page !== undefined) ? req.query.page : 0);
        const limit = parseInt((req.query.limit !== undefined) ? req.query.limit : constants.paginationMaxDocumentPerPage);
        let queryParams = "";
        for (const [key, value] of Object.entries(req.query)) {
            if (key !== "limit" && key !== "page") {
                queryParams += `&${key}=${value}`;
            }
        }
        const baseUrl = req.originalUrl.split("?")[0] + `?limit=${limit}` + queryParams;

        let pipeline = constants.paginationPipeline(pageNumber, limit, baseUrl);

        // Add projection
        pipeline.unshift({
            $project: {
                _id: 0,
                __v: 0,
            }
        });

        // Add sorting
        const sort = peerInfoSort(req.query);
        pipeline.unshift({ $sort: sort });

        // Add filtering
        const filter = peerInfoFilter(req.query);
        pipeline.unshift({ $match: filter, });

        peerInfoModel.aggregate(pipeline)
            .then(result => {

                if (result.length == 1) {

                    if (result[0].error) {
                        if (result[0].error.type === "currentPageOutOfRange") {
                            throw new errors.PageNumberOutOfRangeError()
                        }
                    }

                    const returnedResult = {
                        metadata: result[0].metadata,
                        data: result[0].data,
                    }
                    return res.status(200).json(returnedResult);
                }
                throw new Error("Unknown aggregation error");
            })
            .catch(err => {
                next(err);
            });
    }
    catch (err) {
        next(err);
    }
})


const updatePeerInfo = (req, res, next) => {

    try {

        const filter = {
            gameUuid: req.params.gameUuid,
            playerId: req.params.playerId,
        }

        const update = req.body;

        const options = {
            runValidator: true,
            new: true,
            upsert: false,
            includeResultMetadata: true,
        }

        peerInfoModel.findOneAndUpdate(filter, update, options).then(updateResult => {

            if (!updateResult.lastErrorObject.updatedExisting)
                throw new errors.NotFoundError("peerInfo", `(gameUuid=${filter.gameUuid} - playerId=${filter.playerId})`);

            return res.status(200).send();

        }).catch(next);
    }
    catch (err) {
        next(err);
    }
}


const deletePeerInfoByGameUuid = ((req, res, next) => {

    try {
        const filter = { gameUuid: req.params.gameUuid };
        peerInfoModel.find(filter).then(peerInfoArray => {

            var deletedGameUuid = [];
            var processedPeerInfo = 0;

            if (peerInfoArray.length > 0) {

                peerInfoArray.forEach(peerInfo => {

                    peerInfoModel.deleteOne(peerInfo._id).then(summary => {

                        processedPeerInfo += 1;

                        if (summary.deletedCount == 1) {

                            deletedGameUuid.push({
                                gameUuid: peerInfo.gameUuid,
                                playerId: peerInfo.playerId
                            });

                            // Notification
                            notifyDeletion(peerInfo, "peerInfo");
                        }

                        if (processedPeerInfo == peerInfoArray.length) {

                            if (deletedGameUuid.length == peerInfoArray.length) {
                                return res.status(200).send(deletedGameUuid);
                            }
                            else {
                                return res.status(400).send(deletedGameUuid);
                            }
                        }

                    }).catch(err => {
                        processedPeerInfo += 1;

                        console.log("Unable to delete peer info '" + peerInfo.gameUuid + "' / '" + peerInfo.playerId + "'");
                        console.log(err);
                    });
                });

            }
            else {
                return res.status(404).send();
            }

        }).catch(err => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
});


module.exports = {
    peerInfoFilter,
    createPeerInfo,
    getPeerInfo,
    updatePeerInfo,
    deletePeerInfoByGameUuid,
}