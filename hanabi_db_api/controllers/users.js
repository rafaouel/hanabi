const errors = require('../middleware/errors');
const user_model = require('../model/users');
const tools = require('./tools');
const constants = require('../constants');


const create_user = (req, res, next) => {

    try {
        const encryptedPassword = tools.encryptPassword(req.body.password);

        const user = {
            login: req.body.login,
            encryptedPassword,
            accessTokenTTL: req.body.accessTokenTTL ? req.body.accessTokenTTL : constants.defaultAccessTokenTTL,
            refreshTokenTTL: req.body.refreshTokenTTL ? req.body.refreshTokenTTL : constants.defaultRefreshTokenTTL,
            description: req.body.description,
            enabled: req.body.enabled !== undefined ? req.body.enabled : false,
        };

        if (user.login == "admin") {
            throw new errors.AdminOverwriteError();
        }

        user_model.create(user).then(data => {
            return res.status(201).send();
        }).catch(error => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
}

const get_user = (req, res, next) => {

    try {
        let filter = {};

        (req.query.login !== undefined) && (filter.login = req.query.login);

        user_model.find(filter, { _id: 0, __v: 0, encryptedPassword: 0 }).then(data => {
            return res.status(200).json(data);
        }).catch(error => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
}

const delete_user_by_login = (req, res, next) => {

    try {
        const filter = {
            login: req.params.login
        };

        user_model.deleteOne(filter).then(data => {
            if (data.deletedCount === 0) {
                return res.status(204).send();
            }
            return res.status(200).send();
        }).catch(err => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
}

const update_user_by_login = (req, res, next) => {

    try {
        const filter = {
            login: req.params.login
        };

        const encryptedPassword = (req.body.password !== undefined) ? tools.encryptPassword(req.body.password) : undefined;

        const updateToApply = {
            encryptedPassword,
            accessTokenTTL: req.body.accessTokenTTL ? req.body.accessTokenTTL : constants.defaultAccessTokenTTL,
            refreshTokenTTL: req.body.refreshTokenTTL ? req.body.refreshTokenTTL : constants.defaultRefreshTokenTTL,
            description: req.body.description,
            enabled: req.body.enabled !== undefined ? req.body.enabled : false,
        }

        user_model.updateOne(filter, updateToApply).then(data => {
            if (data.modifiedCount === 0) {
                return res.status(204).send();
            }
            return res.status(200).send();
        }).catch(error => {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
}

module.exports = {
    create_user,
    get_user,
    delete_user_by_login,
    update_user_by_login
}