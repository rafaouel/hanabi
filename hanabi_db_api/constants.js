const fs = require('fs');

try {

    ///////////////
    // DATA BASE //
    exports.dbHost = process.env.DB_HOST || "127.0.0.1";
    exports.dbPort = process.env.DB_PORT || 27017;
    exports.dbName = process.env.DB_NAME || "hanabi";

    if (process.env.DB_USERNAME === undefined)
        throw new Error("Please define 'DB_USERNAME' environment variable");    
    exports.dbLogin = process.env.DB_USERNAME;

    if (process.env.DB_PASSWORD === undefined)
        throw new Error("Please define 'DB_PASSWORD' environment variable");
    exports.dbPassword = process.env.DB_PASSWORD;

    exports.dbUri = `mongodb://${this.dbLogin}:${this.dbPassword}@${this.dbHost}:${this.dbPort}/${this.dbName}`;
    exports.dbMaxConnectionTentatives = 10;
    exports.dbConnectionDelayMS = 2000;
    exports.dbConnectionOptions = {
        serverSelectionTimeoutMS: 3000,
    }
    

    //////////////////
    // SUBSCRIPTION //
    exports.subscriptionExpireAfterSeconds = 3600;
    exports.subscriptionRemoveAfterError = false;


    ////////////////////
    // AUTHENTICATION //
    exports.defaultRefreshTokenTTL = process.env.defaultRefreshTokenTTL || 86400;
    exports.defaultAccessTokenTTL = process.env.defaultAccessTokenTTL || 3600;
    exports.saltRounds = 10;

    const apiPrivateKeyFilePath = process.env.API_KEY_PATH;
    if (apiPrivateKeyFilePath === undefined)
        throw new Error("Please define 'API_KEY_PATH' environment variable");
    exports.apiPrivateKey = fs.readFileSync(apiPrivateKeyFilePath, 'utf8');

    if (process.env.API_ADMIN_PASSWORD === undefined)
        throw new Error("Please define 'API_ADMIN_PASSWORD' environment variable");
    exports.apiAdminPassword = process.env.API_ADMIN_PASSWORD;
    exports.accessTokenType = "Access";
    exports.refreshTokenType = "Refresh";


    //////////////////
    // API SETTINGS //
    exports.apiPort = process.env.API_PORT || 3000;
    exports.apiVersionArray = [2, 11, 2];
    exports.apiVersionString = this.apiVersionArray.toString().replaceAll(',', '.');


    ////////////////
    // PAGINATION //
    exports.paginationMaxDocumentPerPage = 50;
    exports.paginationPipeline = (pageNumber, limit, baseUrl) => {
        return [
            {
                $facet: {
                    data: [
                        {
                            $skip: pageNumber * limit
                        },
                        {
                            $limit: limit
                        },
                    ],
                    count: [
                        {
                            $count: 'total',
                        }
                    ]
                }
            },
            {
                $project: {
                    "metadata.documentCount": {
                        $ifNull: [
                            { $first: "$count.total" },
                            0
                        ]
                    },
                    data: 1,
                }
            },
            {
                $addFields: {
                    "metadata.maxDocumentPerPage": limit,
                    "metadata.currentPageNumber": pageNumber,
                    "metadata.numberOfPage": {
                        $ceil: {
                            $divide: [
                                "$metadata.documentCount",
                                limit
                            ]
                        }
                    },
                    "metadata.firstPage": {
                        $cond: {
                            if: {
                                $gt: ["$metadata.documentCount", 0]
                            },
                            then: {
                                $concat: [
                                    baseUrl,
                                    "&page=0"
                                ]
                            },
                            else: undefined
                        }
                    },
                }
            },
            {
                $addFields: {
                    "error": {
                        $cond: {
                            if: {
                                $gte: ["$metadata.currentPageNumber", "$metadata.numberOfPage"]
                            },
                            then: {
                                type: "currentPageOutOfRange",
                            },
                            else: undefined
                        }
                    },
                    "metadata.previousPage": {
                        $cond: {
                            if: { $gt: ["$metadata.documentCount", 0] },
                            then: {
                                $concat: [
                                    baseUrl,
                                    "&page=",
                                    {
                                        $toString: {
                                            $max: [
                                                0,
                                                { $add: ["$metadata.currentPageNumber", -1] }
                                            ]
                                        }
                                    }
                                ]
                            },
                            else: undefined,
                        }
                    },
                    "metadata.nextPage": {
                        $cond: {
                            if: { $gt: ["$metadata.documentCount", 0] },
                            then: {
                                $concat: [
                                    baseUrl,
                                    "&page=",
                                    {
                                        $toString: {
                                            $min: [
                                                { $add: ["$metadata.numberOfPage", -1] },
                                                { $add: ["$metadata.currentPageNumber", 1] }
                                            ]
                                        }
                                    }
                                ]
                            },
                            else: undefined,
                        }
                    },
                    "metadata.lastPage": {
                        $cond: {
                            if: { $gt: ["$metadata.documentCount", 0] },
                            then: {
                                $concat: [
                                    baseUrl,
                                    "&page=",
                                    {
                                        $toString: {
                                            $add: [
                                                "$metadata.numberOfPage",
                                                -1
                                            ]
                                        }
                                    }
                                ]
                            },
                            else: undefined
                        }
                    }
                },
            },
            {
                $project: {
                    "metadata.documentCount": 1,
                    "metadata.maxDocumentPerPage": 1,
                    "metadata.currentPageNumber": {
                        $cond: { if: { $eq: ["$metadata.documentCount", 0] }, then: "$false", else: "$metadata.currentPageNumber" }
                    },
                    "metadata.numberOfPage": {
                        $cond: { if: { $eq: ["$metadata.documentCount", 0] }, then: "$false", else: "$metadata.numberOfPage" }
                    },
                    "metadata.firstPage": { $ifNull: ["$metadata.firstPage", "$false"] },
                    "metadata.previousPage": { $ifNull: ["$metadata.previousPage", "$false"] },
                    "metadata.nextPage": { $ifNull: ["$metadata.nextPage", "$false"] },
                    "metadata.lastPage": { $ifNull: ["$metadata.lastPage", "$false"] },
                    "data": 1,
                }
            }
        ]
    }
}
catch (err) {
    console.error(err);
    process.exit(2);
}