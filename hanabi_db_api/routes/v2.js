const express = require('express');
const router = express.Router();

const auth = require('../middleware/authorization');


// Game data : game options
const {
    getGameOption,
    deleteGameOptionById,
    reserveGameOptionFromParam,
    reserveGameOptionFromPreset,
    reserveGameOptionById,
    createGameOptionFromStack,
    createRandomGameOption,
    replaceTags,
    appendTags,
} = require('../controllers/gameoption');
// Read
router.get('/hanabi/gameOption/', auth.bearerAuthorization, auth.isAccessToken, getGameOption);
// Update (on tags)
router.put('/hanabi/gameOption/gameOptionId/:gameOptionId', auth.bearerAuthorization, auth.isAccessToken, replaceTags);
router.patch('/hanabi/gameOption/gameOptionId/:gameOptionId', auth.bearerAuthorization, auth.isAccessToken, appendTags);
// Delete
router.delete('/hanabi/gameOption/gameOptionId/:gameOptionId', auth.bearerAuthorization, auth.isAccessToken, deleteGameOptionById);
// Reserve
router.patch('/hanabi/gameOption/reserveFromParam', auth.bearerAuthorization, auth.isAccessToken, reserveGameOptionFromParam);
router.patch('/hanabi/gameOption/reserveFromPreset', auth.bearerAuthorization, auth.isAccessToken, reserveGameOptionFromPreset);
router.patch('/hanabi/gameOption/gameOptionId/:gameOptionId/reserve', auth.bearerAuthorization, auth.isAccessToken, reserveGameOptionById);
// Create
router.post('/hanabi/gameOption', auth.bearerAuthorization, auth.isAccessToken, createGameOptionFromStack);
router.post('/hanabi/gameOption/random', auth.bearerAuthorization, auth.isAccessToken, createRandomGameOption);


// Game data : game
const {
    listGameUuidFromGameStart,
    listGameUuidFromPeerInfo,
    listGameUuidFromGameState,
    listGameUuidFromPlayerAction,
    listGameUuidFromGameEnd,
    gameOverview,
} = require('../controllers/game');
// List game UUID from game start
router.get('/hanabi/gameUuid/gameStart', auth.bearerAuthorization, auth.isAccessToken, listGameUuidFromGameStart);
// List game UUID from peer info
router.get('/hanabi/gameUuid/peerInfo', auth.bearerAuthorization, auth.isAccessToken, listGameUuidFromPeerInfo);
// List game UUDI from game state
router.get('/hanabi/gameUuid/gameState', auth.bearerAuthorization, auth.isAccessToken, listGameUuidFromGameState);
// List game UUDI from player action
router.get('/hanabi/gameUuid/playerAction', auth.bearerAuthorization, auth.isAccessToken, listGameUuidFromPlayerAction);
// List game UUDI from game end
router.get('/hanabi/gameUuid/gameEnd', auth.bearerAuthorization, auth.isAccessToken, listGameUuidFromGameEnd);
// Get game overview
router.get('/hanabi/game/overview', auth.bearerAuthorization, auth.isAccessToken, gameOverview)

module.exports = router;