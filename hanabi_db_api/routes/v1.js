const express = require('express');
const router = express.Router();

const auth = require('../middleware/authorization');


// Healthchecks
const {
    readiness_probe,
    liveness_probe,
    startup_probe
} = require('../controllers/healthcheck');
// Liveness probe
router.get('/healthcheck/liveness-probe/', liveness_probe);
// Readiness probe
router.get('/healthcheck/readiness-probe/', readiness_probe);
// Startup probe
router.get('/healthcheck/startup-probe/', startup_probe);


// Game data : game start
const {
    createGameStart,
    getGameStart,
    replaceTags,
    appendTags,
    deleteGameStartByGameUuid
} = require('../controllers/gamestart');
// Create
router.post('/hanabi/gameStart', auth.bearerAuthorization, auth.isAccessToken, createGameStart);
// Read
router.get('/hanabi/gameStart', auth.bearerAuthorization, auth.isAccessToken, getGameStart);
// Update (on tags)
router.put('/hanabi/gameStart/gameUuid/:gameUuid', auth.bearerAuthorization, auth.isAccessToken, replaceTags);
router.patch('/hanabi/gameStart/gameUuid/:gameUuid', auth.bearerAuthorization, auth.isAccessToken, appendTags);
// Delete
router.delete('/hanabi/gameStart/gameUuid/:gameUuid', auth.bearerAuthorization, auth.isAccessToken, deleteGameStartByGameUuid);


// Game data : Peer info
const {
    createPeerInfo,
    getPeerInfo,
    updatePeerInfo,
    deletePeerInfoByGameUuid,
} = require('../controllers/peerinfo');
// Create
router.post('/hanabi/peerInfo/', auth.bearerAuthorization, auth.isAccessToken, createPeerInfo);
// Read
router.get('/hanabi/peerInfo/', auth.bearerAuthorization, auth.isAccessToken, getPeerInfo);
// Update
router.patch('/hanabi/peerInfo/gameUuid/:gameUuid/playerId/:playerId', auth.bearerAuthorization, auth.isAccessToken, updatePeerInfo);
// Delete
router.delete('/hanabi/peerInfo/gameUuid/:gameUuid', auth.bearerAuthorization, auth.isAccessToken, deletePeerInfoByGameUuid);


// Game data : Game state
const {
    createGameState,
    getGameStates,
    deleteGameStateByGameUuid,
} = require('../controllers/gamestates');
// Create
router.post('/hanabi/gameState/', auth.bearerAuthorization, auth.isAccessToken, createGameState);
// Read
router.get('/hanabi/gameState/', auth.bearerAuthorization, auth.isAccessToken, getGameStates);
// Delete
router.delete('/hanabi/gameState/gameUuid/:gameUuid', auth.bearerAuthorization, auth.isAccessToken, deleteGameStateByGameUuid);


// Game data : Player actions
const {
    createPlayerAction,
    getPlayerAction,
    deletePlayerActionByGameUuid,
} = require('../controllers/playeraction');
// Create
router.post('/hanabi/playerAction/', auth.bearerAuthorization, auth.isAccessToken, createPlayerAction);
// Read
router.get('/hanabi/playerAction/', auth.bearerAuthorization, auth.isAccessToken, getPlayerAction);
// Delete
router.delete('/hanabi/playerAction/gameUuid/:gameUuid', auth.bearerAuthorization, auth.isAccessToken, deletePlayerActionByGameUuid);


// Game data : game end
const {
    createGameEnd,
    getGameEnd,
    deleteGameEndByGameUuid,
} = require('../controllers/gameend');
// Create
router.post('/hanabi/gameEnd/', auth.bearerAuthorization, auth.isAccessToken, createGameEnd);
// Read
router.get('/hanabi/gameEnd/', auth.bearerAuthorization, auth.isAccessToken, getGameEnd);
// Delete
router.delete('/hanabi/gameEnd/gameUuid/:gameUuid', auth.bearerAuthorization, auth.isAccessToken, deleteGameEndByGameUuid);


// Subscriptions
const {
    subscribe,
    unsubscribe,
    unsubscribeAll,
    getSubscription,
    updateSubscription
} = require('../controllers/subscription');
// Create
router.put('/subscription/', auth.bearerAuthorization, auth.isAccessToken, subscribe);
// Read
router.get('/subscription/', auth.basicAuthorization, auth.isAdmin, getSubscription);
// Update
router.patch('/subscription/_id/:_id', auth.basicAuthorization, auth.isAdmin, updateSubscription);
// Delete
router.delete('/subscription/_id/:_id', auth.basicAuthorization, auth.isAdmin, unsubscribe);
router.delete('/subscription/all/', auth.basicAuthorization, auth.isAdmin, unsubscribeAll);


// Tokens
const {
    check_token,
    ask_token,
    refresh_token
} = require('../controllers/tokens');
// Ask for a token
router.post('/tokens/', auth.basicAuthorization, ask_token);
// Refresh a token
router.put('/tokens/', auth.bearerAuthorization, auth.isRefreshToken, refresh_token);
// Check a token
router.get('/tokens/', auth.bearerAuthorization, check_token);


// Users
const {
    create_user,
    get_user,
    delete_user_by_login,
    update_user_by_login
} = require('../controllers/users');
// Create
router.post('/users/', auth.basicAuthorization, auth.isAdmin, create_user);
// Read
router.get('/users/', auth.basicAuthorization, auth.isAdmin, get_user);
// Update
router.patch('/user/login/:login', auth.basicAuthorization, auth.isAdmin, update_user_by_login);
// Delete
router.delete('/user/login/:login', auth.basicAuthorization, auth.isAdmin, delete_user_by_login);


module.exports = router;