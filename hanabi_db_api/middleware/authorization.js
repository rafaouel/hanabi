const constants = require('../constants');
const errors = require('./errors');
const tools = require('../controllers/tools');


const bearerAuthorization = (req, res, next) => {

    try {
        const authHeaderContent = req.headers["authorization"];
        if (authHeaderContent === undefined) {
            throw new errors.NoAuthorizationHeaderError();
        }

        const authContent = authHeaderContent.split(" ");

        if (authContent[0] !== "Bearer") {
            throw new errors.BadAuthorizationHeaderError();
        }

        const token = authContent[1];
        const tokenPayload = tools.extractFromToken(token);
        res.locals.token = {
            payload: tokenPayload
        }

        next();
    }
    catch (err) {
        next(err);
    }
}

const basicAuthorization = (req, res, next) => {

    try {
        const authHeaderContent = req.headers["authorization"];
        if (authHeaderContent === undefined) {
            throw new errors.NoAuthorizationHeaderError();
        }

        const authContent = authHeaderContent.split(" ");

        if (authContent[0] !== "Basic") {
            throw new errors.BadAuthorizationHeaderError();
        }

        const decodedString = Buffer.from(authContent[1], 'base64').toString().split(":");
        res.locals.user = {
            login: decodedString[0],
            password: decodedString[1]
        };

        next();
    }
    catch (err) {
        next(err);
    }
}

const isAdmin = (req, res, next) => {

    try {
        if (res.locals.user.login != "admin" || res.locals.user.password != constants.api_admin_password) {
            throw new errors.ForbiddenAccessError();
        }

        next();
    }
    catch (err) {
        next(err);
    }
}

const isAccessToken = (req, res, next) => {

    try {
        if (res.locals.token.payload.tokenType !== constants.accessTokenType) {
            throw new errors.InvalidTokenError();
        }
        next();
    }
    catch (err) {
        next(err);
    }
}

const isRefreshToken = (req, res, next) => {

    try {
        if (res.locals.token.payload.tokenType !== constants.refreshTokenType) {
            throw new errors.InvalidTokenError();
        }
        next();
    }
    catch (err) {
        next(err);
    }
}

module.exports = {
    isAdmin,
    isAccessToken,
    isRefreshToken,
    basicAuthorization,
    bearerAuthorization
}