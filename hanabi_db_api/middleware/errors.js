const mongoose = require('mongoose');
const mongodb = require('mongodb');


class APIError extends (Error) {

    constructor(message, status) {
        super(message);
        this.name = this.constructor.name;
        this.status = status;
        Error.captureStackTrace(this, this.constructor);
    }

    getReport() {
        return {
            errorType: this.name,
            errorMessage: this.message,
            status: this.status,
        }
    }
}

class Error400 extends (APIError) {
    constructor(message) {
        super(message, 400);
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
    }
}

class Error401 extends (APIError) {
    constructor(message) {
        super(message, 401);
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
    }
}

class Error403 extends (APIError) {
    constructor(message) {
        super(message, 403);
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
    }
}

class Error404 extends (APIError) {
    constructor(message) {
        super(message, 404);
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
    }
}

class Error500 extends (APIError) {
    constructor(message) {
        super(message, 500);
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
    }
}


class TokenPayloadError extends (Error500) {
    constructor() {
        super("Unable to extract the token payload");
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
    }
}


class InvalidPasswordError extends (Error401) {
    constructor() {
        super("Invalid password");
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
    }
}


class DisabledUserError extends (Error401) {
    constructor() {
        super("The user is disabled");
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
    }
}


class AdminOverwriteError extends (Error400) {
    constructor() {
        super("Unable to create a user named 'admin'");
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
    }
}


class UserNotFoundError extends (Error403) {
    constructor(){
        super("The user is not found");
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
    }
}


class DatabaseInconsistencyError extends (Error500) {
    constructor() {
        super("Database innconsistency detected");
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
    }
}


class NotFoundError extends (Error404) {
    constructor(item, id) {
        super(`The item '${item}' with id='${id}' is not found'`);
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
    }
}


class NoAuthorizationHeaderError extends (Error401) {
    constructor() {
        super("No authorization header found");
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
    }
}


class BadAuthorizationHeaderError extends (Error400) {
    constructor() {
        super("Bad authorization header found");
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
    }
}


class ForbiddenAccessError extends (Error403) {
    constructor() {
        super("Invalid login or password");
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
    }
}

class InvalidTokenError extends (Error403) {
    constructor() {
        super("Invalid token");
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
    }
}


class PageNumberOutOfRangeError extends (Error400) {
    constructor() {
        super("The page number is out of range");
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
    }
}


class MissingArgumentError extends (Error400) {
    constructor(argumentName) {
        super(`The attribute '${argumentName}' is missing`);
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
    }
}


const generateErrorReport = (error) => {

    if (error === undefined) {
        return {
            errorType: 'undefined',
            errorMessage: 'Undefined error',
            status: 500,
        };
    }

    if (error instanceof APIError) {
        return error.getReport();
    }

    if (error instanceof mongoose.Error.ValidationError) {
        return {
            errorType: "mongoose.Error.ValidationError",
            errorMessage: error.message,
            status: 400,
        }
    }

    if (error instanceof mongodb.MongoServerError && error.code === 11000) {
        return {
            errorType: "mongoose.Error.ValidationError",
            errorMessage: error.message,
            status: 400,
        }
    }

    if (error instanceof Error) {
        return {
            errorType: error.name,
            errorMessage: error.message,
            status: 500,
        }
    }
}

const errorHandler = (err, req, res, next) => {
    // In case the response headers has already been transmited
    if (res.headersSent) {
        return next(err);
    }

    // Custom error handling
    const errorReport = generateErrorReport(err);
    console.error(err);
    return res.status(errorReport.status).json(errorReport);
}

module.exports = {
    TokenPayloadError,
    InvalidPasswordError,
    DisabledUserError,
    UserNotFoundError,
    AdminOverwriteError,
    DatabaseInconsistencyError,
    NotFoundError,
    NoAuthorizationHeaderError,
    BadAuthorizationHeaderError,
    ForbiddenAccessError,
    InvalidTokenError,
    PageNumberOutOfRangeError,
    MissingArgumentError,
    errorHandler,
}