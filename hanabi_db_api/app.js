const constants = require('./constants');
const cors = require('cors');
const { errorHandler } = require('./middleware/errors');
const mongoose = require('mongoose');
const morgan = require('morgan');


const sleep = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}


const main = async () => {

    // Database connection
    for (let tentative = 0; tentative < constants.dbMaxConnectionTentatives; ++tentative) {

        try {
            console.log(`Connecting to database... (${tentative + 1}/${constants.dbMaxConnectionTentatives})`);

            await mongoose.connect(constants.dbUri, constants.dbConnectionOptions);

            break;
        }
        catch (err) {
            console.error(err);
            await sleep(constants.dbConnectionDelayMS);
            continue;
        }
    }

    if (mongoose.connection.readyState !== 1)
        throw new Error("Unable to connect to the database");
    console.log("Connected to the database");


    // Server
    console.log("Starting server...");
    const express = require('express');
    const app = express();

    // Use CORS
    app.use(cors());

    // Set a json body parser
    app.use(express.json());

    // Add logging on requests
    app.use(morgan('dev'));


    // Build informations about the API
    const db_admin = new mongoose.mongo.Admin(mongoose.connection.db);
    db_admin.buildInfo().catch(err => {
        console.log(err);
        process.exit(1);
    }).then(mongoInfo => {

        // Add the routes
        app.use('/api', require('./routes/api'));

        // Add the error middleware
        app.use(errorHandler);

        // Run server
        app.listen(constants.apiPort, () => {
            console.log("API started");
            console.log("API port=" + constants.apiPort);
            console.log("API version=" + constants.apiVersionString);
            console.log("Server running");
        });
    });

}

main().catch(err => {
    console.error(err);
    process.exit(1);
})


process.on("SIGINT", () => {

    console.log("'SIGINT' received");

    if (mongoose.connection) {
        mongoose.disconnect();
        console.log("Disconnect from database");
    }

    console.log("Shuting down the server...");
    process.exit(128);
});


process.on("SIGHUP", () => {

    console.log("'SIGHUP' received");

    if (mongoose.connection) {
        mongoose.disconnect();
        console.log("Disconnect from database");
    }

    console.log("Shuting down the server...");
    process.exit(129);
});


process.on("SIGTERM", () => {

    console.log("'SIGTERM' received");

    if (mongoose.connection) {
        mongoose.disconnect();
        console.log("Disconnect from database");
    }

    console.log("Shuting down the server...");
    process.exit(130);
});
