# Hanabi Api

## Build image

Use the following command to build the API docker image from the 'hanabi_db_api' directory :

```
docker build -t registry.gitlab.com/rafaouel/hanabi/hanabi_api:<api_version> .
```

To push the images to the registry :
```
# Login
docker login registry.gitlab.com/rafaouel/hanabi
# Enter the username then a project access token

# Push image
docker push registry.gitlab.com/rafaouel/hanabi/hanabi_api:<api_version>
```
## Run image

### Docker

Use the following command to run the API docker image:

```
docker run -e db_port=$db_port -e db_host=$db_host -e db_name=$db_name -e db_username=$db_username -e db_password=db_password -p 3000:3000 registry.gitlab.com/rafaouel/hanabi/hanabi_api:<api_version>
```

With:

| variable    | description                              | default value |
|-------------|------------------------------------------|---------------|
| db_port     | port of the database                     | 27017         |
| db_host     | host of the database                     | localhost     |
| db_name     | name of the database                     | hanabi        |
| db_username | username used to connect to the database | ""            |
| db_password | password used to connect to the database | ""            |

Example:
```
docker run -e db_port=27017 -e db_host=192.168.0.1 -e db_name=hanabi -e db_username=toto -e db_password=pass -p 3000:3000 registry.gitlab.com/rafaouel/hanabi/hanabi_api:<api_version>
```

### Kubernetes

``` yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: hanabi-api
  labels:
    workload.user.cattle.io/workloadselector: apps.deployment-hanabi-hanabi-api
  namespace: hanabi
spec:
  replicas: 1
  selector:
    matchLabels:
      workload.user.cattle.io/workloadselector: apps.deployment-hanabi-hanabi-api
  template:
    metadata:
      labels:
        workload.user.cattle.io/workloadselector: apps.deployment-hanabi-hanabi-api
      namespace: hanabi
    spec:
      containers:
        - env:
            - name: db_host
              valueFrom:
                configMapKeyRef:
                  key: database-host
                  name: db-settings
                  optional: false
            - name: db_port
              value: '27017'
            - name: db_username
              valueFrom:
                secretKeyRef:
                  key: api-login
                  name: api-db-credentials
                  optional: false
            - name: db_password
              valueFrom:
                secretKeyRef:
                  key: api-password
                  name: api-db-credentials
                  optional: false
          image: registry.gitlab.com/rafaouel/hanabi/hanabi_api:<api_version>
          imagePullPolicy: IfNotPresent
          livenessProbe:
            failureThreshold: 3
            httpGet:
              path: /hanabi/healthcheck/liveness-probe/
              port: 3000
              scheme: HTTP
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 1
          name: container-0
          ports:
            - containerPort: 3000
              name: cluster
              protocol: TCP
          readinessProbe:
            failureThreshold: 3
            httpGet:
              path: /hanabi/healthcheck/readiness-probe/
              port: 3000
              scheme: HTTP
            periodSeconds: 30
            successThreshold: 1
            timeoutSeconds: 1
          securityContext:
            allowPrivilegeEscalation: false
            privileged: false
            readOnlyRootFilesystem: false
            runAsNonRoot: false
          startupProbe:
            failureThreshold: 3
            httpGet:
              path: /hanabi/healthcheck/startup-probe/
              port: 3000
              scheme: HTTP
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 1
```