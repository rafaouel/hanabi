#include "pch.h"
#include "randomai.h"

using namespace hanabi;


int main(int argc, const char* argv[])
{
	try
	{
		BOOST_LOG_TRIVIAL(trace) << "Program starts";

		const std::string default_host = "127.0.0.1";
		std::string host = default_host;

		const boost::asio::ip::port_type default_port = 32000;
		boost::asio::ip::port_type port = default_port;

		boost::program_options::options_description description("Allowed options");
		description.add_options()
			("help,h", "produce help message")
			("host", boost::program_options::value<std::string>(&host)->default_value(default_host), "IP address of the server")
			("port", boost::program_options::value<boost::asio::ip::port_type>(&port)->default_value(default_port), "Port of the server")
			;

		boost::program_options::variables_map variables_map;
		boost::program_options::store(boost::program_options::parse_command_line(argc, argv, description), variables_map);
		boost::program_options::notify(variables_map);

		if (variables_map.count("help"))
		{
			std::cout << description << std::endl;
			return 1;
		}

		// Version to log
		BOOST_LOG_TRIVIAL(info) << "Versions:";
		BOOST_LOG_TRIVIAL(info) << " - boost: " << common::to_string(common::to_version(BOOST_VERSION));
		BOOST_LOG_TRIVIAL(info) << " - Hanabi lib: " << common::to_string(common::to_version(HANABI_LIB_VERSION));

		// Command line to log
		std::ostringstream oss;
		for (int i = 0; i < argc; ++i)
		{
			if (i != 0)
				oss << " ";
			oss << argv[i];
		}
		BOOST_LOG_TRIVIAL(debug) << "Command line: " << oss.str();

		// Arguments to log
		BOOST_LOG_TRIVIAL(debug) << "Arguments: ";
		BOOST_LOG_TRIVIAL(debug) << " - host=" << host << " (default=" << default_host << ")";
		BOOST_LOG_TRIVIAL(debug) << " - port=" << port << " (default=" << default_port << ")";

		// Create client
		BOOST_LOG_TRIVIAL(info) << "Creating client...";
		BOOST_LOG_TRIVIAL(debug) << "Server : host=" << host << " port=" << port;

		boost::asio::io_service io_service;
		const boost::asio::ip::address host_address = boost::asio::ip::make_address(host.c_str());

		std::random_device random_device;
		std::shared_ptr<std::mt19937> p_random_generator = std::make_shared<std::mt19937>(random_device());

		CRandomAI ai(io_service, host_address, port, p_random_generator);
		
		ai.run();

		BOOST_LOG_TRIVIAL(trace) << "Program ends normally";
		return 0;
	}
	catch (std::exception& e)
	{
		BOOST_LOG_TRIVIAL(fatal) << "A standard exception has been catch.";
		BOOST_LOG_TRIVIAL(fatal) << e.what();
		return 1;
	}
	catch (...)
	{
		BOOST_LOG_TRIVIAL(fatal) << "An unknown error occured.";
		return 1;
	}
}