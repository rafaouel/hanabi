#ifndef RANDOMAI_H_6957B7E6_FCE1_45DA_8BBB_BE6491FC79FE
#define RANDOMAI_H_6957B7E6_FCE1_45DA_8BBB_BE6491FC79FE
#pragma once


class CRandomAI : public hanabi::client_side::IGameClient
{
public:
	CRandomAI(boost::asio::io_service& io_service, const boost::asio::ip::address& server_address, const boost::asio::ip::port_type& server_port, const std::shared_ptr<std::mt19937>& p_random_generator);
	virtual ~CRandomAI() noexcept;


	hanabi::common::TVersionArray version_array() const noexcept;

	const std::shared_ptr<std::mt19937>& random_generator() const noexcept;

private:
	virtual std::shared_ptr<hanabi::common::IAction> process_action_request(const hanabi::client_side::CGameState& game_state, const hanabi::client_side::TPeerInfoMap& peer_info, hanabi::common::TIndex my_id);
	virtual hanabi::client_side::CPeerInfoResponse process_peer_info_request(hanabi::common::TIndex my_id, const std::string server_verison);

	const std::shared_ptr<std::mt19937> m_p_random_generator;
};

#endif 