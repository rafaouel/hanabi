#include "pch.h"
#include "randomai.h"

CRandomAI::CRandomAI(boost::asio::io_service& io_service, const boost::asio::ip::address& server_address, const boost::asio::ip::port_type& server_port, const std::shared_ptr<std::mt19937>& p_random_generator) :
	IGameClient(io_service, server_address, server_port),
	m_p_random_generator(p_random_generator)
{

}

CRandomAI::~CRandomAI() noexcept
{

}

hanabi::common::TVersionArray CRandomAI::version_array() const noexcept
{
	return hanabi::common::to_version(HANABI_LIB_VERSION);
}

const std::shared_ptr<std::mt19937>& CRandomAI::random_generator() const noexcept
{
	return this->m_p_random_generator;
}

std::shared_ptr<hanabi::common::IAction> CRandomAI::process_action_request(const hanabi::client_side::CGameState& game_state, const hanabi::client_side::TPeerInfoMap& peer_info, hanabi::common::TIndex my_id)
{
	// Wait
	const bool wait = true;
	const std::size_t wait_duration = 2000;
	if (wait)
		std::this_thread::sleep_for(std::chrono::milliseconds(wait_duration));

	// List the available actions
	const bool is_allowed_to_speak = this->game_state().speak_token_count() > 0;
	const bool is_allowed_to_drop = this->game_state().speak_token_count() < this->game_state().game_option().max_speak_token_count();
	const bool is_allowed_to_place = this->game_state().information_collection().information().at(this->my_id()).size() > 0;

	std::vector<std::string> action_vector;
	if (is_allowed_to_drop)
	{
		action_vector.push_back("drop");
	}
	if (is_allowed_to_place)
	{
		action_vector.push_back("place");
	}
	if (is_allowed_to_speak)
	{
		action_vector.push_back("speak");
	}

	assert(action_vector.size() > 0);

	// Choose one action randomly
	std::uniform_int_distribution<int> action_choice(0, int(action_vector.size() - 1));
	const int action_index = action_choice(*this->random_generator());
	const std::string& action = action_vector.at(action_index);

	if (action.compare("drop") == 0)
	{
		// The chosen action is 'drop'

		// Choose the card to drop
		std::uniform_int_distribution<int> hand_choice(0, int(this->game_state().information_collection().information().at(this->my_id()).size() - 1));
		const int hand_index = hand_choice(*this->random_generator());
		return std::shared_ptr<hanabi::common::CDrop>(new hanabi::common::CDrop(hand_index));
	}
	else if (action.compare("place") == 0)
	{
		// The chosen action is 'place'

		// Choose the card to place
		std::uniform_int_distribution<int> hand_choice(0, int(this->game_state().information_collection().information().at(this->my_id()).size() - 1));
		const int hand_index = hand_choice(*this->random_generator());
		return std::shared_ptr<hanabi::common::CPlace>(new hanabi::common::CPlace(hand_index));
	}
	else if (action.compare("speak") == 0)
	{
		// The chosen action is 'speak'

		// Choose the target player
		std::vector<hanabi::common::TIndex> targets;
		for (const auto& pair : this->game_state().other_hands().other_hands())
			targets.push_back(pair.first);
		std::uniform_int_distribution<int> player_choice(0, int(targets.size() - 1));
		const hanabi::common::TIndex target_player_id = targets.at(player_choice(*this->random_generator()));

		// Choose the kind of information to give
		std::bernoulli_distribution bool_generator;
		const bool is_value_info = bool_generator(*this->random_generator());

		if (is_value_info)
		{
			// The chosen speak type is value

			// Choose the value
			std::uniform_int_distribution<hanabi::common::TValue> value_choice(this->game_state().game_option().min_card_value(), this->game_state().game_option().max_card_value());
			const hanabi::common::TValue value_info = value_choice(*this->random_generator());

			std::shared_ptr<hanabi::common::CSpeak> p_action(new hanabi::common::CSpeak(target_player_id));
			p_action->value_info(value_info);
			return p_action;
		}
		else
		{
			// The chosen speak type is color

			// Choose the color
			std::vector<hanabi::common::CardColor> colors;
			for (const auto& pair : this->game_state().game_option().color_option())
			{
				if (pair.first != hanabi::common::CardColor::black && pair.first != hanabi::common::CardColor::multicolor)
					colors.push_back(pair.first);
			}
			std::uniform_int_distribution<int> color_choice(0, int(colors.size() - 1));
			const int color_index = color_choice(*this->random_generator());
			const hanabi::common::CardColor color_info = colors.at(color_index);

			std::shared_ptr<hanabi::common::CSpeak> p_action(new hanabi::common::CSpeak(target_player_id));
			p_action->color_info(color_info);
			return p_action;
		}
	}
	else
	{
		assert(false);
		return std::shared_ptr<hanabi::common::IAction>();
	}
}

hanabi::client_side::CPeerInfoResponse CRandomAI::process_peer_info_request(hanabi::common::TIndex my_id, const std::string server_verison)
{
	const hanabi::common::TTagList tags = { "random_ai" };
	return hanabi::client_side::CPeerInfoResponse("AI_random", hanabi::common::to_version(HANABI_LIB_VERSION), hanabi::common::PeerType::AI, compiled_with_debug_option, tags);
}