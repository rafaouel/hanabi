#ifndef SERVERTRASH_H_B4FE207D_2EEB_4DE0_B136_8D26CECF4C01
#define SERVERTRASH_H_B4FE207D_2EEB_4DE0_B136_8D26CECF4C01
#pragma once

#include "card.h"
#include "serializable.h"
#include "servergameoption.h"
#include "sendable.h"

namespace hanabi
{
	namespace server_side
	{
		class CTrash : public common::ISerializable, public common::ISendable
		{
		public:
			CTrash(const CGameOption& game_option);
			virtual ~CTrash() noexcept;


			common::TScore current_max_score() const noexcept;
			const common::TCardStack& stack() const noexcept;
			common::TCardStack& stack() noexcept;

			const CGameOption& game_option() const noexcept;


			void trash(const common::CCard& card);

		private:
			typedef std::map<common::CardColor, common::TScore> TColorScore;


			virtual boost::json::value serialize_to_json() const;
			virtual void parse_from_json(const boost::json::value& /*root*/) {}
			virtual boost::json::value virtual_pack(common::TIndex player_id) const;

			const TColorScore& color_score() const noexcept;
			TColorScore& color_score() noexcept;
			const common::TScore& color_score(common::CardColor color) const;
			common::TScore& color_score(common::CardColor color);

			TColorScore m_color_score;

			const CGameOption& m_game_option;

			common::TCardStack m_stack;

		};
	}
}

#endif