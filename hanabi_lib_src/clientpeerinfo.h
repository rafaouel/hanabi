#ifndef CLIENTPEERINFO_H_F34AED54_9748_45F4_90AB_8B231D4BD788
#define CLIENTPEERINFO_H_F34AED54_9748_45F4_90AB_8B231D4BD788
#pragma once

#include "updatable.h"
#include "types.h"


namespace hanabi
{
	namespace client_side
	{
		class CPeerInfo : public common::IUpdatable
		{
		public:
			CPeerInfo();
			virtual ~CPeerInfo() noexcept;


			void name(const std::string& name);
			const std::string& name() const noexcept;

			common::PeerType type() const noexcept;
			void type(common::PeerType type);

			std::string version() const noexcept;
			const common::TVersionArray& version_array() const noexcept;
			void version_array(const common::TVersionArray& array);

			bool debug() const noexcept;
			void debug(bool is_debug);

		private:
			virtual void virtual_update(const boost::json::value& root);

			std::string m_name;
			common::PeerType m_type;
			common::TVersionArray m_version;
			bool m_debug;
		};

		typedef std::map<common::TIndex, CPeerInfo> TPeerInfoMap;
	}
}

#endif