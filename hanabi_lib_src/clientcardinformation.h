#ifndef INFORMATIONCOLLECTION_H_BB4E83F9_0063_450D_BA90_7524212A2A90
#define INFORMATIONCOLLECTION_H_BB4E83F9_0063_450D_BA90_7524212A2A90
#pragma once

#include "cardrawinformation.h"
#include "updatable.h"


namespace hanabi
{
	namespace client_side
	{
		class CCardInformation : public common::IUpdatable
		{
		public:
			typedef std::vector<common::CCardRawInformation> TInformation;
			typedef std::vector<TInformation> TPlayerInformation;

			CCardInformation();
			virtual ~CCardInformation() noexcept;


			const TPlayerInformation& information() const noexcept;
			TPlayerInformation& information() noexcept;

			const TInformation& information(common::TIndex player_id) const noexcept;

			
		private:
			virtual void virtual_update(const boost::json::value& root_value);


			TPlayerInformation m_player_information;
		};
	}
}
#endif 