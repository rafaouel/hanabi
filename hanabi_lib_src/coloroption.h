#ifndef COLOROPTION_H_72B020BA_0812_49F2_BAD3_9261137C413E
#define COLOROPTION_H_72B020BA_0812_49F2_BAD3_9261137C413E
#pragma once

#include "types.h"


namespace hanabi
{
	namespace common
	{
		class CColorOption
		{
		public:
			typedef std::map<TValue, common::TCount> TValueDistribution;


			CColorOption(const TValueDistribution& distribution, bool is_ascending, const common::TColorSet& reacts_to);
			virtual ~CColorOption() noexcept;


			const TValueDistribution& distribution() const noexcept;
			common::TCount distribution(TValue value) const noexcept;

			bool is_ascending() const noexcept;

			const common::TColorSet& reacts_to() const noexcept;

		private:
			const TValueDistribution m_value_distribution;
			const bool m_is_ascending;
			const common::TColorSet m_reacts_to;
		};

		typedef std::map<CardColor, CColorOption> TColorOptionMap;
		typedef std::set<common::CardColor> TColorSet;

		std::string card_color_to_string(CardColor color);
		char card_color_to_char(CardColor color);

		CardColor string_to_card_color(const std::string& string);
		CardColor char_to_card_color(const char& c);
	}
}

#endif