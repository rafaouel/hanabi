#include "pch.h"
#include "clientgameend.h"

namespace hanabi
{
	namespace client_side
	{
		CGameEnd::CGameEnd() :
			m_victory(false),
			m_game_start_timestamp(boost::posix_time::not_a_date_time),
			m_game_end_timestamp(boost::posix_time::not_a_date_time),
			m_game_uuid(),
			m_elapsed_player_turn_count(-1),
			m_elapsed_game_turn_count(-1),
			m_score(0),
			m_max_score(0),
			m_error_token_count(0),
			m_max_error_token_count(0),
			m_player_statistics_map()
		{

		}

		CGameEnd::~CGameEnd() noexcept
		{

		}

		bool CGameEnd::victory() const noexcept
		{
			return this->m_victory;
		}

		void CGameEnd::victory(bool victory)
		{
			this->m_victory = victory;
		}

		const boost::posix_time::ptime& CGameEnd::game_start_timestamp() const noexcept
		{
			assert(this->m_game_start_timestamp != boost::posix_time::not_a_date_time);
			return this->m_game_start_timestamp;
		}

		void CGameEnd::game_start_timestamp(const boost::posix_time::ptime& timestamp)
		{
			assert(timestamp != boost::posix_time::not_a_date_time);
			this->m_game_start_timestamp = timestamp;
		}

		const boost::posix_time::ptime& CGameEnd::game_end_timestamp() const noexcept
		{
			assert(this->m_game_end_timestamp != boost::posix_time::not_a_date_time);
			return this->m_game_end_timestamp;
		}

		void CGameEnd::game_end_timestamp(const boost::posix_time::ptime& timestamp)
		{
			assert(timestamp != boost::posix_time::not_a_date_time);
			this->m_game_end_timestamp = timestamp;
		}

		const boost::uuids::uuid& CGameEnd::game_uuid() const noexcept
		{
			return this->m_game_uuid;
		}

		void CGameEnd::game_uuid(const boost::uuids::uuid& game_uuid)
		{
			this->m_game_uuid = game_uuid;
		}

		common::TCount CGameEnd::elapsed_player_turn_count() const noexcept
		{
			return this->m_elapsed_player_turn_count;
		}

		void CGameEnd::elapsed_player_turn_count(common::TCount turn_count)
		{
			this->m_elapsed_player_turn_count = turn_count;
		}

		common::TCount CGameEnd::elapsed_game_turn_count() const noexcept
		{
			assert(this->m_elapsed_game_turn_count >= 0);
			return this->m_elapsed_game_turn_count;
		}

		void CGameEnd::elapsed_game_turn_count(common::TCount turn_count)
		{
			this->m_elapsed_game_turn_count = turn_count;
		}

		common::TScore CGameEnd::score() const noexcept
		{
			assert(this->m_score >= 0);
			return this->m_score;
		}

		void CGameEnd::score(common::TScore score)
		{
			assert(score >= 0);
			this->m_score = score;
		}

		common::TScore CGameEnd::max_score() const noexcept
		{
			assert(this->m_max_score >= 0);
			return this->m_max_score;
		}

		void CGameEnd::max_score(common::TScore score)
		{
			assert(score >= 0);
			this->m_max_score = score;
		}

		common::TCount CGameEnd::error_token_count() const noexcept
		{
			return this->m_error_token_count;
		}

		void CGameEnd::error_token_count(common::TCount count)
		{
			this->m_error_token_count = count;
		}

		common::TCount CGameEnd::max_error_token_count() const noexcept
		{
			return this->m_max_error_token_count;
		}

		void CGameEnd::max_error_token_count(common::TCount count)
		{
			this->m_max_error_token_count = count;
		}

		const common::TPlayerStatisticsMap& CGameEnd::player_statistics() const noexcept
		{
			return this->m_player_statistics_map;
		}

		void CGameEnd::player_statistics(const common::TPlayerStatisticsMap& map)
		{
			this->m_player_statistics_map = map;
		}

		void CGameEnd::virtual_update(const boost::json::value& root)
		{
			const boost::json::object root_object = root.as_object();

			this->victory(root_object.at("victory").as_bool());
			this->game_start_timestamp(boost::posix_time::from_iso_extended_string(root_object.at("gameStartTimestamp").as_string().c_str()));
			this->game_end_timestamp(boost::posix_time::from_iso_extended_string(root_object.at("gameEndTimestamp").as_string().c_str()));
			this->game_uuid(boost::lexical_cast<boost::uuids::uuid>(root_object.at("gameUuid").as_string().c_str()));
			this->elapsed_player_turn_count(root_object.at("elapsedPlayerTurnCount").as_int64());
			this->elapsed_game_turn_count(root_object.at("elapsedGameTurnCount").as_int64());
			this->score(root_object.at("score").as_int64());
			this->max_score(root_object.at("maxScore").as_int64());
			this->error_token_count(root_object.at("errorTokenCount").as_int64());
			this->max_error_token_count(root_object.at("maxErrorTokenCount").as_int64());

			const boost::json::array& stat_array = root_object.at("statistics").as_array();
			common::TPlayerStatisticsMap player_stat;
			for (const auto& stat_value : stat_array)
			{
				const boost::json::object& stat_object = stat_value.as_object();

				const common::TIndex player_id = stat_object.at("playerId").as_int64();
				common::CPlayerStatistics stat(player_id);

				stat.drop_count(stat_object.at("dropCount").as_int64());
				stat.place_count(stat_object.at("placeCount").as_int64());
				stat.speak_count(stat_object.at("speakCount").as_int64());
				stat.drop_duration(boost::posix_time::duration_from_string("0:0:" + std::to_string(stat_object.at("dropDuration").as_double())));	// TODO becarefull when the value is an integer
				stat.place_duration(boost::posix_time::duration_from_string("0:0:" + std::to_string(stat_object.at("placeDuration").as_double())));	// TODO becarefull when the value is an integer
				stat.speak_duration(boost::posix_time::duration_from_string("0:0:" + std::to_string(stat_object.at("speakDuration").as_double())));	// TODO becarefull when the value is an integer
				stat.total_duration(boost::posix_time::duration_from_string("0:0:" + std::to_string(stat_object.at("totalDuration").as_double())));	// TODO becarefull when the value is an integer

				player_stat.insert(std::pair<common::TIndex, common::CPlayerStatistics>(player_id, stat));
			}
			this->player_statistics(player_stat);
		}
	}
}