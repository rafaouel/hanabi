#ifndef TYPES_H_5E7ED8D2_A694_41DB_A50B_8480E322FBFC
#define TYPES_H_5E7ED8D2_A694_41DB_A50B_8480E322FBFC
#pragma once


namespace hanabi
{
	namespace common
	{
		typedef std::int64_t TIndex;
		constexpr TIndex invalid_index = -1;
		constexpr bool is_invalid_index(TIndex index) { return index < 0; }
		constexpr bool is_valid_index(TIndex index) { return index >= 0; }

		typedef std::int64_t TValue;
		constexpr TValue invalid_value = -1;
		constexpr bool is_invalid_value(TValue value) { return value <= 0; }
		constexpr bool is_valid_value(TValue value) { return value > 0; }

		typedef std::int64_t TScore;
		typedef std::int64_t TCount;

		typedef std::string TGameOptionId;

		enum class Preset
		{
			CAKE,	// 1 color
			EASY,	// 3 colors
			STD,	// 5 colors
			MULTI,	// 5 colors + multi
			BLACK,	// 5 colors + black
			HARD	// 5 colors + multi + black
		};
		std::string to_string(Preset preset);
		Preset preset_from_string(const std::string& string);

		enum class PeerType
		{
			UNDEFINED,
			HUMAN,
			AI
		};
		std::string to_string(PeerType type);
		PeerType peer_type_from_string(const std::string& string);

		typedef std::array<int, 3> TVersionArray;
		std::string to_string(const TVersionArray& array);
		TVersionArray to_version(int version);
		TVersionArray to_version(const std::string& version);

		typedef std::string TTag;
		typedef std::set<TTag> TTagList;

		enum class CardColor
		{
			blue,
			white,
			red,
			yellow,
			green,
			multicolor,
			black
		};

		typedef std::set<CardColor> TColorSet;


		// 3 actions available:
		// Drop a card to get a token and complete the hand
		// Use a token to give an information to an other player
		// Place a card on the board and complete the hand
		enum class ActionType
		{
			SPEAK,
			DROP,
			PLACE
		};

		std::string to_string(ActionType type);

		typedef boost::beast::http::request<boost::beast::http::string_body> TRequest;
		typedef boost::beast::http::response<boost::beast::http::string_body> TResponse;
	}
}

#endif