#ifndef CLIENTSOCKET_H_CA44B35A_8712_4BD9_BF42_B8EA98037232
#define CLIENTSOCKET_H_CA44B35A_8712_4BD9_BF42_B8EA98037232
#pragma once

#include "socket.h"


namespace hanabi
{
	namespace client_side
	{
		class CSocket : public network::ISocket
		{
		public:
			CSocket(boost::asio::io_service& io_context, const boost::asio::ip::address& server_ip_address, const boost::asio::ip::port_type& server_port);
			virtual ~CSocket() noexcept;


			const boost::asio::ip::port_type& get_port() const noexcept;
			const boost::asio::ip::address& get_ip_address() const noexcept;

		private:
			virtual void virtual_init();

			const boost::asio::ip::port_type m_port;
			const boost::asio::ip::address m_ip_address;
		};
	}
}

#endif