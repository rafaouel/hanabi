#ifndef CARDRAWINFORMATION_H_25B40CD1_C635_4E74_97DE_94F0EB97430F
#define CARDRAWINFORMATION_H_25B40CD1_C635_4E74_97DE_94F0EB97430F
#pragma once

#include "card.h"


namespace hanabi
{
	namespace common
	{
		class CCardRawInformation
		{
		public:
			typedef std::set<TValue> TValueSet;
			typedef std::set<CardColor> TColorSet;


			CCardRawInformation();
			virtual ~CCardRawInformation() noexcept;


			void put(TValue value);
			void put_not(TValue value);

			void put(CardColor color);
			void put_not(CardColor color);

			TValue positive_value() const;
			const TValueSet& negative_value() const;

			const TColorSet& positive_color() const;
			const TColorSet& negative_color() const;

		private:
			TValue m_positive_value;
			TValueSet m_negative_value;

			TColorSet m_positive_color;
			TColorSet m_negative_color;
		};
	}
}
#endif