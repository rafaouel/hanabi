#include "pch.h"
#include "clientsocket.h"

namespace hanabi
{
	namespace client_side
	{
		CSocket::CSocket(boost::asio::io_service& io_context, const boost::asio::ip::address& server_ip_address, const boost::asio::ip::port_type& server_port) :
			network::ISocket(io_context),
			m_port(server_port),
			m_ip_address(server_ip_address)
		{

		}

		CSocket::~CSocket() noexcept
		{

		}

		const boost::asio::ip::port_type& CSocket::get_port() const noexcept
		{
			return this->m_port;
		}

		const boost::asio::ip::address& CSocket::get_ip_address() const noexcept
		{
			return this->m_ip_address;
		}

		void CSocket::virtual_init()
		{
			const boost::asio::ip::tcp::endpoint endpoint(this->m_ip_address, this->m_port);
			this->socket().connect(endpoint);
		}
	}
}

