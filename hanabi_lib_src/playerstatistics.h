#ifndef PLAYER_STATISTICS_H_B0E0A3A9_7E5E_4E94_8CB6_1F6C4C5CED35
#define PLAYER_STATISTICS_H_B0E0A3A9_7E5E_4E94_8CB6_1F6C4C5CED35
#pragma once

#include "types.h"


namespace hanabi
{
	namespace common
	{
		class CPlayerStatistics
		{
		public:
			CPlayerStatistics(common::TIndex player_id);
			virtual ~CPlayerStatistics() noexcept;


			common::TIndex player_id() const noexcept;

			common::TCount drop_count() const noexcept;
			void drop_count(common::TCount count);
			void increment_drop_count();

			common::TCount place_count() const noexcept;
			void place_count(common::TCount count);
			void increment_place_count();
			
			common::TCount speak_count() const noexcept;
			void speak_count(common::TCount count);
			void increment_speak_count();

			const boost::posix_time::time_duration& drop_duration() const noexcept;
			void drop_duration(const boost::posix_time::time_duration& duration);
			void cumul_drop_duration(const boost::posix_time::time_duration& delta);

			const boost::posix_time::time_duration& place_duration() const noexcept;
			void place_duration(const boost::posix_time::time_duration& duration);
			void cumul_place_duration(const boost::posix_time::time_duration& delta);

			const boost::posix_time::time_duration& speak_duration() const noexcept;
			void speak_duration(const boost::posix_time::time_duration& duration);
			void cumul_speak_duration(const boost::posix_time::time_duration& delta);

			const boost::posix_time::time_duration& total_duration() const noexcept;
			void total_duration(const boost::posix_time::time_duration& duration);
			void cumul_total_duration(const boost::posix_time::time_duration& delta);

		private:
			const common::TIndex m_player_id;
			common::TCount m_drop_count;
			common::TCount m_place_count;
			common::TCount m_speak_count;
			boost::posix_time::time_duration m_drop_duration;
			boost::posix_time::time_duration m_place_duration;
			boost::posix_time::time_duration m_speak_duration;
			boost::posix_time::time_duration m_total_duration;
		};

		typedef std::map<common::TIndex, CPlayerStatistics> TPlayerStatisticsMap;
	}
}

#endif