#ifndef SERVERSOCKET_H_78F4C6E8_A6E5_4FC8_9383_DA20B1ABC72C
#define SERVERSOCKET_H_78F4C6E8_A6E5_4FC8_9383_DA20B1ABC72C
#pragma once

#include "socket.h"


namespace hanabi
{
	namespace server_side
	{
		class CSocket : public network::ISocket
		{
		public:
			CSocket(boost::asio::io_context& io_context, boost::asio::ip::tcp::acceptor& acceptor);
			virtual ~CSocket() noexcept;


		private:
			virtual void virtual_init();

			boost::asio::ip::tcp::acceptor& m_acceptor;
		};
	}
}
#endif