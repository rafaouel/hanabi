#include "pch.h"
#include "clientgamestart.h"

namespace hanabi
{
	namespace client_side
	{
		CClientGameStart::CClientGameStart() :
			m_game_uuid(),
			m_timestamp(boost::posix_time::not_a_date_time),
			m_game_option_id()
		{

		}

		CClientGameStart::~CClientGameStart() noexcept
		{

		}

		void CClientGameStart::game_uuid(const boost::uuids::uuid& game_uuid)
		{
			this->m_game_uuid = game_uuid;
		}

		const boost::uuids::uuid& CClientGameStart::game_uuid() const noexcept
		{
			return this->m_game_uuid;
		}

		void CClientGameStart::timestamp(const boost::posix_time::ptime& timestamp)
		{
			assert(timestamp != boost::posix_time::not_a_date_time);
			this->m_timestamp = timestamp;
		}

		const boost::posix_time::ptime& CClientGameStart::timestamp() const noexcept
		{
			assert(this->m_timestamp != boost::posix_time::not_a_date_time);
			return this->m_timestamp;
		}

		void CClientGameStart::game_option_id(const common::TGameOptionId& game_option_id)
		{
			assert(game_option_id.length() > 0);
			this->m_game_option_id = game_option_id;
		}

		const common::TGameOptionId& CClientGameStart::game_option_id() const noexcept
		{
			assert(this->m_game_option_id.length() > 0);
			return this->m_game_option_id;
		}

		void CClientGameStart::virtual_update(const boost::json::value& root)
		{
			const boost::json::object& root_object = root.as_object();

			this->game_option_id(root_object.at("gameOptionId").as_string().c_str());
			this->game_uuid(boost::lexical_cast<boost::uuids::uuid>(root_object.at("gameUuid").as_string().c_str()));
			this->timestamp(boost::posix_time::from_iso_extended_string(root_object.at("timestamp").as_string().c_str()));
		}
	}
}