#ifndef HANABI_LIB_H_911A8260_0096_4329_ACF3_F673520123D3
#define HANABI_LIB_H_911A8260_0096_4329_ACF3_F673520123D3
#pragma once

// std
#define _CRT_SECURE_NO_WARNINGS
#include <algorithm>
#include <cassert>
#include <exception>
#include <filesystem>
#include <fstream>
#include <functional>
#include <iostream>
#include <list>
#include <map>
#include <memory>
#include <random>
#include <set>
#include <sstream>
#include <vector>

// boost
#include <boost/predef.h>
#if BOOST_OS_WINDOWS > 0
#	ifndef _WIN32_WINNT
#		define _WIN32_WINNT 0x0A00
#	endif
#elif BOOST_OS_LINUX > 0
#	pragma message("Maybe needed to define the OS ?")
#else
#	error "Not eligible platform target"
#endif

#include <boost/algorithm/string.hpp>
#include <boost/archive/iterators/base64_from_binary.hpp>
#include <boost/archive/iterators/binary_from_base64.hpp>
#include <boost/archive/iterators/transform_width.hpp>
#include <boost/asio.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/json.hpp>
#include <boost/stacktrace.hpp>
#include <boost/url.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

// hanabi
#include "apigateway.h"
#include "compatibility.h"
#include "gameclient.h"
#include "gameserver.h"
#include "packetcoder.h"
#include "packetparser.h"
#include "servergamestart.h"
#include "tcpplayer.h"
#include "version.h"

// Check Architecture
#if BOOST_ARCH_X86_64 > 0
#else
#error "The architecture should be x86_64"
#endif

// Auto link
#ifdef _DEBUG

#if BOOST_COMP_MSVC > 0
#pragma comment(lib, "hanabi_lib_x64_Debug.lib")
#else
#pragma message ("Add 'hanabi_lib_x64_Debug.lib' to linker input")
#endif

#else

#if BOOST_COMP_MSVC > 0
#pragma comment(lib, "hanabi_lib_x64_Release.lib")
#else
#pragma message ("Add 'hanabi_lib_x64_Release.lib' to linker input")
#endif

#endif

#endif