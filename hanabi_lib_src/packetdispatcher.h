#ifndef PACKETDISPATCHER_H_C8EF012A_ECD5_41DB_A2F4_9FAD8DA537F8
#define PACKETDISPATCHER_H_C8EF012A_ECD5_41DB_A2F4_9FAD8DA537F8
#pragma once

#include "packetparser.h"


namespace hanabi
{
	namespace network
	{
		class CPacketDispatcher
		{
		public:
			typedef std::function<void(const CPacket& packet)> TDecodeFunctor;


			CPacketDispatcher();
			virtual ~CPacketDispatcher() noexcept;


			void decode(const CPacket& packet) const;

			void add_functor(const PacketType& packet_type, const TDecodeFunctor& functor);

		private:
			typedef std::map<PacketType, TDecodeFunctor> TDecodeFunctorMap;

			TDecodeFunctorMap m_decode_functor_map;
		};
	}
}
#endif