#ifndef PCH_H_486DBC2E_4FDA_471E_BD56_A1867F06D587
#define PCH_H_486DBC2E_4FDA_471E_BD56_A1867F06D587
#pragma once

// std
#define _CRT_SECURE_NO_WARNINGS
#include <algorithm>
#include <cassert>
#include <exception>
#include <filesystem>
#include <fstream>
#include <functional>
#include <iostream>
#include <list>
#include <map>
#include <memory>
#include <random>
#include <set>
#include <sstream>
#include <vector>

// boost
#include <boost/predef.h>
#if BOOST_OS_WINDOWS > 0
#	ifndef _WIN32_WINNT
#		define _WIN32_WINNT 0x0601
#	endif
#elif BOOST_OS_LINUX > 0
#	pragma message("Maybe needed to define the OS ?")
#else
#	error "Not eligible platform target"
#endif

#include <boost/algorithm/string.hpp>
#include <boost/archive/iterators/base64_from_binary.hpp>
#include <boost/archive/iterators/binary_from_base64.hpp>
#include <boost/archive/iterators/transform_width.hpp>
#include <boost/asio.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/json.hpp>
#include <boost/stacktrace.hpp>
#include <boost/url.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

#endif //PCH_H
