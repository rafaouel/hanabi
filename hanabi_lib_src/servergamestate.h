#ifndef SERVERGAMESTATE_H_AE0FD16C_F2ED_4185_BD7F_A861A74CDA08
#define SERVERGAMESTATE_H_AE0FD16C_F2ED_4185_BD7F_A861A74CDA08
#pragma once

#include "serializable.h"
#include "serverboard.h"
#include "servercardinformation.h"
#include "servergameoption.h"
#include "servergamestart.h"
#include "serverplayerhand.h"
#include "serverstack.h"
#include "servertrash.h"
#include "sendable.h"


namespace hanabi
{
	namespace server_side
	{
		class CGameState : public common::ISerializable, public common::ISendable
		{
		public:
			CGameState(const CGameOption& game_option, const CGameStart& start);
			virtual ~CGameState() noexcept;


			// All about options
			const CGameOption& game_option() const noexcept;
			const CGameStart& game_start() const noexcept;
			CGameStart& game_start() noexcept;

			// All about speak token
			common::TCount speak_token_count() const noexcept;
			void speak_token_count(common::TCount new_count);
			bool can_speak() const noexcept;
			bool can_drop() const noexcept;
			void add_speak_token();
			void remove_speak_token();

			// All about error token
			common::TCount error_token_count() const noexcept;
			void error_token_count(common::TCount new_count);
			bool is_game_over() const noexcept;
			void add_error_token();
			void remove_error_token();

			// All about turn count
			common::TCount player_turn_count() const noexcept;
			void reset_player_turn_count();
			void increase_player_turn_count() noexcept;
			common::TCount game_turn_count() const noexcept;
			void reset_game_turn_count();
			void increase_game_turn_count() noexcept;

			// All about current player id
			common::TIndex current_player_id() const noexcept;
			void current_player_id(common::TIndex id);
			void move_current_player();

			// All about timestamp
			const boost::posix_time::ptime& player_turn_timestamp() const noexcept;
			void capture_player_turn_timestamp();

			// All about game items
			const CBoard& board() const noexcept;
			CBoard& board() noexcept;
			const CTrash& trash() const noexcept;
			CTrash& trash() noexcept;
			const CStack& stack() const noexcept;
			CStack& stack() noexcept;
			const CPlayerHand& player_hand() const noexcept;
			CPlayerHand& player_hand() noexcept;
			const CCardInformation& card_information() const noexcept;
			CCardInformation& card_information() noexcept;

		private:
			virtual boost::json::value serialize_to_json() const;
			virtual void parse_from_json(const boost::json::value& root);
			virtual boost::json::value virtual_pack(common::TIndex player_id) const;

			// Game options
			const CGameOption m_game_option;
			CGameStart m_game_start;

			// Game variables
			common::TCount m_speak_token_count;
			common::TCount m_error_token_count;
			common::TCount m_player_turn_count;
			common::TCount m_game_turn_count;
			common::TIndex m_current_player_id;
			boost::posix_time::ptime m_player_turn_timestamp;

			// Game items
			CBoard m_board;
			CTrash m_trash;
			CStack m_stack;
			CPlayerHand m_player_hand;
			CCardInformation m_card_information;
		};
	}
}

#endif