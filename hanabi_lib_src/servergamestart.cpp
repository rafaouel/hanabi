#include "pch.h"
#include "servergamestart.h"

namespace hanabi
{
	namespace server_side
	{
		CGameStart::CGameStart(const common::TGameOptionId& game_option_id, const common::TTagList& tags) :
			m_game_uuid(boost::uuids::random_generator_mt19937()()),
			m_timestamp(boost::posix_time::microsec_clock::universal_time()),
			m_tags(tags),
			m_game_option_id(game_option_id)
		{

		}

		CGameStart::~CGameStart() noexcept
		{

		}

		const boost::uuids::uuid& CGameStart::game_uuid() const noexcept
		{
			return this->m_game_uuid;
		}

		const boost::posix_time::ptime& CGameStart::timestamp() const noexcept
		{
			assert(this->m_timestamp != boost::posix_time::not_a_date_time);
			return this->m_timestamp;
		}

		void CGameStart::add_tag(const common::TTag& tag)
		{
			this->m_tags.insert(tag);
		}

		void CGameStart::clear_tags()
		{
			this->m_tags.clear();
		}

		const common::TTagList& CGameStart::tags() const noexcept
		{
			return this->m_tags;
		}

		const common::TGameOptionId& CGameStart::game_option_id() const noexcept
		{
			return this->m_game_option_id;
		}


		/// <summary>
		/// Save all the values in this object to a json object
		/// </summary>
		/// <returns>A json object</returns>
		boost::json::value CGameStart::serialize_to_json() const
		{
			boost::json::object root = this->prepack_data().as_object();

			root["tags"] = boost::json::value_from(this->m_tags);

			return root;
		}

		/// <summary>
		/// Update the values from a given json object
		/// </summary>
		/// <param name="root">json object that stores the values</param>
		void CGameStart::parse_from_json(const boost::json::value& /*root*/)
		{
			
		}

		boost::json::value CGameStart::virtual_pack(common::TIndex /*target_player_id*/) const
		{
			boost::json::object root = this->prepack_data().as_object();

			return root;
		}

		boost::json::value CGameStart::prepack_data() const
		{
			boost::json::object root;

			root["gameUuid"] = boost::json::value_from(boost::uuids::to_string(this->game_uuid()));
			root["gameOptionId"] = boost::json::value_from(this->game_option_id());
			root["timestamp"] = boost::json::value_from(boost::posix_time::to_iso_extended_string(this->timestamp()) + "Z");
			
			return root;
		}
	}
}