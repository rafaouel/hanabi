#include "pch.h"
#include "packetdispatcher.h"

#include "exception.h"


namespace hanabi
{
	namespace network
	{
		CPacketDispatcher::CPacketDispatcher()
		{

		}

		CPacketDispatcher::~CPacketDispatcher() noexcept
		{

		}

		void CPacketDispatcher::decode(const CPacket& packet) const
		{
			const PacketType type = packet_type(packet);

			const auto iterator = this->m_decode_functor_map.find(type);
			if (iterator == this->m_decode_functor_map.cend())
				THROW_ERROR_1(EUnknownPacketTypeError, int(type));

			return this->m_decode_functor_map.at(type)(packet);
		}

		void CPacketDispatcher::add_functor(const PacketType& packet_type, const TDecodeFunctor& functor)
		{
			if (this->m_decode_functor_map.contains(packet_type))
				THROW_ERROR_1(EAlreadyExistsError, int(packet_type));

			this->m_decode_functor_map.insert(std::pair(packet_type, functor));// [packet_type] = functor;
		}
	}
}