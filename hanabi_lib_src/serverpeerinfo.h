#ifndef SERVERPERRINFO_H_1E8C442D_AC33_48C1_8517_B6F98BE60B0B
#define SERVERPERRINFO_H_1E8C442D_AC33_48C1_8517_B6F98BE60B0B
#pragma once

#include "sendable.h"
#include "updatable.h"
#include "serializable.h"
#include "types.h"


namespace hanabi
{
	namespace server_side
	{
		class CPeerInfo : public common::ISendable, public common::IUpdatable, public common::ISerializable
		{
		public:
			CPeerInfo();
			virtual ~CPeerInfo() noexcept;


			const std::string& name() const noexcept;
			void name(const std::string& name);

			const boost::asio::ip::address& address() const noexcept;
			void address(const boost::asio::ip::address& address);

			common::TIndex id() const noexcept;
			void id(common::TIndex id);

			std::string version() const noexcept;
			const common::TVersionArray& version_array() const noexcept;
			void version_array(const common::TVersionArray& array);

			bool debug() const noexcept;
			void debug(bool is_debug);

			common::PeerType type() const noexcept;
			void type(common::PeerType type);

			const common::TTagList& tags() const noexcept;
			void tags(const common::TTagList& tags);

		private:
			virtual boost::json::value virtual_pack(common::TIndex player_id) const;
			virtual void virtual_update(const boost::json::value& root);
			virtual boost::json::value serialize_to_json() const;
			virtual void parse_from_json(const boost::json::value& /*value*/) {};

			std::string m_name;
			boost::asio::ip::address m_ip;
			common::TIndex m_id;
			std::array<int, 3> m_version;
			bool m_debug;
			common::PeerType m_type;
			common::TTagList m_tags;
		};

		typedef std::map<common::TIndex, CPeerInfo> TPeerInfoMap;
	}
}

#endif