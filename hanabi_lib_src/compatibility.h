#ifndef COMPATIBILITY_H_01B70092_FFC2_4D3F_9D9C_D4CB47875562
#define COMPATIBILITY_H_01B70092_FFC2_4D3F_9D9C_D4CB47875562
#pragma once

#include "types.h"


namespace hanabi
{
	namespace server_side
	{
		class CCompatibilityData
		{
		public:
			CCompatibilityData();
			virtual ~CCompatibilityData() noexcept;


			bool has_ref(const common::TVersionArray& ref_version) const;

			void add_min(const common::TVersionArray& ref_version, const common::TVersionArray& min);
			void add_min(const std::string& ref_version, const common::TVersionArray& min);
			void add_max(const common::TVersionArray& ref_version, const common::TVersionArray& max);
			void add_max(const std::string& ref_version, const common::TVersionArray& max);

			bool has_min(const common::TVersionArray& ref_version) const;
			bool has_max(const common::TVersionArray& ref_version) const;

			const common::TVersionArray& min(const common::TVersionArray& ref_version) const;
			const common::TVersionArray& max(const common::TVersionArray& ref_version) const;

		private:
			std::map<std::string, std::map<std::string, common::TVersionArray>> m_versions;
		};

		CCompatibilityData load_compatibility_data(const std::filesystem::path& path);

		class CCompatibilityChecker
		{
		public:
			CCompatibilityChecker(const CCompatibilityData& data, const common::TVersionArray& my_version);
			virtual ~CCompatibilityChecker() noexcept;


			const common::TVersionArray& my_version() const noexcept;
			void my_version(const common::TVersionArray& version);

			bool check(const common::TVersionArray& version) const;

			static bool is_gte(const common::TVersionArray& left, const common::TVersionArray& right);

		private:
			const CCompatibilityData m_data;
			common::TVersionArray m_my_version;
		};
	}
}

#endif