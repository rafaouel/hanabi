#include "pch.h"
#include "compatibility.h"

#include "exception.h"


namespace hanabi
{
	namespace server_side
	{
		CCompatibilityData::CCompatibilityData()
		{

		}

		CCompatibilityData::~CCompatibilityData() noexcept
		{

		}

		bool CCompatibilityData::has_ref(const common::TVersionArray & ref_version) const
		{
			const std::string ref_string = common::to_string(ref_version);
			return this->m_versions.contains(ref_string);
		}

		void CCompatibilityData::add_min(const common::TVersionArray& ref_version, const common::TVersionArray& min)
		{
			const std::string ref_string = common::to_string(ref_version);
			if (!this->m_versions.contains(ref_string))
			{
				this->m_versions.insert(std::pair<std::string, std::map<std::string, common::TVersionArray>>(ref_string, std::map<std::string, common::TVersionArray>()));
			}
			this->m_versions.at(ref_string).insert(std::pair<std::string, common::TVersionArray>("min", min));
		}

		void CCompatibilityData::add_min(const std::string& ref_string, const common::TVersionArray& min)
		{
			if (!this->m_versions.contains(ref_string))
			{
				this->m_versions.insert(std::pair<std::string, std::map<std::string, common::TVersionArray>>(ref_string, std::map<std::string, common::TVersionArray>()));
			}
			this->m_versions.at(ref_string).insert(std::pair<std::string, common::TVersionArray>("min", min));
		}

		void CCompatibilityData::add_max(const common::TVersionArray& ref_version, const common::TVersionArray& max)
		{
			const std::string ref_string = common::to_string(ref_version);
			if (!this->m_versions.contains(ref_string))
			{
				this->m_versions.insert(std::pair<std::string, std::map<std::string, common::TVersionArray>>(ref_string, std::map<std::string, common::TVersionArray>()));
			}
			this->m_versions.at(ref_string).insert(std::pair<std::string, common::TVersionArray>("max", max));
		}

		void CCompatibilityData::add_max(const std::string& ref_string, const common::TVersionArray& max)
		{
			if (!this->m_versions.contains(ref_string))
			{
				this->m_versions.insert(std::pair<std::string, std::map<std::string, common::TVersionArray>>(ref_string, std::map<std::string, common::TVersionArray>()));
			}
			this->m_versions.at(ref_string).insert(std::pair<std::string, common::TVersionArray>("max", max));
		}

		bool CCompatibilityData::has_min(const common::TVersionArray& ref_version) const
		{
			const std::string ref_string = common::to_string(ref_version);
			if (this->m_versions.contains(ref_string))
				return this->m_versions.at(ref_string).contains("min");
			return false;
		}

		bool CCompatibilityData::has_max(const common::TVersionArray& ref_version) const
		{
			const std::string ref_string = common::to_string(ref_version);
			if (this->m_versions.contains(ref_string))
				return this->m_versions.at(ref_string).contains("max");
			return false;
		}

		const common::TVersionArray& CCompatibilityData::min(const common::TVersionArray& ref_version) const
		{
			const std::string ref_string = common::to_string(ref_version);
			return this->m_versions.at(ref_string).at("min");
		}

		const common::TVersionArray& CCompatibilityData::max(const common::TVersionArray& ref_version) const
		{
			const std::string ref_string = common::to_string(ref_version);
			return this->m_versions.at(ref_string).at("max");
		}

		CCompatibilityChecker::CCompatibilityChecker(const CCompatibilityData& data, const common::TVersionArray& my_version) :
			m_data(data),
			m_my_version(my_version)
		{
			if (!this->m_data.has_ref(this->m_my_version))
				THROW_ERROR_1(ECompatibilityError, this->m_my_version);
		}

		CCompatibilityChecker::~CCompatibilityChecker() noexcept
		{

		}

		const common::TVersionArray& CCompatibilityChecker::my_version() const noexcept
		{
			return this->m_my_version;
		}

		void CCompatibilityChecker::my_version(const common::TVersionArray& version)
		{
			this->m_my_version = version;
		}

		bool CCompatibilityChecker::check(const common::TVersionArray& version) const
		{
			// Get the min
			bool min_valid = true;
			if (this->m_data.has_min(this->m_my_version))
				min_valid = this->is_gte(this->m_data.min(this->m_my_version), version);

			// Get the max
			bool max_valid = true;
			if (this->m_data.has_max(this->m_my_version))
				max_valid = this->is_gte(version, this->m_data.max(this->m_my_version));

			return min_valid && max_valid;
		}

		bool CCompatibilityChecker::is_gte(const common::TVersionArray& left, const common::TVersionArray& right)
		{
			if (left[0] > right[0])
				return false;

			// left[0] <= right[0]
			if (left[0] == right[0])
			{
				if (left[1] > right[1])
					return false;

				// left[0] == right[0]
				// left[1] <= right[1]

				if (left[1] == right[1])
				{
					// left[0] == right[0]
					// left[1] == right[1]
					return left[2] <= right[2];
				}

				// left[0] == right[0]
				// left[1] < right[1]
				return true;
			}

			// left[0] < right[0]

			return true;
		}

		CCompatibilityData load_compatibility_data(const std::filesystem::path& path)
		{
			CCompatibilityData data;

			// Opening the file
			std::ifstream istream(path.c_str());

			// Create parser
			boost::json::stream_parser parser;

			// Read line by line
			std::string line;

			while (std::getline(istream, line))
			{
				// Parse
				parser.write(line);
			}

			// Close the file
			istream.close();

			// End the pasing
			parser.finish();

			// Get the json data
			const boost::json::value root = parser.release();

			/*
			[
				{
					ref_version: "1.0.0",
					min: "1.0.0",
					max: "2.0.0"
				},
				{
					ref_version: "2.0.0",
					min: "1.1.0",
					max: "2.0.0"
				}
			]
			*/
			
			// The main value is an array
			for (const boost::json::value& item : root.as_array())
			{
				// In the array there are objects
				const boost::json::object& version_data = item.as_object();

				// In each object there is a key (the refVersion) and its value (a string)
				const std::string ref_version = version_data.at("refVersion").as_string().c_str();

				// Check if there is a min
				if (version_data.contains("min"))
				{
					const std::string min_version = version_data.at("min").as_string().c_str();
					data.add_min(ref_version, common::to_version(min_version));
				}

				// Check if there is a max
				if (version_data.contains("max"))
				{
					const std::string min_version = version_data.at("max").as_string().c_str();
					data.add_max(ref_version, common::to_version(min_version));
				}
			}

			return data;
		}
	}
}