#ifndef EXCEPTION_H_54AAB60E_6E13_4939_8C7F_352BBDA9F29F
#define EXCEPTION_H_54AAB60E_6E13_4939_8C7F_352BBDA9F29F
#pragma once

#define THROW_ERROR(x) throw x(__FILE__, __FUNCTION__, __LINE__)
#define THROW_ERROR_1(x, arg0) throw x(__FILE__, __FUNCTION__, __LINE__, (arg0))
#define THROW_ERROR_2(x, arg0, arg1) throw x(__FILE__, __FUNCTION__, __LINE__, (arg0), (arg1))
#define THROW_ERROR_3(x, arg0, arg1, arg2) throw x(__FILE__, __FUNCTION__, __LINE__, (arg0), (arg1), (arg2))
#define THROW_ERROR_4(x, arg0, arg1, arg2, arg3) throw x(__FILE__, __FUNCTION__, __LINE__, (arg0), (arg1), (arg2), (arg3))

#include "apicontext.h"
#include "types.h"


namespace hanabi
{
	class EHanabiException : public std::exception
	{
	public:
		EHanabiException(const std::string& file_name, const std::string& function_name, std::size_t line);

		virtual ~EHanabiException() noexcept;

		const char* what() const noexcept;

		std::string get_error_type_name() const;

	private:
		virtual std::string virtual_get_error_type_name() const = 0;
		virtual std::string virtual_get_message() const = 0;

		const std::string m_file_name;
		const std::string m_function_name;
		const std::size_t m_line;
		std::string m_stack;

		mutable std::string m_message;
	};

	class EImpossibleError : public EHanabiException
	{
	public:
		EImpossibleError(const std::string& file_name, const std::string& function_name, std::size_t line);
		EImpossibleError(const std::string& file_name, const std::string& function_name, std::size_t line, const std::string& message);
		virtual ~EImpossibleError() noexcept;

	private:
		virtual std::string virtual_get_message() const;
		virtual std::string virtual_get_error_type_name() const;

		const std::string m_message;
	};

	class ENotImplementedError : public EHanabiException
	{
	public:
		ENotImplementedError(const std::string& file_name, const std::string& function_name, std::size_t line);
		virtual ~ENotImplementedError() noexcept;

	private:
		virtual std::string virtual_get_message() const;
		virtual std::string virtual_get_error_type_name() const;
	};

	class EServerError : public EHanabiException
	{
	public:
		EServerError(const std::string& file_name, const std::string& function_name, std::size_t line);
		virtual ~EServerError() noexcept;
	};

	class ECompatibilityError : public EServerError
	{
	public:
		ECompatibilityError(const std::string& file_name, const std::string& function_name, std::size_t line, const common::TVersionArray& version);
		virtual ~ECompatibilityError() noexcept;

	private:
		virtual std::string virtual_get_message() const;
		virtual std::string virtual_get_error_type_name() const;

		const common::TVersionArray m_version;
	};

	class EServerFull : public EServerError
	{
	public:
		EServerFull(const std::string& file_name, const std::string& function_name, std::size_t line);
		virtual ~EServerFull() noexcept;

	private:
		virtual std::string virtual_get_message() const;
		virtual std::string virtual_get_error_type_name() const;
	};

	class EParsingError : public EServerError
	{
	public:
		EParsingError(const std::string& file_name, const std::string& function_name, std::size_t line, const std::string& description);
		virtual ~EParsingError() noexcept;

	private:
		virtual std::string virtual_get_message() const;
		virtual std::string virtual_get_error_type_name() const;

		const std::string m_description;
	};

	class EInvalidPresetError : public EServerError
	{
	public:
		EInvalidPresetError(const std::string& file_name, const std::string& function_name, std::size_t line, const std::string& preset_value);
		virtual ~EInvalidPresetError() noexcept;

	private:
		virtual std::string virtual_get_message() const;
		virtual std::string virtual_get_error_type_name() const;

		const std::string m_invalid_preset_value;
	};

	class EOutOfRangeError : public EServerError
	{
	public:
		EOutOfRangeError(const std::string& file_name, const std::string& function_name, std::size_t line, std::int64_t invalid_value);
		virtual ~EOutOfRangeError() noexcept;

	private:
		virtual std::string virtual_get_message() const;
		virtual std::string virtual_get_error_type_name() const;

		const std::int64_t m_invalid_value;
	};

	class ENotAllowedActionError : public EServerError
	{
	public:
		ENotAllowedActionError(const std::string& file_name, const std::string& function_name, std::size_t line, const std::string& action_type_name);
		virtual ~ENotAllowedActionError() noexcept;

	private:
		virtual std::string virtual_get_message() const;
		virtual std::string virtual_get_error_type_name() const;

		const std::string m_action_type_name;
	};

	class EInvalidPlayerNumberError : public EServerError
	{
	public:
		EInvalidPlayerNumberError(const std::string& file_name, const std::string& function_name, std::size_t line, common::TCount player_count);
		virtual ~EInvalidPlayerNumberError() noexcept;

	private:
		virtual std::string virtual_get_message() const;
		virtual std::string virtual_get_error_type_name() const;

		const common::TCount m_player_count;
	};

	class EInvalidColorError : public EServerError
	{
	public:
		EInvalidColorError(const std::string& file_name, const std::string& function_name, std::size_t line, const std::string& invalid_color_name);
		EInvalidColorError(const std::string& file_name, const std::string& function_name, std::size_t line, const common::CardColor& invalid_color);
		virtual ~EInvalidColorError() noexcept;

	private:
		virtual std::string virtual_get_message() const;
		virtual std::string virtual_get_error_type_name() const;

		const std::string m_invalid_color_name;
	};

	class ENetworkError : public EHanabiException
	{
	public:
		ENetworkError(const std::string& file_name, const std::string& function_name, std::size_t line);
		virtual ~ENetworkError() noexcept;
	};

	class EAPIRequestError : public ENetworkError
	{
	public:
		EAPIRequestError(const std::string& file_name, const std::string& function_name, std::size_t line, const std::string& reason);
		virtual ~EAPIRequestError() noexcept;

	private:
		virtual std::string virtual_get_message() const;
		virtual std::string virtual_get_error_type_name() const;

		const std::string m_reason;
	};

	class EUnknownPacketTypeError : public ENetworkError
	{
	public:
		EUnknownPacketTypeError(const std::string& file_name, const std::string& function_name, std::size_t line, int packet_type);
		virtual ~EUnknownPacketTypeError() noexcept;

		int packet_type() const noexcept;

	private:
		virtual std::string virtual_get_message() const;
		virtual std::string virtual_get_error_type_name() const;

		const int m_packet_type;
	};

	template <class T>
	class EAlreadyExistsError : public EServerError
	{
	public:
		EAlreadyExistsError(const std::string& file_name, const std::string& function_name, std::size_t line, const T& key);
		virtual ~EAlreadyExistsError() noexcept;

	private:
		virtual std::string virtual_get_message() const;
		virtual std::string virtual_get_error_type_name() const;

		const T m_key;
	};

	class EHttpError : public EHanabiException
	{
	public:
		EHttpError(const std::string& file_name, const std::string& function_name, std::size_t line, const common::TRequest& request, const common::CAPIContext& context, const common::TResponse& response, const std::string& description);
		virtual ~EHttpError() noexcept;

	private:
		virtual std::string virtual_get_message() const;
		virtual std::string virtual_get_error_type_name() const;

		const common::TRequest m_request;
		const common::CAPIContext m_context;
		const common::TResponse m_response;
		const std::string m_description;
	};


	template <class T>
	EAlreadyExistsError<T>::EAlreadyExistsError(const std::string& file_name, const std::string& function_name, std::size_t line, const T& key) :
		EServerError(file_name, function_name, line),
		m_key(key)
	{

	}

	template <class T>
	EAlreadyExistsError<T>::~EAlreadyExistsError() noexcept
	{

	}

	template <class T>
	std::string EAlreadyExistsError<T>::virtual_get_message() const
	{
		std::ostringstream oss;
		oss << "The functor associated with the key \"" << int(this->m_key) << "\"" << " have already been registered" << std::endl;
		return oss.str();
	}

	template <class T>
	std::string EAlreadyExistsError<T>::virtual_get_error_type_name() const
	{
		return typeid(*this).name();
	}
}

#endif