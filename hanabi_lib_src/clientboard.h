#ifndef CLIENTBOARD_H_FA308084_AA46_4D19_9FC9_452558953229
#define CLIENTBOARD_H_FA308084_AA46_4D19_9FC9_452558953229
#pragma once

#include "card.h"
#include "updatable.h"


namespace hanabi
{
	namespace client_side
	{
		class CBoard : public common::IUpdatable
		{
		public:
			CBoard();
			virtual ~CBoard() noexcept;


			common::TScore score() const noexcept;
			void score(common::TScore score);

			const common::TCardBoard& board() const noexcept;
			common::TCardBoard& board() noexcept;

		private:
			virtual void virtual_update(const boost::json::value& root);

			common::TScore m_score;
			common::TCardBoard m_board;
		};
	}
}

#endif