#ifndef SERVERPLAYER_H_6B791CD3_7A26_40BA_A3B0_7B7108D646F4
#define SERVERPLAYER_H_6B791CD3_7A26_40BA_A3B0_7B7108D646F4
#pragma once

#include "serverpeerinfo.h"
#include "playergateway.h"


namespace hanabi
{
	namespace server_side
	{
		class CServerPlayer
		{
		public:
			CServerPlayer(const std::shared_ptr<IPlayerGateway> p_gateway, const CPeerInfo& peer_info);
			virtual ~CServerPlayer() noexcept;

			const std::shared_ptr<IPlayerGateway>& gateway() const noexcept;

			const CPeerInfo& peer_info() const noexcept;

		private:
			const std::shared_ptr<IPlayerGateway> m_p_gateway;
			const CPeerInfo m_peer_info;
		};
	}
}
#endif