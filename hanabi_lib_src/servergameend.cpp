#include "pch.h"
#include "servergameend.h"

#include "exception.h"
#include "clientgameend.h"


namespace hanabi
{
	namespace server_side
	{
		CGameEnd::CGameEnd(const boost::uuids::uuid& game_uuid, common::TCount player_count) :
			m_victory(false),
			m_game_start_timestamp(boost::posix_time::not_a_date_time),
			m_game_end_timestamp(boost::posix_time::not_a_date_time),
			m_game_uuid(game_uuid),
			m_elapsed_player_turn_count(-1),
			m_elapsed_game_turn_count(-1),
			m_score(0),
			m_max_score(0),
			m_error_token_count(0),
			m_max_error_token_count(0),
			m_player_statistics_map()
		{
			for (common::TIndex player_id = 0; player_id < player_count; ++player_id)
			{
				this->add_statistics(player_id, common::CPlayerStatistics(player_id));
			}
		}

		CGameEnd::~CGameEnd() noexcept
		{

		}

		bool CGameEnd::victory() const noexcept
		{
			return this->m_victory;
		}

		void CGameEnd::victory(bool victory)
		{
			this->m_victory = victory;
		}

		const boost::posix_time::ptime& CGameEnd::game_start_timestamp() const noexcept
		{
			assert(this->m_game_start_timestamp != boost::posix_time::not_a_date_time);
			return this->m_game_start_timestamp;
		}

		void CGameEnd::capture_game_start_timestamp()
		{
			this->m_game_start_timestamp = boost::posix_time::microsec_clock::universal_time();
		}

		const boost::posix_time::ptime& CGameEnd::game_end_timestamp() const noexcept
		{
			assert(this->m_game_end_timestamp != boost::posix_time::not_a_date_time);
			return this->m_game_end_timestamp;
		}

		void CGameEnd::capture_game_end_timestamp()
		{
			this->m_game_end_timestamp = boost::posix_time::microsec_clock::universal_time();
		}

		boost::posix_time::time_duration CGameEnd::compute_game_duration() const
		{
			assert(this->game_start_timestamp() != boost::posix_time::not_a_date_time);
			assert(this->game_end_timestamp() != boost::posix_time::not_a_date_time);
			assert(this->game_end_timestamp() > this->game_start_timestamp());

			return this->game_end_timestamp() - this->game_start_timestamp();
		}

		common::TCount CGameEnd::elapsed_player_turn_count() const noexcept
		{
			assert(this->m_elapsed_player_turn_count >= 0);
			return this->m_elapsed_player_turn_count;
		}

		void CGameEnd::elapsed_player_turn_count(common::TCount turn_count)
		{
			this->m_elapsed_player_turn_count = turn_count;
		}

		common::TCount CGameEnd::elapsed_game_turn_count() const noexcept
		{
			assert(this->m_elapsed_game_turn_count >= 0);
			return this->m_elapsed_game_turn_count;
		}

		void CGameEnd::elapsed_game_turn_count(common::TCount turn_count)
		{
			this->m_elapsed_game_turn_count = turn_count;
		}

		common::TScore CGameEnd::score() const noexcept
		{
			assert(this->m_score >= 0);
			return this->m_score;
		}

		void CGameEnd::score(common::TScore score)
		{
			assert(score >= 0);
			this->m_score = score;
		}

		common::TScore CGameEnd::max_score() const noexcept
		{
			assert(this->m_max_score >= 0);
			return this->m_max_score;
		}

		void CGameEnd::max_score(common::TScore score)
		{
			assert(score >= 0);
			this->m_max_score = score;
		}

		common::TCount CGameEnd::error_token_count() const noexcept
		{
			return this->m_error_token_count;
		}

		void CGameEnd::error_token_count(common::TCount count)
		{
			this->m_error_token_count = count;
		}

		common::TCount CGameEnd::max_error_token_count() const noexcept
		{
			return this->m_max_error_token_count;
		}

		void CGameEnd::max_error_token_count(common::TCount count)
		{
			this->m_max_error_token_count = count;
		}

		const common::TPlayerStatisticsMap& CGameEnd::player_statistics() const noexcept
		{
			return this->m_player_statistics_map;
		}

		common::TPlayerStatisticsMap& CGameEnd::player_statistics() noexcept
		{
			return this->m_player_statistics_map;
		}

		void CGameEnd::add_statistics(common::TIndex player_id, const common::CPlayerStatistics& stat)
		{
			this->m_player_statistics_map.insert(std::pair<common::TIndex, common::CPlayerStatistics>(player_id, stat));
		}

		const boost::uuids::uuid& CGameEnd::game_uuid() const noexcept
		{
			return this->m_game_uuid;
		}

		boost::json::value CGameEnd::serialize_to_json() const
		{
			boost::json::object root;

			root["gameUuid"] = boost::json::value_from(boost::uuids::to_string(this->game_uuid()));
			root["gameStartTimestamp"] = boost::json::value_from(boost::posix_time::to_iso_extended_string(this->game_start_timestamp()) + "Z");
			root["gameEndTimestamp"] = boost::json::value_from(boost::posix_time::to_iso_extended_string(this->game_end_timestamp()) + "Z");
			root["gameDuration"] = boost::json::value_from(double(this->compute_game_duration().total_milliseconds()) / 1000.);
			root["victory"] = boost::json::value_from(this->victory());
			root["elapsedPlayerTurnCount"] = boost::json::value_from(this->elapsed_player_turn_count());
			root["elapsedGameTurnCount"] = boost::json::value_from(this->elapsed_game_turn_count());
			root["errorTokenCount"] = boost::json::value_from(this->error_token_count());
			root["maxErrorTokenCount"] = boost::json::value_from(this->max_error_token_count());
			root["score"] = boost::json::value_from(this->score());
			root["maxScore"] = boost::json::value_from(this->max_score());

			std::vector<boost::json::value> stat_vector;
			for (const auto& stat : this->player_statistics())
			{
				boost::json::object stat_object;

				assert(stat.first == stat.second.player_id());
				stat_object["playerId"] = boost::json::value_from(stat.first);
				stat_object["dropCount"] = boost::json::value_from(stat.second.drop_count());
				stat_object["placeCount"] = boost::json::value_from(stat.second.place_count());
				stat_object["speakCount"] = boost::json::value_from(stat.second.speak_count());
				stat_object["dropDuration"] = boost::json::value_from(stat.second.drop_duration().total_milliseconds() / 1000.);
				stat_object["placeDuration"] = boost::json::value_from(stat.second.place_duration().total_milliseconds() / 1000.);
				stat_object["speakDuration"] = boost::json::value_from(stat.second.speak_duration().total_milliseconds() / 1000.);
				stat_object["totalDuration"] = boost::json::value_from(stat.second.total_duration().total_milliseconds() / 1000.);

				stat_vector.push_back(stat_object);
			}
			root["statistics"] = boost::json::value_from(stat_vector);
			
			return root;
		}

		void CGameEnd::parse_from_json(const boost::json::value& /*root*/)
		{
			THROW_ERROR(ENotImplementedError);
		}

		boost::json::value CGameEnd::virtual_pack(common::TIndex /*player_id*/) const
		{
			return this->to_json();
		}
	}
}