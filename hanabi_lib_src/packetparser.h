#ifndef PACKETCONTENT_H_F975694B_DA0B_415A_82D2_7F6ECF5A70A7
#define PACKETCONTENT_H_F975694B_DA0B_415A_82D2_7F6ECF5A70A7
#pragma once

#include "packet.h"


namespace hanabi
{
	namespace network
	{
		class CPacketParser
		{
		public:
			CPacketParser(PacketType packet_type);
			virtual ~CPacketParser() noexcept;


			// Get the packet type
			PacketType packet_type() const noexcept;

			// Read / write data
			void data(const boost::json::value& data);
			const boost::json::value& data() const noexcept;

			// Create data from a packet
			void decode(const CPacket& packet);

		private:
			boost::json::value m_data;
			const PacketType m_packet_type;
		};
	}
}

#endif