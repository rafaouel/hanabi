#ifndef PLAYERGATEWAY_H_CF2659F9_4524_4A28_A936_2E9B05FC9916
#define PLAYERGATEWAY_H_CF2659F9_4524_4A28_A936_2E9B05FC9916
#pragma once

#include "action.h"
#include "servergameend.h"
#include "servergameoption.h"
#include "servergamestate.h"
#include "serverpeerinfo.h"


namespace hanabi
{
	namespace server_side
	{
		class IPlayerGateway
		{
		public:
			IPlayerGateway();
			virtual ~IPlayerGateway() noexcept;


			// Initialize the gateway
			void init();

			// Close the gateway
			void close();

			// The server say hello to the peer
			void say_hello(const std::string& message);

			// The server gives information about the game initialization
			void send_game_option(const CGameOption& game_option, common::TIndex player_id);

			// The server gives information about the game
			void send_game_update(const CGameState& game_state, common::TIndex player_id);

			// The server gives information about the game end
			void send_game_end(const CGameEnd& game_end, common::TIndex player_id);

			// The server asks for an action
			std::shared_ptr<common::IAction> retrieve_action();

			// The server gives information about an action
			void send_action_place_update(common::TIndex source_player_id, const common::CCard& placed_card);
			void send_action_drop_update(common::TIndex source_player_id, const common::CCard& droped_card);
			void send_action_speak_update(common::TIndex source_player_id, common::TIndex target_player_id, common::TValue value);
			void send_action_speak_update(common::TIndex source_player_id, common::TIndex target_player_id, common::CardColor color);

			// The server gives information about a color completion
			void send_bonus_given_update(common::TIndex current_player_id, const common::CardColor& color);

			// The server gives information about a committed error
			void send_error_token_given_update(common::TIndex current_player_id, common::TCount error_token_count);

			// The server ask for peer info
			std::shared_ptr<CPeerInfo> retrieve_peer_info(common::TIndex player_id, const common::TVersionArray& server_version);

			// Send peer info
			void send_peer_info_update(const CPeerInfo& peer_info, common::TIndex player_id);

			// OK / Not OK
			void send_ok();
			void send_not_ok(const std::string& reason);


		private:
			virtual void virtual_init() = 0;
			virtual void virtual_close() = 0;

			virtual void virtual_say_hello(const std::string& message) = 0;
			virtual void virtual_send_game_option(const CGameOption& game_option, common::TIndex player_id) = 0;
			virtual void virtual_send_game_update(const CGameState& game_state, common::TIndex player_id) = 0;
			virtual void virtual_send_game_end(const CGameEnd& game_end, common::TIndex player_id) = 0;
			virtual std::shared_ptr<common::IAction> virtual_retrieve_action() = 0;
			virtual void virtual_send_action_place_update(common::TIndex source_player_id, const common::CCard& placed_card) = 0;
			virtual void virtual_send_action_drop_update(common::TIndex source_player_id, const common::CCard& droped_card) = 0;
			virtual void virtual_send_action_speak_update(common::TIndex source_player_id, common::TIndex target_player_id, common::TValue value) = 0;
			virtual void virtual_send_action_speak_update(common::TIndex source_player_id, common::TIndex target_player_id, common::CardColor color) = 0;
			virtual std::shared_ptr<CPeerInfo> virtual_retrieve_peer_info(common::TIndex player_id, const common::TVersionArray& server_version) = 0;
			virtual void virtual_send_peer_info_update(const CPeerInfo& peer_info, common::TIndex player_id) = 0;
			virtual void virtual_send_ok() = 0;
			virtual void virtual_send_not_ok(const std::string& not_ok) = 0;
			virtual void virtual_send_bonus_given_update(common::TIndex current_player_id, const common::CardColor& color) = 0;
			virtual void virtual_send_error_token_given_update(common::TIndex current_player_id, common::TCount error_token_count) = 0;
		};
	}
}

#endif