#ifndef CLIENTSTACK_H_B0C9BB8D_FEA4_4002_B9BA_E7B577D20B15
#define CLIENTSTACK_H_B0C9BB8D_FEA4_4002_B9BA_E7B577D20B15
#pragma once

#include "updatable.h"
#include "types.h"


namespace hanabi
{
	namespace client_side
	{
		class CStack : public common::IUpdatable
		{
		public:
			CStack();
			virtual ~CStack() noexcept;


			common::TCount stack_count() const noexcept;
			void stack_count(common::TCount count);


		private:
			virtual void virtual_update(const boost::json::value& root_value);


			common::TCount m_stack_count;

		};
	}
}
#endif