#include "pch.h"
#include "serverplayerhand.h"

namespace hanabi
{
	namespace server_side
	{
		CPlayerHand::CPlayerHand(const CGameOption& option) :
			m_player_hand(option.player_count())
		{

		}

		CPlayerHand::~CPlayerHand() noexcept
		{

		}

		const CPlayerHand::TPlayerHand& CPlayerHand::player_hand() const noexcept
		{
			return this->m_player_hand;
		}

		CPlayerHand::TPlayerHand& CPlayerHand::player_hand() noexcept
		{
			return this->m_player_hand;
		}

		const CPlayerHand::THand& CPlayerHand::hand(common::TIndex player_id) const
		{
			assert(player_id < common::TIndex(this->player_hand().size()));

			return this->player_hand().at(player_id);
		}

		CPlayerHand::THand& CPlayerHand::hand(common::TIndex player_id)
		{
			assert(player_id < common::TIndex(this->player_hand().size()));

			return this->player_hand().at(player_id);
		}

		void CPlayerHand::insert(common::TIndex player_id, const common::CCard& card)
		{
			assert(player_id < common::TIndex(this->player_hand().size()));

			this->hand(player_id).push_back(card);
		}

		common::CCard CPlayerHand::remove(common::TIndex player_id, common::TIndex hand_index)
		{
			assert(player_id < common::TIndex(this->player_hand().size()));
			assert(hand_index < common::TIndex(this->hand(player_id).size()));

			const common::CCard card = this->hand(player_id)[hand_index];

			THand tmp_hand;
			for (common::TIndex card_index = 0; card_index < common::TIndex(this->hand(player_id).size()); ++card_index)
			{
				if (card_index == hand_index)
					continue;
				tmp_hand.push_back(this->hand(player_id)[card_index]);
			}

			this->hand(player_id) = tmp_hand;

			return card;
		}

		boost::json::value CPlayerHand::serialize_to_json() const
		{
			std::vector<boost::json::value> player_hand_vector;
			for (common::TIndex player_index = 0; player_index < common::TIndex(this->player_hand().size()); ++player_index)
			{
				std::vector<boost::json::value> hand_vector;
				for (const auto& card : this->hand(player_index))
				{
					hand_vector.push_back(card.to_json());
				}
				player_hand_vector.push_back(boost::json::value_from(hand_vector));
			}

			return boost::json::value_from(player_hand_vector);
		}

		boost::json::value CPlayerHand::virtual_pack(common::TIndex target_player_id) const
		{
			std::vector<boost::json::value> player_hand_vector;
			for (common::TIndex player_index = 0; player_index < common::TIndex(this->player_hand().size()); ++player_index)
			{
				// We do not send his hand to a player
				if (target_player_id == player_index)
					continue;

				std::vector<boost::json::value> hand_vector;
				for (const auto& card : this->hand(player_index))
				{
					hand_vector.push_back(card.to_json());
				}

				boost::json::object hand_object;
				hand_object["playerId"] = boost::json::value_from(player_index);
				hand_object["hand"] = boost::json::value_from(hand_vector);

				player_hand_vector.push_back(boost::json::value_from(hand_object));
			}

			return boost::json::value_from(player_hand_vector);
		}
	}
}
