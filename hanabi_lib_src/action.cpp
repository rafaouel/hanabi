#include "pch.h"
#include "action.h"

#include "exception.h"
#include "packet.h"


namespace hanabi
{
    namespace common
    {

        IAction::IAction()
        {

        }

        IAction::~IAction() noexcept
        {

        }

        ActionType IAction::type() const
        {
            return this->virtual_type();
        }

        CDrop::CDrop(TIndex hand_index) :
            m_hand_index(hand_index)
        {

        }

        CDrop::CDrop(const boost::json::value& root):
            m_hand_index(invalid_index)
        {
            const boost::json::object root_object = root.as_object();

            this->hand_index(root_object.at("handIndex").as_int64());

            assert(this->m_hand_index != invalid_index);
        }

        CDrop::~CDrop() noexcept
        {

        }

        TIndex CDrop::hand_index() const noexcept
        {
            return this->m_hand_index;
        }

        void CDrop::hand_index(TIndex index)
        {
            this->m_hand_index = index;
        }

        ActionType CDrop::get_unique_id()
        {
            return ActionType::DROP;
        }

        ActionType CDrop::virtual_type() const
        {
            return CDrop::get_unique_id();
        }

        boost::json::value CDrop::serialize_to_json() const
        {
            boost::json::object player_action;

            player_action["actionType"] = boost::json::value_from("drop");
            player_action["handIndex"] = boost::json::value_from(this->hand_index());

            return player_action;
        }

        CPlace::CPlace(TIndex hand_index) :
            m_hand_index(hand_index)
        {

        }

        CPlace::CPlace(const boost::json::value& root) :
            m_hand_index(invalid_index)
        {
            const boost::json::object root_object = root.as_object();

            this->hand_index(root_object.at("handIndex").as_int64());

            assert(this->m_hand_index != invalid_index);
        }

        CPlace::~CPlace() noexcept
        {

        }

        TIndex CPlace::hand_index() const noexcept
        {
            return this->m_hand_index;
        }

        void CPlace::hand_index(TIndex index)
        {
            this->m_hand_index = index;
        }

        ActionType CPlace::get_unique_id()
        {
            return ActionType::PLACE;
        }

        ActionType CPlace::virtual_type() const
        {
            return CPlace::get_unique_id();
        }

        boost::json::value CPlace::serialize_to_json() const
        {
            boost::json::object player_action;

            player_action["actionType"] = boost::json::value_from("place");
            player_action["handIndex"] = boost::json::value_from(this->hand_index());

            return player_action;
        }

        CSpeak::CSpeak(TIndex player_index) :
            m_player_index(player_index)
        {

        }

        CSpeak::CSpeak(const boost::json::value& root) :
            m_player_index(invalid_index)
        {
            const boost::json::object& root_object = root.as_object();

            this->player_index(root_object.at("targetPlayerId").as_int64());

            const boost::json::object& card_info_object = root_object.at("cardInfo").as_object();

            if (card_info_object.contains("value"))
            {
                const common::TValue value = card_info_object.at("value").as_int64();
                this->value_info(value);
            }
            else if (card_info_object.contains("color"))
            {
                const CardColor color = common::string_to_card_color(card_info_object.at("color").as_string().c_str());
                this->color_info(color);
            }
            else
            {
                THROW_ERROR_1(EParsingError, "card info type is invalid");
            }

            assert(this->m_player_index != invalid_index);
        }

        CSpeak::~CSpeak() noexcept
        {
            
        }

        TIndex CSpeak::player_index() const noexcept
        {
            return this->m_player_index;
        }

        void CSpeak::player_index(TIndex index)
        {
            this->m_player_index = index;
        }

        bool CSpeak::is_color_info() const
        {
            return bool(this->m_p_color_info);
        }

        bool CSpeak::is_value_info() const
        {
            return bool(this->m_p_value_info);
        }

        TValue CSpeak::value_info() const
        {
            return *this->m_p_value_info;
        }

        common::CardColor CSpeak::color_info() const
        {
            return *this->m_p_color_info;
        }

        void CSpeak::value_info(TValue value)
        {
            this->m_p_value_info.reset(new TValue(value));
        }

        void CSpeak::color_info(common::CardColor color)
        {
            this->m_p_color_info.reset(new common::CardColor(color));
        }

        ActionType CSpeak::get_unique_id()
        {
            return ActionType::SPEAK;
        }

        ActionType CSpeak::virtual_type() const
        {
            return CSpeak::get_unique_id();
        }

        boost::json::value CSpeak::serialize_to_json() const
        {
            boost::json::object player_action;

            player_action["actionType"] = boost::json::value_from("speak");
            player_action["targetPlayerId"] = boost::json::value_from(this->player_index());
            boost::json::object card_info;
            if (this->is_value_info())
            {
                card_info["value"] = boost::json::value_from(this->value_info());
            }
            else if (this->is_color_info())
            {
                card_info["color"] = boost::json::value_from(common::card_color_to_string(this->color_info()));
            }
            else
            {
                THROW_ERROR_1(EImpossibleError, "Unknown information type");
            }
            player_action["cardInfo"] = boost::json::value_from(card_info);

            return player_action;
        }

        std::shared_ptr<IAction> create_action(const boost::json::value& root)
        {
            std::shared_ptr<IAction> p_action;

            const boost::json::object root_object = root.as_object();

            const auto action_type = root_object.at("actionType").as_string();
            if (action_type.compare("speak") == 0)
            {
                p_action.reset(new CSpeak(root_object));
            }
            else if (action_type.compare("drop") == 0)
            {
                p_action.reset(new CDrop(root_object));
            }
            else if (action_type.compare("place") == 0)
            {
                p_action.reset(new CPlace(root_object));
            }
            else
                THROW_ERROR(EImpossibleError);

            return p_action;
        }
    }
}

