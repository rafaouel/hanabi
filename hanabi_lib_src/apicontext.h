#ifndef APICONTEXT_H_E228F3CF_FAAC_41B3_A230_E7D08754F1CA
#define APICONTEXT_H_E228F3CF_FAAC_41B3_A230_E7D08754F1CA
#pragma once

namespace hanabi
{
	namespace common
	{
		class CAPIContext
		{
		public:
			CAPIContext(const std::string& api_host, const std::string& api_port, const std::string& api_login, const std::string& api_password, std::size_t max_tentative, std::size_t tentative_delay);
			virtual ~CAPIContext() noexcept;

			const std::string& api_host() const noexcept;
			const std::string& api_port() const noexcept;
			const std::string& api_login() const noexcept;
			const std::string& api_password() const noexcept;
			std::size_t max_tentative() const noexcept;
			std::size_t tentative_delay() const noexcept;

			void max_tentative(std::size_t max_tentative);
			void tentative_delay(std::size_t tentative_delay);


		private:
			const std::string m_api_host;
			const std::string m_api_port;
			const std::string m_api_login;
			const std::string m_api_password;
			std::size_t m_max_tentative;
			std::size_t m_tentative_delay;
		};
	}
}
#endif