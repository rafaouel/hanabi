#include "pch.h"
#include "playerstatistics.h"

namespace hanabi
{
	namespace common
	{
		CPlayerStatistics::CPlayerStatistics(common::TIndex player_id) :
			m_player_id(player_id),
			m_drop_count(0),
			m_place_count(0),
			m_speak_count(0),
			m_drop_duration(),
			m_place_duration(),
			m_speak_duration(),
			m_total_duration()
		{

		}

		CPlayerStatistics::~CPlayerStatistics() noexcept
		{

		}

		common::TIndex CPlayerStatistics::player_id() const noexcept
		{
			return this->m_player_id;
		}

		common::TCount CPlayerStatistics::drop_count() const noexcept
		{
			return this->m_drop_count;
		}

		void CPlayerStatistics::drop_count(common::TCount count)
		{
			this->m_drop_count = count;
		}

		void CPlayerStatistics::increment_drop_count()
		{
			++this->m_drop_count;
		}

		common::TCount CPlayerStatistics::place_count() const noexcept
		{
			return this->m_place_count;
		}

		void CPlayerStatistics::place_count(common::TCount count)
		{
			this->m_place_count = count;
		}

		void CPlayerStatistics::increment_place_count()
		{
			++this->m_place_count;
		}

		common::TCount CPlayerStatistics::speak_count() const noexcept
		{
			return this->m_speak_count;
		}

		void CPlayerStatistics::speak_count(common::TCount count)
		{
			this->m_speak_count = count;
		}

		void CPlayerStatistics::increment_speak_count()
		{
			++this->m_speak_count;
		}

		const boost::posix_time::time_duration& CPlayerStatistics::drop_duration() const noexcept
		{
			return this->m_drop_duration;
		}

		void CPlayerStatistics::drop_duration(const boost::posix_time::time_duration& duration)
		{
			this->m_drop_duration = duration;
		}

		void CPlayerStatistics::cumul_drop_duration(const boost::posix_time::time_duration& delta)
		{
			this->m_drop_duration += delta;
		}

		const boost::posix_time::time_duration& CPlayerStatistics::place_duration() const noexcept
		{
			return this->m_place_duration;
		}

		void CPlayerStatistics::place_duration(const boost::posix_time::time_duration& duration)
		{
			this->m_place_duration = duration;
		}

		void CPlayerStatistics::cumul_place_duration(const boost::posix_time::time_duration& delta)
		{
			this->m_place_duration += delta;
		}

		const boost::posix_time::time_duration& CPlayerStatistics::speak_duration() const noexcept
		{
			return this->m_speak_duration;
		}

		void CPlayerStatistics::speak_duration(const boost::posix_time::time_duration& duration)
		{
			this->m_speak_duration = duration;
		}

		void CPlayerStatistics::cumul_speak_duration(const boost::posix_time::time_duration& delta)
		{
			this->m_speak_duration += delta;
		}

		const boost::posix_time::time_duration& CPlayerStatistics::total_duration() const noexcept
		{
			return this->m_total_duration;
		}

		void CPlayerStatistics::total_duration(const boost::posix_time::time_duration& duration)
		{
			this->m_total_duration = duration;
		}

		void CPlayerStatistics::cumul_total_duration(const boost::posix_time::time_duration& delta)
		{
			this->m_total_duration += delta;
		}
	}
}
