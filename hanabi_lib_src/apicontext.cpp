#include "pch.h"
#include "apicontext.h"


namespace hanabi
{
	namespace common
	{
		CAPIContext::CAPIContext(const std::string& api_host, const std::string& api_port, const std::string& api_login, const std::string& api_password, std::size_t max_tentative, std::size_t tentative_delay) :
			m_api_host(api_host),
			m_api_port(api_port),
			m_api_login(api_login),
			m_api_password(api_password),
			m_max_tentative(max_tentative),
			m_tentative_delay(tentative_delay)
		{

		}

		CAPIContext::~CAPIContext() noexcept
		{

		}

		const std::string& CAPIContext::api_host() const noexcept
		{
			return this->m_api_host;
		}

		const std::string& CAPIContext::api_port() const noexcept
		{
			return this->m_api_port;
		}

		const std::string& CAPIContext::api_login() const noexcept
		{
			return this->m_api_login;
		}

		const std::string& CAPIContext::api_password() const noexcept
		{
			return this->m_api_password;
		}

		std::size_t CAPIContext::max_tentative() const noexcept
		{
			assert(this->m_max_tentative > 0);
			return this->m_max_tentative;
		}

		std::size_t CAPIContext::tentative_delay() const noexcept
		{
			assert(this->m_tentative_delay > 0);
			return this->m_tentative_delay;
		}

		void CAPIContext::max_tentative(std::size_t max_tentative)
		{
			assert(max_tentative > 0);
			this->m_max_tentative = max_tentative;
		}

		void CAPIContext::tentative_delay(std::size_t tentative_delay)
		{
			assert(tentative_delay > 0);
			this->m_tentative_delay = tentative_delay;
		}
	}
}