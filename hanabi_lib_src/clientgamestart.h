#ifndef CLIENT_GAME_START_H_247EC632_DD94_434D_ABF7_F3F7E5575F5D
#define CLIENT_GAME_START_H_247EC632_DD94_434D_ABF7_F3F7E5575F5D
#pragma once

#include "updatable.h"
#include "types.h"


namespace hanabi
{
	namespace client_side
	{
		class CClientGameStart : public common::IUpdatable
		{
		public:
			CClientGameStart();
			virtual ~CClientGameStart() noexcept;


			void game_uuid(const boost::uuids::uuid& game_uuid);
			const boost::uuids::uuid& game_uuid() const noexcept;

			void timestamp(const boost::posix_time::ptime& timestamp);
			const boost::posix_time::ptime& timestamp() const noexcept;

			void game_option_id(const common::TGameOptionId& game_option_id);
			const common::TGameOptionId& game_option_id() const noexcept;

		private:
			virtual void virtual_update(const boost::json::value& root);

			boost::uuids::uuid m_game_uuid;
			boost::posix_time::ptime m_timestamp;
			common::TGameOptionId m_game_option_id;
		};
	}
}

#endif