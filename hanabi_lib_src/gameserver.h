#ifndef GAME_SERVER_H_DCA211F4_28D3_410A_89A3_D4D538107C43
#define GAME_SERVER_H_DCA211F4_28D3_410A_89A3_D4D538107C43
#pragma once

#include "action.h"
#include "apigateway.h"
#include "card.h"
#include "compatibility.h"
#include "exception.h"
#include "playergateway.h"
#include "servergameend.h"
#include "servergameoption.h"
#include "servergamestart.h"
#include "servergamestate.h"
#include "serverplayer.h"
#include "types.h"


namespace hanabi
{
	namespace server_side
	{
		class CGameServer
		{
		public:
			typedef std::map<common::TIndex, CServerPlayer> TPlayerMap;

			// Callback
			typedef std::function<void(const CGameOption& game_option, const boost::posix_time::ptime& start_timestamp)> TCallbackOnGameStarts;
			typedef std::function<void(const boost::posix_time::ptime& start_timestamp, const boost::posix_time::ptime& end_timestamp)> TCallbackOnGameEnds;
			typedef std::function<void(const CGameOption& game_option)> TCallbackOnSetupGameStarts;
			typedef std::function<void(const CGameState& game_state, const CGameOption& game_option)> TCallbackOnSetupGameEnds;
			typedef std::function<void()> TCallbackOnClosingConnectionStarts;
			typedef std::function<void()> TCallbackOnClosingConnectionEnds;
			typedef std::function<void(common::TIndex player_index, const std::shared_ptr<common::IAction>& p_action, const EHanabiException& error, const TPlayerMap& peer_info)> TCallbackOnInvalidAction;
			typedef std::function<void(common::TCount player_turn_count, common::TCount game_turn_count, const CGameState& game_state, const TPlayerMap& peer_info)> TCallbackOnTurnStarts;
			typedef std::function<void(common::TCount player_turn_count, common::TCount game_turn_count, const CGameState& game_state, const TPlayerMap& peer_info)> TCallbackOnTurnEnds;
			typedef std::function<void(const boost::posix_time::ptime& start_of_game_loop_timestamp)> TCallbackOnStartOfGameLoop;
			typedef std::function<void(const CGameEnd& game_end, const CGameState& game_state)> TCallbackOnEndOfGameLoop;
			typedef std::function<void(common::TIndex player_index, const CGameEnd& game_end, const CGameState& game_state, const TPlayerMap& peer_info)> TCallbackOnGameEndConditionPlayerStuck;
			typedef std::function<void(common::TCount error_token_count, const CGameEnd& game_end, const CGameState& game_state, const TPlayerMap& peer_info)> TCallbackOnGameEndConditionMaxError;
			typedef std::function<void(common::TCount score, const CGameEnd& game_end, const CGameState& game_state, const TPlayerMap& peer_info)> TCallbackOnGameEndConditionMaxScore;
			typedef std::function<void(common::TIndex player_index, const CGameState& game_state, const TPlayerMap& peer_info)> TCallbackOnRetrieveActionStarts;
			typedef std::function<void(common::TIndex player_index, const std::shared_ptr<common::IAction>& p_action, const CGameState& game_state, const TPlayerMap& peer_info)> TCallbackOnRetrieveActionEnds;
			typedef std::function<void(const common::CardColor& completed_color, const CGameState& game_state)> TCallbackOnGivingBonusStarts;
			typedef std::function<void(const common::CardColor& completed_color, const CGameState& game_state)> TCallbackOnGivingBonusEnds;
			typedef std::function<void(common::TIndex player_id)> TCallbackOnSetupPeerStarts;
			typedef std::function<void(common::TIndex player_id, const CPeerInfo& peer_info, const CGameOption& game_option, const CGameStart& game_start)> TCallbackOnSetupPeerEnds;
			typedef std::function<void(common::TIndex player_index, const std::shared_ptr<common::IAction>& p_action, const CGameState& game_state, const TPlayerMap& peer_info)> TCallbackOnProcessPlayerActionStarts;
			typedef std::function<void(common::TIndex player_index, const std::shared_ptr<common::IAction>& p_action, bool valid_action, const CGameState& game_state, const TPlayerMap& peer_info)> TCallbackOnProcessPlayerActionEnds;
			typedef std::function<void(common::TIndex current_player_id, const common::CCard& dropped_card, const CGameState& game_state)> TCallbackOnProcessDropStarts;
			typedef std::function<void(common::TIndex current_player_id, const common::CCard& dropped_card, const CGameState& game_state)> TCallbackOnProcessDropEnds;
			typedef std::function<void(common::TIndex current_player_id, const common::CCard& card_to_place, const CGameState& game_state)> TCallbackOnProcessPlaceStarts;
			typedef std::function<void(common::TIndex current_player_id, const common::CCard& card_to_place, const CGameState& game_state)> TCallbackOnProcessPlaceEnds;
			typedef std::function<void(common::TIndex player_id, const common::CCard& card_to_place, const std::shared_ptr<common::CardColor>& p_completed_color, const CGameState& game_state)> TCallbackOnPlayerPlace;
			typedef std::function<void(common::TIndex player_id, const common::CCard& card_to_trash, const CGameState& game_state)> TCallbackOnPlayerDrop;
			typedef std::function<void(common::TIndex player_id, const common::CCard& drawn_card, const CGameState& game_state)> TCallbackOnPlayerDraw;
			typedef std::function<void(common::TIndex player_index,const boost::posix_time::ptime& request_timestamp, const CGameState& game_state, const TPlayerMap& peer_info)> TCallbackOnPlayerActionRoundtripStarts;
			typedef std::function<void(common::TIndex player_index, const std::shared_ptr<common::IAction>& p_action, const boost::posix_time::ptime& request_timestamp, const boost::posix_time::ptime& response_timestamp, const CGameState& game_state, const TPlayerMap& peer_info)> TCallbackOnPlayerActionRoundtripEnds;
			typedef std::function<void(common::TIndex player_id)> TCallbackOnPlayerHello;
			typedef std::function<void(common::TIndex player_id)> TCallbackOnPeerConnecting;
			typedef std::function<void(common::TIndex player_id, const CPeerInfo& peer_info)> TCallbackOnPeerRejected;
			typedef std::function<void(common::TIndex player_id, const CPeerInfo& peer_info)> TCallbackOnPeerConnected;
			typedef std::function<void()> TCallbackOnLobbyStarts;
			typedef std::function<void(const TPlayerMap& players)> TCallbackOnLobbyEnds;
			typedef std::function<void(const server_side::CGameStart& game_start)> TCallbackOnSaveGameStart;
			typedef std::function<void(const server_side::CGameStart& game_start, const server_side::CPeerInfo& peer_info)> TCallbackOnSavePeerInfo;
			typedef std::function<void(const server_side::CGameStart& game_start, const server_side::CGameState& game_state)> TCallbackOnSaveGameState;
			typedef std::function<void(const server_side::CGameStart& game_start, const common::IAction& action, common::TCount player_turn_count, common::TCount game_turn_count, common::TIndex current_player_id, const boost::posix_time::ptime& request_timestamp, const boost::posix_time::ptime& response_timestamp)> TCallbackOnSavePlayerAction;
			typedef std::function<void(const server_side::CGameStart& game_start, const server_side::CGameEnd& game_end)> TCallbackOnSaveGameEnd;
			typedef std::function<void(EServerError& e)> TCallbackOnServerError;
			typedef std::function<void(EHanabiException& e)> TCallbackOnHanabiError;
			typedef std::function<void(std::exception& e)> TCallbackOnStdError;
			typedef std::function<void()> TCallbackOnError;


			CGameServer(const CGameOption& game_option, std::shared_ptr<common::CAPIGateway> p_api_gateway, boost::asio::io_context& io_context, const boost::asio::ip::port_type& server_port, bool shuffle_peers, const common::TTagList& tags);
			virtual ~CGameServer() noexcept;


			void run();

			const CGameState& game_state() const noexcept;
			CGameState& game_state() noexcept;

			const CGameOption& game_option() const noexcept;
			const CGameStart& game_start() const noexcept;

			const CGameEnd& game_end() const noexcept;
			CGameEnd& game_end() noexcept;

			const TPlayerMap& players() const noexcept;
			TPlayerMap& players() noexcept;
			const CServerPlayer& player(common::TIndex player_id) const;
			const std::shared_ptr<IPlayerGateway>& gateway(common::TIndex player_id) const;
			const CPeerInfo& peer_info(common::TIndex player_id) const;

			const std::string& welcome_message() const noexcept;
			void welcome_message(const std::string& message);

			const common::TTagList& tags() const noexcept;

			const boost::posix_time::ptime& start_timestamp() const noexcept;
			const boost::posix_time::ptime& end_timestamp() const noexcept;
			
			common::TCount turn_count() const;
			common::TScore score() const;
			common::TScore current_max_score() const;

			bool is_game_over() const;
			bool is_game_ended() const;
			bool allowed_to_speak() const;
			bool allowed_to_drop() const;
			bool is_a_valid_place(const common::CCard& card) const;

			const std::shared_ptr<common::CardColor>& completed_color() const noexcept;
			void completed_color(const std::shared_ptr<common::CardColor>& p_color);

			bool error_committed() const noexcept;
			void error_committed(bool committed);


			void capture_start_timestamp();
			void capture_end_timestamp();


			// Callbacks
			void set_callback_on_game_starts(const TCallbackOnGameStarts& callback);
			void set_callback_on_game_ends(const TCallbackOnGameEnds& callback);
			void set_callback_on_lobby_starts(const TCallbackOnLobbyStarts& callback);
			void set_callback_on_lobby_ends(const TCallbackOnLobbyEnds& callback);
			void set_callback_on_setup_game_starts(const TCallbackOnSetupGameStarts& callback);
			void set_callback_on_setup_game_ends(const TCallbackOnSetupGameEnds& callback);
			void set_callback_on_closing_connection_starts(const TCallbackOnClosingConnectionStarts& callback);
			void set_callback_on_closing_connection_ends(const TCallbackOnClosingConnectionEnds& callback);
			void set_callback_on_invalid_action(const TCallbackOnInvalidAction& callback);
			void set_callback_on_turn_starts(const TCallbackOnTurnStarts& callback);
			void set_callback_on_turn_ends(const TCallbackOnTurnEnds& callback);
			void set_callback_on_start_of_game_loop(const TCallbackOnStartOfGameLoop& callback);
			void set_callback_on_end_of_game_loop(const TCallbackOnEndOfGameLoop& callback);
			void set_callback_on_game_end_condition_player_stuck(const TCallbackOnGameEndConditionPlayerStuck& callback);
			void set_callback_on_game_end_condition_max_error(const TCallbackOnGameEndConditionMaxError& callback);
			void set_callback_on_game_end_condition_max_score(const TCallbackOnGameEndConditionMaxScore& callback);
			void set_callback_on_retrieve_action_starts(const TCallbackOnRetrieveActionStarts& callback);
			void set_callback_on_retrieve_action_ends(const TCallbackOnRetrieveActionEnds& callback);
			void set_callback_on_giving_bonus_starts(const TCallbackOnGivingBonusStarts& callback);
			void set_callback_on_giving_bonus_ends(const TCallbackOnGivingBonusEnds& callback);
			void set_callback_on_setup_peer_starts(const TCallbackOnSetupPeerStarts& callback);
			void set_callback_on_setup_peer_ends(const TCallbackOnSetupPeerEnds& callback);
			void set_callback_on_process_player_action_starts(const TCallbackOnProcessPlayerActionStarts& callback);
			void set_callback_on_process_player_action_ends(const TCallbackOnProcessPlayerActionEnds& callback);
			void set_callback_on_process_drop_starts(const TCallbackOnProcessDropStarts& callback);
			void set_callback_on_process_drop_ends(const TCallbackOnProcessDropEnds& callback);
			void set_callback_on_process_place_starts(const TCallbackOnProcessPlaceStarts& callback);
			void set_callback_on_process_place_ends(const TCallbackOnProcessPlaceEnds& callback);
			void set_callback_on_player_place(const TCallbackOnPlayerPlace& callback);
			void set_callback_on_player_drop(const TCallbackOnPlayerDrop& callback);
			void set_callback_on_player_draw(const TCallbackOnPlayerDraw& callback);
			void set_callback_on_player_action_roundtrip_starts(const TCallbackOnPlayerActionRoundtripStarts& callback);
			void set_callback_on_player_action_roundtrip_ends(const TCallbackOnPlayerActionRoundtripEnds& callback);
			void set_callback_on_player_hello(const TCallbackOnPlayerHello& callback);
			void set_callback_on_peer_connecting(const TCallbackOnPeerConnecting& callback);
			void set_callback_on_peer_rejected(const TCallbackOnPeerRejected& callback);
			void set_callback_on_peer_connected(const TCallbackOnPeerConnected& callback);
			void set_callback_on_save_game_start_starts(const TCallbackOnSaveGameStart& callback);
			void set_callback_on_save_game_start_ends(const TCallbackOnSaveGameStart& callback);
			void set_callback_on_save_peer_info_starts(const TCallbackOnSavePeerInfo& callback);
			void set_callback_on_save_peer_info_ends(const TCallbackOnSavePeerInfo& callback);
			void set_callback_on_save_game_state_starts(const TCallbackOnSaveGameState& callback);
			void set_callback_on_save_game_state_ends(const TCallbackOnSaveGameState& callback);
			void set_callback_on_save_player_action_starts(const TCallbackOnSavePlayerAction& callback);
			void set_callback_on_save_player_action_ends(const TCallbackOnSavePlayerAction& callback);
			void set_callback_on_save_game_end_starts(const TCallbackOnSaveGameEnd& callback);
			void set_callback_on_save_game_end_ends(const TCallbackOnSaveGameEnd& callback);

			void set_callback_on_server_error(const TCallbackOnServerError& callback);
			void set_callback_on_hanabi_error(const TCallbackOnHanabiError& callback);
			void set_callback_on_std_error(const TCallbackOnStdError& callback);
			void set_callback_on_error(const TCallbackOnError& callback);

		private:
			void lobby();
			void setup_game();
			void close_connections();

			void save_game_start();
			void save_peer_info(const CPeerInfo& peer_info);
			void save_current_game_state();
			void save_player_action(const std::shared_ptr<common::IAction>& p_action, const boost::posix_time::ptime& request_timestamp, const boost::posix_time::ptime& response_timestamp);
			void save_game_end();

			void give_bonus(const common::CardColor& completed_color);

			void process_player_action(const std::shared_ptr<common::IAction>& p_action);
			void process_drop(const common::CDrop& action);
			void process_place(const common::CPlace& action);
			void process_speak(const common::CSpeak& action);
			void stack_player_action(const std::shared_ptr<common::IAction>& p_action, const boost::posix_time::ptime& request_timestamp, const boost::posix_time::ptime& response_timestamp);

			void broadcast_game_update() const;
			void broadcast_game_end() const;
			void broadcast_action_place(common::TIndex current_player_id, const common::CCard& placed_card) const;
			void broadcast_action_drop(common::TIndex current_player_id, const common::CCard& droped_card) const;
			void broadcast_action_speak(common::TIndex current_player_id, common::TIndex target_player_id, common::TValue value) const;
			void broadcast_action_speak(common::TIndex current_player_id, common::TIndex target_player_id, const common::CardColor& color) const;
			void broadcast_bonus_given(common::TIndex current_player_id, const common::CardColor& color) const;
			void broadcast_error_token_given(common::TIndex current_player_id, common::TCount error_token_count) const;

			void place(const common::CCard& card_to_place);
			void trash(const common::CCard& card_to_trash);
			void draw_from_stack(common::TIndex player_id);

			common::TIndex get_available_peer_id() const;

			boost::asio::io_context& m_io_context;
			const boost::asio::ip::tcp::endpoint m_endpoint;
			const common::TCount m_player_count;
			const bool m_shuffle_peers;
			CGameState m_game_state;
			CGameEnd m_game_end;
			TPlayerMap m_player_container;
			std::string m_welcome_message;
			boost::posix_time::ptime m_start_timestamp;
			boost::posix_time::ptime m_end_timestamp;
			std::shared_ptr<common::CardColor> m_p_completed_color;
			bool m_error_committed;
			std::shared_ptr<common::CAPIGateway> m_p_api_gateway;
			const CCompatibilityChecker m_compatibility_checker;


			// Callbacks
			void call_on_player_hello(common::TIndex player_id) const;
			void call_on_peer_connecting(common::TIndex player_id) const;
			void call_on_peer_rejected(common::TIndex player_id, const CPeerInfo& peer_info) const;
			void call_on_peer_connected(common::TIndex player_id, const CPeerInfo& peer_info) const;
			void call_on_game_starts() const;
			void call_on_game_ends() const;
			void call_on_lobby_starts() const;
			void call_on_lobby_ends() const;
			void call_on_setup_game_starts() const;
			void call_on_setup_game_ends() const;
			void call_on_closing_connection_starts() const;
			void call_on_closing_connection_ends() const;
			void call_on_invalid_action(const std::shared_ptr<common::IAction>& p_action, const EHanabiException& error) const;
			void call_on_turn_starts() const;
			void call_on_turn_ends() const;
			void call_on_start_of_game_loop() const;
			void call_on_end_of_game_loop() const;
			void call_on_game_end_condition_player_stuck() const;
			void call_on_game_end_condition_max_error() const;
			void call_on_game_end_condition_max_score() const;
			void call_on_retrieve_action_starts() const;
			void call_on_retrieve_action_ends(const std::shared_ptr<common::IAction>& p_action) const;
			void call_on_giving_bonus_starts(const common::CardColor& completed_color) const;
			void call_on_giving_bonus_ends(const common::CardColor& completed_color) const;
			void call_on_setup_peer_starts(common::TIndex player_id) const;
			void call_on_setup_peer_ends(common::TIndex player_id, const CPeerInfo& peer_info) const;
			void call_on_process_player_action_starts(const std::shared_ptr<common::IAction>& p_action) const;
			void call_on_process_player_action_ends(const std::shared_ptr<common::IAction>& p_action, bool valid_action) const;
			void call_on_process_drop_starts(const common::CCard& dropped_card) const;
			void call_on_process_drop_ends(const common::CCard& dropped_card) const;
			void call_on_process_place_starts(const common::CCard& card_to_place) const;
			void call_on_process_place_ends(const common::CCard& card_to_place) const;
			void call_on_player_place(const common::CCard& card_to_place, const std::shared_ptr<common::CardColor>& p_completed_color) const;
			void call_on_player_drop(const common::CCard& card_to_drop) const;
			void call_on_player_draw(const common::CCard& drawn_card) const;
			void call_on_player_action_roundtrip_starts(const boost::posix_time::ptime& request_timestamp) const;
			void call_on_player_action_roundtrip_ends(const std::shared_ptr<common::IAction>& p_action, const boost::posix_time::ptime& request_timestamp, const boost::posix_time::ptime& response_timestamp) const;
			void call_on_save_game_start_starts(const server_side::CGameStart& game_start) const;
			void call_on_save_game_start_ends(const server_side::CGameStart& game_start) const;
			void call_on_save_peer_info_starts(const server_side::CGameStart& game_start, const server_side::CPeerInfo& peer_info) const;
			void call_on_save_peer_info_ends(const server_side::CGameStart& game_start, const server_side::CPeerInfo& peer_info) const;
			void call_on_save_game_state_starts(const server_side::CGameStart& game_start, const server_side::CGameState& game_state) const;
			void call_on_save_game_state_ends(const server_side::CGameStart& game_start, const server_side::CGameState& game_state) const;
			void call_on_save_player_action_starts(const server_side::CGameStart& game_start, const common::IAction& action, common::TCount player_turn_count, common::TCount game_turn_count, common::TIndex current_player_id, const boost::posix_time::ptime& request_timestamp, const boost::posix_time::ptime& response_timestamp) const;
			void call_on_save_player_action_ends(const server_side::CGameStart& game_start, const common::IAction& action, common::TCount player_turn_count, common::TCount game_turn_count, common::TIndex current_player_id, const boost::posix_time::ptime& request_timestamp, const boost::posix_time::ptime& response_timestamp) const;
			void call_on_save_game_end_starts(const server_side::CGameStart& game_start, const server_side::CGameEnd& game_end) const;
			void call_on_save_game_end_ends(const server_side::CGameStart& game_start, const server_side::CGameEnd& game_end) const;

			void call_on_server_error(EServerError& e) const;
			void call_on_hanabi_error(EHanabiException& e) const;
			void call_on_std_error(std::exception& e) const;
			void call_on_error() const;


			TCallbackOnGameStarts m_on_game_starts;
			TCallbackOnGameEnds m_on_game_ends;
			TCallbackOnLobbyStarts m_on_lobby_starts;
			TCallbackOnLobbyEnds m_on_lobby_ends;
			TCallbackOnSetupGameStarts m_on_setup_game_starts;
			TCallbackOnSetupGameEnds m_on_setup_game_ends;
			TCallbackOnClosingConnectionStarts m_on_closing_connection_starts;
			TCallbackOnClosingConnectionEnds m_on_closing_connection_ends;
			TCallbackOnInvalidAction m_on_invalid_action;
			TCallbackOnTurnStarts m_on_turn_starts;
			TCallbackOnTurnEnds m_on_turn_ends;
			TCallbackOnStartOfGameLoop m_on_start_of_game_loop;
			TCallbackOnEndOfGameLoop m_on_end_of_game_loop;
			TCallbackOnGameEndConditionPlayerStuck m_on_game_end_condition_player_stuck;
			TCallbackOnGameEndConditionMaxError m_on_game_end_condition_max_error;
			TCallbackOnGameEndConditionMaxScore m_on_game_end_condition_max_score;
			TCallbackOnRetrieveActionStarts m_on_retrieve_action_starts;
			TCallbackOnRetrieveActionEnds m_on_retrieve_action_ends;
			TCallbackOnGivingBonusStarts m_on_giving_bonus_starts;
			TCallbackOnGivingBonusEnds m_on_giving_bonus_ends;
			TCallbackOnSetupPeerStarts m_on_setup_peer_starts;
			TCallbackOnSetupPeerEnds m_on_setup_peer_ends;
			TCallbackOnProcessPlayerActionStarts m_on_process_player_action_starts;
			TCallbackOnProcessPlayerActionEnds m_on_process_player_action_ends;
			TCallbackOnProcessDropStarts m_on_process_drop_starts;
			TCallbackOnProcessDropEnds m_on_process_drop_ends;
			TCallbackOnProcessPlaceStarts m_on_process_place_starts;
			TCallbackOnProcessPlaceEnds m_on_process_place_ends;
			TCallbackOnPlayerPlace m_on_player_place;
			TCallbackOnPlayerDrop m_on_player_drop;
			TCallbackOnPlayerDraw m_on_player_draw;
			TCallbackOnPlayerActionRoundtripStarts m_on_player_action_roundtrip_starts;
			TCallbackOnPlayerActionRoundtripEnds m_on_player_action_roundtrip_ends;
			TCallbackOnPlayerHello m_on_player_hello;
			TCallbackOnPeerConnecting m_on_peer_connecting;
			TCallbackOnPeerRejected m_on_peer_rejected;
			TCallbackOnPeerConnected m_on_peer_connected;
			TCallbackOnSaveGameStart m_on_save_game_start_starts;
			TCallbackOnSaveGameStart m_on_save_game_start_ends;
			TCallbackOnSavePeerInfo m_on_save_peer_info_starts;
			TCallbackOnSavePeerInfo m_on_save_peer_info_ends;
			TCallbackOnSaveGameState m_on_save_game_state_starts;
			TCallbackOnSaveGameState m_on_save_game_state_ends;
			TCallbackOnSavePlayerAction m_on_save_player_action_starts;
			TCallbackOnSavePlayerAction m_on_save_player_action_ends;
			TCallbackOnSaveGameEnd m_on_save_game_end_starts;
			TCallbackOnSaveGameEnd m_on_save_game_end_ends;

			TCallbackOnServerError m_on_server_error;
			TCallbackOnHanabiError m_on_hanabi_error;
			TCallbackOnStdError m_on_std_error;
			TCallbackOnError m_on_error;
		};
	}
}

#endif
