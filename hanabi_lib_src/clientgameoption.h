#ifndef CLIENTGAMEOPTION_H_FB641E3D_AA92_40B6_8DED_861849EBF593
#define CLIENTGAMEOPTION_H_FB641E3D_AA92_40B6_8DED_861849EBF593
#pragma once

#include "coloroption.h"
#include "updatable.h"


namespace hanabi
{
	namespace client_side
	{
		class CGameOption : public common::IUpdatable
		{
		public:
			CGameOption();
			virtual ~CGameOption() noexcept;


			void game_option_id(const common::TGameOptionId& game_option_id);
			const common::TGameOptionId& game_option_id() const noexcept;

			void timestamp(const boost::posix_time::ptime& timestamp);
			const boost::posix_time::ptime& timestamp() const noexcept;

			void player_count(common::TCount count);
			common::TCount player_count() const noexcept;

			void player_hand_max_size(common::TCount size);
			common::TCount player_hand_max_size() const noexcept;

			void start_player_id(common::TIndex player_id);
			common::TIndex start_player_id() const noexcept;

			void max_error_token_count(common::TCount count);
			common::TCount max_error_token_count() const noexcept;

			void max_speak_token_count(common::TCount count);
			common::TCount max_speak_token_count() const noexcept;

			void start_error_token_count(common::TCount count);
			common::TCount start_error_token_count() const noexcept;

			void start_speak_token_count(common::TCount count);
			common::TCount start_speak_token_count() const noexcept;

			void min_card_value(common::TValue value);
			common::TValue min_card_value() const noexcept;

			void max_card_value(common::TValue value);
			common::TValue max_card_value() const noexcept;

			void game_max_score(common::TScore score);
			common::TScore game_max_score() const noexcept;

			void callable_colors(const common::TColorSet& colors);
			const common::TColorSet& callable_colors() const noexcept;

			void color_option(const common::TColorOptionMap& color_option);
			const common::TColorOptionMap& color_option() const noexcept;

			void stack_size(std::size_t size);
			std::size_t stack_size() const noexcept;

		private:
			virtual void virtual_update(const boost::json::value& root);

			common::TGameOptionId m_game_option_id;
			boost::posix_time::ptime m_timestamp;
			common::TCount m_player_count;
			common::TCount m_player_hand_max_size;
			common::TIndex m_start_player_id;
			common::TCount m_max_error_token_count;
			common::TCount m_max_speak_token_count;
			common::TCount m_start_error_token_count;
			common::TCount m_start_speak_token_count;
			common::TValue m_min_card_value;
			common::TValue m_max_card_value;
			common::TScore m_game_max_score;
			common::TColorSet m_callable_colors;
			common::TColorOptionMap m_color_option;
			std::size_t m_stack_size;
		};
	}
}

#endif