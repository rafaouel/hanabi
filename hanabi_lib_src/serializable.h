#ifndef SERIALIZABLE_CBE26EB0_F2DA_4ED8_881F_9FC412187E2B
#define SERIALIZABLE_CBE26EB0_F2DA_4ED8_881F_9FC412187E2B
#pragma once


namespace hanabi
{
	namespace common
	{
		/// <summary>
		/// Class to implement a way to load and save an object from / to a json (and save it in a database, for instance). There is no data omited.
		/// </summary>
		class ISerializable
		{
		public:
			ISerializable();
			virtual ~ISerializable() noexcept;


			// Save to a json value
			boost::json::value to_json() const;

			// Load from a json value
			void from_json(const boost::json::value& value);

		private:
			virtual boost::json::value serialize_to_json() const = 0;

			virtual void parse_from_json(const boost::json::value& value) = 0;
		};


		/// <summary>
		/// Load a json file and parse it
		/// </summary>
		/// <param name="is">stream to read from</param>
		/// <param name="ec">error data</param>
		/// <returns>json value containing the parsed data</returns>
		boost::json::value read_json(std::istream& is, boost::system::error_code& ec);

		/// <summary>
		/// Save a value into a json file
		/// </summary>
		/// <param name="os">stream to write in</param>
		/// <param name="root">data as a json value</param>
		void write_json(std::ostream& os, const boost::json::value& root);
	}
}
#endif