#include "pch.h"
#include "clientgamestate.h"


namespace hanabi
{
	namespace client_side
	{
		CGameState::CGameState() :
			m_game_option(),
			m_player_turn_count(0),
			m_speak_token_count(0),
			m_error_token_count(0),
			m_current_player_index(common::invalid_index)
		{

		}

		CGameState::~CGameState() noexcept
		{

		}

		void CGameState::game_option(const CGameOption& game_option)
		{
			this->m_game_option = game_option;
		}

		const CGameOption& CGameState::game_option() const noexcept
		{
			return this->m_game_option;
		}

		CGameOption& CGameState::game_option() noexcept
		{
			return this->m_game_option;
		}

		common::TCount CGameState::player_turn_count() const noexcept
		{
			return this->m_player_turn_count;
		}

		void CGameState::player_turn_count(common::TCount count)
		{
			this->m_player_turn_count = count;
		}

		common::TCount CGameState::game_turn_count() const noexcept
		{
			return this->m_game_turn_count;
		}

		void CGameState::game_turn_count(common::TCount count)
		{
			this->m_game_turn_count = count;
		}

		const CBoard& CGameState::board() const noexcept
		{
			return this->m_board;
		}

		CBoard& CGameState::board() noexcept
		{
			return this->m_board;
		}

		const CTrash& CGameState::trash() const noexcept
		{
			return this->m_trash;
		}

		CTrash& CGameState::trash() noexcept
		{
			return this->m_trash;
		}

		common::TCount CGameState::speak_token_count() const noexcept
		{
			return this->m_speak_token_count;
		}

		void CGameState::speak_token_count(common::TCount count)
		{
			this->m_speak_token_count = count;
		}

		common::TCount CGameState::error_token_count() const noexcept
		{
			return this->m_error_token_count;
		}

		void CGameState::error_token_count(common::TCount count)
		{
			this->m_error_token_count = count;
		}

		common::TIndex CGameState::current_player_id() const noexcept
		{
			assert(common::is_valid_index(this->m_current_player_index));
			return this->m_current_player_index;
		}

		void CGameState::current_player_id(common::TIndex player_id)
		{
			assert(common::is_valid_index(player_id));
			this->m_current_player_index = player_id;
		}

		const CStack& CGameState::stack() const noexcept
		{
			return this->m_stack;
		}

		CStack& CGameState::stack() noexcept
		{
			return this->m_stack;
		}

		const CPlayerHand& CGameState::other_hands() const noexcept
		{
			return this->m_other_hands;
		}

		CPlayerHand& CGameState::other_hands() noexcept
		{
			return this->m_other_hands;
		}

		const CCardInformation& CGameState::information_collection() const noexcept
		{
			return this->m_informations;
		}

		CCardInformation& CGameState::information_collection() noexcept
		{
			return this->m_informations;
		}

		void CGameState::virtual_update(const boost::json::value& root)
		{
			const boost::json::object& root_object = root.as_object();

			this->player_turn_count(root_object.at("playerTurnCount").as_int64());
			this->game_turn_count(root_object.at("gameTurnCount").as_int64());
			this->speak_token_count(root_object.at("speakTokenCount").as_int64());
			this->error_token_count(root_object.at("errorTokenCount").as_int64());
			this->current_player_id(root_object.at("currentPlayerId").as_int64());

			this->board().update(root_object.at("board"));
			
			this->trash().update(root_object.at("trash"));

			this->stack().update(root_object.at("stack"));

			this->other_hands().update(root_object.at("playerHand"));

			this->information_collection().update(root_object.at("playerCardInfo"));
		}
	}
}
