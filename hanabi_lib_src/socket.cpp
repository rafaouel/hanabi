#include "pch.h"
#include "socket.h"

namespace hanabi
{
	namespace network
	{
		ISocket::ISocket(boost::asio::io_service& io_context) :
			m_socket(io_context)
		{

		}

		ISocket::~ISocket() noexcept
		{
			if (this->m_socket.is_open())
				this->m_socket.close();
		}

		void ISocket::init()
		{
			this->virtual_init();
		}

		void ISocket::close()
		{
			if (this->m_socket.is_open())
				this->m_socket.close();
		}

		void ISocket::send(const network::CPacket& packet)
		{
			const std::size_t count = boost::asio::write(this->m_socket, boost::asio::buffer(packet.data(), packet.length()));
		}

		network::CPacket ISocket::read()
		{
			CPacket packet;

			// Read the packet body length
			std::size_t body_length;
			{
				char data[CPacket::header_length + 1] = "";
				const std::size_t count = boost::asio::read(this->m_socket, boost::asio::buffer(data, CPacket::header_length));

				body_length = std::atoi(data);
				packet.body_length(body_length);
			}

			// Read the body
			char data[CPacket::max_body_length] = "";
			const std::size_t byte_count = boost::asio::read(this->m_socket, boost::asio::buffer(data, body_length));

			assert(byte_count < CPacket::max_body_length);

			std::memcpy(packet.body(), data, body_length);

			return packet;
		}

		boost::asio::ip::tcp::socket& ISocket::socket() noexcept
		{
			return this->m_socket;
		}

		const boost::asio::ip::tcp::socket& ISocket::socket() const noexcept
		{
			return this->m_socket;
		}
	}
}