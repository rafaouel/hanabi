#include "pch.h"
#include "sendable.h"

namespace hanabi
{
	namespace common
	{
		ISendable::ISendable()
		{

		}

		ISendable::~ISendable() noexcept
		{

		}

		boost::json::value ISendable::pack(TIndex target_player_id) const
		{
			return this->virtual_pack(target_player_id);
		}
	}
}
