#ifndef SERVER_GAME_OPTION_H_180FCD20_CC7F_4371_A030_ABBC902BB712
#define SERVER_GAME_OPTION_H_180FCD20_CC7F_4371_A030_ABBC902BB712
#pragma once

#include "card.h"
#include "sendable.h"
#include "serializable.h"
#include "types.h"


namespace hanabi
{
	namespace server_side
	{
		class CGameOption : public common::ISerializable, public common::ISendable
		{
		public:
			CGameOption();
			virtual ~CGameOption() noexcept;


			void game_option_id(const common::TGameOptionId& game_option_id);
			const common::TGameOptionId& game_option_id() const noexcept;

			void timestamp(const boost::posix_time::ptime& timestamp);
			const boost::posix_time::ptime& timestamp() const noexcept;

			void player_count(common::TCount count);
			common::TCount player_count() const noexcept;

			void player_hand_max_size(common::TCount size);
			common::TCount player_hand_max_size() const noexcept;

			void start_player_id(common::TIndex player_id);
			common::TIndex start_player_id() const noexcept;

			void max_error_token_count(common::TCount count);
			common::TCount max_error_token_count() const noexcept;

			void max_speak_token_count(common::TCount count);
			common::TCount max_speak_token_count() const noexcept;

			void start_error_token_count(common::TCount count);
			common::TCount start_error_token_count() const noexcept;

			void start_speak_token_count(common::TCount count);
			common::TCount start_speak_token_count() const noexcept;

			void min_card_value(common::TValue value);
			common::TValue min_card_value() const noexcept;

			void max_card_value(common::TValue value);
			common::TValue max_card_value() const noexcept;

			common::TScore game_max_score() const;

			void color_option(const common::TColorOptionMap& color_option);
			const common::TColorOptionMap& color_option() const noexcept;

			void callable_colors(const common::TColorSet& color_set);
			const common::TColorSet& callable_colors() const noexcept;

			void stack(const common::TCardStack& stack);
			const common::TCardStack& stack() const noexcept;

			void tags(const common::TTagList& tags);
			const common::TTagList& tags() const noexcept;

		private:
			virtual boost::json::value serialize_to_json() const;
			virtual void parse_from_json(const boost::json::value& root);
			virtual boost::json::value virtual_pack(common::TIndex target_player_id) const;

			boost::json::value prepack_data() const;

			common::TGameOptionId m_game_option_id;
			boost::posix_time::ptime m_timestamp;
			common::TCount m_player_count;
			common::TCount m_player_hand_max_size;
			common::TIndex m_start_player_id;
			common::TCount m_max_error_token_count;
			common::TCount m_max_speak_token_count;
			common::TCount m_start_error_token_count;
			common::TCount m_start_speak_token_count;
			common::TValue m_min_card_value;
			common::TValue m_max_card_value;
			common::TColorOptionMap m_color_option;
			common::TColorSet m_callable_colors;
			common::TCardStack m_stack;
			common::TTagList m_tags;
		};
	}
}

#endif