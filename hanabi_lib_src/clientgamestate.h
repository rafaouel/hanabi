#ifndef GAMESTATE_H_EAF459A6_805F_4138_A0C7_826D2CFBFE41
#define GAMESTATE_H_EAF459A6_805F_4138_A0C7_826D2CFBFE41
#pragma once

#include "clientboard.h"
#include "clientcardinformation.h"
#include "clientgameoption.h"
#include "clientplayerhand.h"
#include "clientstack.h"
#include "clienttrash.h"
#include "coloroption.h"
#include "servergamestate.h"
#include "updatable.h"


namespace hanabi
{
	namespace client_side
	{
		class CGameState : public common::IUpdatable
		{
		public:
			CGameState();
			virtual ~CGameState() noexcept;

			void game_option(const CGameOption& game_option);
			const CGameOption& game_option() const noexcept;
			CGameOption& game_option() noexcept;

			common::TCount player_turn_count() const noexcept;
			void player_turn_count(common::TCount count);

			common::TCount game_turn_count() const noexcept;
			void game_turn_count(common::TCount count);

			common::TCount speak_token_count() const noexcept;
			void speak_token_count(common::TCount count);

			common::TCount error_token_count() const noexcept;
			void error_token_count(common::TCount count);

			common::TIndex current_player_id() const noexcept;
			void current_player_id(common::TIndex player_id);

			const CBoard& board() const noexcept;
			CBoard& board() noexcept;

			const CTrash& trash() const noexcept;
			CTrash& trash() noexcept;

			const CStack& stack() const noexcept;
			CStack& stack() noexcept;

			const CPlayerHand& other_hands() const noexcept;
			CPlayerHand& other_hands() noexcept;

			const CCardInformation& information_collection() const noexcept;
			CCardInformation& information_collection() noexcept;


		private:
			virtual void virtual_update(const boost::json::value& root);


			CBoard m_board;
			CTrash m_trash;
			CStack m_stack;
			CPlayerHand m_other_hands;
			CCardInformation m_informations;
			CGameOption m_game_option;
			common::TCount m_player_turn_count;
			common::TCount m_game_turn_count;
			common::TCount m_speak_token_count;
			common::TCount m_error_token_count;
			common::TIndex m_current_player_index;
		};
	}
}


#endif