#ifndef TCPPLAYER_H_F47FE5D9_8177_4EF9_8284_643765B83DA4
#define TCPPLAYER_H_F47FE5D9_8177_4EF9_8284_643765B83DA4
#pragma once

#include "packetdispatcher.h"
#include "playergateway.h"
#include "serverpeerinfo.h"
#include "serversocket.h"
#include "action.h"


namespace hanabi
{
	namespace server_side
	{
		class CTCPPlayer : public IPlayerGateway
		{
		public:
			CTCPPlayer(boost::asio::io_context& io_context, boost::asio::ip::tcp::acceptor& acceptor);
			virtual ~CTCPPlayer() noexcept;


		private:
			virtual void virtual_init();
			virtual void virtual_close();

			virtual void virtual_say_hello(const std::string& message);
			virtual std::shared_ptr<server_side::CPeerInfo> virtual_retrieve_peer_info(common::TIndex player_id, const common::TVersionArray& server_version);
			virtual void virtual_send_peer_info_update(const server_side::CPeerInfo& peer_info, common::TIndex player_id);
			virtual void virtual_send_game_option(const CGameOption& game_option, common::TIndex player_id);
			virtual void virtual_send_game_update(const CGameState& game_state, common::TIndex player_id);
			virtual void virtual_send_game_end(const CGameEnd& game_end, common::TIndex player_id);
			virtual std::shared_ptr<common::IAction> virtual_retrieve_action();
			virtual void virtual_send_action_place_update(common::TIndex source_player_id, const common::CCard& placed_card);
			virtual void virtual_send_action_drop_update(common::TIndex source_player_id, const common::CCard& droped_card);
			virtual void virtual_send_action_speak_update(common::TIndex source_player_id, common::TIndex target_player_id, common::TValue value);
			virtual void virtual_send_action_speak_update(common::TIndex source_player_id, common::TIndex target_player_id, common::CardColor color);
			virtual void virtual_send_ok();
			virtual void virtual_send_not_ok(const std::string& reason);
			virtual void virtual_send_bonus_given_update(common::TIndex current_player_id, const common::CardColor& color);
			virtual void virtual_send_error_token_given_update(common::TIndex current_player_id, common::TCount error_token_count);

			virtual const std::string& virtual_get_name() const;
			virtual const boost::asio::ip::address& virtual_get_ip() const;

			virtual void send(const network::CPacket& packet);
			virtual network::CPacket receive();


			CSocket m_socket;

			std::string m_name;
			boost::asio::ip::address m_ip;
		};
	}
}

#endif