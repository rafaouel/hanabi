#ifndef SERVER_GAME_START_60758C95_DE7B_4A26_A8B7_E53F27F57EBA
#define SERVER_GAME_START_60758C95_DE7B_4A26_A8B7_E53F27F57EBA
#pragma once

#include "sendable.h"
#include "serializable.h"
#include "types.h"

namespace hanabi
{
	namespace server_side
	{
		class CGameStart : public common::ISerializable, public common::ISendable
		{
		public:
			CGameStart(const common::TGameOptionId& game_option_id, const common::TTagList& tags);
			virtual ~CGameStart() noexcept;


			const boost::uuids::uuid& game_uuid() const noexcept;
			const boost::posix_time::ptime& timestamp() const noexcept;

			void add_tag(const common::TTag& tag);
			void clear_tags();
			const common::TTagList& tags() const noexcept;

			const common::TGameOptionId& game_option_id() const noexcept;
			
		private:
			virtual boost::json::value serialize_to_json() const;
			virtual void parse_from_json(const boost::json::value& root);
			virtual boost::json::value virtual_pack(common::TIndex target_player_id) const;

			boost::json::value prepack_data() const;

			const boost::uuids::uuid m_game_uuid;
			const boost::posix_time::ptime m_timestamp;
			const common::TGameOptionId m_game_option_id;
			common::TTagList m_tags;
		};
	}
}

#endif