#ifndef PLAYERHAND_H_0E459501_8119_45C8_A997_49ED2C9F76A8
#define PLAYERHAND_H_0E459501_8119_45C8_A997_49ED2C9F76A8
#pragma once

#include "card.h"
#include "updatable.h"


namespace hanabi
{
	namespace client_side
	{
		class CPlayerHand : public common::IUpdatable
		{
		public:
			typedef common::TCardStack THand;
			typedef std::map<common::TIndex, THand> TPlayerHand;


			CPlayerHand();
			virtual ~CPlayerHand() noexcept;


			const TPlayerHand& other_hands() const noexcept;
			TPlayerHand& other_hands() noexcept;

			const THand& hand(common::TIndex player_id) const;


		private:
			virtual void virtual_update(const boost::json::value& root_value);


			TPlayerHand m_player_hand;
		};
	}
}

#endif