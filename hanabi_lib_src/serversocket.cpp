#include "pch.h"
#include "serversocket.h"


namespace hanabi
{
	namespace server_side
	{
		CSocket::CSocket(boost::asio::io_context& io_context, boost::asio::ip::tcp::acceptor& acceptor) :
			ISocket(io_context),
			m_acceptor(acceptor)
		{

		}

		CSocket::~CSocket() noexcept
		{

		}

		void CSocket::virtual_init()
		{
			this->m_acceptor.accept(this->socket());
		}
	}
}
