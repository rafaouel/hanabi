#ifndef CLIENTGAMEEND_H_392DEB17_A494_42F1_900B_80EDAAE8C66A
#define CLIENTGAMEEND_H_392DEB17_A494_42F1_900B_80EDAAE8C66A
#pragma once

#include "playerstatistics.h"
#include "types.h"
#include "updatable.h"


namespace hanabi
{
	namespace client_side
	{
		class CGameEnd : public common::IUpdatable
		{
		public:
			CGameEnd();
			virtual ~CGameEnd() noexcept;

			bool victory() const noexcept;
			void victory(bool victory);

			const boost::posix_time::ptime& game_start_timestamp() const noexcept;
			void game_start_timestamp(const boost::posix_time::ptime& timestamp);

			const boost::posix_time::ptime& game_end_timestamp() const noexcept;
			void game_end_timestamp(const boost::posix_time::ptime& timestamp);

			const boost::uuids::uuid& game_uuid() const noexcept;
			void game_uuid(const boost::uuids::uuid& game_uuid);

			common::TCount elapsed_player_turn_count() const noexcept;
			void elapsed_player_turn_count(common::TCount turn_count);

			common::TCount elapsed_game_turn_count() const noexcept;
			void elapsed_game_turn_count(common::TCount turn_count);

			common::TScore score() const noexcept;
			void score(common::TScore score);

			common::TScore max_score() const noexcept;
			void max_score(common::TScore score);

			common::TCount error_token_count() const noexcept;
			void error_token_count(common::TCount count);

			common::TCount max_error_token_count() const noexcept;
			void max_error_token_count(common::TCount count);

			const common::TPlayerStatisticsMap& player_statistics() const noexcept;
			void player_statistics(const common::TPlayerStatisticsMap& map);

		private:
			virtual void virtual_update(const boost::json::value& root);

			bool m_victory;
			boost::posix_time::ptime m_game_start_timestamp;
			boost::posix_time::ptime m_game_end_timestamp;
			boost::uuids::uuid m_game_uuid;
			common::TCount m_elapsed_player_turn_count;
			common::TCount m_elapsed_game_turn_count;
			common::TScore m_score;
			common::TScore m_max_score;
			common::TCount m_error_token_count;
			common::TCount m_max_error_token_count;
			common::TPlayerStatisticsMap m_player_statistics_map;
		};
	}
}

#endif