#ifndef CARD_H_C0923F5B_6F3F_40B5_B97E_FBEF5DB04E35
#define CARD_H_C0923F5B_6F3F_40B5_B97E_FBEF5DB04E35
#pragma once

#include "coloroption.h"
#include "serializable.h"
#include "types.h"


namespace hanabi
{
	namespace common
	{
		class CCard : public ISerializable
		{
		public:
			CCard(TValue value, CardColor color);
			CCard(const boost::json::value& json_value);

			virtual ~CCard() noexcept;
			

			TValue value() const;
			CardColor color() const;

			bool is(TValue value) const;
			bool is(CardColor color, const common::CColorOption& color_option) const;

			bool operator<(const CCard& other) const;
			bool operator==(const CCard& other) const;

		private:
			virtual boost::json::value serialize_to_json() const;

			virtual void parse_from_json(const boost::json::value& value);

			TValue m_value;
			CardColor m_color;
		};

		typedef std::list<common::CCard> TCardStack;
		typedef std::map<common::CardColor, TValue> TCardBoard;
	}
}

std::ostream& operator<<(std::ostream& os, const enum hanabi::common::CardColor& color);

std::ostream& operator<<(std::ostream& os, const hanabi::common::CCard& card);

void swap(hanabi::common::CCard& left, hanabi::common::CCard& right);

#endif