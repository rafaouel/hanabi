#include "pch.h"
#include "packetcoder.h"

namespace hanabi
{
	namespace network
	{
		CPacketCoder::CPacketCoder(PacketType packet_type) :
			m_data(),
			m_packet_type(packet_type)
		{

		}

		CPacketCoder::~CPacketCoder() noexcept
		{

		}

		PacketType CPacketCoder::packet_type() const noexcept
		{
			return this->m_packet_type;
		}

		void CPacketCoder::data(const boost::json::value& data)
		{
			this->m_data = data;
		}

		const boost::json::value& CPacketCoder::data() const noexcept
		{
			return this->m_data;
		}

		CPacket CPacketCoder::encode() const
		{
			std::ostringstream oss;
			oss << int(this->packet_type()) << delimiter;

			// Insert the data
			oss << boost::json::serialize(this->data());

			const CPacket packet = build_packet(oss.str());

			return packet;
		}
	}
}