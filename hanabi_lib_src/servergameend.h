#ifndef SERVERGAMEEND_H_2D006A50_C9BE_446E_BB13_0ABDC190BC82
#define SERVERGAMEEND_H_2D006A50_C9BE_446E_BB13_0ABDC190BC82
#pragma once

#include "playerstatistics.h"
#include "serializable.h"
#include "sendable.h"
#include "types.h"


namespace hanabi
{
	namespace server_side
	{
		class CGameEnd : public common::ISerializable, public common::ISendable
		{
		public:
			CGameEnd(const boost::uuids::uuid& game_uuid, common::TCount player_count);
			virtual ~CGameEnd() noexcept;


			bool victory() const noexcept;
			void victory(bool victory);

			const boost::posix_time::ptime& game_start_timestamp() const noexcept;
			const boost::posix_time::ptime& game_end_timestamp() const noexcept;

			const boost::uuids::uuid& game_uuid() const noexcept;


			void capture_game_start_timestamp();
			void capture_game_end_timestamp();

			boost::posix_time::time_duration compute_game_duration() const;

			common::TCount elapsed_player_turn_count() const noexcept;
			void elapsed_player_turn_count(common::TCount turn_count);

			common::TCount elapsed_game_turn_count() const noexcept;
			void elapsed_game_turn_count(common::TCount turn_count);

			common::TScore score() const noexcept;
			void score(common::TScore score);

			common::TScore max_score() const noexcept;
			void max_score(common::TScore score);

			common::TCount error_token_count() const noexcept;
			void error_token_count(common::TCount count);

			common::TCount max_error_token_count() const noexcept;
			void max_error_token_count(common::TCount count);

			const common::TPlayerStatisticsMap& player_statistics() const noexcept;
			common::TPlayerStatisticsMap& player_statistics() noexcept;
			void add_statistics(common::TIndex player_id, const common::CPlayerStatistics& stat);

		private:
			virtual boost::json::value serialize_to_json() const;
			virtual void parse_from_json(const boost::json::value& root);
			virtual boost::json::value virtual_pack(common::TIndex player_id) const;

			bool m_victory;
			boost::posix_time::ptime m_game_start_timestamp;
			boost::posix_time::ptime m_game_end_timestamp;
			const boost::uuids::uuid m_game_uuid;
			common::TCount m_elapsed_player_turn_count;
			common::TCount m_elapsed_game_turn_count;
			common::TScore m_score;
			common::TScore m_max_score;
			common::TCount m_error_token_count;
			common::TCount m_max_error_token_count;
			common::TPlayerStatisticsMap m_player_statistics_map;
		};
	}
}

#endif
