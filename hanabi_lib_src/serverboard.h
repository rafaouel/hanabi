#ifndef SERVERBOARD_H_623B2DE6_017A_49CE_B5FD_227AB2DCD8FA
#define SERVERBOARD_H_623B2DE6_017A_49CE_B5FD_227AB2DCD8FA
#pragma once

#include "card.h"
#include "sendable.h"
#include "serializable.h"
#include "servergameoption.h"

namespace hanabi
{
	namespace server_side
	{
		class CBoard : public common::ISerializable, public common::ISendable
		{
		public:
			CBoard(const CGameOption& game_option);
			virtual ~CBoard() noexcept;


			bool can_place(const common::CCard& card) const;
			std::shared_ptr<common::CardColor> place(const common::CCard& card);

			common::TScore score() const noexcept;

			common::TValue value(common::CardColor color) const;

			const common::TCardBoard& board() const noexcept;
			common::TCardBoard& board() noexcept;

			const CGameOption& game_option() const noexcept;

		private:
			virtual boost::json::value serialize_to_json() const;
			virtual void parse_from_json(const boost::json::value& /*root*/) {}
			virtual boost::json::value virtual_pack(common::TIndex target_player_id) const;

			common::TScore m_score;

			const CGameOption& m_game_option;

			common::TCardBoard m_board;
		};
	}
}

#endif