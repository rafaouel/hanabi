#include "pch.h"
#include "clientgameoption.h"

namespace hanabi
{
	namespace client_side
	{
		CGameOption::CGameOption() :
			m_game_option_id(),
			m_timestamp(boost::posix_time::not_a_date_time),
			m_player_count(0),
			m_player_hand_max_size(0),
			m_start_player_id(0),
			m_max_error_token_count(0),
			m_max_speak_token_count(0),
			m_start_error_token_count(0),
			m_start_speak_token_count(0),
			m_min_card_value(0),
			m_max_card_value(0),
			m_color_option(),
			m_stack_size(0)
		{

		}

		CGameOption::~CGameOption() noexcept
		{

		}

		void CGameOption::game_option_id(const common::TGameOptionId& game_option_id)
		{
			this->m_game_option_id = game_option_id;
		}

		const common::TGameOptionId& CGameOption::game_option_id() const noexcept
		{
			return this->m_game_option_id;
		}

		void CGameOption::timestamp(const boost::posix_time::ptime& timestamp)
		{
			this->m_timestamp = timestamp;
		}

		const boost::posix_time::ptime& CGameOption::timestamp() const noexcept
		{
			return this->m_timestamp;
		}

		void CGameOption::player_count(common::TCount count)
		{
			this->m_player_count = count;
		}

		common::TCount CGameOption::player_count() const noexcept
		{
			return this->m_player_count;
		}

		void CGameOption::player_hand_max_size(common::TCount size)
		{
			this->m_player_hand_max_size = size;
		}

		common::TCount CGameOption::player_hand_max_size() const noexcept
		{
			return this->m_player_hand_max_size;
		}

		void CGameOption::start_player_id(common::TIndex player_id)
		{
			this->m_start_player_id = player_id;
		}

		common::TIndex CGameOption::start_player_id() const noexcept
		{
			return this->m_start_player_id;
		}

		void CGameOption::max_error_token_count(common::TCount count)
		{
			this->m_max_error_token_count = count;
		}

		common::TCount CGameOption::max_error_token_count() const noexcept
		{
			return this->m_max_error_token_count;
		}

		void CGameOption::max_speak_token_count(common::TCount count)
		{
			this->m_max_speak_token_count = count;
		}

		common::TCount CGameOption::max_speak_token_count() const noexcept
		{
			return this->m_max_speak_token_count;
		}

		void CGameOption::start_error_token_count(common::TCount count)
		{
			this->m_start_error_token_count = count;
		}

		common::TCount CGameOption::start_error_token_count() const noexcept
		{
			return this->m_start_error_token_count;
		}

		void CGameOption::start_speak_token_count(common::TCount count)
		{
			this->m_start_speak_token_count = count;
		}

		common::TCount CGameOption::start_speak_token_count() const noexcept
		{
			return this->m_start_speak_token_count;
		}

		void CGameOption::min_card_value(common::TValue value)
		{
			this->m_min_card_value = value;
		}

		common::TValue CGameOption::min_card_value() const noexcept
		{
			return this->m_min_card_value;
		}

		void CGameOption::max_card_value(common::TValue value)
		{
			this->m_max_card_value = value;
		}

		common::TValue CGameOption::max_card_value() const noexcept
		{
			return this->m_max_card_value;
		}

		void CGameOption::game_max_score(common::TScore score) {
			this->m_game_max_score = score;
		}

		common::TScore CGameOption::game_max_score() const noexcept
		{
			// TODO need to be verified
			return this->m_game_max_score;
		}

		void CGameOption::color_option(const common::TColorOptionMap& color_option)
		{
			this->m_color_option = color_option;
		}

		void CGameOption::callable_colors(const common::TColorSet& colors)
		{
			this->m_callable_colors = colors;
		}

		const common::TColorSet& CGameOption::callable_colors() const noexcept
		{
			return this->m_callable_colors;
		}

		const common::TColorOptionMap& CGameOption::color_option() const noexcept
		{
			return this->m_color_option;
		}

		void CGameOption::stack_size(std::size_t size)
		{
			this->m_stack_size = size;
		}

		std::size_t CGameOption::stack_size() const noexcept
		{
			return this->m_stack_size;
		}

		void CGameOption::virtual_update(const boost::json::value& root)
		{
			const boost::json::object& root_object = root.as_object();

			this->game_option_id(root_object.at("gameOptionId").as_string().c_str());
			this->timestamp(boost::posix_time::from_iso_extended_string(root_object.at("timestamp").as_string().c_str()));
			this->player_count(root_object.at("playerCount").as_int64());
			this->player_hand_max_size(root_object.at("playerHandMaxSize").as_int64());
			this->start_player_id(root_object.at("startPlayerId").as_int64());
			this->max_error_token_count(root_object.at("maxErrorTokenCount").as_int64());
			this->max_speak_token_count(root_object.at("maxSpeakTokenCount").as_int64());
			this->start_error_token_count(root_object.at("startErrorTokenCount").as_int64());
			this->start_speak_token_count(root_object.at("startSpeakTokenCount").as_int64());
			this->min_card_value(root_object.at("minCardValue").as_int64());
			this->max_card_value(root_object.at("maxCardValue").as_int64());
			
			common::TColorSet colors;
			const boost::json::array& callable_colors_array = root_object.at("callableColors").as_array();
			for (const auto& item : callable_colors_array)
			{
				const std::string color = item.as_string().c_str();
				colors.insert(common::string_to_card_color(color));
			}
			this->callable_colors(colors);

			common::TColorOptionMap color_option;
			const boost::json::array& color_option_array = root_object.at("colorOption").as_array();
			for (const auto& item : color_option_array)
			{
				const boost::json::object& color_option_object = item.as_object();

				const common::CardColor color = common::string_to_card_color(color_option_object.at("color").as_string().c_str());
				const bool is_ascending = color_option_object.at("isAscending").as_bool();

				common::TColorSet reacts_to;
				const boost::json::array& reacts_to_array = color_option_object.at("reactsTo").as_array();
				for (const auto& reaction_value : reacts_to_array)
				{
					const std::string reaction_color = reaction_value.as_string().c_str();
					reacts_to.insert(common::string_to_card_color(reaction_color));
				}

				common::CColorOption::TValueDistribution distribution;
				const boost::json::array& distribution_array = color_option_object.at("distribution").as_array();

				for (const auto& distribution_item : distribution_array)
				{
					const boost::json::object& distribution_object = distribution_item.as_object();

					const common::TValue value = distribution_object.at("value").as_int64();
					const common::TCount count = distribution_object.at("count").as_int64();

					distribution.insert(std::pair(value, count));
				}
				const common::CColorOption option(distribution, is_ascending, reacts_to);
				color_option.insert(std::pair(color, option));
			}
			this->color_option(color_option);

			this->stack_size(root_object.at("stackSize").as_int64());
		}
	}
}

