#include "pch.h"
#include "servertrash.h"

#include "exception.h"


namespace hanabi
{
	namespace server_side
	{
		CTrash::CTrash(const CGameOption & game_option) :
			m_color_score(),
			m_game_option(game_option),
			m_stack()
		{
			const common::TScore color_max_score = 1 + this->game_option().max_card_value() - this->game_option().min_card_value();;
			for (const auto& pair : this->game_option().color_option())
			{
				const common::CardColor& color = pair.first;

				this->m_color_score[color] = color_max_score;
			}
		}

		CTrash::~CTrash() noexcept
		{

		}

		common::TScore CTrash::current_max_score() const noexcept
		{
			common::TScore max_score = 0;
			for (const auto& pair : this->m_color_score)
				max_score += pair.second;
			return max_score;
		}

		const common::TCardStack& CTrash::stack() const noexcept
		{
			return this->m_stack;
		}

		common::TCardStack& CTrash::stack() noexcept
		{
			return this->m_stack;
		}

		void CTrash::trash(const common::CCard& card)
		{
			const common::CardColor color = card.color();

			// Add the card
			this->stack().push_back(card);

			// Recompute the max score...
			const auto option_iterator = this->game_option().color_option().find(color);
			if (option_iterator == this->game_option().color_option().cend())
				THROW_ERROR_1(EInvalidColorError, color);

			const common::CColorOption& option = option_iterator->second;

			if (option.is_ascending())
			{
				for (common::TValue i = this->game_option().min_card_value(); i <= this->game_option().max_card_value(); ++i)
				{
					const common::CCard virtual_card(i, color);
					const common::TCount count = std::count(this->stack().cbegin(), this->stack().cend(), virtual_card);

					const common::TCount distri = option.distribution(i);
					assert(count <= distri);
					if (count == distri)
					{
						this->color_score(color) = i - this->game_option().min_card_value();
						return;
					}
				}
				this->color_score(color) = 1 + this->game_option().max_card_value() - this->game_option().min_card_value();
				return;
			}
			else
			{
				for (common::TValue i = this->game_option().max_card_value(); i >= this->game_option().min_card_value(); --i)
				{
					const common::CCard virtual_card(i, color);
					const common::TCount count = std::count(this->stack().cbegin(), this->stack().cend(), virtual_card);

					const common::TCount distri = option.distribution(i);
					assert(count <= distri);
					if (count == distri)
					{
						this->color_score(color) = this->game_option().max_card_value() - i;
						return;
					}
				}
				this->color_score(color) = 1 + this->game_option().max_card_value() - this->game_option().min_card_value();
				return;
			}
		}

		const CGameOption& CTrash::game_option() const noexcept
		{
			return this->m_game_option;
		}

		boost::json::value CTrash::serialize_to_json() const
		{
			boost::json::object trash_object;

			trash_object["currentMaxScore"] = boost::json::value_from(this->current_max_score());

			std::vector<boost::json::value> trash_vector;
			for (const auto& card : this->stack())
			{
				const boost::json::object card_json = card.to_json().as_object();
				trash_vector.push_back(card_json);
			}

			trash_object["trash"] = boost::json::value_from(trash_vector);

			return trash_object; 
		}

		boost::json::value CTrash::virtual_pack(common::TIndex /*target_player_id*/) const
		{
			return this->to_json();
		}

		const CTrash::TColorScore& CTrash::color_score() const noexcept
		{
			return this->m_color_score;
		}

		CTrash::TColorScore& CTrash::color_score() noexcept
		{
			return this->m_color_score;
		}

		const common::TScore& CTrash::color_score(common::CardColor color) const
		{
			return this->color_score().at(color);
		}

		common::TScore& CTrash::color_score(common::CardColor color)
		{
			return this->color_score().at(color);
		}
	}
}
