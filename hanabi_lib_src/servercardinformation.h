#ifndef SERVERCARDINFORMATION_H_D768E563_5260_4753_82A7_FC1FF852A8E4
#define SERVERCARDINFORMATION_H_D768E563_5260_4753_82A7_FC1FF852A8E4
#pragma once

#include "cardrawinformation.h"
#include "sendable.h"
#include "serializable.h"
#include "servergameoption.h"


namespace hanabi
{
	namespace server_side
	{
		class CCardInformation : public common::ISerializable, public common::ISendable
		{
		public:
			typedef std::vector<common::CCardRawInformation> TInformation;
			typedef std::vector<TInformation> TPlayerInformation;


			CCardInformation(const CGameOption& game_option);
			virtual ~CCardInformation() noexcept;


			void new_card(common::TIndex player_id);
			void remove(common::TIndex player_id, common::TIndex card_id);

			const TInformation& hand(common::TIndex player_id) const ;
			TInformation& hand(common::TIndex player_id);

			const TPlayerInformation& informations() const noexcept;

		private:
			virtual boost::json::value serialize_to_json() const;
			virtual void parse_from_json(const boost::json::value& /*root*/) {}
			virtual boost::json::value virtual_pack(common::TIndex target_player_id) const;

			TPlayerInformation m_player_information;
		};
	}
}
#endif