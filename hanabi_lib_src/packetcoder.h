#ifndef PACKETPCODER_H_93D2CD99_625E_436D_B56E_4FB8BE41546C
#define PACKETPCODER_H_93D2CD99_625E_436D_B56E_4FB8BE41546C
#pragma once

#include "packet.h"


namespace hanabi
{
	namespace network
	{
		class CPacketCoder
		{
		public:
			CPacketCoder(PacketType packet_type);
			virtual ~CPacketCoder() noexcept;


			// Get the packet type
			PacketType packet_type() const noexcept;

			// Read / write data
			void data(const boost::json::value& data);
			const boost::json::value& data() const noexcept;

			// Create a packet from the given data
			CPacket encode() const;

		private:
			boost::json::value m_data;
			const PacketType m_packet_type;
		};
	}
}
#endif