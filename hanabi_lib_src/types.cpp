#include "pch.h"
#include "types.h"

#include "exception.h"


namespace hanabi
{
    namespace common
    {

        std::string to_string(Preset preset)
        {
            switch (preset)
            {
            case Preset::CAKE:
                return "cake";

            case Preset::EASY:
                return "easy";

            case Preset::STD:
                return "std";

            case Preset::MULTI:
                return "multi";

            case Preset::BLACK:
                return "black";

            case Preset::HARD:
                return "hard";

            default:
                THROW_ERROR_1(EOutOfRangeError, std::int64_t(preset));
            }
        }

        Preset preset_from_string(const std::string& string)
        {
            if (string.compare("cake") == 0)
                return Preset::CAKE;
            else if (string.compare("easy") == 0)
                return Preset::EASY;
            else if (string.compare("std") == 0)
                return Preset::STD;
            else if (string.compare("multi") == 0)
                return Preset::MULTI;
            else if (string.compare("black") == 0)
                return Preset::BLACK;
            else if (string.compare("hard") == 0)
                return Preset::HARD;
            else
                THROW_ERROR_1(EInvalidPresetError, string);

        }

        std::string to_string(PeerType type)
        {
            switch (type)
            {
            case PeerType::HUMAN:
                return "human";

            case PeerType::AI:
                return "ai";

            default:
                THROW_ERROR_1(EOutOfRangeError, std::int64_t(type));
            }
        }

        PeerType peer_type_from_string(const std::string& string)
        {
            if (string.compare("human") == 0)
                return PeerType::HUMAN;
            else if (string.compare("ai") == 0)
                return PeerType::AI;
            else
                return PeerType::UNDEFINED;
        }

        std::string to_string(const TVersionArray& array)
        {
            std::ostringstream oss;
            bool first = true;
            for (const auto& value : array)
            {
                if (first)
                    first = false;
                else
                    oss << ".";
                oss << value;
            }
            return std::string(oss.str());
        }

        TVersionArray to_version(int version)
        {
            TVersionArray version_array = {};
            version_array[0] = version / 100000;
            version_array[1] = version / 100 % 1000;
            version_array[2] = version % 100;

            return version_array;
        }

        TVersionArray to_version(const std::string& version)
        {
            std::vector<std::string> splited_string;
            boost::split(splited_string, version, boost::is_any_of("."));
            
            TVersionArray result;

            for (int i = 0; i < 3; ++i)
                result[i] = std::atoi(splited_string[i].c_str());

            return result;
        }

        std::string to_string(ActionType type)
        {
            switch (type)
            {
            case ActionType::DROP:
                return "drop";
            case ActionType::SPEAK:
                return "speak";
            case ActionType::PLACE:
                return "place";
            default:
                THROW_ERROR_1(EOutOfRangeError, std::int64_t(type));
            }
        }
    }
}