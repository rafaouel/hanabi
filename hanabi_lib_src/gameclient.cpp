#include "pch.h"
#include "gameclient.h"

#include "action.h"
#include "exception.h"
#include "packetcoder.h"
#include "packetparser.h"


namespace hanabi
{
	namespace client_side
	{
		CPeerInfoResponse::CPeerInfoResponse(const std::string& name, const common::TVersionArray& version_array, const common::PeerType& peer_type, bool debug, const common::TTagList& tags) :
			m_name(name),
			m_version_array(version_array),
			m_peer_type(peer_type),
			m_debug(debug),
			m_tags(tags)
		{

		}

		CPeerInfoResponse::~CPeerInfoResponse() noexcept
		{

		}

		const std::string& CPeerInfoResponse::name() const noexcept
		{
			return this->m_name;
		}

		const std::string CPeerInfoResponse::version() const
		{
			return common::to_string(this->m_version_array);
		}

		const common::TVersionArray& CPeerInfoResponse::version_array() const noexcept
		{
			return this->m_version_array;
		}

		const common::PeerType& CPeerInfoResponse::peer_type() const noexcept
		{
			return this->m_peer_type;
		}

		bool CPeerInfoResponse::debug() const noexcept
		{
			return this->m_debug;
		}

		const common::TTagList CPeerInfoResponse::tags() const noexcept
		{
			return this->m_tags;
		}


		IGameClient::IGameClient(boost::asio::io_service& io_service, const boost::asio::ip::address& server_address, const boost::asio::ip::port_type& server_port) :
			m_run(true),
            m_game_state(),
			m_my_id(common::invalid_index),
            m_socket(io_service, server_address, server_port),
            m_packet_dispatcher()
		{
			this->m_packet_dispatcher.add_functor(network::PacketType::HELLO, std::bind(&IGameClient::on_receiving_hello_packet, this, std::placeholders::_1));
			this->m_packet_dispatcher.add_functor(network::PacketType::GAME_END_UPDATE, std::bind(&IGameClient::on_receiving_game_end_update_packet, this, std::placeholders::_1));
			this->m_packet_dispatcher.add_functor(network::PacketType::GAME_STATE_UPDATE, std::bind(&IGameClient::on_receiving_game_state_update_packet, this, std::placeholders::_1));
			this->m_packet_dispatcher.add_functor(network::PacketType::GAME_OPTION_UPDATE, std::bind(&IGameClient::on_receiving_game_option_update_packet, this, std::placeholders::_1));
			this->m_packet_dispatcher.add_functor(network::PacketType::ACTION_REQUEST, std::bind(&IGameClient::on_receiving_action_request_packet, this, std::placeholders::_1));
			this->m_packet_dispatcher.add_functor(network::PacketType::ACTION_SPEAK_UPDATE, std::bind(&IGameClient::on_receiving_action_speak_update_packet, this, std::placeholders::_1));
			this->m_packet_dispatcher.add_functor(network::PacketType::ACTION_DROP_UPDATE, std::bind(&IGameClient::on_receiving_action_drop_update_packet, this, std::placeholders::_1));
			this->m_packet_dispatcher.add_functor(network::PacketType::ACTION_PLACE_UPDATE, std::bind(&IGameClient::on_receiving_action_place_update_packet, this, std::placeholders::_1));
			this->m_packet_dispatcher.add_functor(network::PacketType::PEER_INFO_REQUEST, std::bind(&IGameClient::on_receiving_peer_info_request_packet, this, std::placeholders::_1));
			this->m_packet_dispatcher.add_functor(network::PacketType::PEER_INFO_UPDATE, std::bind(&IGameClient::on_receiving_peer_info_update_packet, this, std::placeholders::_1));
			this->m_packet_dispatcher.add_functor(network::PacketType::BONUS_EVENT, std::bind(&IGameClient::on_receiving_bonus_event_packet, this, std::placeholders::_1));
			this->m_packet_dispatcher.add_functor(network::PacketType::ERROR_EVENT, std::bind(&IGameClient::on_receiving_error_event_packet, this, std::placeholders::_1));
		}

		IGameClient::~IGameClient() noexcept
		{

		}

		void IGameClient::run()
		{
            // Setup the socket
			this->m_socket.init();

            // Game loop
            while (this->m_run)
            {
				try
				{
					// Get a packet
					network::CPacket packet = this->m_socket.read();

					// Process the packet
					this->m_packet_dispatcher.decode(packet);
				}
				catch (EUnknownPacketTypeError& error)
				{
					// If the dispatcher received an unknown packet type
					std::cerr << "/!\\ An unknown packet type arrived (type = " << error.packet_type() << ")" << std::endl;
					continue;
				}
            }
		}

		common::TIndex IGameClient::my_id() const noexcept
		{
			return this->m_my_id;
		}

		void IGameClient::my_id(common::TIndex my_id)
		{
			this->m_my_id = my_id;
		}

		const CGameState& IGameClient::game_state() const noexcept
		{
			return this->m_game_state;
		}

		CGameState& IGameClient::game_state() noexcept
		{
			return this->m_game_state;
		}

		const CGameEnd& IGameClient::game_end() const noexcept
		{
			return this->m_game_end;
		}

		CGameEnd& IGameClient::game_end() noexcept
		{
			return this->m_game_end;
		}

		const TPeerInfoMap& IGameClient::peer_info() const noexcept
		{
			return this->m_peer_info;
		}

		TPeerInfoMap& IGameClient::peer_info() noexcept
		{
			return this->m_peer_info;
		}

		const CPeerInfo& IGameClient::peer_info(common::TIndex player_id) const
		{
			assert(common::is_valid_index(player_id));
			assert(this->m_peer_info.contains(player_id));

			return this->m_peer_info.at(player_id);
		}

		CPeerInfo& IGameClient::peer_info(common::TIndex player_id)
		{
			assert(common::is_valid_index(player_id));
			assert(this->m_peer_info.contains(player_id));

			return this->m_peer_info.at(player_id);
		}

		void IGameClient::set_callback_on_receiving_hello(const TCallbackOnReceivingHello& callback)
		{
			this->m_on_receiving_hello = callback;
		}

		void IGameClient::set_callback_on_receiving_game_end_update(const TCallbackOnReceivingGameEndUpdate& callback)
		{
			this->m_on_receiving_game_end_update = callback;
		}

		void IGameClient::set_callback_on_receiving_game_state_update(const TCallbackOnReceivingGameStateUpdate& callback)
		{
			this->m_on_receiving_game_state_update = callback;
		}

		void IGameClient::set_callback_on_receiving_game_option_update(const TCallbackOnReceivingGameOptionUpdate& callback)
		{
			this->m_on_receiving_game_option_update = callback;
		}

		void IGameClient::set_callback_on_receiving_action_speak_value_update(const TCallbackOnReceivingActionSpeakValueUpdate& callback)
		{
			this->m_on_receiving_action_speak_value_update = callback;
		}

		void IGameClient::set_callback_on_receiving_action_speak_color_update(const TCallbackOnReceivingActionSpeakColorUpdate& callback)
		{
			this->m_on_receiving_action_speak_color_update = callback;
		}

		void IGameClient::set_callback_on_receiving_action_drop_update(const TCallbackOnReceivingActionDropUpdate& callback)
		{
			this->m_on_receiving_action_drop_update = callback;
		}

		void IGameClient::set_callback_on_receiving_action_place_update(const TCallbackOnReceivingActionPlaceUpdate& callback)
		{
			this->m_on_receiving_action_place_update = callback;
		}

		void IGameClient::set_callback_on_receiving_peer_info_request(const TCallbackOnReceivingPeerInfoRequest& callback)
		{
			this->m_on_receiving_peer_info_request = callback;
		}

		void IGameClient::set_callback_on_receiving_peer_info_update(const TCallbackOnReceivingPeerInfoUpdate& callback)
		{
			this->m_on_receiving_peer_info_update = callback;
		}

		void IGameClient::set_callback_on_receiving_action_accepted(const TCallbackOnReceivingActionAccepted& callback)
		{
			this->m_on_receiving_action_accepted = callback;
		}

		void IGameClient::set_callback_on_receiving_action_rejected(const TCallbackOnReceivingActionRejected& callback)
		{
			this->m_on_receiving_action_rejected = callback;
		}

		void IGameClient::set_callback_on_receiving_bonus_event(const TCallbackOnReceivingBonusEvent& callback)
		{
			this->m_on_receiving_bonus_event = callback;
		}

		void IGameClient::set_callback_on_receiving_error_event(const TCallbackOnReceivingErrorEvent& callback)
		{
			this->m_on_receiving_error_event = callback;
		}

		void IGameClient::call_on_receiving_hello(const std::string& server_hello_message) const
		{
			if (this->m_on_receiving_hello)
				this->m_on_receiving_hello(server_hello_message);
		}

		void IGameClient::call_on_receiving_game_end_update(const CGameEnd& game_end, const CGameState& last_game_state, const TPeerInfoMap& peer_info) const
		{
			if (this->m_on_receiving_game_end_update)
				this->m_on_receiving_game_end_update(game_end, last_game_state, peer_info);
		}

		void IGameClient::call_on_receiving_game_state_update(const CGameState& game_state, common::TIndex my_id, const TPeerInfoMap& peer_info) const
		{
			if (this->m_on_receiving_game_state_update)
				this->m_on_receiving_game_state_update(game_state, my_id, peer_info);
		}

		void IGameClient::call_on_receiving_game_option_update(const CGameOption& game_option) const
		{
			if (this->m_on_receiving_game_option_update)
				this->m_on_receiving_game_option_update(game_option);
		}

		void IGameClient::call_on_receiving_action_speak_value_update(common::TIndex source_player_id, common::TIndex target_player_id, const TPeerInfoMap& peer_info, common::TValue value_info) const
		{
			if (this->m_on_receiving_action_speak_value_update)
				this->m_on_receiving_action_speak_value_update(source_player_id, target_player_id, peer_info, value_info);
		}

		void IGameClient::call_on_receiving_action_speak_color_update(common::TIndex source_player_id, common::TIndex target_player_id, const TPeerInfoMap& peer_info, common::CardColor color_info) const
		{
			if (this->m_on_receiving_action_speak_color_update)
				this->m_on_receiving_action_speak_color_update(source_player_id, target_player_id, peer_info, color_info);
		}

		void IGameClient::call_on_receiving_action_drop_update(common::TIndex source_player_id, const TPeerInfoMap& peer_info, const common::CCard& card) const
		{
			if (this->m_on_receiving_action_drop_update)
				this->m_on_receiving_action_drop_update(source_player_id, peer_info, card);
		}

		void IGameClient::call_on_receiving_action_place_update(common::TIndex source_player_id, const TPeerInfoMap& peer_info, const common::CCard& card) const
		{
			if (this->m_on_receiving_action_place_update)
				this->m_on_receiving_action_place_update(source_player_id, peer_info, card);
		}

		void IGameClient::call_on_receiving_peer_info_request(const std::string& server_version) const
		{
			if (this->m_on_receiving_peer_info_request)
				this->m_on_receiving_peer_info_request(server_version);
		}

		void IGameClient::call_on_receiving_peer_info_update(common::TIndex new_peer_id, const CPeerInfo& new_peer, const TPeerInfoMap& peer_info) const
		{
			if (this->m_on_receiving_peer_info_update)
				this->m_on_receiving_peer_info_update(new_peer_id, new_peer, peer_info);
		}

		void IGameClient::call_on_receiving_action_accepted() const
		{
			if (this->m_on_receiving_action_accepted)
				this->m_on_receiving_action_accepted();
		}

		void IGameClient::call_on_receiving_action_rejected(const std::string& reason) const
		{
			if (this->m_on_receiving_action_rejected)
				this->m_on_receiving_action_rejected(reason);
		}

		void IGameClient::call_on_receiving_bonus_event(common::TIndex current_player_id, const common::CardColor& color) const
		{
			if (this->m_on_receiving_bonus_event)
				this->m_on_receiving_bonus_event(current_player_id, color);
		}

		void IGameClient::call_on_receiving_error_event(common::TIndex current_player_id, common::TCount error_token_count) const
		{
			if (this->m_on_receiving_error_event)
				this->m_on_receiving_error_event(current_player_id, error_token_count);
		}

		void IGameClient::on_receiving_hello_packet(const network::CPacket& packet)
		{
			// Hello request reception
			network::CPacketParser parser(network::PacketType::HELLO);
			parser.decode(packet);

			const boost::json::object root = parser.data().as_object();
			const std::string server_message = root.at("message").as_string().c_str();

			// Callback
			this->call_on_receiving_hello(server_message);
		}

		void IGameClient::on_receiving_game_end_update_packet(const network::CPacket& packet)
		{
			network::CPacketParser parser(network::PacketType::GAME_END_UPDATE);
			parser.decode(packet);

			this->game_end().update(parser.data());

			// Callback
			this->call_on_receiving_game_end_update(this->game_end(), this->game_state(), this->peer_info());

			// Exit the client
			this->m_run = false;
		}

		void IGameClient::on_receiving_game_state_update_packet(const network::CPacket& packet)
		{
			network::CPacketParser parser(network::PacketType::GAME_STATE_UPDATE);
			parser.decode(packet);
			this->game_state().update(parser.data());

			// Callback
			this->call_on_receiving_game_state_update(this->game_state(), this->my_id(), this->peer_info());
		}

		void IGameClient::on_receiving_game_option_update_packet(const network::CPacket& packet)
		{
			network::CPacketParser parser(network::PacketType::GAME_OPTION_UPDATE);
			parser.decode(packet);
			this->game_state().game_option().update(parser.data());

			// Callback
			this->call_on_receiving_game_option_update(this->game_state().game_option());
		}

		void IGameClient::on_receiving_action_request_packet(const network::CPacket& /*packet*/)
		{
			// Get the action
			const std::shared_ptr<common::IAction> p_action = this->process_action_request(this->game_state(), this->peer_info(), this->my_id());

			// Encode it
			network::CPacketCoder coder(network::PacketType::ACTION_RESPONSE);
			coder.data(p_action->to_json());

			const network::CPacket packet = coder.encode();

			// Send the response
			this->m_socket.send(packet);

			// Wait for answer 'ok' or 'not ok'
			const network::CPacket acknowledgment = this->m_socket.read();
			const network::PacketType packet_type = network::packet_type(acknowledgment);

			if (packet_type == network::PacketType::OK)
			{
				this->call_on_receiving_action_accepted();
			}
			else if (packet_type == network::PacketType::NOT_OK)
			{
				network::CPacketParser parser(network::PacketType::NOT_OK);
				parser.decode(acknowledgment);

				const std::string reason = parser.data().as_object().at("reason").as_string().c_str();

				this->call_on_receiving_action_rejected(reason);
			}
			else
			{
				THROW_ERROR_1(EUnknownPacketTypeError, int(packet_type));
			}
		}

		void IGameClient::on_receiving_action_speak_update_packet(const network::CPacket& packet)
		{
			network::CPacketParser parser(network::PacketType::ACTION_SPEAK_UPDATE);
			parser.decode(packet);
			const boost::json::object root = parser.data().as_object();

			const common::TIndex source_player_id = root.at("sourcePlayerId").as_int64();
			const common::TIndex target_player_id = root.at("targetPlayerId").as_int64();

			if (root.contains("value"))
			{
				const common::TValue value = root.at("value").as_int64();
				
				this->call_on_receiving_action_speak_value_update(source_player_id, target_player_id, this->peer_info(), value);
			}
			else if (root.contains("color"))
			{
				const common::CardColor color = common::string_to_card_color(root.at("color").as_string().c_str());
				
				this->call_on_receiving_action_speak_color_update(source_player_id, target_player_id, this->peer_info(), color);
			}
			else
			{
				THROW_ERROR(EImpossibleError);
			}
		}

		void IGameClient::on_receiving_action_drop_update_packet(const network::CPacket& packet)
		{
			network::CPacketParser parser(network::PacketType::ACTION_DROP_UPDATE);
			parser.decode(packet);
			const boost::json::object root = parser.data().as_object();

			const common::TIndex source_player_id = root.at("sourcePlayerId").as_int64();
		
			const boost::json::object card_object = root.at("card").as_object();
			const common::TIndex value = card_object.at("value").as_int64();
			const common::CardColor color = common::string_to_card_color(card_object.at("color").as_string().c_str());

			const common::CCard card(value, color);

			this->call_on_receiving_action_drop_update(source_player_id, this->peer_info(), card);
		}

		void IGameClient::on_receiving_action_place_update_packet(const network::CPacket& packet)
		{
			network::CPacketParser parser(network::PacketType::ACTION_PLACE_UPDATE);
			parser.decode(packet);
			const boost::json::object root = parser.data().as_object();

			const common::TIndex source_player_id = root.at("sourcePlayerId").as_int64();

			const boost::json::object card_object = root.at("card").as_object();
			const common::TValue value = card_object.at("value").as_int64();
			const common::CardColor color = common::string_to_card_color(card_object.at("color").as_string().c_str());

			const common::CCard card(value, color);

			this->call_on_receiving_action_place_update(source_player_id, this->peer_info(), card);
		}

		void IGameClient::on_receiving_peer_info_request_packet(const network::CPacket& packet)
		{
			network::CPacketParser parser(network::PacketType::PEER_INFO_REQUEST);
			parser.decode(packet);
			const boost::json::object root = parser.data().as_object();

			this->my_id(root.at("playerId").as_int64());
			const std::string server_version = root.at("serverVersion").as_string().c_str();

			CPeerInfoResponse response_content = this->process_peer_info_request(this->my_id(), server_version);

			network::CPacketCoder coder(network::PacketType::PEER_INFO_RESPONSE);
			boost::json::object content;
			content["name"] = boost::json::value_from(response_content.name());
			content["version"] = boost::json::value_from(response_content.version());
			content["versionArray"] = boost::json::value_from(response_content.version_array());
			content["type"] = boost::json::value_from(common::to_string(response_content.peer_type()));
			content["debug"] = boost::json::value_from(response_content.debug());
			content["tags"] = boost::json::value_from(response_content.tags());
			coder.data(content);

			const network::CPacket response_packet = coder.encode();

			this->m_socket.send(response_packet);

			this->call_on_receiving_peer_info_request(server_version);
		}

		void IGameClient::on_receiving_peer_info_update_packet(const network::CPacket& packet)
		{
			network::CPacketParser parser(network::PacketType::PEER_INFO_UPDATE);
			parser.decode(packet);
			const boost::json::object root = parser.data().as_object();

			const common::TIndex player_id = root.at("playerId").as_int64();
			
			CPeerInfo peer_info;
			peer_info.name(root.at("name").as_string().c_str());
			this->peer_info().insert(std::pair(player_id, peer_info));

			this->call_on_receiving_peer_info_update(player_id, peer_info, this->peer_info());
		}

		void IGameClient::on_receiving_bonus_event_packet(const network::CPacket& packet)
		{
			network::CPacketParser parser(network::PacketType::BONUS_EVENT);
			parser.decode(packet);
			const boost::json::object root = parser.data().as_object();

			const common::TIndex player_id = root.at("playerId").as_int64();
			const common::CardColor completed_color = common::string_to_card_color(root.at("color").as_string().c_str());

			this->call_on_receiving_bonus_event(player_id, completed_color);
		}

		void IGameClient::on_receiving_error_event_packet(const network::CPacket& packet)
		{
			network::CPacketParser parser(network::PacketType::ERROR_EVENT);
			parser.decode(packet);
			const boost::json::object root = parser.data().as_object();

			const common::TIndex player_id = root.at("playerId").as_int64();
			const common::TCount error_token_count = root.at("errorTokenCount").as_int64();

			this->call_on_receiving_error_event(player_id, error_token_count);
		}
	}
}
