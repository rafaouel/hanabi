#ifndef SENDABLE_H_A519A124_D49C_4966_AE31_B1D9854A8942
#define SENDABLE_H_A519A124_D49C_4966_AE31_B1D9854A8942
#pragma once

#include "types.h"


namespace hanabi
{
	namespace common
	{
		/// <summary>
		/// Class that packs some data in a json value, ready to be loaded by an IUpdatable object
		/// </summary>
		class ISendable
		{
		public:
			ISendable();
			virtual ~ISendable() noexcept;


			boost::json::value pack(TIndex target_player_id) const;

		private:
			virtual boost::json::value virtual_pack(TIndex target_player_id) const = 0;
		};
	}
}

#endif