#include "pch.h"
#include "exception.h"

#include "apicontext.h"
#include "apigateway.h"
#include "coloroption.h"
#include "types.h"


namespace hanabi
{
	EHanabiException::EHanabiException(const std::string& file_name, const std::string& function_name, std::size_t line) :
		m_file_name(file_name),
		m_function_name(function_name),
		m_line(line),
		m_stack("")
	{
		std::ostringstream oss;
		oss << boost::stacktrace::stacktrace();
		this->m_stack = oss.str();
	}

	EHanabiException::~EHanabiException() noexcept
	{

	}

	std::string EHanabiException::get_error_type_name() const
	{
		return this->virtual_get_error_type_name();
	}

	const char* EHanabiException::what() const noexcept
	{
		std::ostringstream oss;
		oss << "A " << this->get_error_type_name() << " exception has been catch" << std::endl;
		oss << "The error occured in the file \"" << this->m_file_name << "\"" << std::endl;
		oss << "in the function \"" << this->m_function_name << "\"" << std::endl;
		oss << "at line \"" << this->m_line << "\"" << std::endl;
		oss << "and carry the following message :" << std::endl;
		oss << this->virtual_get_message();
		oss << "Here is the call stack: " << std::endl;
		oss << this->m_stack;
		this->m_message = oss.str();
		return this->m_message.c_str();
	}

	EImpossibleError::EImpossibleError(const std::string& file_name, const std::string& function_name, std::size_t line) :
		EHanabiException(file_name, function_name, line),
		m_message("")
	{

	}

	EImpossibleError::EImpossibleError(const std::string& file_name, const std::string& function_name, std::size_t line, const std::string& message) :
		EHanabiException(file_name, function_name, line),
		m_message(message)
	{

	}

	EImpossibleError::~EImpossibleError() noexcept
	{

	}

	std::string EImpossibleError::virtual_get_message() const
	{
		std::ostringstream oss;
		oss << "This error should not be thrown" << std::endl;
		if (this->m_message.size() > 0)
		{
			oss << "The following message has been attached: " << this->m_message << std::endl;
		}
		return oss.str();
	}

	std::string EImpossibleError::virtual_get_error_type_name() const
	{
		return typeid(*this).name();
	}

	ENotImplementedError::ENotImplementedError(const std::string& file_name, const std::string& function_name, std::size_t line) :
		EHanabiException(file_name, function_name, line)
	{

	}

	ENotImplementedError::~ENotImplementedError() noexcept
	{

	}

	std::string ENotImplementedError::virtual_get_message() const
	{
		std::ostringstream oss;
		oss << "This feature is not implemented (yet ?)" << std::endl;
		return oss.str();
	}

	std::string ENotImplementedError::virtual_get_error_type_name() const
	{
		return typeid(*this).name();
	}

	EServerError::EServerError(const std::string& file_name, const std::string& function_name, std::size_t line) :
		EHanabiException(file_name, function_name, line)
	{

	}

	EServerError::~EServerError() noexcept
	{

	}

	ECompatibilityError::ECompatibilityError(const std::string& file_name, const std::string& function_name, std::size_t line, const common::TVersionArray& version) :
		EServerError(file_name, function_name, line),
		m_version(version)
	{

	}

	ECompatibilityError::~ECompatibilityError() noexcept
	{

	}
	
	std::string ECompatibilityError::virtual_get_message() const
	{
		std::ostringstream oss;
		oss << "The server version is not found in compatibility table (version=" << common::to_string(this->m_version) << ")" << std::endl;
		return oss.str();
	}

	std::string ECompatibilityError::virtual_get_error_type_name() const
	{
		return typeid(*this).name();
	}

	EServerFull::EServerFull(const std::string& file_name, const std::string& function_name, std::size_t line) :
		EServerError(file_name, function_name, line)
	{

	}

	EServerFull::~EServerFull() noexcept
	{

	}

	std::string EServerFull::virtual_get_message() const
	{
		return "The server is full";
	}

	std::string EServerFull::virtual_get_error_type_name() const
	{
		return typeid(*this).name();
	}

	EParsingError::EParsingError(const std::string& file_name, const std::string& function_name, std::size_t line, const std::string& description) :
		EServerError(file_name, function_name, line),
		m_description(description)
	{

	}

	EParsingError::~EParsingError() noexcept
	{

	}

	std::string EParsingError::virtual_get_message() const
	{
		std::ostringstream oss;
		oss << "Error while parsing. The description is:" << std::endl;
		oss << this->m_description << std::endl;
		return oss.str();
	}

	std::string EParsingError::virtual_get_error_type_name() const
	{
		return typeid(*this).name();
	}

	EInvalidPresetError::EInvalidPresetError(const std::string& file_name, const std::string& function_name, std::size_t line, const std::string& preset_value) :
		EServerError(file_name, function_name, line),
		m_invalid_preset_value(preset_value)
	{

	}

	EInvalidPresetError::~EInvalidPresetError() noexcept
	{

	}

	std::string EInvalidPresetError::virtual_get_message() const
	{
		std::ostringstream oss;
		oss << "Invalid preset value (given value=" << this->m_invalid_preset_value << ")" << std::endl;
		return oss.str();
	}

	std::string EInvalidPresetError::virtual_get_error_type_name() const
	{
		return typeid(*this).name();
	}

	EOutOfRangeError::EOutOfRangeError(const std::string& file_name, const std::string& function_name, std::size_t line, std::int64_t invalid_value) :
		EServerError(file_name, function_name, line),
		m_invalid_value(invalid_value)
	{

	}

	EOutOfRangeError::~EOutOfRangeError() noexcept
	{

	}

	std::string EOutOfRangeError::virtual_get_message() const
	{
		std::ostringstream oss;
		oss << "The index is out of range (given value=" << this->m_invalid_value << ")" << std::endl;
		return oss.str();
	}

	std::string EOutOfRangeError::virtual_get_error_type_name() const
	{
		return typeid(*this).name();
	}

	ENotAllowedActionError::ENotAllowedActionError(const std::string& file_name, const std::string& function_name, std::size_t line, const std::string& action_type_name) :
		EServerError(file_name, function_name, line),
		m_action_type_name(action_type_name)
	{

	}

	ENotAllowedActionError::~ENotAllowedActionError() noexcept
	{

	}

	std::string ENotAllowedActionError::virtual_get_message() const
	{
		std::ostringstream oss;
		oss << "The action '" << this->m_action_type_name << "' is not allowed." << std::endl;
		return oss.str();
	}

	std::string ENotAllowedActionError::virtual_get_error_type_name() const
	{
		return typeid(*this).name();
	}

	EInvalidPlayerNumberError::EInvalidPlayerNumberError(const std::string& file_name, const std::string& function_name, std::size_t line, common::TCount player_count) :
		EServerError(file_name, function_name, line),
		m_player_count(player_count)
	{

	}

	EInvalidPlayerNumberError::~EInvalidPlayerNumberError() noexcept
	{

	}

	std::string EInvalidPlayerNumberError::virtual_get_message() const
	{
		std::ostringstream oss;
		oss << "The players count is not valid (" << this->m_player_count << ")" << std::endl;
		return oss.str();
	}

	std::string EInvalidPlayerNumberError::virtual_get_error_type_name() const
	{
		return typeid(*this).name();
	}

	EInvalidColorError::EInvalidColorError(const std::string& file_name, const std::string& function_name, std::size_t line, const std::string& invalid_color_name) :
		EServerError(file_name, function_name, line),
		m_invalid_color_name(invalid_color_name)
	{

	}

	EInvalidColorError::EInvalidColorError(const std::string& file_name, const std::string& function_name, std::size_t line, const common::CardColor& invalid_color) :
		EServerError(file_name, function_name, line),
		m_invalid_color_name(common::card_color_to_string(invalid_color))
	{

	}

	EInvalidColorError::~EInvalidColorError() noexcept
	{

	}

	std::string EInvalidColorError::virtual_get_message() const
	{
		std::ostringstream oss;
		oss << "The color '" << this->m_invalid_color_name << "' is invalid" << std::endl;
		return oss.str();
	}

	std::string EInvalidColorError::virtual_get_error_type_name() const
	{
		return typeid(*this).name();
	}

	ENetworkError::ENetworkError(const std::string& file_name, const std::string& function_name, std::size_t line) :
		EHanabiException(file_name, function_name, line)
	{

	}

	ENetworkError::~ENetworkError() noexcept
	{

	}

	EAPIRequestError::EAPIRequestError(const std::string& file_name, const std::string& function_name, std::size_t line, const std::string& reason) :
		ENetworkError(file_name, function_name, line),
		m_reason(reason)
	{

	}

	EAPIRequestError::~EAPIRequestError() noexcept
	{

	}

	std::string EAPIRequestError::virtual_get_message() const
	{
		std::ostringstream oss;
		oss << "An error occured while using the API (reason=" << this->m_reason << ")" << std::endl;
		return oss.str();
	}

	std::string EAPIRequestError::virtual_get_error_type_name() const
	{
		return typeid(*this).name();
	}

	EUnknownPacketTypeError::EUnknownPacketTypeError(const std::string& file_name, const std::string& function_name, std::size_t line, int packet_type) :
		ENetworkError(file_name, function_name, line),
		m_packet_type(packet_type)
	{

	}

	EUnknownPacketTypeError::~EUnknownPacketTypeError() noexcept
	{

	}

	int EUnknownPacketTypeError::packet_type() const noexcept
	{
		return this->m_packet_type;
	}

	std::string EUnknownPacketTypeError::virtual_get_message() const
	{
		std::ostringstream oss;
		oss << "Unknown packet type (id = " << this->m_packet_type << ")" << std::endl;
		return oss.str();
	}

	std::string EUnknownPacketTypeError::virtual_get_error_type_name() const
	{
		return typeid(*this).name();
	}

	EHttpError::EHttpError(const std::string& file_name, const std::string& function_name, std::size_t line, const common::TRequest& request, const common::CAPIContext& context, const common::TResponse& response, const std::string& description) :
		EHanabiException(file_name, function_name, line),
		m_request(request),
		m_context(context),
		m_response(response),
		m_description(description)
	{

	}

	EHttpError::~EHttpError() noexcept
	{

	}

	std::string EHttpError::virtual_get_message() const
	{
		std::ostringstream oss;
		oss << "A request failed (" << this->m_context.api_host() << ":" << this->m_context.api_port() << this->m_request.target() << " " << std::endl;
		return oss.str();
	}

	std::string EHttpError::virtual_get_error_type_name() const
	{
		return typeid(*this).name();
	}
}