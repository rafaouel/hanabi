#include "pch.h"
#include "servercardinformation.h"

namespace hanabi
{
	namespace server_side
	{
		CCardInformation::CCardInformation(const CGameOption& game_option):
			m_player_information(game_option.player_count())
		{

		}

		CCardInformation::~CCardInformation() noexcept
		{

		}

		void CCardInformation::new_card(common::TIndex player_id)
		{
			assert(player_id < common::TIndex(this->informations().size()));

			this->hand(player_id).push_back(common::CCardRawInformation());
		}

		void CCardInformation::remove(common::TIndex player_id, common::TIndex card_id)
		{
			assert(player_id < common::TIndex(this->informations().size()));
			assert(card_id < common::TIndex(this->hand(player_id).size()));

			TInformation tmp_info;
			for (common::TIndex card_index = 0; card_index < common::TIndex(this->hand(player_id).size()); ++card_index)
			{
				if (card_index == card_id)
					continue;

				tmp_info.push_back(this->hand(player_id)[card_index]);
			}

			this->hand(player_id) = tmp_info;
		}

		const CCardInformation::TInformation& CCardInformation::hand(common::TIndex player_id) const
		{
			assert(player_id < common::TIndex(this->m_player_information.size()));

			return this->m_player_information.at(player_id);
		}

		CCardInformation::TInformation& CCardInformation::hand(common::TIndex player_id)
		{
			assert(player_id < common::TIndex(this->m_player_information.size()));

			return this->m_player_information.at(player_id);
		}

		const CCardInformation::TPlayerInformation& CCardInformation::informations() const noexcept
		{
			assert(!this->m_player_information.empty());

			return this->m_player_information;
		}

		boost::json::value CCardInformation::serialize_to_json() const
		{
			std::vector<boost::json::value> player_information_vector;
			for (common::TIndex player_index = 0; player_index < common::TIndex(this->informations().size()); ++player_index)
			{
				std::vector<boost::json::value> hand_vector;
				for (const auto& card_info : this->hand(player_index))
				{
					boost::json::object card_info_json;
					card_info_json["positiveValue"] = boost::json::value_from(card_info.positive_value());
					std::vector<boost::json::value> negative_values;
					for (const auto value : card_info.negative_value())
					{
						negative_values.push_back(boost::json::value_from(value));
					}
					card_info_json["negativeValue"] = boost::json::value_from(negative_values);
					std::vector<boost::json::value> positive_colors;
					for (const auto color : card_info.positive_color())
					{
						positive_colors.push_back(boost::json::value_from(card_color_to_string(color)));
					}
					card_info_json["positiveColor"] = boost::json::value_from(positive_colors);
					std::vector<boost::json::value> negative_colors;
					for (const auto color : card_info.negative_color())
					{
						negative_colors.push_back(boost::json::value_from(card_color_to_string(color)));
					}
					card_info_json["negativeColor"] = boost::json::value_from(negative_colors);
					hand_vector.push_back(boost::json::value_from(card_info_json));
				}
				player_information_vector.push_back(boost::json::value_from(hand_vector));
			}

			return boost::json::value_from(player_information_vector);
		}

		boost::json::value CCardInformation::virtual_pack(common::TIndex /*target_player_id*/) const
		{
			return this->to_json();
		}

	}
}
