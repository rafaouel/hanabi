#ifndef APIGATEWAY_H_EB2AFE84_2F45_40D5_B45B_074E40FE3D30
#define APIGATEWAY_H_EB2AFE84_2F45_40D5_B45B_074E40FE3D30
#pragma once

#include "action.h"
#include "apicontext.h"
#include "exception.h"
#include "servergameend.h"
#include "servergamestart.h"
#include "servergamestate.h"
#include "servergameoption.h"
#include "serverpeerinfo.h"
#include "types.h"


namespace hanabi
{
	namespace common
	{
		class CAPIGateway
		{
		public:
			typedef std::map<std::string, std::string> TQueryMap;
			typedef std::map<std::string, boost::posix_time::ptime> TTimings;
			typedef std::function<void(std::size_t tentative, std::size_t max_tentative, const TRequest& req, const TTimings& timings)> TCallbackOnConnecting;
			typedef std::function<void(std::size_t tentative, std::size_t max_tentative, const TRequest& req, const TTimings& timings, const boost::asio::ip::basic_resolver_results<boost::asio::ip::tcp>& resolver_results)> TCallbackOnConnected;
			typedef std::function<void(std::size_t tentative, std::size_t max_tentative, const TRequest& req, const TTimings& timings, std::size_t write_bytes_count)> TCallbackOnRequestSent;
			typedef std::function<void(std::size_t tentative, std::size_t max_tentative, const TRequest& req, const TResponse& res, const TTimings& timings, std::size_t read_bytes_count)> TCallbackOnResponseReceived;
			typedef std::function<void(std::size_t tentative, std::size_t max_tentative, const TRequest& req, const TResponse& res, const TTimings& timings)> TCallbackOnDisconnected;
			typedef std::function<void(std::size_t tentative, std::size_t max_tentative, const TRequest& req, const TTimings& timings, std::size_t tentative_delay, const std::exception& error)> TCallbackOnSendingFailure;


			CAPIGateway(const std::shared_ptr<CAPIContext>& p_context);
			virtual ~CAPIGateway() noexcept;


			const std::shared_ptr<CAPIContext>& context() const noexcept;

			// Reserve from preset
			virtual hanabi::common::TGameOptionId reserve_game_option(const hanabi::common::TCount& player_count, const hanabi::common::Preset& preset);
			// Reserve from game option id
			virtual void reserve_game_option(const hanabi::common::TGameOptionId& game_option_id);

			virtual void save_game_start(const hanabi::server_side::CGameStart& game_start);
			virtual void save_peer_info(const boost::uuids::uuid& game_uuid, const hanabi::server_side::CPeerInfo& peer_info);
			virtual void save_game_state(const hanabi::server_side::CGameState& game_state);
			virtual void save_player_action(const hanabi::common::IAction& player_action, const boost::uuids::uuid& game_uuid, hanabi::common::TCount player_turn_count, hanabi::common::TCount game_turn_count, hanabi::common::TIndex current_player_id, const boost::posix_time::ptime& request_timestamp, const boost::posix_time::ptime& response_timestamp);
			virtual void save_game_end(const hanabi::server_side::CGameEnd& game_end);

			virtual boost::json::value load_player_action(const boost::uuids::uuid& game_uuid, hanabi::common::TIndex current_player_id, std::size_t page = 0, std::size_t limit = 100);
			virtual boost::json::value load_game_option(const common::TGameOptionId& game_option_id);

			virtual void add_tag(const boost::uuids::uuid& game_uuid, const hanabi::common::TTagList& tags);

			virtual bool liveness_probe();
			virtual bool readiness_probe();
			virtual bool startup_probe();

			void set_callback_on_connecting(const TCallbackOnConnecting& callback);
			void set_callback_on_connected(const TCallbackOnConnected& callback);
			void set_callback_on_request_sent(const TCallbackOnRequestSent& callback);
			void set_callback_on_response_received(const TCallbackOnResponseReceived& callback);
			void set_callback_on_disconnected(const TCallbackOnDisconnected& callback);
			void set_callback_on_sending_failure(const TCallbackOnSendingFailure& callback);

		private:
			std::string base_64_encode(const std::string& s) const;
			std::string base_64_decode(const std::string& s) const;

			TResponse send_request(const TRequest& req) const;

			virtual const std::string& get_access_token();
			virtual void retrieve_tokens();
			virtual void refresh_tokens();

			void call_on_connecting(std::size_t tentative, std::size_t max_tentative, const TRequest& req, const TTimings& timings) const;
			void call_on_connected(std::size_t tentative, std::size_t max_tentative, const TRequest& req, const TTimings& timings, const boost::asio::ip::basic_resolver_results<boost::asio::ip::tcp>& resolver_results) const;
			void call_on_request_sent(std::size_t tentative, std::size_t max_tentative, const TRequest& req, const TTimings& timings, std::size_t write_bytes_count) const;
			void call_on_response_received(std::size_t tentative, std::size_t max_tentative, const TRequest& req, const TResponse& res, const TTimings& timings, std::size_t read_bytes_count) const;
			void call_on_disconnected(std::size_t tentative, std::size_t max_tentative, const TRequest& req, const TResponse& res, const TTimings& timings) const;
			void call_on_sending_failure(std::size_t tentative, std::size_t max_tentative, const TRequest& req, const TTimings& timings, std::size_t tentative_delay, const std::exception& error) const;

			const std::shared_ptr<CAPIContext> m_p_context;
			const int m_version;

			std::string m_access_token;
			boost::posix_time::ptime m_access_token_expiration_date;
			std::string m_refresh_token;
			boost::posix_time::ptime m_refresh_token_expiration_date;

			// Callbacks
			TCallbackOnConnecting m_on_connecting;
			TCallbackOnConnected m_on_connected;
			TCallbackOnRequestSent m_on_request_sent;
			TCallbackOnResponseReceived m_on_response_received;
			TCallbackOnDisconnected m_on_disconnected;
			TCallbackOnSendingFailure m_on_sending_failure;
		};
	}
}
#endif