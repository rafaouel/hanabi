#include "pch.h"
#include "updatable.h"

namespace hanabi
{
	namespace common
	{
		IUpdatable::IUpdatable()
		{

		}

		IUpdatable::~IUpdatable() noexcept
		{

		}

		void IUpdatable::update(const boost::json::value& value)
		{
			this->virtual_update(value);
		}
	}
}