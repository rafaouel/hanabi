#ifndef PACKET_H_3470CE79_95E8_415E_B4BB_5AF2B67E1B27
#define PACKET_H_3470CE79_95E8_415E_B4BB_5AF2B67E1B27

namespace hanabi
{
	namespace network
	{
		constexpr const char delimiter = ';';

		class CPacket
		{
		public:
			CPacket();
			virtual ~CPacket() noexcept;


			const char* data() const;
			char* data();

			std::size_t length() const;

			const char* body() const;
			char* body();

			std::size_t body_length() const;
			void body_length(std::size_t new_length);

			static constexpr std::size_t header_length = 6;
			static constexpr std::size_t max_body_length = 10000;

			bool decode_header();
			void encode_header();

		private:
			std::size_t m_body_length;
			char m_data[CPacket::header_length + CPacket::max_body_length];
		};

		enum class PacketType
		{
			HELLO,
			OK,
			NOT_OK,
			PEER_INFO_REQUEST,
			PEER_INFO_RESPONSE,
			PEER_INFO_UPDATE,
			GAME_OPTION_UPDATE,
			GAME_STATE_UPDATE,
			GAME_END_UPDATE,
			ACTION_REQUEST,
			ACTION_RESPONSE,
			ACTION_SPEAK_UPDATE,
			ACTION_DROP_UPDATE,
			ACTION_PLACE_UPDATE,
			BONUS_EVENT,
			ERROR_EVENT
		};

		PacketType packet_type(const CPacket& packet);

		CPacket build_packet(const std::string& content);
	}
}

#endif