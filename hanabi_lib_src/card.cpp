#include "pch.h"
#include "card.h"

#include "exception.h"


namespace hanabi
{
	namespace common
	{
		CCard::CCard(TValue value, CardColor color) :
			m_value(value),
			m_color(color)
		{

		}

		CCard::CCard(const boost::json::value& json_value) :
			m_value(json_value.as_object().at("value").as_int64()),
			m_color(string_to_card_color(json_value.as_object().at("color").as_string().c_str()))
		{

		}

		CCard::~CCard() noexcept
		{

		}

		TValue CCard::value() const
		{
			return this->m_value;
		}
		
		CardColor CCard::color() const
		{
			return this->m_color;
		}

		bool CCard::is(TValue value) const
		{
			return this->m_value == value;
		}

		bool CCard::is(CardColor color, const common::CColorOption& color_option) const
		{
			return color_option.reacts_to().contains(color);
		}

		bool CCard::operator<(const CCard& other) const
		{
			if (this->color() < other.color())
				return true;
			else
			{
				if (this->color() > other.color())
					return false;
				else
				{
					// left color == right color
					return this->value() < other.value();
				}
			}
		}

		bool CCard::operator==(const CCard& other) const
		{
			return this->color() == other.color() && this->value() == other.value();
		}

		boost::json::value CCard::serialize_to_json() const
		{
			boost::json::object card_json;
			card_json["color"] = boost::json::value_from(common::card_color_to_string(this->color()));
			card_json["value"] = boost::json::value_from(this->value());

			return card_json;
		}

		void CCard::parse_from_json(const boost::json::value& /*value*/)
		{
			THROW_ERROR(EImpossibleError);
		}
	}
	
}

std::ostream& operator<<(std::ostream& os, const enum hanabi::common::CardColor& color)
{
	switch (color)
	{
	case hanabi::common::CardColor::blue:
		os << "blue";
		break;
	case hanabi::common::CardColor::white:
		os << "white";
		break;
	case hanabi::common::CardColor::red:
		os << "red";
		break;
	case hanabi::common::CardColor::yellow:
		os << "yellow";
		break;
	case hanabi::common::CardColor::green:
		os << "green";
		break;
	case hanabi::common::CardColor::multicolor:
		os << "multi";
		break;
	case hanabi::common::CardColor::black:
		os << "black";
		break;
	default:
		THROW_ERROR(hanabi::EImpossibleError);
	}
	return os;
}

std::ostream& operator<<(std::ostream& os, const hanabi::common::CCard& card)
{
	std::ostringstream oss;
	oss << card.value() << "-" << card.color();
	os << oss.str();
	return os;
}

void swap(hanabi::common::CCard& left, hanabi::common::CCard& right)
{
	hanabi::common::CCard tmp(right);
	right = left;
	left = tmp;
}
