#include "pch.h"
#include "serverpeerinfo.h"

namespace hanabi
{
	namespace server_side
	{
		CPeerInfo::CPeerInfo() :
			m_name(""),
			m_ip(),
			m_id(common::invalid_index),
			m_version({0, 0, 0}),
			m_debug(true),
			m_type(common::PeerType::UNDEFINED)
		{

		}

		CPeerInfo::~CPeerInfo() noexcept
		{

		}

		const std::string& CPeerInfo::name() const noexcept
		{
			return this->m_name;
		}

		void CPeerInfo::name(const std::string& name)
		{
			this->m_name = name;
		}

		const boost::asio::ip::address& CPeerInfo::address() const noexcept
		{
			return this->m_ip;
		}

		void CPeerInfo::address(const boost::asio::ip::address& address)
		{
			this->m_ip = address;
		}

		common::TIndex CPeerInfo::id() const noexcept
		{
			return this->m_id;
		}

		void CPeerInfo::id(common::TIndex id)
		{
			this->m_id = id;
		}

		std::string CPeerInfo::version() const noexcept
		{
			return common::to_string(this->m_version);
		}

		const common::TVersionArray& CPeerInfo::version_array() const noexcept
		{
			return this->m_version;
		}

		void CPeerInfo::version_array(const common::TVersionArray& array)
		{
			this->m_version = array;
		}

		bool CPeerInfo::debug() const noexcept
		{
			return this->m_debug;
		}

		void CPeerInfo::debug(bool is_debug)
		{
			this->m_debug = is_debug;
		}

		common::PeerType CPeerInfo::type() const noexcept
		{
			assert(this->m_type != common::PeerType::UNDEFINED);
			return this->m_type;
		}

		void CPeerInfo::type(common::PeerType type)
		{
			assert(type != common::PeerType::UNDEFINED);
			this->m_type = type;
		}

		const common::TTagList& CPeerInfo::tags() const noexcept
		{
			return this->m_tags;
		}

		void CPeerInfo::tags(const common::TTagList& tags)
		{
			this->m_tags = tags;
		}

		boost::json::value CPeerInfo::virtual_pack(common::TIndex /*player_id*/) const
		{
			boost::json::object root;

			root["name"] = boost::json::value_from(this->name());
			root["playerId"] = boost::json::value_from(this->id());
			root["type"] = boost::json::value_from(common::to_string(this->type()));

			return root;
		}

		void CPeerInfo::virtual_update(const boost::json::value& root)
		{
			const boost::json::object& root_object = root.as_object();

			this->name(root_object.at("name").as_string().c_str());
			this->type(common::peer_type_from_string(root_object.at("type").as_string().c_str()));
			const boost::json::array& version_json_array = root_object.at("versionArray").as_array();
			common::TVersionArray version_array = {};
			for (std::size_t i = 0; i < version_array.size(); ++i)
			{
				version_array[i] = int(version_json_array.at(i).as_int64());
			}
			this->version_array(version_array);
			this->type(common::peer_type_from_string(root_object.at("type").as_string().c_str()));
			this->debug(root_object.at("debug").as_bool());
			const boost::json::array& tags_array = root_object.at("tags").as_array();
			common::TTagList tags = {};
			for (const boost::json::value& json_tag : tags_array)
			{
				tags.insert(json_tag.as_string().c_str());
			}
			this->tags(tags);
		}

		boost::json::value CPeerInfo::serialize_to_json() const
		{
			boost::json::object root;

			root["name"] = boost::json::value_from(this->name());
			root["playerId"] = boost::json::value_from(this->id());
			root["type"] = boost::json::value_from(common::to_string(this->type()));

			root["ip"] = boost::json::value_from(this->address().to_v4().to_string());
			root["versionArray"] = boost::json::value_from(this->version_array());
			root["version"] = boost::json::value_from(this->version());
			root["debug"] = boost::json::value_from(this->debug());

			root["tags"] = boost::json::value_from(this->tags());

			return root;
		}
	}
}