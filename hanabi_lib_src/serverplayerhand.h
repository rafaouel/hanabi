#ifndef SERVERPLAYERHAND_H_BA280457_AAA4_4F4D_869D_B8F325ECC58C
#define SERVERPLAYERHAND_H_BA280457_AAA4_4F4D_869D_B8F325ECC58C
#pragma once

#include "sendable.h"
#include "serializable.h"
#include "servergameoption.h"


namespace hanabi
{
	namespace server_side 
	{
		class CPlayerHand : public common::ISerializable, public common::ISendable
		{
		public:
			typedef std::vector<common::CCard> THand;
			typedef std::vector<THand> TPlayerHand;


			CPlayerHand(const CGameOption& option);
			virtual ~CPlayerHand() noexcept;


			const TPlayerHand& player_hand() const noexcept;
			TPlayerHand& player_hand() noexcept;

			const THand& hand(common::TIndex player_id) const;
			THand& hand(common::TIndex player_id);

			void insert(common::TIndex player_id, const common::CCard& card);
			common::CCard remove(common::TIndex player_id, common::TIndex hand_index);

		private:
			virtual boost::json::value serialize_to_json() const;
			virtual void parse_from_json(const boost::json::value& /*root*/) {}
			virtual boost::json::value virtual_pack(common::TIndex target_player_id) const;

			TPlayerHand m_player_hand;
		};
	}
}

#endif