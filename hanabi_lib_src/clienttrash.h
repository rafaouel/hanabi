#ifndef CLIENTTRASH_H_7AE71D03_4992_43E3_9BC3_2DC8DA85B2BF
#define CLIENTTRASH_H_7AE71D03_4992_43E3_9BC3_2DC8DA85B2BF
#pragma once

#include "card.h"
#include "updatable.h"


namespace hanabi
{
	namespace client_side
	{
		class CTrash : public common::IUpdatable
		{
		public:
			CTrash();
			virtual ~CTrash() noexcept;


			common::TScore current_max_score() const noexcept;
			void current_max_score(common::TScore score);

			const common::TCardStack& stack() const noexcept;
			common::TCardStack& stack();


		private:
			virtual void virtual_update(const boost::json::value& root_value);


			common::TScore m_max_score;
			common::TCardStack m_stack;
		};
	}
}

#endif