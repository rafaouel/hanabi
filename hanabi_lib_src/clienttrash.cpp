#include "pch.h"
#include "clienttrash.h"


namespace hanabi
{
	namespace client_side
	{
		CTrash::CTrash() :
			m_max_score(0)
		{

		}

		CTrash::~CTrash() noexcept
		{

		}

		common::TScore CTrash::current_max_score() const noexcept
		{
			return this->m_max_score;
		}

		void CTrash::current_max_score(common::TScore score)
		{
			this->m_max_score = score;
		}

		const common::TCardStack& CTrash::stack() const noexcept
		{
			return this->m_stack;
		}

		common::TCardStack& CTrash::stack()
		{
			return this->m_stack;
		}

		void CTrash::virtual_update(const boost::json::value& root_value)
		{
			const boost::json::object root_object = root_value.as_object();

			this->current_max_score(root_object.at("currentMaxScore").as_int64());

			this->stack().clear();
			for (const auto& card_value : root_object.at("trash").as_array())
			{
				const common::CCard card(card_value);

				this->stack().push_back(card);
			}
		}
	}
}
