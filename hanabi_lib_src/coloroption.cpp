#include "pch.h"
#include "coloroption.h"

#include "exception.h"


namespace hanabi
{
	namespace common
	{
		CColorOption::CColorOption(const TValueDistribution& distribution, bool is_ascending, const common::TColorSet& reacts_to) :
			m_value_distribution(distribution),
			m_is_ascending(is_ascending),
			m_reacts_to(reacts_to)
		{

		}

		CColorOption::~CColorOption() noexcept
		{

		}

		const CColorOption::TValueDistribution& CColorOption::distribution() const noexcept
		{
			return this->m_value_distribution;
		}

		common::TCount CColorOption::distribution(TValue value) const noexcept
		{
			return this->m_value_distribution.at(value);
		}

		bool CColorOption::is_ascending() const noexcept
		{
			return this->m_is_ascending;
		}

		const common::TColorSet& CColorOption::reacts_to() const noexcept
		{
			return this->m_reacts_to;
		}

		std::string card_color_to_string(CardColor color)
		{
			switch (color)
			{
				case CardColor::blue:
					return "blue";
				case CardColor::white:
					return "white";
				case CardColor::red:
					return "red";
				case CardColor::black:
					return "black";
				case CardColor::green:
					return "green";
				case CardColor::yellow:
					return "yellow";
				case CardColor::multicolor:
					return "multicolor";
				default:
					std::ostringstream oss;
					oss << "Unknown color value";
					THROW_ERROR_1(EImpossibleError, oss.str());
			}
		}

		char card_color_to_char(CardColor color)
		{
			switch (color)
			{
			case CardColor::blue:
				return 'b';
			case CardColor::white:
				return 'w';
			case CardColor::red:
				return 'r';
			case CardColor::black:
				return 'k';
			case CardColor::green:
				return 'g';
			case CardColor::yellow:
				return 'y';
			case CardColor::multicolor:
				return 'm';
			default:
				std::ostringstream oss;
				oss << "Unknown color value";
				THROW_ERROR_1(EImpossibleError, oss.str());
			}
		}

		CardColor string_to_card_color(const std::string& string)
		{
			if (string.compare("blue") == 0)
				return CardColor::blue;
			else if (string.compare("white") == 0)
				return CardColor::white;
			else if (string.compare("red") == 0)
				return CardColor::red;
			else if (string.compare("black") == 0)
				return CardColor::black;
			else if (string.compare("green") == 0)
				return CardColor::green;
			else if (string.compare("yellow") == 0)
				return CardColor::yellow;
			else if (string.compare("multicolor") == 0)
				return CardColor::multicolor;
			else
				THROW_ERROR_1(EParsingError, "Unknown color '" + string + "'");
		}

		CardColor char_to_card_color(const char& c)
		{
			if (c == 'b')
				return CardColor::blue;
			else if (c == 'w')
				return CardColor::white;
			else if (c == 'r')
				return CardColor::red;
			else if (c == 'k')
				return CardColor::black;
			else if (c == 'g')
				return CardColor::green;
			else if (c == 'y')
				return CardColor::yellow;
			else if (c == 'm')
				return CardColor::multicolor;
			else
			{
				std::ostringstream oss;
				oss << "Unknown color '" << c << "'";
				THROW_ERROR_1(EParsingError, oss.str());
			}	
		}
	}
}
