#include "pch.h"
#include "clientcardinformation.h"


namespace hanabi
{
	namespace client_side
	{
		CCardInformation::CCardInformation()
		{

		}

		CCardInformation::~CCardInformation() noexcept
		{

		}

		const CCardInformation::TPlayerInformation& CCardInformation::information() const noexcept
		{
			return this->m_player_information;
		}

		CCardInformation::TPlayerInformation& CCardInformation::information() noexcept
		{
			return this->m_player_information;
		}

		const CCardInformation::TInformation& CCardInformation::information(common::TIndex player_id) const noexcept
		{
			return this->information().at(player_id);
		}

		void CCardInformation::virtual_update(const boost::json::value& root_value)
		{
			this->information().clear();

			const boost::json::array& root_array = root_value.as_array();

			for (const auto& hand : root_array)
			{
				TInformation information_container;

				for (const auto& info_value : hand.as_array())
				{
					const boost::json::object& info_object = info_value.as_object();

					common::CCardRawInformation card_information;

					// Add positive infos (if any)
					const common::TValue value_info = info_object.at("positiveValue").as_int64();
					if (value_info > 0)
						card_information.put(value_info);

					for (const auto& info : info_object.at("positiveColor").as_array())
					{
						card_information.put(common::string_to_card_color(info.as_string().c_str()));
					}

					// Add negative infos (if any)
					for (const auto& info : info_object.at("negativeValue").as_array())
					{
						card_information.put_not(info.as_int64());
					}

					for (const auto& info : info_object.at("negativeColor").as_array())
					{
						card_information.put_not(common::string_to_card_color(info.as_string().c_str()));
					}

					information_container.push_back(card_information);
				}

				this->information().push_back(information_container);
			}
		}
	}
}