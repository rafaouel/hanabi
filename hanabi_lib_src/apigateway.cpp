#include "pch.h"
#include "apigateway.h"

#include "exception.h"


namespace hanabi
{
	namespace common
	{
		CAPIGateway::CAPIGateway(const std::shared_ptr<CAPIContext>& p_context) :
			m_p_context(p_context),
			m_version(11),
			m_access_token(""),
			m_access_token_expiration_date(boost::posix_time::min_date_time),
			m_refresh_token(""),
			m_refresh_token_expiration_date(boost::posix_time::min_date_time)
		{
			assert(p_context);
		}

		CAPIGateway::~CAPIGateway() noexcept
		{

		}

		const std::shared_ptr<CAPIContext>& CAPIGateway::context() const noexcept
		{
			assert(this->m_p_context);
			return this->m_p_context;
		}

		hanabi::common::TGameOptionId CAPIGateway::reserve_game_option(const hanabi::common::TCount& player_count, const hanabi::common::Preset& preset)
		{
			// Target
			const std::string api_target = "/api/v2/hanabi/gameOption/reserveFromPreset";
		
			// Auth
			const std::string bearer_auth = "Bearer " + this->get_access_token();
		
			// Create the body of the request
			boost::json::object content;
			content["playerCount"] = player_count;
			content["preset"] = common::to_string(preset);
			const std::string body = boost::json::serialize(content);
		
			// Create the request
			TRequest req{ boost::beast::http::verb::patch, api_target, this->m_version };
			req.set(boost::beast::http::field::host, this->context()->api_host());
			req.set(boost::beast::http::field::user_agent, BOOST_BEAST_VERSION_STRING);
			req.set(boost::beast::http::field::content_type, "application/json");
			req.set(boost::beast::http::field::authorization, bearer_auth);
			req.body() = body;
			req.prepare_payload();
		
			TResponse res = this->send_request(req);
		
			// If result is not ok
			if (res.result_int() < 200 && res.result_int() >= 400)
			{
				THROW_ERROR_4(EHttpError, req, *this->context(), res, "Unable to reserve a game option");
			}
		
			// Parse the response body into json
			std::istringstream is(res.body());
			boost::json::stream_parser parser;
			std::string line;
			while (std::getline(is, line))
			{
				parser.write(line);
			}
			parser.finish();
			const boost::json::value parser_result = parser.release();

			// Extract the game option id
			const common::TGameOptionId game_option_id(parser_result.at("gameOptionId").as_string().c_str());

			return game_option_id;
		}

		void CAPIGateway::reserve_game_option(const hanabi::common::TGameOptionId& game_option_id)
		{
			// Target
			const std::string api_target = "/api/v2/hanabi/gameOption/gameOptionId/" + game_option_id + "/reserve";

			// Auth
			const std::string bearer_auth = "Bearer " + this->get_access_token();

			// Create the request
			TRequest req{ boost::beast::http::verb::patch, api_target, this->m_version };
			req.set(boost::beast::http::field::host, this->context()->api_host());
			req.set(boost::beast::http::field::user_agent, BOOST_BEAST_VERSION_STRING);
			req.set(boost::beast::http::field::authorization, bearer_auth);

			TResponse res = this->send_request(req);

			// If result is not ok
			if (res.result_int() < 200 && res.result_int() >= 400)
			{
				THROW_ERROR_4(EHttpError, req, *this->context(), res, "Unable to reserve a game option");
			}
		}

		void CAPIGateway::save_game_start(const hanabi::server_side::CGameStart& game_start)
		{
			// Target
			const std::string api_target = "/api/v1/hanabi/gameStart";

			// Auth
			const std::string bearer_auth = "Bearer " + this->get_access_token();

			// Create the body of the request
			boost::json::object content = game_start.to_json().as_object();
			const std::string body = boost::json::serialize(content);

			// Create the request
			TRequest req{ boost::beast::http::verb::post, api_target, this->m_version };
			req.set(boost::beast::http::field::host, this->context()->api_host());
			req.set(boost::beast::http::field::user_agent, BOOST_BEAST_VERSION_STRING);
			req.set(boost::beast::http::field::content_type, "application/json");
			req.set(boost::beast::http::field::authorization, bearer_auth);
			req.body() = body;
			req.prepare_payload();

			TResponse res = this->send_request(req);

			// If result is not ok
			if (res.result_int() < 200 && res.result_int() >= 400)
			{
				THROW_ERROR_4(EHttpError, req, *this->context(), res, "Unable to save game start");
			}
		}

		void CAPIGateway::save_peer_info(const boost::uuids::uuid& game_uuid, const hanabi::server_side::CPeerInfo& peer_info)
		{
			// Target
			const std::string api_target = "/api/v1/hanabi/peerInfo";

			// Auth
			const std::string bearer_auth = "Bearer " + this->get_access_token();

			// Create the body of the request
			boost::json::object content = peer_info.to_json().as_object();
			content["gameUuid"] = boost::json::value_from(boost::uuids::to_string(game_uuid));
			const std::string body = boost::json::serialize(content);

			// Create the request
			TRequest req{ boost::beast::http::verb::post, api_target, this->m_version };
			req.set(boost::beast::http::field::host, this->context()->api_host());
			req.set(boost::beast::http::field::user_agent, BOOST_BEAST_VERSION_STRING);
			req.set(boost::beast::http::field::content_type, "application/json");
			req.set(boost::beast::http::field::authorization, bearer_auth);
			req.body() = body;
			req.prepare_payload();

			TResponse res = this->send_request(req);

			// If result is not ok
			if (res.result_int() < 200 && res.result_int() >= 400)
			{
				THROW_ERROR_4(EHttpError, req, *this->context(), res, "Unable to save peer info");
			}
		}

		void CAPIGateway::save_game_state(const hanabi::server_side::CGameState& game_state)
		{
			// Target
			const std::string api_target = "/api/v1/hanabi/gameState";

			// Auth
			const std::string bearer_auth = "Bearer " + this->get_access_token();

			// Create the body of the request
			boost::json::object content = game_state.to_json().as_object();
			const std::string body = boost::json::serialize(content);

			// Create the request
			TRequest req{ boost::beast::http::verb::post, api_target, this->m_version };
			req.set(boost::beast::http::field::host, this->context()->api_host());
			req.set(boost::beast::http::field::user_agent, BOOST_BEAST_VERSION_STRING);
			req.set(boost::beast::http::field::content_type, "application/json");
			req.set(boost::beast::http::field::authorization, bearer_auth);
			req.body() = body;
			req.prepare_payload();

			TResponse res = this->send_request(req);

			// If result is not ok
			if (res.result_int() < 200 && res.result_int() >= 400)
			{
				THROW_ERROR_4(EHttpError, req, *this->context(), res, "Unable to save game state");
			}
		}

		void CAPIGateway::save_player_action(const hanabi::common::IAction& player_action, const boost::uuids::uuid& game_uuid, hanabi::common::TCount player_turn_count, hanabi::common::TCount game_turn_count, hanabi::common::TIndex current_player_id, const boost::posix_time::ptime& request_timestamp, const boost::posix_time::ptime& response_timestamp)
		{
			// Target
			const std::string api_target = "/api/v1/hanabi/playerAction";

			// Auth
			const std::string bearer_auth = "Bearer " + this->get_access_token();

			// Create the body of the request
			boost::json::object content = player_action.to_json().as_object();
			content["gameUuid"] = boost::json::value_from(boost::uuids::to_string(game_uuid));
			content["playerTurnCount"] = boost::json::value_from(player_turn_count);
			content["gameTurnCount"] = boost::json::value_from(game_turn_count);
			content["currentPlayerId"] = boost::json::value_from(current_player_id);
			content["requestTimestamp"] = boost::json::value_from(boost::posix_time::to_iso_extended_string(request_timestamp) + "Z");
			content["responseTimestamp"] = boost::json::value_from(boost::posix_time::to_iso_extended_string(response_timestamp) + "Z");
			content["roundtripDuration"] = boost::json::value_from(double((response_timestamp - request_timestamp).total_milliseconds()) / 1000.);
			const std::string body = boost::json::serialize(content);

			// Create the request
			TRequest req{ boost::beast::http::verb::post, api_target, this->m_version };
			req.set(boost::beast::http::field::host, this->context()->api_host());
			req.set(boost::beast::http::field::user_agent, BOOST_BEAST_VERSION_STRING);
			req.set(boost::beast::http::field::content_type, "application/json");
			req.set(boost::beast::http::field::authorization, bearer_auth);
			req.body() = body;
			req.prepare_payload();

			TResponse res = this->send_request(req);

			// If result is not ok
			if (res.result_int() < 200 && res.result_int() >= 400)
			{
				THROW_ERROR_4(EHttpError, req, *this->context(), res, "Unable to save player action");
			}
		}

		void CAPIGateway::save_game_end(const hanabi::server_side::CGameEnd& game_end)
		{
			// Target
			const std::string api_target = "/api/v1/hanabi/gameEnd";

			// Auth
			const std::string bearer_auth = "Bearer " + this->get_access_token();

			// Create the body of the request
			boost::json::object content = game_end.to_json().as_object();
			const std::string body = boost::json::serialize(content);

			// Create the request
			TRequest req{ boost::beast::http::verb::post, api_target, this->m_version };
			req.set(boost::beast::http::field::host, this->context()->api_host());
			req.set(boost::beast::http::field::user_agent, BOOST_BEAST_VERSION_STRING);
			req.set(boost::beast::http::field::content_type, "application/json");
			req.set(boost::beast::http::field::authorization, bearer_auth);
			req.body() = body;
			req.prepare_payload();

			TResponse res = this->send_request(req);

			// If result is not ok
			if (res.result_int() < 200 && res.result_int() >= 400)
			{
				THROW_ERROR_4(EHttpError, req, *this->context(), res, "Unable to save game end");
			}
		}

		boost::json::value CAPIGateway::load_player_action(const boost::uuids::uuid& game_uuid, hanabi::common::TIndex current_player_id, std::size_t page, std::size_t limit)
		{
			// Target
			const std::string api_target = "/api/v1/hanabi/playerAction";

			// Auth
			const std::string bearer_auth = "Bearer " + this->get_access_token();

			// Build the path
			std::ostringstream target_full;
			target_full << api_target << "?" << "gameUuid=" << boost::uuids::to_string(game_uuid);
			target_full << "&" << "currentPlayerId=" << current_player_id;
			target_full << "&" << "page=" << page;
			target_full << "&" << "limit=" << limit;

			// Create the request
			TRequest req{ boost::beast::http::verb::get, target_full.str(), this->m_version };
			req.set(boost::beast::http::field::host, this->context()->api_host());
			req.set(boost::beast::http::field::user_agent, BOOST_BEAST_VERSION_STRING);
			req.set(boost::beast::http::field::authorization, bearer_auth);

			TResponse res = this->send_request(req);

			// If result is not ok
			if (res.result_int() < 200 && res.result_int() >= 400)
			{
				THROW_ERROR_4(EHttpError, req, *this->context(), res, "Unable to load a player action");
			}

			// Parse the response body into json
			std::istringstream is(res.body());
			boost::json::stream_parser parser;
			std::string line;
			while (std::getline(is, line))
			{
				parser.write(line);
			}
			parser.finish();

			return parser.release();
		}

		boost::json::value CAPIGateway::load_game_option(const common::TGameOptionId& game_option_id)
		{
			// Target
			const std::string api_target = "/api/v2/hanabi/gameOption";

			// Auth
			const std::string bearer_auth = "Bearer " + this->get_access_token();

			// Build the path
			std::ostringstream target_full;
			target_full << api_target << "?" << "gameOptionId=" << game_option_id;

			// Create the request
			TRequest req{ boost::beast::http::verb::get, target_full.str(), this->m_version };
			req.set(boost::beast::http::field::host, this->context()->api_host());
			req.set(boost::beast::http::field::user_agent, BOOST_BEAST_VERSION_STRING);
			req.set(boost::beast::http::field::authorization, bearer_auth);

			TResponse res = this->send_request(req);

			// If result is not ok
			if (res.result_int() < 200 && res.result_int() >= 400)
			{
				THROW_ERROR_4(EHttpError, req, *this->context(), res, "Unable to retrieve a game option");
			}

			// Parse the response body into json
			std::istringstream is(res.body());
			boost::json::stream_parser parser;
			std::string line;
			while (std::getline(is, line))
			{
				parser.write(line);
			}
			parser.finish();

			// Get the parsed value
			const boost::json::value parsed_value = parser.release();

			return parsed_value;
		}

		void CAPIGateway::add_tag(const boost::uuids::uuid& game_uuid, const hanabi::common::TTagList& tags)
		{
			// Target
			const std::string api_target = "/api/v1/hanabi/gameStart/gameUuid/" + boost::uuids::to_string(game_uuid);

			// Auth
			const std::string bearer_auth = "Bearer " + this->get_access_token();

			// Create the body of the request
			boost::json::array tag_array;
			for (const std::string& tag : tags) {

				const boost::json::string json_tag(tag.c_str(), tag.size());
				tag_array.push_back(json_tag);
			}
			boost::json::object content;
			content["tags"] = tag_array;
			const std::string body = boost::json::serialize(content);

			// Create the request
			TRequest req{ boost::beast::http::verb::patch, api_target, this->m_version };
			req.set(boost::beast::http::field::host, this->context()->api_host());
			req.set(boost::beast::http::field::user_agent, BOOST_BEAST_VERSION_STRING);
			req.set(boost::beast::http::field::content_type, "application/json");
			req.set(boost::beast::http::field::authorization, bearer_auth);
			req.body() = body;
			req.prepare_payload();

			TResponse res = this->send_request(req);

			// If result is not ok
			if (res.result_int() < 200 && res.result_int() >= 400)
			{
				THROW_ERROR_4(EHttpError, req, *this->context(), res, "Unable to update tags");
			}
		}

		bool CAPIGateway::liveness_probe()
		{
			try
			{
				// Target
				const std::string api_target = "/api/v1/healthcheck/liveness-probe/";

				// Create the request
				TRequest req{ boost::beast::http::verb::get, api_target, this->m_version };
				req.set(boost::beast::http::field::host, this->context()->api_host());
				req.set(boost::beast::http::field::user_agent, BOOST_BEAST_VERSION_STRING);

				TResponse res = this->send_request(req);
				
				return res.result_int() == 200;
			}
			catch (...)
			{
				return false;
			}
		}

		bool CAPIGateway::readiness_probe()
		{
			try
			{
				// Target
				const std::string api_target = "/api/v1/healthcheck/readiness-probe/";

				// Create the request
				TRequest req{ boost::beast::http::verb::get, api_target, this->m_version };
				req.set(boost::beast::http::field::host, this->context()->api_host());
				req.set(boost::beast::http::field::user_agent, BOOST_BEAST_VERSION_STRING);

				TResponse res = this->send_request(req);

				return res.result_int() == 200;
			}
			catch (...)
			{
				return false;
			}
		}

		bool CAPIGateway::startup_probe()
		{
			try
			{
				// Target
				const std::string api_target = "/api/v1/healthcheck/startup-probe/";

				// Create the request
				TRequest req{ boost::beast::http::verb::get, api_target, this->m_version };
				req.set(boost::beast::http::field::host, this->context()->api_host());
				req.set(boost::beast::http::field::user_agent, BOOST_BEAST_VERSION_STRING);

				TResponse res = this->send_request(req);

				return res.result_int() == 200;
			}
			catch (...)
			{
				return false;
			}
		}

		void CAPIGateway::set_callback_on_connecting(const TCallbackOnConnecting& callback)
		{
			this->m_on_connecting = callback;
		}

		void CAPIGateway::set_callback_on_connected(const TCallbackOnConnected& callback)
		{
			this->m_on_connected = callback;
		}

		void CAPIGateway::set_callback_on_request_sent(const TCallbackOnRequestSent& callback)
		{
			this->m_on_request_sent = callback;
		}

		void CAPIGateway::set_callback_on_response_received(const TCallbackOnResponseReceived& callback)
		{
			this->m_on_response_received = callback;
		}

		void CAPIGateway::set_callback_on_disconnected(const TCallbackOnDisconnected& callback)
		{
			this->m_on_disconnected = callback;
		}

		void CAPIGateway::set_callback_on_sending_failure(const TCallbackOnSendingFailure& callback)
		{
			this->m_on_sending_failure = callback;
		}

		std::string CAPIGateway::base_64_encode(const std::string& s) const
		{
			using namespace boost::archive::iterators;
			using It = base64_from_binary<transform_width<std::string::const_iterator, 6, 8>>;
			auto tmp = std::string(It(std::begin(s)), It(std::end(s)));
			return tmp.append((3 - s.size() % 3) % 3, '=');
		}

		std::string CAPIGateway::base_64_decode(const std::string& s) const
		{
			using namespace boost::archive::iterators;
			using It = transform_width<binary_from_base64<std::string::const_iterator>, 8, 6>;
			return boost::algorithm::trim_right_copy_if(std::string(It(std::begin(s)), It(std::end(s))), [](char c) {
				return c == '\0';
				});
		}

		const std::string& CAPIGateway::get_access_token()
		{
			const boost::posix_time::time_duration expiration_date_margin = boost::posix_time::duration_from_string("0:00:30");
			const boost::posix_time::ptime now = boost::posix_time::second_clock::universal_time();

			// Does an access token exists ?
			if (this->m_access_token.length() > 0)
			{
				// An accesss token exists...

				// Does it need to be refreshed ?
				if (now + expiration_date_margin < this->m_access_token_expiration_date)
				{
					// This token could be used

					return this->m_access_token;
				}
			}

			// The access token need to be refreshed...
			// Do we have a refresh token ?
			if (this->m_refresh_token.length() > 0)
			{
				// A refresh token exists

				// Has it expired ?
				if (now + expiration_date_margin < this->m_refresh_token_expiration_date)
				{
					// The refresh token could be used
					this->refresh_tokens();
				}
			}

			// We do not have a refresh token available...
			// Ask for a brand new token set
			this->retrieve_tokens();

			return this->m_access_token;
		}

		void CAPIGateway::retrieve_tokens()
		{
			const std::string basic_auth = "Basic " + this->base_64_encode(this->m_p_context->api_login() + ":" + this->m_p_context->api_password());

			// Target
			const std::string api_target = "/api/v1/tokens";

			// Create the request
			TRequest req{ boost::beast::http::verb::post, api_target, this->m_version };
			req.set(boost::beast::http::field::host, this->context()->api_host());
			req.set(boost::beast::http::field::user_agent, BOOST_BEAST_VERSION_STRING);
			req.set(boost::beast::http::field::content_type, "application/json");
			req.set(boost::beast::http::field::authorization, basic_auth);
			req.prepare_payload();

			TResponse res = this->send_request(req);

			// Parse the response body into json
			std::istringstream is(res.body());
			boost::json::stream_parser parser;
			std::string line;
			while (std::getline(is, line))
			{
				parser.write(line);
			}
			parser.finish();

			const boost::json::value body_value = parser.release();
			this->m_access_token = body_value.as_object().at("accessToken").as_string().c_str();
			std::string access_token_expiration_date_string(body_value.as_object().at("accessTokenExpirationDate").as_string().c_str());
			boost::erase_all(access_token_expiration_date_string, "Z");
			this->m_access_token_expiration_date = boost::posix_time::from_iso_extended_string(access_token_expiration_date_string);
			this->m_refresh_token = body_value.as_object().at("refreshToken").as_string().c_str();
			std::string refresh_token_expiration_date_string(body_value.as_object().at("refreshTokenExpirationDate").as_string().c_str());
			boost::erase_all(refresh_token_expiration_date_string, "Z");
			this->m_refresh_token_expiration_date = boost::posix_time::from_iso_extended_string(refresh_token_expiration_date_string);
		}

		void CAPIGateway::refresh_tokens()
		{
			assert(this->m_refresh_token.length() > 0);
			const std::string bearer_auth = "Bearer " + this->m_refresh_token;

			// Target
			const std::string api_target = "/api/v1/tokens";

			// Create the request
			TRequest req{ boost::beast::http::verb::put, api_target, this->m_version };
			req.set(boost::beast::http::field::host, this->context()->api_host());
			req.set(boost::beast::http::field::user_agent, BOOST_BEAST_VERSION_STRING);
			req.set(boost::beast::http::field::content_type, "application/json");
			req.set(boost::beast::http::field::authorization, bearer_auth);
			req.prepare_payload();

			TResponse res = this->send_request(req);

			// If result is not ok
			if (res.result_int() != 201)
			{
				THROW_ERROR_4(EHttpError, req, *this->context(), res, "Unable to refresh the tokens");
			}

			// Parse the response body into json
			std::istringstream is(res.body());
			boost::json::stream_parser parser;
			std::string line;
			while (std::getline(is, line))
			{
				parser.write(line);
			}
			parser.finish();

			const boost::json::value body_value = parser.release();
			this->m_access_token = body_value.as_object().at("accessToken").as_string().c_str();
			std::string access_token_expiration_date_string(body_value.as_object().at("accessTokenExpirationDate").as_string().c_str());
			boost::erase_all(access_token_expiration_date_string, "Z");
			this->m_access_token_expiration_date = boost::posix_time::from_iso_extended_string(access_token_expiration_date_string);
			this->m_refresh_token = body_value.as_object().at("refreshToken").as_string().c_str();
			std::string refresh_token_expiration_date_string(body_value.as_object().at("refreshTokenExpirationDate").as_string().c_str());
			boost::erase_all(refresh_token_expiration_date_string, "Z");
			this->m_refresh_token_expiration_date = boost::posix_time::from_iso_extended_string(refresh_token_expiration_date_string);
		}

		void CAPIGateway::call_on_connecting(std::size_t tentative, std::size_t max_tentative, const TRequest& req, const TTimings& timings) const
		{
			if (this->m_on_connecting)
				this->m_on_connecting(tentative, max_tentative, req, timings);
		}

		void CAPIGateway::call_on_connected(std::size_t tentative, std::size_t max_tentative, const TRequest& req, const TTimings& timings, const boost::asio::ip::basic_resolver_results<boost::asio::ip::tcp>& resolver_results) const
		{
			if (this->m_on_connected)
				this->m_on_connected(tentative, max_tentative, req, timings, resolver_results);
		}

		void CAPIGateway::call_on_request_sent(std::size_t tentative, std::size_t max_tentative, const TRequest& req, const TTimings& timings, std::size_t write_bytes_count) const
		{
			if (this->m_on_request_sent)
				this->m_on_request_sent(tentative, max_tentative, req, timings, write_bytes_count);
		}

		void CAPIGateway::call_on_response_received(std::size_t tentative, std::size_t max_tentative, const TRequest& req, const TResponse& res, const TTimings& timings, std::size_t read_bytes_count) const
		{
			if (this->m_on_response_received)
				this->m_on_response_received(tentative, max_tentative, req, res, timings, read_bytes_count);
		}

		void CAPIGateway::call_on_disconnected(std::size_t tentative, std::size_t max_tentative, const TRequest& req, const TResponse& res, const TTimings& timings) const
		{
			if (this->m_on_disconnected)
				this->m_on_disconnected(tentative, max_tentative, req, res, timings);
		}

		void CAPIGateway::call_on_sending_failure(std::size_t tentative, std::size_t max_tentative, const TRequest& req, const TTimings& timings, std::size_t tentative_delay, const std::exception& error) const
		{
			if (this->m_on_sending_failure)
				this->m_on_sending_failure(tentative, max_tentative, req, timings, tentative_delay, error);
		}

		TResponse CAPIGateway::send_request(const TRequest& req) const
		{
			for (std::size_t tentative = 0; tentative < this->context()->max_tentative(); ++tentative)
			{
				TTimings timings;
				try
				{
					timings["connecting"] = boost::posix_time::microsec_clock().universal_time();
					this->call_on_connecting(tentative, this->context()->max_tentative(), req, timings);

					// Initiate connection
					boost::asio::io_context io_context;
					boost::asio::ip::tcp::resolver resolver(io_context);
					boost::beast::tcp_stream stream(io_context);
					const auto resolver_results = resolver.resolve(this->context()->api_host(), this->context()->api_port());
					stream.connect(resolver_results);

					// Connected
					timings["connected"] = boost::posix_time::microsec_clock().universal_time();
					this->call_on_connected(tentative, this->context()->max_tentative(), req, timings, resolver_results);
					
					// Sending the request
					const std::size_t write_bytes_count = boost::beast::http::write(stream, req);
					timings["sent"] = boost::posix_time::microsec_clock().universal_time();
					this->call_on_request_sent(tentative, this->context()->max_tentative(), req, timings, write_bytes_count);

					// Wait for the response
					boost::beast::flat_buffer buffer;
					TResponse res;
					const std::size_t read_bytes_count = boost::beast::http::read(stream, buffer, res);
					timings["response"] = boost::posix_time::microsec_clock().universal_time();
					this->call_on_response_received(tentative, this->context()->max_tentative(), req, res, timings, read_bytes_count);

					// Close the connection
					boost::beast::error_code ec;
					stream.socket().shutdown(boost::asio::ip::tcp::socket::shutdown_both, ec);
					if (ec && ec != boost::beast::errc::not_connected)
						throw boost::beast::system_error{ ec };

					timings["disconnected"] = boost::posix_time::microsec_clock().universal_time();
					this->call_on_disconnected(tentative, this->context()->max_tentative(), req, res, timings);

					return res;
				}
				catch (std::exception& error)
				{
					this->call_on_sending_failure(tentative, this->context()->max_tentative(), req, timings, this->context()->tentative_delay(), error);
					std::this_thread::sleep_for(std::chrono::milliseconds(this->context()->tentative_delay()));
					continue;
				}
			}
			THROW_ERROR_1(EAPIRequestError, "Max tentative reached");
		}
	}
}