#include "pch.h"
#include "serverboard.h"

#include "exception.h"

namespace hanabi
{
	namespace server_side
	{
		CBoard::CBoard(const CGameOption& game_option) :
			m_score(0),
			m_game_option(game_option),
			m_board()
		{
			// Copy the used colors
			for (const auto& pair : this->game_option().color_option())
			{
				this->m_board[pair.first] = 0;
			}
		}

		CBoard::~CBoard() noexcept
		{

		}

		bool CBoard::can_place(const common::CCard& card) const
		{
			const common::CardColor color = card.color();

			const auto board_iterator = this->m_board.find(color);
			if (board_iterator == this->m_board.cend())
				THROW_ERROR_1(EInvalidColorError, color);

			const auto option_iterator = this->game_option().color_option().find(color);
			if (option_iterator == this->game_option().color_option().cend())
				THROW_ERROR_1(EInvalidColorError, color);

			const common::TValue board_value = board_iterator->second;
			const common::CColorOption& color_option = option_iterator->second;
			const bool is_ascending_color = color_option.is_ascending();

			if (is_ascending_color)
			{
				if (board_value == 0 && card.value() == this->game_option().min_card_value())
				{
					return true;
				}
				return board_value + 1 == card.value();
			}
			else
			{
				if (board_value == 0 && card.value() == this->game_option().max_card_value())
				{
					return true;
				}
				return board_value - 1 == card.value();
			}
		}

		std::shared_ptr<common::CardColor> CBoard::place(const common::CCard& card)
		{
			if (!this->can_place(card))
				THROW_ERROR_1(ENotAllowedActionError, "Place");

			++this->m_score;

			const common::CardColor color = card.color();

			this->m_board[color] = card.value();

			const common::CColorOption& color_option = this->game_option().color_option().at(color);

			if (color_option.is_ascending() && card.value() == this->game_option().max_card_value())
			{
				return std::shared_ptr<common::CardColor>(new common::CardColor(color));
			}
			if (!color_option.is_ascending() && card.value() == this->game_option().min_card_value())
			{
				return std::shared_ptr<common::CardColor>(new common::CardColor(color));
			}
			return std::shared_ptr<common::CardColor>();
		}

		common::TScore CBoard::score() const noexcept
		{
			return this->m_score;
		}

		common::TValue CBoard::value(common::CardColor color) const
		{
			assert(this->board().contains(color));
			return this->m_board.at(color);
		}

		const common::TCardBoard& CBoard::board() const noexcept
		{
			return this->m_board;
		}

		common::TCardBoard& CBoard::board() noexcept
		{
			return this->m_board;
		}

		const CGameOption& CBoard::game_option() const noexcept
		{
			return this->m_game_option;
		}

		boost::json::value CBoard::serialize_to_json() const
		{
			boost::json::object board_object;

			board_object["score"] = boost::json::value_from(this->score());

			std::vector<boost::json::value> board_vector;
			for (const auto& pair : this->board())
			{
				const std::string color_string = card_color_to_string(pair.first);
				const common::TValue value = pair.second;
				boost::json::object color_object;
				color_object["color"] = boost::json::value_from(color_string);
				color_object["value"] = boost::json::value_from(value);
				board_vector.push_back(color_object);
			}

			board_object["board"] = boost::json::value_from(board_vector);

			return board_object;
		}

		boost::json::value CBoard::virtual_pack(common::TIndex /*target_player_id*/) const
		{
			return this->to_json();
		}
	}
}
