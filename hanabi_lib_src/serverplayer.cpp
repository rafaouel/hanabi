#include "pch.h"
#include "serverplayer.h"

namespace hanabi
{
	namespace server_side
	{
		CServerPlayer::CServerPlayer(const std::shared_ptr<IPlayerGateway> p_gateway, const CPeerInfo& peer_info) :
			m_p_gateway(p_gateway),
			m_peer_info(peer_info)
		{

		}

		CServerPlayer::~CServerPlayer() noexcept
		{

		}

		const std::shared_ptr<IPlayerGateway>& CServerPlayer::gateway() const noexcept
		{
			return this->m_p_gateway;
		}

		const CPeerInfo& CServerPlayer::peer_info() const noexcept
		{
			return this->m_peer_info;
		}
	}
}