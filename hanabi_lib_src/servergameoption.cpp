#include "pch.h"
#include "servergameoption.h"


namespace hanabi
{
	namespace server_side
	{
		/// <summary>
		/// Create a game option with default values
		/// </summary>
		CGameOption::CGameOption() :
			m_game_option_id(),
			m_timestamp(boost::posix_time::not_a_date_time),
			m_player_count(0),
			m_player_hand_max_size(0),
			m_start_player_id(0),
			m_max_error_token_count(0),
			m_max_speak_token_count(0),
			m_start_error_token_count(0),
			m_start_speak_token_count(0),
			m_min_card_value(0),
			m_max_card_value(0)
		{

		}

		CGameOption::~CGameOption() noexcept
		{

		}

		void CGameOption::game_option_id(const common::TGameOptionId& game_option_id)
		{
			assert(game_option_id.length() > 0);
			this->m_game_option_id = game_option_id;
		}

		const common::TGameOptionId& CGameOption::game_option_id() const noexcept
		{
			assert(this->m_game_option_id.length() > 0);
			return this->m_game_option_id;
		}

		void CGameOption::timestamp(const boost::posix_time::ptime& timestamp)
		{
			assert(timestamp != boost::posix_time::not_a_date_time);
			this->m_timestamp = timestamp;
		}

		const boost::posix_time::ptime& CGameOption::timestamp() const noexcept
		{
			assert(this->m_timestamp != boost::posix_time::not_a_date_time);
			return this->m_timestamp;
		}


		void CGameOption::player_count(common::TCount count)
		{
			assert(count > 1);
			this->m_player_count = count;
		}

		common::TCount CGameOption::player_count() const noexcept
		{
			assert(this->m_player_count > 1);
			return this->m_player_count;
		}

		void CGameOption::player_hand_max_size(common::TCount size)
		{
			assert(size > 0);
			this->m_player_hand_max_size = size;
		}

		common::TCount CGameOption::player_hand_max_size() const noexcept
		{
			assert(this->m_player_hand_max_size > 0);
			return this->m_player_hand_max_size;
		}

		void CGameOption::start_player_id(common::TIndex player_id)
		{
			assert(common::is_valid_index(player_id));
			assert(player_id < this->player_count());
			this->m_start_player_id = player_id;
		}

		common::TIndex CGameOption::start_player_id() const noexcept
		{
			assert(this->m_start_player_id < this->player_count());
			return this->m_start_player_id;
		}

		void CGameOption::max_error_token_count(common::TCount count)
		{
			assert(count > 0);
			this->m_max_error_token_count = count;
		}

		common::TCount CGameOption::max_error_token_count() const noexcept
		{
			assert(this->m_max_error_token_count > 0);
			return this->m_max_error_token_count;
		}

		void CGameOption::max_speak_token_count(common::TCount count)
		{
			assert(count > 0);
			this->m_max_speak_token_count = count;
		}

		common::TCount CGameOption::max_speak_token_count() const noexcept
		{
			assert(this->m_max_speak_token_count > 0);
			return this->m_max_speak_token_count;
		}

		void CGameOption::start_error_token_count(common::TCount count)
		{
			assert(count <= this->max_error_token_count());
			this->m_start_error_token_count = count;
		}

		common::TCount CGameOption::start_error_token_count() const noexcept
		{
			assert(this->m_start_error_token_count <= this->max_error_token_count());
			return this->m_start_error_token_count;
		}

		void CGameOption::start_speak_token_count(common::TCount count)
		{
			assert(count <= this->max_speak_token_count());
			this->m_start_speak_token_count = count;
		}

		common::TCount CGameOption::start_speak_token_count() const noexcept
		{
			assert(this->m_start_speak_token_count <= this->max_speak_token_count());
			return this->m_start_speak_token_count;
		}

		void CGameOption::min_card_value(common::TValue value)
		{
			assert(value > 0);
			this->m_min_card_value = value;
		}

		common::TValue CGameOption::min_card_value() const noexcept
		{
			assert(this->m_min_card_value > 0);
			return this->m_min_card_value;
		}

		void CGameOption::max_card_value(common::TValue value)
		{
			assert(value >= this->min_card_value());
			this->m_max_card_value = value;
		}

		common::TValue CGameOption::max_card_value() const noexcept
		{
			assert(this->m_max_card_value >= this->min_card_value());
			return this->m_max_card_value;
		}

		common::TScore CGameOption::game_max_score() const
		{
			return common::TScore(this->color_option().size() * (this->max_card_value() - this->min_card_value() + 1));
		}

		void CGameOption::color_option(const common::TColorOptionMap& color_option)
		{
			assert(!color_option.empty());
			this->m_color_option = color_option;
		}

		const common::TColorOptionMap& CGameOption::color_option() const noexcept
		{
			assert(!this->m_color_option.empty());
			return this->m_color_option;
		}

		void CGameOption::callable_colors(const common::TColorSet& color_set)
		{
			assert(!color_set.empty());
			this->m_callable_colors = color_set;
		}

		const common::TColorSet& CGameOption::callable_colors() const noexcept
		{
			assert(!this->m_callable_colors.empty());
			return this->m_callable_colors;
		}

		void CGameOption::stack(const common::TCardStack& stack)
		{
			assert(!stack.empty());
			this->m_stack = stack;
		}

		const common::TCardStack& CGameOption::stack() const noexcept
		{
			assert(!this->m_stack.empty());
			return this->m_stack;
		}

		void CGameOption::tags(const common::TTagList& tags)
		{
			this->m_tags = tags;
		}

		const common::TTagList& CGameOption::tags() const noexcept
		{
			return this->m_tags;
		}


		/// <summary>
		/// Save all the values in this object to a json object
		/// </summary>
		/// <returns>A json object</returns>
		boost::json::value CGameOption::serialize_to_json() const
		{
			boost::json::object root = this->prepack_data().as_object();

			std::vector<boost::json::value> stack_vector;
			for (const auto& card : this->stack())
			{
				boost::json::object card_object;
				card_object["value"] = boost::json::value_from(card.value());
				card_object["color"] = boost::json::value_from(common::card_color_to_string(card.color()));
				stack_vector.push_back(card_object);
			}
			root["stack"] = boost::json::value_from(stack_vector);
			root["tags"] = boost::json::value_from(this->m_tags);

			return root;
		}

		/// <summary>
		/// Update the values from a given json object
		/// </summary>
		/// <param name="root">json object that stores the values</param>
		/// <remarks>game_uuid and timestamp values are not updated since the constructor gives a random / actual value for them</remarks>
		void CGameOption::parse_from_json(const boost::json::value& root)
		{
			const boost::json::object& root_object = root.as_object();
			
			this->game_option_id(root_object.at("gameOptionId").as_string().c_str());
			std::string timestamp = root_object.at("timestamp").as_string().c_str();
			if (timestamp.back() == 'Z')
				timestamp.pop_back();
			this->timestamp(boost::posix_time::from_iso_extended_string(timestamp));
			this->player_count(root_object.at("playerCount").as_int64());
			this->player_hand_max_size(root_object.at("playerHandMaxSize").as_int64());
			this->start_player_id(root_object.at("startPlayerId").as_int64());
			this->max_error_token_count(root_object.at("maxErrorTokenCount").as_int64());
			this->max_speak_token_count(root_object.at("maxSpeakTokenCount").as_int64());
			this->start_error_token_count(root_object.at("startErrorTokenCount").as_int64());
			this->start_speak_token_count(root_object.at("startSpeakTokenCount").as_int64());
			this->min_card_value(root_object.at("minCardValue").as_int64());
			this->max_card_value(root_object.at("maxCardValue").as_int64());

			common::TColorSet callable_colors;
			const boost::json::array& callable_colors_array = root_object.at("callableColors").as_array();
			for (const auto& color : callable_colors_array)
			{
				const common::CardColor card_color = common::string_to_card_color(color.as_string().c_str());
				callable_colors.insert(card_color);
			}
			this->callable_colors(callable_colors);

			common::TColorOptionMap color_option;
			const boost::json::array& color_option_array = root_object.at("colorOption").as_array();
			for (const auto& item : color_option_array)
			{
				const boost::json::object& color_option_object = item.as_object();

				const common::CardColor color = common::string_to_card_color(color_option_object.at("color").as_string().c_str());
				const bool is_ascending = color_option_object.at("isAscending").as_bool();

				common::TColorSet reacts_to;
				const boost::json::array& reacts_to_array = color_option_object.at("reactsTo").as_array();

				for (const auto& reacts_to_item : reacts_to_array)
				{
					const std::string reaction_string = reacts_to_item.as_string().c_str();
					reacts_to.insert(common::string_to_card_color(reaction_string));
				}

				common::CColorOption::TValueDistribution distribution;
				const boost::json::array& distribution_array = color_option_object.at("distribution").as_array();

				for (const auto& distribution_item : distribution_array)
				{
					const boost::json::object& distribution_object = distribution_item.as_object();

					const common::TValue value = distribution_object.at("value").as_int64();
					const common::TCount count = distribution_object.at("count").as_int64();

					distribution.insert(std::pair(value, count));
				}
				const common::CColorOption option(distribution, is_ascending, reacts_to);
				color_option.insert(std::pair<common::CardColor, common::CColorOption>(color, option));
			}
			this->color_option(color_option);

			common::TCardStack stack;
			const boost::json::array& stack_array = root_object.at("stack").as_array();
			for (const auto& item : stack_array)
			{
				const boost::json::object& card_object = item.as_object();
				const common::TValue value = common::TValue(card_object.at("value").as_int64());
				const common::CardColor color = common::string_to_card_color(card_object.at("color").as_string().c_str());
				const common::CCard card(value, color);
				stack.push_back(card);
			}
			this->stack(stack);

			common::TTagList tags;
			const auto tag_array_it = root_object.find("tags");
			if (tag_array_it != root_object.end())
			{
				const boost::json::array& tag_array = tag_array_it->value().as_array();
				for (const auto& item : tag_array)
				{
					tags.insert(item.as_string().c_str());
				}
			}
			this->tags(tags);
		}

		boost::json::value CGameOption::virtual_pack(common::TIndex /*target_player_id*/) const
		{
			boost::json::object root = this->prepack_data().as_object();

			root["stackSize"] = boost::json::value_from(common::TCount(this->stack().size()));

			return root;
		}

		boost::json::value CGameOption::prepack_data() const
		{
			boost::json::object root;

			root["gameOptionId"] = boost::json::value_from(this->game_option_id());
			root["timestamp"] = boost::json::value_from(boost::posix_time::to_iso_extended_string(this->timestamp()) + "Z");
			root["playerCount"] = boost::json::value_from(this->player_count());
			root["playerHandMaxSize"] = boost::json::value_from(this->player_hand_max_size());
			root["startPlayerId"] = boost::json::value_from(this->start_player_id());
			root["maxErrorTokenCount"] = boost::json::value_from(this->max_error_token_count());
			root["maxSpeakTokenCount"] = boost::json::value_from(this->max_speak_token_count());
			root["startErrorTokenCount"] = boost::json::value_from(this->start_error_token_count());
			root["startSpeakTokenCount"] = boost::json::value_from(this->start_speak_token_count());
			root["minCardValue"] = boost::json::value_from(this->min_card_value());
			root["maxCardValue"] = boost::json::value_from(this->max_card_value());

			std::vector<boost::json::value> callable_color_vector;
			for (const auto& color : this->callable_colors())
			{
				const std::string color_string = common::card_color_to_string(color);
				callable_color_vector.push_back(boost::json::value_from(color_string));
			}
			root["callableColors"] = boost::json::value_from(callable_color_vector);
			
			std::vector<boost::json::value> color_option_vector;
			for (const auto& pair : this->color_option())
			{
				boost::json::object color_option_object;
				color_option_object["color"] = boost::json::value_from(common::card_color_to_string(pair.first));
				color_option_object["isAscending"] = boost::json::value_from(pair.second.is_ascending());

				std::vector<boost::json::value> reacts_to_vector;
				for (const auto& reaction : pair.second.reacts_to())
				{
					const boost::json::value reaction_value = boost::json::value_from(common::card_color_to_string(reaction));
					reacts_to_vector.push_back(reaction_value);
				}
				color_option_object["reactsTo"] = boost::json::value_from(reacts_to_vector);

				std::vector<boost::json::value> distribution_vector;
				for (const auto& distribution_pair : pair.second.distribution())
				{
					boost::json::object distribution_object;
					distribution_object["value"] = boost::json::value_from(distribution_pair.first);
					distribution_object["count"] = boost::json::value_from(distribution_pair.second);
					distribution_vector.push_back(distribution_object);
				}
				color_option_object["distribution"] = boost::json::value_from(distribution_vector);

				color_option_vector.push_back(color_option_object);
			}
			root["colorOption"] = boost::json::value_from(color_option_vector);

			return root;
		}
	}
}