#ifndef ACTION_H_B008B367_B952_4E20_AC16_0F09BD0214C0
#define ACTION_H_B008B367_B952_4E20_AC16_0F09BD0214C0
#pragma once

#include "card.h"
#include "serializable.h"
#include "types.h"


namespace hanabi
{
	namespace common
	{
		class IAction : public common::ISerializable
		{
		public:
			IAction();
			virtual ~IAction() noexcept;


			ActionType type() const;

		private:
			virtual ActionType virtual_type() const = 0;

			virtual void parse_from_json(const boost::json::value& /*root*/) {}
		};

		class CDrop : public IAction
		{
		public:
			CDrop(TIndex hand_index);
			CDrop(const boost::json::value& root);
			virtual ~CDrop() noexcept;


			static ActionType get_unique_id();

			TIndex hand_index() const noexcept;
			void hand_index(TIndex index);

		private:
			virtual ActionType virtual_type() const;
			
			virtual boost::json::value serialize_to_json() const;

			TIndex m_hand_index;
		};

		class CPlace : public IAction
		{
		public:
			CPlace(TIndex hand_index);
			CPlace(const boost::json::value& root);
			virtual ~CPlace() noexcept;


			static ActionType get_unique_id();

			TIndex hand_index() const noexcept;
			void hand_index(TIndex index);

		private:
			virtual ActionType virtual_type() const;

			virtual boost::json::value serialize_to_json() const;

			TIndex m_hand_index;
		};

		class CSpeak : public IAction
		{
		public:
			CSpeak(TIndex player_index);
			CSpeak(const boost::json::value& root);
			virtual ~CSpeak() noexcept;

			static ActionType get_unique_id();


			TIndex player_index() const noexcept;
			void player_index(TIndex index);

			bool is_color_info() const;
			bool is_value_info() const;

			TValue value_info() const;
			common::CardColor color_info() const;

			void value_info(TValue value);
			void color_info(common::CardColor color);

		private:
			virtual ActionType virtual_type() const;

			virtual boost::json::value serialize_to_json() const;

			TIndex m_player_index;
			std::shared_ptr<TValue> m_p_value_info;
			std::shared_ptr<common::CardColor> m_p_color_info;
		};

		std::shared_ptr<IAction> create_action(const boost::json::value& root);
	}
}


#endif