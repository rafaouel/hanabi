#include "pch.h"
#include "cardrawinformation.h"

#include "exception.h"


namespace hanabi
{
	namespace common
	{
		CCardRawInformation::CCardRawInformation() :
			m_positive_value(0),
			m_negative_value(),
			m_positive_color(),
			m_negative_color()
		{

		}

		CCardRawInformation::~CCardRawInformation() noexcept
		{

		}

		void CCardRawInformation::put(TValue value)
		{
			if (this->m_positive_value > 0 && this->m_positive_value != value)
			{
				std::ostringstream oss;
				oss << "The value was already known as '" << this->m_positive_value << "' but new information is '" << value << "'";
				THROW_ERROR_1(EImpossibleError, oss.str());
			}

			this->m_positive_value = value;
		}

		void CCardRawInformation::put_not(TValue value)
		{
			if (!this->m_negative_value.contains(value))
				this->m_negative_value.insert(value);
		}

		void CCardRawInformation::put(CardColor color)
		{
			if (!this->m_positive_color.contains(color))
				this->m_positive_color.insert(color);
		}

		void CCardRawInformation::put_not(CardColor color)
		{
			if (!this->m_negative_color.contains(color))
				this->m_negative_color.insert(color);
		}

		TValue CCardRawInformation::positive_value() const
		{
			return this->m_positive_value;
		}

		const CCardRawInformation::TValueSet& CCardRawInformation::negative_value() const
		{
			return this->m_negative_value;
		}

		const CCardRawInformation::TColorSet& CCardRawInformation::positive_color() const
		{
			return this->m_positive_color;
		}

		const CCardRawInformation::TColorSet& CCardRawInformation::negative_color() const
		{
			return this->m_negative_color;
		}
	}
}
