#include "pch.h"
#include "serverstack.h"

namespace hanabi
{
	namespace server_side
	{
		CStack::CStack(const CGameOption& game_option) :
			m_stack(game_option.stack().cbegin(), game_option.stack().cend())
		{

		}

		CStack::~CStack() noexcept
		{

		}

		bool CStack::is_empty() const noexcept
		{
			return this->stack().empty();
		}

		common::TCount CStack::size() const
		{
			return common::TCount(this->stack().size());
		}

		common::CCard CStack::draw() noexcept
		{
			assert(!this->is_empty());

			const common::CCard card = this->stack().front();

			this->stack().pop_front();

			return card;
		}

		const common::TCardStack& CStack::stack() const noexcept
		{
			return this->m_stack;
		}

		common::TCardStack& CStack::stack() noexcept
		{
			return this->m_stack;
		}

		boost::json::value CStack::serialize_to_json() const
		{
			std::vector<boost::json::value> stack_vector;
			for (const auto& card : this->stack())
			{
				const boost::json::object card_json = card.to_json().as_object();
				stack_vector.push_back(card_json);
			}

			return boost::json::value_from(stack_vector);
		}

		boost::json::value CStack::virtual_pack(common::TIndex /*target_player_id*/) const
		{
			boost::json::object stack_object;
			stack_object["stackCount"] = boost::json::value_from(this->size());
			return stack_object;
		}

	}
}
