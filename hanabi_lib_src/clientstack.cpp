#include "pch.h"
#include "clientstack.h"


namespace hanabi
{
	namespace client_side
	{
		CStack::CStack() :
			m_stack_count(0)
		{

		}

		CStack::~CStack() noexcept
		{

		}

		common::TCount CStack::stack_count() const noexcept
		{
			return this->m_stack_count;
		}

		void CStack::stack_count(common::TCount count)
		{
			this->m_stack_count = count;
		}

		void CStack::virtual_update(const boost::json::value& root_value)
		{
			const boost::json::object stack_object = root_value.as_object();

			this->stack_count(stack_object.at("stackCount").as_int64());
		}
	}
}
