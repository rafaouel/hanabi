#include "pch.h"
#include "clientpeerinfo.h"

namespace hanabi
{
	namespace client_side
	{
		CPeerInfo::CPeerInfo() :
			m_name(""),
			m_debug(true)
		{

		}

		CPeerInfo::~CPeerInfo() noexcept
		{

		}

		void CPeerInfo::name(const std::string& name)
		{
			this->m_name = name;
		}

		const std::string& CPeerInfo::name() const noexcept
		{
			return this->m_name;
		}

		common::PeerType CPeerInfo::type() const noexcept
		{
			assert(this->m_type != common::PeerType::UNDEFINED);
			return this->m_type;
		}

		void CPeerInfo::type(common::PeerType type)
		{
			assert(type != common::PeerType::UNDEFINED);
			this->m_type = type;
		}

		std::string CPeerInfo::version() const noexcept
		{
			return common::to_string(this->m_version);
		}

		const common::TVersionArray& CPeerInfo::version_array() const noexcept
		{
			return this->m_version;
		}

		void CPeerInfo::version_array(const common::TVersionArray& array)
		{
			this->m_version = array;
		}

		bool CPeerInfo::debug() const noexcept
		{
			return this->m_debug;
		}

		void CPeerInfo::debug(bool is_debug)
		{
			this->m_debug = is_debug;
		}

		void CPeerInfo::virtual_update(const boost::json::value& root)
		{
			const boost::json::object root_object = root.as_object();

			this->name(root_object.at("name").as_string().c_str());
			this->type(common::PeerType(root_object.at("type").as_int64()));
			const boost::json::array& version_json_array = root_object.at("versionArray").as_array();
			std::array<int, 3> version_array = {};
			for (std::size_t i = 0; i < version_array.size(); ++i)
			{
				version_array[i] = int(version_json_array.at(i).as_int64());
			}
			this->version_array(version_array);
			this->debug(root_object.at("debug").as_bool());
		}
	}
}