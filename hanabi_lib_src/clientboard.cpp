#include "pch.h"
#include "clientboard.h"

namespace hanabi
{
	namespace client_side
	{
		CBoard::CBoard() :
			m_score(0),
			m_board()
		{

		}

		CBoard::~CBoard() noexcept
		{

		}

		common::TValue CBoard::score() const noexcept
		{
			return this->m_score;
		}

		void CBoard::score(common::TValue score)
		{
			this->m_score = score;
		}

		const common::TCardBoard& CBoard::board() const noexcept
		{
			return this->m_board;
		}

		common::TCardBoard& CBoard::board() noexcept
		{
			return this->m_board;
		}

		void CBoard::virtual_update(const boost::json::value& root)
		{
			const boost::json::object& root_object = root.as_object();

			this->score(root_object.at("score").as_int64());

			this->board().clear();
			for (const auto& item_value : root_object.at("board").as_array())
			{
				const boost::json::object& item_object = item_value.as_object();

				const common::CardColor color = common::string_to_card_color(item_object.at("color").as_string().c_str());
				const common::TValue value = item_object.at("value").as_int64();
				this->board().insert(std::pair(color, value));
			}
		}
	}
}