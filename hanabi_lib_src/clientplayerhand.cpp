#include "pch.h"
#include "clientplayerhand.h"


namespace hanabi
{
	namespace client_side
	{
		CPlayerHand::CPlayerHand()
		{

		}

		CPlayerHand::~CPlayerHand() noexcept
		{

		}

		const CPlayerHand::TPlayerHand& CPlayerHand::other_hands() const noexcept
		{
			return this->m_player_hand;
		}

		CPlayerHand::TPlayerHand& CPlayerHand::other_hands() noexcept
		{
			return this->m_player_hand;
		}

		const CPlayerHand::THand& CPlayerHand::hand(common::TIndex player_id) const
		{
			assert(this->m_player_hand.contains(player_id));

			return this->m_player_hand.at(player_id);
		}

		void CPlayerHand::virtual_update(const boost::json::value& root_value)
		{
			this->other_hands().clear();

			const boost::json::array& root_array = root_value.as_array();

			for (const auto& item : root_array)
			{
				const boost::json::object& item_object = item.as_object();

				const common::TIndex player_id = item_object.at("playerId").as_int64();
				
				CPlayerHand::THand hand;
				for (const auto& card_value : item_object.at("hand").as_array())
				{
					const common::CCard card(card_value);

					hand.push_back(card);
				}

				this->other_hands().insert(std::pair(player_id, hand));
			}

		}
	}
}
