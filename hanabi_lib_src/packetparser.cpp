#include "pch.h"
#include "packetparser.h"

namespace hanabi
{
	namespace network
	{
		CPacketParser::CPacketParser(PacketType packet_type) :
			m_data(),
			m_packet_type(packet_type)
		{

		}

		CPacketParser::~CPacketParser() noexcept
		{

		}

		PacketType CPacketParser::packet_type() const noexcept
		{
			return this->m_packet_type;
		}

		void CPacketParser::data(const boost::json::value& data)
		{
			this->m_data = data;
		}

		const boost::json::value& CPacketParser::data() const noexcept
		{
			return this->m_data;
		}

		void CPacketParser::decode(const CPacket& packet)
		{
			std::string buffer = packet.body();
			std::replace(buffer.begin(), buffer.end(), delimiter, '\n');

			// Get the body into a stream
			std::istringstream is(buffer);
			int packet_type;
			is >> packet_type;
			assert(packet_type == int(this->packet_type()));

			// Extract data
			boost::json::stream_parser parser;
			std::string line;
			while (std::getline(is, line))
			{
				parser.write(line);
			}
			parser.finish();

			this->data(parser.release());
		}
	}
}
