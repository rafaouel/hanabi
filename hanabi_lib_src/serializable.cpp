#include "pch.h"
#include "serializable.h"

namespace hanabi
{
	namespace common
	{
		ISerializable::ISerializable()
		{

		}

		ISerializable::~ISerializable() noexcept
		{

		}

		boost::json::value ISerializable::to_json() const
		{
			return this->serialize_to_json();
		}

		void ISerializable::from_json(const boost::json::value& value)
		{
			this->parse_from_json(value);
		}

		boost::json::value read_json(std::istream& is, boost::system::error_code& ec)
		{
			boost::json::stream_parser parser;
			std::string line;
			while (std::getline(is, line))
			{
				parser.write(line, ec);
				if (ec)
					return nullptr;
			}
			parser.finish(ec);
			if (ec)
				return nullptr;

			return parser.release();
		}

		void write_json(std::ostream& os, const boost::json::value& root)
		{
			os << root;
		}
	}
}
