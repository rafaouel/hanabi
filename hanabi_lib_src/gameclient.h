#ifndef GAMECLIENT_H_CFA3A2F8_77C4_4C9B_9223_CD8C79480324
#define GAMECLIENT_H_CFA3A2F8_77C4_4C9B_9223_CD8C79480324
#pragma once

#include "action.h"
#include "clientgameend.h"
#include "clientgamestate.h"
#include "clientpeerinfo.h"
#include "clientsocket.h"
#include "packetdispatcher.h"

#ifdef _DEBUG
constexpr bool compiled_with_debug_option = true;
#else
constexpr bool compiled_with_debug_option = false;
#endif


namespace hanabi
{
	namespace client_side
	{
		class CPeerInfoResponse
		{
		public:
			CPeerInfoResponse(const std::string& name, const common::TVersionArray& version_array, const common::PeerType& peer_type, bool debug, const common::TTagList& tags);
			virtual ~CPeerInfoResponse() noexcept;


			const std::string& name() const noexcept;
			const std::string version() const;
			const common::TVersionArray& version_array() const noexcept;
			const common::PeerType& peer_type() const noexcept;
			bool debug() const noexcept;
			const common::TTagList tags() const noexcept;

		private:
			const std::string m_name;
			const common::TVersionArray m_version_array;
			const common::PeerType m_peer_type;
			const bool m_debug;
			const common::TTagList m_tags;
		};


		class IGameClient
		{
		public:
			typedef std::function<void(const std::string& server_hello_message)> TCallbackOnReceivingHello;
			typedef std::function<void(const CGameEnd& game_end, const CGameState& last_game_state, const TPeerInfoMap& peer_info)> TCallbackOnReceivingGameEndUpdate;
			typedef std::function<void(const CGameState& game_state, common::TIndex my_id, const TPeerInfoMap& peer_info)> TCallbackOnReceivingGameStateUpdate;
			typedef std::function<void(const CGameOption& game_option)> TCallbackOnReceivingGameOptionUpdate;
			typedef std::function<void(common::TIndex source_player_id, common::TIndex target_player_id, const TPeerInfoMap& peer_info, common::TValue value_info)> TCallbackOnReceivingActionSpeakValueUpdate;
			typedef std::function<void(common::TIndex source_player_id, common::TIndex target_player_id, const TPeerInfoMap& peer_info, common::CardColor color_info)> TCallbackOnReceivingActionSpeakColorUpdate;
			typedef std::function<void(common::TIndex source_player_id, const TPeerInfoMap& peer_info, const common::CCard& card)> TCallbackOnReceivingActionDropUpdate;
			typedef std::function<void(common::TIndex source_player_id, const TPeerInfoMap& peer_info, const common::CCard& card)> TCallbackOnReceivingActionPlaceUpdate;
			typedef std::function<void(const std::string& server_version)> TCallbackOnReceivingPeerInfoRequest;
			typedef std::function<void(common::TIndex new_peer_id, const CPeerInfo& new_peer, const TPeerInfoMap& peer_info)> TCallbackOnReceivingPeerInfoUpdate;
			typedef std::function<void()> TCallbackOnReceivingActionAccepted;
			typedef std::function<void(const std::string& reason)> TCallbackOnReceivingActionRejected;
			typedef std::function<void(common::TIndex current_player_id, const common::CardColor& color)> TCallbackOnReceivingBonusEvent;
			typedef std::function<void(common::TIndex current_player_id, common::TCount error_token_count)> TCallbackOnReceivingErrorEvent;


			IGameClient(boost::asio::io_service& io_service, const boost::asio::ip::address& server_address, const boost::asio::ip::port_type& server_port);
			virtual ~IGameClient() noexcept;


			void run();

			common::TIndex my_id() const noexcept;
			void my_id(common::TIndex my_id);

			const CGameState& game_state() const noexcept;
			CGameState& game_state() noexcept;

			const CGameEnd& game_end() const noexcept;
			CGameEnd& game_end() noexcept;

			const TPeerInfoMap& peer_info() const noexcept;
			TPeerInfoMap& peer_info() noexcept;
			const CPeerInfo& peer_info(common::TIndex player_id) const;
			CPeerInfo& peer_info(common::TIndex player_id);

			void set_callback_on_receiving_hello(const TCallbackOnReceivingHello& callback);
			void set_callback_on_receiving_game_end_update(const TCallbackOnReceivingGameEndUpdate& callback);
			void set_callback_on_receiving_game_state_update(const TCallbackOnReceivingGameStateUpdate& callback);
			void set_callback_on_receiving_game_option_update(const TCallbackOnReceivingGameOptionUpdate& callback);
			void set_callback_on_receiving_action_speak_value_update(const TCallbackOnReceivingActionSpeakValueUpdate& callback);
			void set_callback_on_receiving_action_speak_color_update(const TCallbackOnReceivingActionSpeakColorUpdate& callback);
			void set_callback_on_receiving_action_drop_update(const TCallbackOnReceivingActionDropUpdate& callback);
			void set_callback_on_receiving_action_place_update(const TCallbackOnReceivingActionPlaceUpdate& callback);
			void set_callback_on_receiving_peer_info_request(const TCallbackOnReceivingPeerInfoRequest& callback);
			void set_callback_on_receiving_peer_info_update(const TCallbackOnReceivingPeerInfoUpdate& callback);
			void set_callback_on_receiving_action_accepted(const TCallbackOnReceivingActionAccepted& callback);
			void set_callback_on_receiving_action_rejected(const TCallbackOnReceivingActionRejected& callback);
			void set_callback_on_receiving_bonus_event(const TCallbackOnReceivingBonusEvent& callback);
			void set_callback_on_receiving_error_event(const TCallbackOnReceivingErrorEvent& callback);

		private:
			void call_on_receiving_hello(const std::string& server_hello_message) const;
			void call_on_receiving_game_end_update(const CGameEnd& game_end, const CGameState& last_game_state, const TPeerInfoMap& peer_info) const;
			void call_on_receiving_game_state_update(const CGameState& game_state, common::TIndex my_id, const TPeerInfoMap& peer_info) const;
			void call_on_receiving_game_option_update(const CGameOption& game_option) const;
			void call_on_receiving_action_speak_value_update(common::TIndex source_player_id, common::TIndex target_player_id, const TPeerInfoMap& peer_info, common::TValue value_info) const;
			void call_on_receiving_action_speak_color_update(common::TIndex source_player_id, common::TIndex target_player_id, const TPeerInfoMap& peer_info, common::CardColor color_info) const;
			void call_on_receiving_action_drop_update(common::TIndex source_player_id, const TPeerInfoMap& peer_info, const common::CCard& card) const;
			void call_on_receiving_action_place_update(common::TIndex source_player_id, const TPeerInfoMap& peer_info, const common::CCard& card) const;
			void call_on_receiving_peer_info_request(const std::string& server_version) const;
			void call_on_receiving_peer_info_update(common::TIndex new_peer_id, const CPeerInfo& new_peer, const TPeerInfoMap& peer_info) const;
			void call_on_receiving_action_accepted() const;
			void call_on_receiving_action_rejected(const std::string& reason) const;
			void call_on_receiving_bonus_event(common::TIndex current_player_id, const common::CardColor& color) const;
			void call_on_receiving_error_event(common::TIndex current_player_id, common::TCount error_token_count) const;

			void on_receiving_hello_packet(const network::CPacket& packet);
			void on_receiving_game_end_update_packet(const network::CPacket& packet);
			void on_receiving_game_state_update_packet(const network::CPacket& packet);
			void on_receiving_game_option_update_packet(const network::CPacket& packet);
			void on_receiving_action_request_packet(const network::CPacket& packet);
			void on_receiving_action_speak_update_packet(const network::CPacket& packet);
			void on_receiving_action_drop_update_packet(const network::CPacket& packet);
			void on_receiving_action_place_update_packet(const network::CPacket& packet);
			void on_receiving_peer_info_request_packet(const network::CPacket& packet);
			void on_receiving_peer_info_update_packet(const network::CPacket& packet);
			void on_receiving_bonus_event_packet(const network::CPacket& packet);
			void on_receiving_error_event_packet(const network::CPacket& packet);

			virtual std::shared_ptr<common::IAction> process_action_request(const CGameState& game_state, const TPeerInfoMap& peer_info, common::TIndex my_id) = 0;
			virtual CPeerInfoResponse process_peer_info_request(common::TIndex my_id, const std::string server_verison) = 0;

			bool m_run;

			CGameState m_game_state;
			CGameEnd m_game_end;
			TPeerInfoMap m_peer_info;

			common::TIndex m_my_id;

			CSocket m_socket;

			network::CPacketDispatcher m_packet_dispatcher;	

			TCallbackOnReceivingHello m_on_receiving_hello;
			TCallbackOnReceivingGameEndUpdate m_on_receiving_game_end_update;
			TCallbackOnReceivingGameStateUpdate m_on_receiving_game_state_update;
			TCallbackOnReceivingGameOptionUpdate m_on_receiving_game_option_update;
			TCallbackOnReceivingActionSpeakValueUpdate m_on_receiving_action_speak_value_update;
			TCallbackOnReceivingActionSpeakColorUpdate m_on_receiving_action_speak_color_update;
			TCallbackOnReceivingActionDropUpdate m_on_receiving_action_drop_update;
			TCallbackOnReceivingActionPlaceUpdate m_on_receiving_action_place_update;
			TCallbackOnReceivingPeerInfoRequest m_on_receiving_peer_info_request;
			TCallbackOnReceivingPeerInfoUpdate m_on_receiving_peer_info_update;
			TCallbackOnReceivingActionAccepted m_on_receiving_action_accepted;
			TCallbackOnReceivingActionRejected m_on_receiving_action_rejected;
			TCallbackOnReceivingBonusEvent m_on_receiving_bonus_event;
			TCallbackOnReceivingErrorEvent m_on_receiving_error_event;
		};
	}
}


#endif