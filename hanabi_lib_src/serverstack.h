#ifndef SERVERSTACK_H_7FA408D1_CC2E_4314_A547_87FD59BF621F
#define SERVERSTACK_H_7FA408D1_CC2E_4314_A547_87FD59BF621F
#pragma once

#include "card.h"
#include "sendable.h"
#include "serializable.h"
#include "servergameoption.h"

namespace hanabi
{
	namespace server_side
	{
		class CStack : public common::ISerializable, public common::ISendable
		{
		public:
			CStack(const CGameOption& game_option);
			virtual ~CStack() noexcept;


			bool is_empty() const noexcept;

			common::TCount size() const;

			common::CCard draw() noexcept;

			const common::TCardStack& stack() const noexcept;
			common::TCardStack& stack() noexcept;

		private:
			virtual boost::json::value serialize_to_json() const;
			virtual void parse_from_json(const boost::json::value& /*root*/) {}
			virtual boost::json::value virtual_pack(common::TIndex target_player_id) const;

			common::TCardStack m_stack;
		};
	}
}

#endif