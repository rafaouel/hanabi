#include "pch.h"
#include "playergateway.h"

#include "packet.h"


namespace hanabi
{
    namespace server_side
    {
        IPlayerGateway::IPlayerGateway()
        {

        }

        IPlayerGateway::~IPlayerGateway() noexcept
        {

        }

        std::shared_ptr<common::IAction> IPlayerGateway::retrieve_action()
        {
            return this->virtual_retrieve_action();
        }

        void IPlayerGateway::send_action_place_update(common::TIndex source_player_id, const common::CCard& placed_card)
        {
            this->virtual_send_action_place_update(source_player_id, placed_card);
        }

        void IPlayerGateway::send_action_drop_update(common::TIndex source_player_id, const common::CCard& droped_card)
        {
            this->virtual_send_action_drop_update(source_player_id, droped_card);
        }

        void IPlayerGateway::send_action_speak_update(common::TIndex source_player_id, common::TIndex target_player_id, common::TValue value)
        {
            this->virtual_send_action_speak_update(source_player_id, target_player_id, value);
        }

        void IPlayerGateway::send_action_speak_update(common::TIndex source_player_id, common::TIndex target_player_id, common::CardColor color)
        {
            this->virtual_send_action_speak_update(source_player_id, target_player_id, color);
        }

        void IPlayerGateway::send_bonus_given_update(common::TIndex current_player_id, const common::CardColor& color)
        {
            this->virtual_send_bonus_given_update(current_player_id, color);
        }

        void IPlayerGateway::send_error_token_given_update(common::TIndex current_player_id, common::TCount error_token_count)
        {
            this->virtual_send_error_token_given_update(current_player_id, error_token_count);
        }

        std::shared_ptr<CPeerInfo> IPlayerGateway::retrieve_peer_info(common::TIndex player_id, const common::TVersionArray& server_version)
        {
            return this->virtual_retrieve_peer_info(player_id, server_version);
        }

        void IPlayerGateway::send_peer_info_update(const CPeerInfo& peer_info, common::TIndex player_id)
        {
            this->virtual_send_peer_info_update(peer_info, player_id);
        }

        void IPlayerGateway::send_ok()
        {
            this->virtual_send_ok();
        }
        
        void IPlayerGateway::send_not_ok(const std::string& reason)
        {
            this->virtual_send_not_ok(reason);
        }

        void IPlayerGateway::init()
        {
            this->virtual_init();
        }

        void IPlayerGateway::close()
        {
            this->virtual_close();
        }

        void IPlayerGateway::say_hello(const std::string& message)
        {
            this->virtual_say_hello(message);
        }

        void IPlayerGateway::send_game_option(const CGameOption& game_option, common::TIndex player_id)
        {
            this->virtual_send_game_option(game_option, player_id);
        }

        void IPlayerGateway::send_game_update(const CGameState& game_state, common::TIndex player_id)
        {
            this->virtual_send_game_update(game_state, player_id);
        }

        void IPlayerGateway::send_game_end(const CGameEnd& game_end, common::TIndex player_id)
        {
            this->virtual_send_game_end(game_end, player_id);
        }
    }
}

