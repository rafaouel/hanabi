#ifndef UPDATABLE_H_F3D1A11E_D036_424F_9263_5EC616636AD6
#define UPDATABLE_H_F3D1A11E_D036_424F_9263_5EC616636AD6
#pragma once

namespace hanabi
{
	namespace common
	{
		class IUpdatable
		{
		public:
			IUpdatable();
			virtual ~IUpdatable() noexcept;


			void update(const boost::json::value& value);

		private:
			virtual void virtual_update(const boost::json::value& value) = 0;
		};
	}
}

#endif