#include "pch.h"
#include "gameserver.h"

#include "exception.h"
#include "serverplayer.h"
#include "tcpplayer.h"
#include "version.h"


namespace hanabi
{
	namespace server_side
	{
		hanabi::server_side::CGameServer::CGameServer(const CGameOption& game_option, std::shared_ptr<common::CAPIGateway> p_api_gateway, boost::asio::io_context& io_context, const boost::asio::ip::port_type& server_port, bool shuffle_peers, const common::TTagList& tags) :
			m_io_context(io_context),
			m_endpoint(boost::asio::ip::address(), server_port),
			m_player_count(game_option.player_count()),
			m_shuffle_peers(shuffle_peers),
			m_game_state(game_option, CGameStart(game_option.game_option_id(), tags)),
			m_game_end(this->game_start().game_uuid(), game_option.player_count()),
			m_player_container(),
			m_welcome_message("Welcome to the game !"),
			m_start_timestamp(boost::posix_time::not_a_date_time),
			m_end_timestamp(boost::posix_time::not_a_date_time),
			m_error_committed(false),
			m_p_api_gateway(p_api_gateway),
			m_compatibility_checker(server_side::load_compatibility_data(std::filesystem::path("compatibility.json")), common::to_version(HANABI_LIB_VERSION))
		{
			
		}

		CGameServer::~CGameServer() noexcept
		{

		}

		void CGameServer::run()
		{
			try
			{
				this->capture_start_timestamp();

				// Setup the party
				this->setup_game();

				// Wait for players
				this->lobby();

				// From this point the party is started
				this->call_on_game_starts();

				// Starting the party
				this->game_end().capture_game_start_timestamp();
				this->game_state().reset_player_turn_count();
				this->game_state().reset_game_turn_count();
				this->call_on_start_of_game_loop();

				// Run game loop
				while (true)
				{
					// Callback on the begining of the turn
					this->call_on_turn_starts();

					// Send a snapshot to every player
					this->broadcast_game_update();

					// Save the game state
					this->save_current_game_state();

					// Select the current player
					const common::TIndex current_player_id = this->game_state().current_player_id();

					// Detect Game end condition :
					bool exit_game_loop = false;
					bool victory = false;
					// The game is over if the current player is not able to do any action (cannot speak with an empty hand)
					const bool empty_hand = this->game_state().player_hand().hand(current_player_id).empty();
					const bool can_speak = this->allowed_to_speak();
					if (empty_hand && !can_speak)
					{
						// The player is not able to do any action
						victory = false;
						exit_game_loop = true;
						this->call_on_game_end_condition_player_stuck();
					}
					else if (this->is_game_over())
					{
						// # error == max error ?
						victory = false;
						exit_game_loop = true;
						this->call_on_game_end_condition_max_error();

					}
					else if (this->is_game_ended())
					{
						// No card to place anymore
						victory = !this->is_game_over();
						exit_game_loop = true;
						this->call_on_game_end_condition_max_score();
					}

					// If the end has been triggered...
					if (exit_game_loop)
					{
						// End of the loop condition
						this->game_end().capture_game_end_timestamp();
						this->game_end().victory(victory);
						this->game_end().elapsed_player_turn_count(this->game_state().player_turn_count());
						this->game_end().elapsed_game_turn_count(this->game_state().game_turn_count());
						this->game_end().score(this->game_state().board().score());
						this->game_end().max_score(this->game_state().trash().current_max_score());
						this->game_end().error_token_count(this->game_state().error_token_count());
						this->game_end().max_error_token_count(this->game_option().max_error_token_count());

						// Callback
						this->call_on_end_of_game_loop();

						// Leave the game loop
						break;
					}
					
					// Select current player gateway
					const std::shared_ptr<IPlayerGateway> p_player = this->gateway(this->game_state().current_player_id());
					assert(p_player);

					// Start of the player action roundtrip
					const boost::posix_time::ptime request_timestamp = boost::posix_time::microsec_clock::universal_time();
					this->call_on_player_action_roundtrip_starts(request_timestamp);
					bool valid_action = false;
					std::shared_ptr<common::IAction> p_action;
					while (!valid_action)
					{
						try
						{
							// Ask current player for an action
							this->call_on_retrieve_action_starts();
							p_action = p_player->retrieve_action();
							assert(p_action);
							this->call_on_retrieve_action_ends(p_action);

							// Process the current player action
							this->call_on_process_player_action_starts(p_action);
							this->process_player_action(p_action);
							valid_action = true;
							this->call_on_process_player_action_ends(p_action, valid_action);
						}
						catch (hanabi::EHanabiException& error)
						{
							this->call_on_invalid_action(p_action, error);
							const std::string reason = error.what();
							p_player->send_not_ok(reason);
							continue;
						}
					}
					const boost::posix_time::ptime response_timestamp = boost::posix_time::microsec_clock::universal_time();
					this->call_on_player_action_roundtrip_ends(p_action, request_timestamp, response_timestamp);
					this->stack_player_action(p_action, request_timestamp, response_timestamp);
					this->save_player_action(p_action, request_timestamp, response_timestamp);

					// Do we need to set an error ?
					if (this->error_committed())
					{
						// Add a token
						this->game_state().add_error_token();

						// Send a packet to every player to tell that an error token is set
						this->broadcast_error_token_given(this->game_state().current_player_id(), this->game_state().error_token_count());

						// Put down the flag
						this->error_committed(false);
					}

					// Do we need to set a bonus ?
					if (this->completed_color())
					{
						// Give the bonus
						this->give_bonus(*this->completed_color());

						// Send a packet to every player to tell that a color is completed
						this->broadcast_bonus_given(this->game_state().current_player_id(), *this->completed_color());
						
						// Put down the flag
						this->completed_color(std::shared_ptr<common::CardColor>());
					}

					// Callback
					this->call_on_turn_ends();

					// Prepare for next turn...
					// -> Switch to next player
					this->game_state().move_current_player();

					// -> Increase turn count
					this->game_state().increase_player_turn_count();
					if ((this->game_state().player_turn_count() % this->players().size()) == 0)
						this->game_state().increase_game_turn_count();
				}

				// Send update about the end of the game
				this->broadcast_game_end();
				// Save the game end
				this->save_game_end();

				// End of the server
				this->capture_end_timestamp();
				// Close all connections
				this->close_connections();
				// Callback
				assert(this->start_timestamp() < this->end_timestamp());
				this->call_on_game_ends();
			}
			catch (EServerError& e)
			{
				this->call_on_server_error(e);

				// Close all connections
				this->close_connections();

				throw;
			}
			catch (EHanabiException& e)
			{
				this->call_on_hanabi_error(e);

				// Close all connections
				this->close_connections();

				throw;
			}
			catch (std::exception& e)
			{
				this->call_on_std_error(e);

				// Close all connections
				this->close_connections();

				throw;
			}
			catch (...)
			{
				this->call_on_error();

				// Close all connections
				this->close_connections();

				throw;
			}
		}

		const CGameState& CGameServer::game_state() const noexcept
		{
			return this->m_game_state;
		}

		CGameState& CGameServer::game_state() noexcept
		{
			return this->m_game_state;
		}

		const CGameOption& CGameServer::game_option() const noexcept
		{
			return this->game_state().game_option();
		}

		const CGameStart& CGameServer::game_start() const noexcept
		{
			return this->game_state().game_start();
		}

		const CGameEnd& CGameServer::game_end() const noexcept
		{
			return this->m_game_end;
		}

		CGameEnd& CGameServer::game_end() noexcept
		{
			return this->m_game_end;
		}

		const CGameServer::TPlayerMap& CGameServer::players() const noexcept
		{
			return this->m_player_container;
		}

		CGameServer::TPlayerMap& CGameServer::players() noexcept
		{
			return this->m_player_container;
		}

		const CServerPlayer& CGameServer::player(common::TIndex player_id) const
		{
			assert(common::is_valid_index(player_id));
			assert(this->players().contains(player_id));
			return this->players().at(player_id);
		}

		const std::shared_ptr<IPlayerGateway>& CGameServer::gateway(common::TIndex player_id) const
		{
			assert(common::is_valid_index(player_id));
			return this->player(player_id).gateway();
		}

		const CPeerInfo& CGameServer::peer_info(common::TIndex player_id) const
		{
			assert(common::is_valid_index(player_id));
			return this->player(player_id).peer_info();
		}

		const std::string& CGameServer::welcome_message() const noexcept
		{
			return this->m_welcome_message;
		}

		void CGameServer::welcome_message(const std::string& message)
		{
			this->m_welcome_message = message;
		}

		const common::TTagList& CGameServer::tags() const noexcept
		{
			return this->game_state().game_start().tags();
		}

		const boost::posix_time::ptime& CGameServer::start_timestamp() const noexcept
		{
			assert(this->m_start_timestamp != boost::posix_time::not_a_date_time);
			return this->m_start_timestamp;
		}

		void CGameServer::capture_start_timestamp()
		{
			this->m_start_timestamp = boost::posix_time::microsec_clock::universal_time();
		}

		const boost::posix_time::ptime& CGameServer::end_timestamp() const noexcept
		{
			assert(this->m_end_timestamp != boost::posix_time::not_a_date_time);
			return this->m_end_timestamp;
		}

		void CGameServer::capture_end_timestamp()
		{
			this->m_end_timestamp = boost::posix_time::microsec_clock::universal_time();
		}

		common::TCount CGameServer::turn_count() const
		{
			return this->game_state().player_turn_count();
		}

		common::TScore CGameServer::score() const
		{
			return this->game_state().board().score();
		}

		common::TScore CGameServer::current_max_score() const
		{
			return this->game_state().trash().current_max_score();
		}

		// The players accumulated too mush error
		bool CGameServer::is_game_over() const
		{
			return this->game_state().is_game_over();
		}

		// The players cannot increase their score anymore
		bool CGameServer::is_game_ended() const
		{
			const common::TScore current_max_score = this->current_max_score();
			const common::TScore score = this->score();
			assert(current_max_score >= score);
			return score == current_max_score;
		}

		bool CGameServer::allowed_to_speak() const
		{
			return this->game_state().can_speak();
		}

		bool CGameServer::allowed_to_drop() const
		{
			return this->game_state().can_drop();
		}

		bool CGameServer::is_a_valid_place(const common::CCard& card) const
		{
			return this->game_state().board().can_place(card);
		}

		const std::shared_ptr<common::CardColor>& CGameServer::completed_color() const noexcept
		{
			return this->m_p_completed_color;
		}

		void CGameServer::completed_color(const std::shared_ptr<common::CardColor>& p_color)
		{
			this->m_p_completed_color = p_color;
		}

		bool CGameServer::error_committed() const noexcept
		{
			return this->m_error_committed;
		}

		void CGameServer::error_committed(bool committed)
		{
			this->m_error_committed = committed;
		}
		
		void CGameServer::set_callback_on_game_starts(const TCallbackOnGameStarts& callback)
		{
			this->m_on_game_starts = callback;
		}

		void CGameServer::set_callback_on_game_ends(const TCallbackOnGameEnds& callback)
		{
			this->m_on_game_ends = callback;
		}

		void CGameServer::set_callback_on_lobby_starts(const TCallbackOnLobbyStarts& callback)
		{
			this->m_on_lobby_starts = callback;
		}

		void CGameServer::set_callback_on_lobby_ends(const TCallbackOnLobbyEnds& callback)
		{
			this->m_on_lobby_ends = callback;
		}

		void CGameServer::set_callback_on_setup_game_starts(const TCallbackOnSetupGameStarts& callback)
		{
			this->m_on_setup_game_starts = callback;
		}

		void CGameServer::set_callback_on_setup_game_ends(const TCallbackOnSetupGameEnds& callback)
		{
			this->m_on_setup_game_ends = callback;
		}

		void CGameServer::set_callback_on_closing_connection_starts(const TCallbackOnClosingConnectionStarts& callback)
		{
			this->m_on_closing_connection_starts = callback;
		}

		void CGameServer::set_callback_on_closing_connection_ends(const TCallbackOnClosingConnectionEnds& callback)
		{
			this->m_on_closing_connection_ends = callback;
		}

		void CGameServer::set_callback_on_invalid_action(const TCallbackOnInvalidAction& callback)
		{
			this->m_on_invalid_action = callback;
		}

		void CGameServer::set_callback_on_turn_starts(const TCallbackOnTurnStarts& callback)
		{
			this->m_on_turn_starts = callback;
		}

		void CGameServer::set_callback_on_turn_ends(const TCallbackOnTurnEnds& callback)
		{
			this->m_on_turn_ends = callback;
		}

		void CGameServer::set_callback_on_start_of_game_loop(const TCallbackOnStartOfGameLoop& callback)
		{
			this->m_on_start_of_game_loop = callback;
		}

		void CGameServer::set_callback_on_end_of_game_loop(const TCallbackOnEndOfGameLoop& callback)
		{
			this->m_on_end_of_game_loop = callback;
		}

		void CGameServer::set_callback_on_game_end_condition_player_stuck(const TCallbackOnGameEndConditionPlayerStuck& callback)
		{
			this->m_on_game_end_condition_player_stuck = callback;
		}

		void CGameServer::set_callback_on_game_end_condition_max_error(const TCallbackOnGameEndConditionMaxError& callback)
		{
			this->m_on_game_end_condition_max_error = callback;
		}

		void CGameServer::set_callback_on_game_end_condition_max_score(const TCallbackOnGameEndConditionMaxScore& callback)
		{
			this->m_on_game_end_condition_max_score = callback;
		}

		void CGameServer::set_callback_on_retrieve_action_starts(const TCallbackOnRetrieveActionStarts& callback)
		{
			this->m_on_retrieve_action_starts = callback;
		}

		void CGameServer::set_callback_on_retrieve_action_ends(const TCallbackOnRetrieveActionEnds& callback)
		{
			this->m_on_retrieve_action_ends = callback;
		}

		void CGameServer::set_callback_on_giving_bonus_starts(const TCallbackOnGivingBonusStarts& callback)
		{
			this->m_on_giving_bonus_starts = callback;
		}

		void CGameServer::set_callback_on_giving_bonus_ends(const TCallbackOnGivingBonusEnds& callback)
		{
			this->m_on_giving_bonus_ends = callback;
		}

		void CGameServer::set_callback_on_setup_peer_starts(const TCallbackOnSetupPeerStarts& callback)
		{
			this->m_on_setup_peer_starts = callback;
		}

		void CGameServer::set_callback_on_setup_peer_ends(const TCallbackOnSetupPeerEnds& callback)
		{
			this->m_on_setup_peer_ends = callback;
		}

		void CGameServer::set_callback_on_process_player_action_starts(const TCallbackOnProcessPlayerActionStarts& callback)
		{
			this->m_on_process_player_action_starts = callback;
		}

		void CGameServer::set_callback_on_process_player_action_ends(const TCallbackOnProcessPlayerActionEnds& callback)
		{
			this->m_on_process_player_action_ends = callback;
		}

		void CGameServer::set_callback_on_process_drop_starts(const TCallbackOnProcessDropStarts& callback)
		{
			this->m_on_process_drop_starts = callback;
		}

		void CGameServer::set_callback_on_process_drop_ends(const TCallbackOnProcessDropEnds& callback)
		{
			this->m_on_process_drop_ends = callback;
		}

		void CGameServer::set_callback_on_process_place_starts(const TCallbackOnProcessPlaceStarts& callback)
		{
			this->m_on_process_place_starts = callback;
		}

		void CGameServer::set_callback_on_process_place_ends(const TCallbackOnProcessPlaceEnds& callback)
		{
			this->m_on_process_place_ends = callback;
		}

		void CGameServer::set_callback_on_player_place(const TCallbackOnPlayerPlace& callback)
		{
			this->m_on_player_place = callback;
		}

		void CGameServer::set_callback_on_player_drop(const TCallbackOnPlayerDrop& callback)
		{
			this->m_on_player_drop = callback;
		}

		void CGameServer::set_callback_on_player_draw(const TCallbackOnPlayerDraw& callback)
		{
			this->m_on_player_draw = callback;
		}

		void CGameServer::set_callback_on_player_action_roundtrip_starts(const TCallbackOnPlayerActionRoundtripStarts& callback)
		{
			this->m_on_player_action_roundtrip_starts = callback;
		}

		void CGameServer::set_callback_on_player_action_roundtrip_ends(const TCallbackOnPlayerActionRoundtripEnds& callback)
		{
			this->m_on_player_action_roundtrip_ends = callback;
		}

		void CGameServer::set_callback_on_player_hello(const TCallbackOnPlayerHello& callback)
		{
			this->m_on_player_hello = callback;
		}

		void CGameServer::set_callback_on_peer_connecting(const TCallbackOnPeerConnecting& callback)
		{
			this->m_on_peer_connecting = callback;
		}

		void CGameServer::set_callback_on_peer_rejected(const TCallbackOnPeerRejected& callback)
		{
			this->m_on_peer_rejected = callback;
		}

		void CGameServer::set_callback_on_peer_connected(const TCallbackOnPeerConnected& callback)
		{
			this->m_on_peer_connected = callback;
		}

		void CGameServer::set_callback_on_save_game_start_starts(const TCallbackOnSaveGameStart& callback)
		{
			this->m_on_save_game_start_starts = callback;
		}

		void CGameServer::set_callback_on_save_game_start_ends(const TCallbackOnSaveGameStart& callback)
		{
			this->m_on_save_game_start_ends = callback;
		}

		void CGameServer::set_callback_on_save_peer_info_starts(const TCallbackOnSavePeerInfo& callback)
		{
			this->m_on_save_peer_info_starts = callback;
		}

		void CGameServer::set_callback_on_save_peer_info_ends(const TCallbackOnSavePeerInfo& callback)
		{
			this->m_on_save_peer_info_ends = callback;
		}

		void CGameServer::set_callback_on_save_game_state_starts(const TCallbackOnSaveGameState& callback)
		{
			this->m_on_save_game_state_starts = callback;
		}

		void CGameServer::set_callback_on_save_game_state_ends(const TCallbackOnSaveGameState& callback)
		{
			this->m_on_save_game_state_ends = callback;
		}

		void CGameServer::set_callback_on_save_player_action_starts(const TCallbackOnSavePlayerAction& callback)
		{
			this->m_on_save_player_action_starts = callback;
		}

		void CGameServer::set_callback_on_save_player_action_ends(const TCallbackOnSavePlayerAction& callback)
		{
			this->m_on_save_player_action_ends = callback;
		}

		void CGameServer::set_callback_on_save_game_end_starts(const TCallbackOnSaveGameEnd& callback)
		{
			this->m_on_save_game_end_starts = callback;
		}

		void CGameServer::set_callback_on_save_game_end_ends(const TCallbackOnSaveGameEnd& callback)
		{
			this->m_on_save_game_end_ends = callback;
		}

		void CGameServer::set_callback_on_server_error(const TCallbackOnServerError& callback)
		{
			this->m_on_server_error = callback;
		}

		void CGameServer::set_callback_on_hanabi_error(const TCallbackOnHanabiError& callback)
		{
			this->m_on_hanabi_error = callback;
		}

		void CGameServer::set_callback_on_std_error(const TCallbackOnStdError& callback)
		{
			this->m_on_std_error = callback;
		}

		void CGameServer::set_callback_on_error(const TCallbackOnError& callback)
		{
			this->m_on_error = callback;
		}

		void CGameServer::call_on_game_starts() const
		{
			if (this->m_on_game_starts)
				this->m_on_game_starts(this->game_option(), this->start_timestamp());
		}

		void CGameServer::call_on_game_ends() const
		{
			if (this->m_on_game_ends)
				this->m_on_game_ends(this->start_timestamp(), this->end_timestamp());
		}

		void CGameServer::call_on_lobby_starts() const
		{
			if (this->m_on_lobby_starts)
				this->m_on_lobby_starts();
		}

		void CGameServer::call_on_lobby_ends() const
		{
			if (this->m_on_lobby_ends)
				this->m_on_lobby_ends(this->players());
		}

		void CGameServer::call_on_setup_game_starts() const
		{
			if (this->m_on_setup_game_starts)
				this->m_on_setup_game_starts(this->game_option());
		}

		void CGameServer::call_on_setup_game_ends() const
		{
			if (this->m_on_setup_game_ends)
				this->m_on_setup_game_ends(this->game_state(), this->game_option());
		}

		void CGameServer::call_on_closing_connection_starts() const
		{
			if (this->m_on_closing_connection_starts)
				this->m_on_closing_connection_starts();
		}

		void CGameServer::call_on_closing_connection_ends() const
		{
			if (this->m_on_closing_connection_ends)
				this->m_on_closing_connection_ends();
		}

		void CGameServer::call_on_invalid_action(const std::shared_ptr<common::IAction>& p_action, const EHanabiException& error) const
		{
			if (this->m_on_invalid_action)
				this->m_on_invalid_action(this->game_state().current_player_id(), p_action, error, this->players());
		}

		void CGameServer::call_on_turn_starts() const
		{
			if (this->m_on_turn_starts)
				this->m_on_turn_starts(this->game_state().player_turn_count(), this->game_state().game_turn_count(), this->game_state(), this->players());
		}

		void CGameServer::call_on_turn_ends() const
		{
			if (this->m_on_turn_ends)
				this->m_on_turn_ends(this->game_state().player_turn_count(), this->game_state().game_turn_count(), this->game_state(), this->players());
		}

		void CGameServer::call_on_start_of_game_loop() const
		{
			if (this->m_on_start_of_game_loop)
				this->m_on_start_of_game_loop(this->game_end().game_start_timestamp());
		}

		void CGameServer::call_on_end_of_game_loop() const
		{
			if (this->m_on_end_of_game_loop)
				this->m_on_end_of_game_loop(this->game_end(), this->game_state());
		}

		void CGameServer::call_on_game_end_condition_player_stuck() const
		{
			if (this->m_on_game_end_condition_player_stuck)
				this->m_on_game_end_condition_player_stuck(this->game_state().current_player_id(), this->game_end(), this->game_state(), this->players());
		}

		void CGameServer::call_on_game_end_condition_max_error() const
		{
			if (this->m_on_game_end_condition_max_error)
				this->m_on_game_end_condition_max_error(this->game_state().error_token_count(), this->game_end(), this->game_state(), this->players());
		}

		void CGameServer::call_on_game_end_condition_max_score() const
		{
			if (this->m_on_game_end_condition_max_score)
				this->m_on_game_end_condition_max_score(this->game_state().board().score(), this->game_end(), this->game_state(), this->players());
		}

		void CGameServer::call_on_retrieve_action_starts() const
		{
			if (this->m_on_retrieve_action_starts)
				this->m_on_retrieve_action_starts(this->game_state().current_player_id(), this->game_state(), this->players());
		}

		void CGameServer::call_on_retrieve_action_ends(const std::shared_ptr<common::IAction>& p_action) const
		{
			if (this->m_on_retrieve_action_ends)
				this->m_on_retrieve_action_ends(this->game_state().current_player_id(), p_action, this->game_state(), this->players());
		}

		void CGameServer::call_on_giving_bonus_starts(const common::CardColor& completed_color) const
		{
			if (this->m_on_giving_bonus_starts)
				this->m_on_giving_bonus_starts(completed_color, this->game_state());
		}

		void CGameServer::call_on_giving_bonus_ends(const common::CardColor& completed_color) const
		{
			if (this->m_on_giving_bonus_ends)
				this->m_on_giving_bonus_ends(completed_color, this->game_state());
		}

		void CGameServer::call_on_setup_peer_starts(common::TIndex player_id) const
		{
			if (this->m_on_setup_peer_starts)
				this->m_on_setup_peer_starts(player_id);
		}

		void CGameServer::call_on_setup_peer_ends(common::TIndex player_id, const CPeerInfo& peer_info) const
		{
			if (this->m_on_setup_peer_ends)
				this->m_on_setup_peer_ends(player_id, peer_info, this->game_option(), this->game_start());
		}

		void CGameServer::call_on_process_player_action_starts(const std::shared_ptr<common::IAction>& p_action) const
		{
			if (this->m_on_process_player_action_starts)
				this->m_on_process_player_action_starts(this->game_state().current_player_id(), p_action, this->game_state(), this->players());
		}

		void CGameServer::call_on_process_player_action_ends(const std::shared_ptr<common::IAction>& p_action, bool valid_action) const
		{
			if (this->m_on_process_player_action_ends)
				this->m_on_process_player_action_ends(this->game_state().current_player_id(), p_action, valid_action, this->game_state(), this->players());
		}

		void CGameServer::call_on_process_drop_starts(const common::CCard& dropped_card) const
		{
			if (this->m_on_process_drop_starts)
				this->m_on_process_drop_starts(this->game_state().current_player_id(), dropped_card, this->game_state());
		}

		void CGameServer::call_on_process_drop_ends(const common::CCard& dropped_card) const
		{
			if (this->m_on_process_drop_ends)
				this->m_on_process_drop_ends(this->game_state().current_player_id(), dropped_card, this->game_state());
		}

		void CGameServer::call_on_process_place_starts(const common::CCard& card_to_place) const
		{
			if (this->m_on_process_place_starts)
				this->m_on_process_place_starts(this->game_state().current_player_id(), card_to_place, this->game_state());
		}

		void CGameServer::call_on_process_place_ends(const common::CCard& card_to_place) const
		{
			if (this->m_on_process_place_ends)
				this->m_on_process_place_ends(this->game_state().current_player_id(), card_to_place, this->game_state());
		}

		void CGameServer::call_on_player_place(const common::CCard& card_to_place, const std::shared_ptr<common::CardColor>& p_completed_color) const
		{
			if (this->m_on_player_place)
				this->m_on_player_place(this->game_state().current_player_id(), card_to_place, p_completed_color, this->game_state());
		}

		void CGameServer::call_on_player_drop(const common::CCard& card_to_drop) const
		{
			if (this->m_on_player_drop)
				this->m_on_player_drop(this->game_state().current_player_id(), card_to_drop, this->game_state());
		}

		void CGameServer::call_on_player_draw(const common::CCard& drawn_card) const
		{
			if (this->m_on_player_draw)
				this->m_on_player_draw(this->game_state().current_player_id(), drawn_card, this->game_state());
		}

		void CGameServer::call_on_player_action_roundtrip_starts(const boost::posix_time::ptime& request_timestamp) const
		{
			if (this->m_on_player_action_roundtrip_starts)
				this->m_on_player_action_roundtrip_starts(this->game_state().current_player_id(), request_timestamp, this->game_state(), this->players());
		}

		void CGameServer::call_on_player_action_roundtrip_ends(const std::shared_ptr<common::IAction>& p_action, const boost::posix_time::ptime& request_timestamp, const boost::posix_time::ptime& response_timestamp) const
		{
			if (this->m_on_player_action_roundtrip_ends)
				this->m_on_player_action_roundtrip_ends(this->game_state().current_player_id(), p_action, request_timestamp, response_timestamp, this->game_state(), this->players());
		}

		void CGameServer::call_on_save_game_start_starts(const server_side::CGameStart& game_start) const
		{
			if (this->m_on_save_game_start_starts)
				this->m_on_save_game_start_starts(game_start);
		}

		void CGameServer::call_on_save_game_start_ends(const server_side::CGameStart& game_start) const
		{
			if (this->m_on_save_game_start_ends)
				this->m_on_save_game_start_ends(game_start);
		}

		void CGameServer::call_on_save_peer_info_starts(const server_side::CGameStart& game_start, const server_side::CPeerInfo& peer_info) const
		{
			if (this->m_on_save_peer_info_starts)
				this->m_on_save_peer_info_starts(game_start, peer_info);
		}

		void CGameServer::call_on_save_peer_info_ends(const server_side::CGameStart& game_start, const server_side::CPeerInfo& peer_info) const
		{
			if (this->m_on_save_peer_info_ends)
				this->m_on_save_peer_info_ends(game_start, peer_info);
		}

		void CGameServer::call_on_save_game_state_starts(const server_side::CGameStart& game_start, const server_side::CGameState& game_state) const
		{
			if (this->m_on_save_game_state_starts)
				this->m_on_save_game_state_starts(game_start, game_state);
		}

		void CGameServer::call_on_save_game_state_ends(const server_side::CGameStart& game_start, const server_side::CGameState& game_state) const
		{
			if (this->m_on_save_game_state_ends)
				this->m_on_save_game_state_ends(game_start, game_state);
		}

		void CGameServer::call_on_save_player_action_starts(const server_side::CGameStart& game_start, const common::IAction& action, common::TCount player_turn_count, common::TCount game_turn_count, common::TIndex current_player_id, const boost::posix_time::ptime& request_timestamp, const boost::posix_time::ptime& response_timestamp) const
		{
			if (this->m_on_save_player_action_starts)
				this->m_on_save_player_action_starts(game_start, action, player_turn_count, game_turn_count, current_player_id, request_timestamp, response_timestamp);
		}

		void CGameServer::call_on_save_player_action_ends(const server_side::CGameStart& game_start, const common::IAction& action, common::TCount player_turn_count, common::TCount game_turn_count, common::TIndex current_player_id, const boost::posix_time::ptime& request_timestamp, const boost::posix_time::ptime& response_timestamp) const
		{
			if (this->m_on_save_player_action_ends)
				this->m_on_save_player_action_ends(game_start, action, player_turn_count, game_turn_count, current_player_id, request_timestamp, response_timestamp);
		}

		void CGameServer::call_on_save_game_end_starts(const server_side::CGameStart& game_start, const server_side::CGameEnd& game_end) const
		{
			if (this->m_on_save_game_end_starts)
				this->m_on_save_game_end_starts(game_start, game_end);
		}

		void CGameServer::call_on_save_game_end_ends(const server_side::CGameStart& game_start, const server_side::CGameEnd& game_end) const
		{
			if (this->m_on_save_game_end_ends)
				this->m_on_save_game_end_ends(game_start, game_end);
		}

		void CGameServer::call_on_player_hello(common::TIndex player_id) const
		{
			if (this->m_on_player_hello)
				this->m_on_player_hello(player_id);
		}

		void CGameServer::call_on_peer_connecting(common::TIndex player_id) const
		{
			if (this->m_on_peer_connecting)
				this->m_on_peer_connecting(player_id);
		}

		void CGameServer::call_on_peer_rejected(common::TIndex player_id, const CPeerInfo& peer_info) const
		{
			if (this->m_on_peer_rejected)
				this->m_on_peer_rejected(player_id, peer_info);
		}

		void CGameServer::call_on_peer_connected(common::TIndex player_id, const CPeerInfo& peer_info) const
		{
			if (this->m_on_peer_connected)
				this->m_on_peer_connected(player_id, peer_info);
		}

		void CGameServer::call_on_server_error(EServerError& e) const
		{
			if (this->m_on_server_error)
				this->m_on_server_error(e);
		}

		void CGameServer::call_on_hanabi_error(EHanabiException& e) const
		{
			if (this->m_on_hanabi_error)
				this->m_on_hanabi_error(e);
		}

		void CGameServer::call_on_std_error(std::exception& e) const
		{
			if (this->m_on_std_error)
				this->m_on_std_error(e);
		}

		void CGameServer::call_on_error() const
		{
			if (this->m_on_error)
				this->m_on_error();
		}

		void CGameServer::lobby()
		{
			this->call_on_lobby_starts();

			// Create the acceptor
			boost::asio::ip::tcp::acceptor acceptor(this->m_io_context, this->m_endpoint);

			// Get the server version
			const common::TVersionArray server_version = common::to_version(HANABI_LIB_VERSION);

			// Wait for players
			common::TCount inserted_player = 0;
			while (inserted_player < this->m_player_count)
			{
				// Get a peer id
				const common::TIndex peer_id = this->get_available_peer_id();
				this->call_on_setup_peer_starts(peer_id);
				
				// Create a gateway for this peer
				std::shared_ptr<server_side::IPlayerGateway> p_gateway(new CTCPPlayer(this->m_io_context, acceptor));
				p_gateway->init();

				// Hand check with the peer
				this->call_on_peer_connecting(peer_id);
				std::shared_ptr<CPeerInfo> p_peer_info = p_gateway->retrieve_peer_info(peer_id, server_version);
				// Check the peer version
				if (!this->m_compatibility_checker.check(p_peer_info->version_array()))
				{
					p_gateway->close();
					this->call_on_peer_rejected(peer_id, *p_peer_info);
					continue;
				}

				// Player creation
				CServerPlayer new_player(p_gateway, *p_peer_info);

				// Say hello
				p_gateway->say_hello(this->welcome_message());
				this->call_on_player_hello(peer_id);

				// Add the new player to the container
				assert(!this->players().contains(peer_id));
				this->players().insert(std::pair(peer_id, new_player));
				this->save_peer_info(*p_peer_info);

				// Notify all peers that a new one joined
				this->call_on_peer_connected(peer_id, *p_peer_info);

				// Merge the tags from the player with the server tags
				for (const common::TTag tag : p_peer_info->tags())
					this->m_game_state.game_start().add_tag(tag);
				this->m_p_api_gateway->add_tag(this->game_start().game_uuid(), this->tags());

				// Loop on player and 
				// - send the info about the new player to each connected player
				// - send to the new player info about each connected player
				for (const auto& player : this->players())
				{
					// Send info about all connected player to the new one
					// (The new player receive info about him self here)
					p_gateway->send_peer_info_update(player.second.peer_info(), 0);

					// Send info about the new player to all other players
					// (The new player will not receive info about him self here)
					if (player.second.peer_info().id() != peer_id)
					{
						const std::shared_ptr<IPlayerGateway>& p_player_gateway = player.second.gateway();
						assert(p_player_gateway);
						p_player_gateway->send_peer_info_update(*p_peer_info, 0);
					}
				}

				// Send him the game option
				p_gateway->send_game_option(this->game_option(), 0);

				++inserted_player;
			}

			assert(this->players().size() == std::size_t(this->m_player_count));
			this->call_on_lobby_ends();
		}

		void CGameServer::setup_game()
		{
			this->call_on_setup_game_starts();
			
			const common::TCount player_count = this->game_option().player_count();

			// Pre-conditions
			if (player_count < 2)
				THROW_ERROR_1(EInvalidPlayerNumberError, player_count);

			// Deal cards
			const common::TCount player_hand_max_size = this->game_option().player_hand_max_size();
			for (common::TCount i = 0; i < player_hand_max_size; ++i)
			{
				for (common::TIndex player_index = 0; player_index < player_count; ++player_index)
				{
					if (!this->game_state().stack().stack().empty())
					{
						const common::CCard card = this->game_state().stack().draw();
						this->game_state().player_hand().insert(player_index, card);
						this->game_state().card_information().new_card(player_index);
					}
					else
						break;
				}
			}

			// Callback
			this->call_on_setup_game_ends();

			// Save game start
			this->save_game_start();
		}

		void CGameServer::close_connections()
		{
			// Callback
			this->call_on_closing_connection_starts();

			for (const auto& pair : this->players())
			{
				const std::shared_ptr<IPlayerGateway> p_gateway = pair.second.gateway();
				assert(p_gateway);

				// Close all connections
				p_gateway->close();
			}

			// Callback
			this->call_on_closing_connection_ends();
		}

		void CGameServer::save_game_start()
		{
			this->call_on_save_game_start_starts(this->game_start());
			this->m_p_api_gateway->save_game_start(this->game_start());
			this->call_on_save_game_start_ends(this->game_start());
		}

		void CGameServer::save_peer_info(const CPeerInfo& peer_info)
		{
			this->call_on_save_peer_info_starts(this->game_start(), peer_info);
			this->m_p_api_gateway->save_peer_info(this->game_start().game_uuid(), peer_info);
			this->call_on_save_peer_info_ends(this->game_start(), peer_info);
		}

		void CGameServer::save_current_game_state()
		{
			this->call_on_save_game_state_starts(this->game_start(), this->game_state());
			this->m_p_api_gateway->save_game_state(this->game_state());
			this->call_on_save_game_state_ends(this->game_start(), this->game_state());
		}

		void CGameServer::save_player_action(const std::shared_ptr<common::IAction>& p_action, const boost::posix_time::ptime& request_timestamp, const boost::posix_time::ptime& response_timestamp)
		{
			const common::TCount player_turn_count = this->game_state().player_turn_count();
			const common::TCount game_turn_count = this->game_state().game_turn_count();
			const common::TIndex current_player_id = this->game_state().current_player_id();

			this->call_on_save_player_action_starts(this->game_start(), *p_action, player_turn_count, game_turn_count, current_player_id, request_timestamp, response_timestamp);
			this->m_p_api_gateway->save_player_action(*p_action, this->game_start().game_uuid(), player_turn_count, game_turn_count, current_player_id, request_timestamp, response_timestamp);
			this->call_on_save_player_action_ends(this->game_start(), *p_action, player_turn_count, game_turn_count, current_player_id, request_timestamp, response_timestamp);
		}

		void CGameServer::save_game_end()
		{
			this->call_on_save_game_end_starts(this->game_start(), this->game_end());
			this->m_p_api_gateway->save_game_end(this->game_end());
			this->call_on_save_game_end_ends(this->game_start(), this->game_end());
		}

		void CGameServer::process_player_action(const std::shared_ptr<common::IAction>& p_action)
		{
			if (p_action->type() == common::CDrop::get_unique_id())
			{
				const common::CDrop& drop_action = static_cast<const common::CDrop&>(*p_action);
				this->process_drop(drop_action);
			}
			else if (p_action->type() == common::CPlace::get_unique_id())
			{
				const common::CPlace& place_action = static_cast<const common::CPlace&>(*p_action);
				this->process_place(place_action);
			}
			else if (p_action->type() == common::CSpeak::get_unique_id())
			{
				const common::CSpeak& speak_action = static_cast<const common::CSpeak&>(*p_action);
				this->process_speak(speak_action);
			}
			else
			{
				THROW_ERROR(EImpossibleError);
			}
		}

		void CGameServer::process_drop(const common::CDrop& action)
		{
			if (!this->allowed_to_drop())
				THROW_ERROR_1(ENotAllowedActionError, "Drop");

			const common::TIndex current_player_id = this->game_state().current_player_id();

			// Move the card from the player hand to the trash
			const common::TIndex card_index = action.hand_index();
			if (card_index >= common::TIndex(this->game_state().player_hand().hand(current_player_id).size()))
				THROW_ERROR_1(EOutOfRangeError, std::int64_t(card_index));

			const common::CCard dropped_card = this->game_state().player_hand().remove(current_player_id, card_index);

			// Callback
			this->call_on_process_drop_starts(dropped_card);

			this->game_state().card_information().remove(current_player_id, card_index);

			this->trash(dropped_card);

			// Move the top card of the stack to the player hand
			if (!this->game_state().stack().is_empty())
			{
				this->draw_from_stack(current_player_id);
			}

			// Give a speak token
			this->game_state().add_speak_token();

			this->gateway(current_player_id)->send_ok();

			this->broadcast_action_drop(current_player_id, dropped_card);

			this->call_on_process_drop_ends(dropped_card);
		}

		void CGameServer::process_place(const common::CPlace& action)
		{
			const common::TIndex current_player_id = this->game_state().current_player_id();

			// Move the card from the player hand to the board
			const common::TIndex card_index = action.hand_index();
			if (card_index >= common::TIndex(this->game_state().player_hand().hand(current_player_id).size()))
				THROW_ERROR_1(EOutOfRangeError, std::int64_t(card_index));

			const common::CCard card_to_place = this->game_state().player_hand().remove(current_player_id, card_index);

			// Callback
			this->call_on_process_place_starts(card_to_place);

			this->game_state().card_information().remove(current_player_id, card_index);

			// Is it possible to place ?
			if (this->is_a_valid_place(card_to_place))
			{
				// This is possible to place ...

				// Place the card on the board
				this->place(card_to_place);
			}
			else
			{
				// This is not possible to place ...

				// The card is discarded
				this->trash(card_to_place);

				// The error counter is increased
				this->error_committed(true);
			}

			// Move the top card of the stack to the player hand
			if (!this->game_state().stack().is_empty())
			{
				this->draw_from_stack(current_player_id);
			}

			this->gateway(current_player_id)->send_ok();

			// Broadcast
			this->broadcast_action_place(current_player_id, card_to_place);

			this->call_on_process_place_ends(card_to_place);
		}

		void CGameServer::process_speak(const common::CSpeak& action)
		{
			if (!this->allowed_to_speak())
				THROW_ERROR_1(ENotAllowedActionError, "Speak");

			if (this->game_state().current_player_id() == action.player_index())
				THROW_ERROR_1(ENotAllowedActionError, "Speak to him self");

			const common::TIndex current_player_id = this->game_state().current_player_id();

			const common::TIndex target_player_id = action.player_index();

			if (target_player_id < 0 || target_player_id >= common::TIndex(this->game_state().player_hand().player_hand().size()))
				THROW_ERROR_1(ENotAllowedActionError, "Speak to out of range players");

			if (action.is_color_info())
			{
				const common::CardColor& target_color = action.color_info();

				// Check validity
				const bool is_valid_color = this->game_option().callable_colors().contains(target_color);
				if (!is_valid_color)
				{
					std::ostringstream oss;
					oss << target_color;
					THROW_ERROR_1(EInvalidColorError, oss.str());
				}

				const auto& player_hand = this->game_state().player_hand().hand(target_player_id);
				auto& player_info = this->game_state().card_information().hand(target_player_id);

				for (common::TIndex hand_index = 0; hand_index < common::TIndex(player_hand.size()); ++hand_index)
				{
					const auto& card = player_hand[hand_index];
					if (card.is(target_color, this->game_option().color_option().at(card.color())))
					{
						player_info[hand_index].put(target_color);
					}
					else
					{
						player_info[hand_index].put_not(target_color);
					}
				}

				this->game_state().remove_speak_token();

				this->gateway(current_player_id)->send_ok();

				// Broadcast
				this->broadcast_action_speak(this->game_state().current_player_id(), target_player_id, target_color);
			}
			else if (action.is_value_info())
			{
				const common::TValue& target_value = action.value_info();

				// Check validity
				const bool is_greater_than_max = target_value > this->game_option().max_card_value();
				const bool is_lower_than_min = target_value < this->game_option().min_card_value();
				if (is_greater_than_max || is_lower_than_min)
					THROW_ERROR_1(EOutOfRangeError, std::int64_t(target_value));

				const auto& player_hand = this->game_state().player_hand().hand(target_player_id);
				auto& player_info = this->game_state().card_information().hand(target_player_id);

				for (common::TIndex hand_index = 0; hand_index < common::TIndex(player_hand.size()); ++hand_index)
				{
					const auto& card = player_hand[hand_index];
					if (card.is(target_value))
					{
						player_info[hand_index].put(target_value);
					}
					else
					{
						player_info[hand_index].put_not(target_value);
					}
				}

				this->game_state().remove_speak_token();

				this->gateway(current_player_id)->send_ok();

				// Broadcast
				this->broadcast_action_speak(this->game_state().current_player_id(), target_player_id, target_value);
			}
			else
			{
				THROW_ERROR(EImpossibleError);
			}
		}

		void CGameServer::stack_player_action(const std::shared_ptr<common::IAction>& p_action, const boost::posix_time::ptime& request_timestamp, const boost::posix_time::ptime& response_timestamp)
		{
			const boost::posix_time::time_duration duration = response_timestamp - request_timestamp;
			this->game_end().player_statistics().at(this->game_state().current_player_id()).cumul_total_duration(duration);
			if (p_action->type() == common::CDrop::get_unique_id())
			{
				this->game_end().player_statistics().at(this->game_state().current_player_id()).cumul_drop_duration(duration);
				this->game_end().player_statistics().at(this->game_state().current_player_id()).increment_drop_count();
			}
			else if (p_action->type() == common::CPlace::get_unique_id())
			{
				this->game_end().player_statistics().at(this->game_state().current_player_id()).cumul_place_duration(duration);
				this->game_end().player_statistics().at(this->game_state().current_player_id()).increment_place_count();
			}
			else if (p_action->type() == common::CSpeak::get_unique_id())
			{
				this->game_end().player_statistics().at(this->game_state().current_player_id()).cumul_speak_duration(duration);
				this->game_end().player_statistics().at(this->game_state().current_player_id()).increment_speak_count();
			}
			else
			{
				THROW_ERROR(EImpossibleError);
			}
		}

		void CGameServer::broadcast_game_update() const
		{
			common::TIndex player_id = 0;
			for (const auto& pair : this->players())
			{
				const std::shared_ptr<IPlayerGateway>& p_gateway = pair.second.gateway();
				assert(p_gateway);
				
				p_gateway->send_game_update(this->game_state(), player_id);

				++player_id;
			}
		}

		void CGameServer::broadcast_game_end() const
		{
			common::TIndex player_id = 0;
			for (const auto& pair : this->players())
			{
				const std::shared_ptr<IPlayerGateway>& p_gateway = pair.second.gateway();
				assert(p_gateway);

				p_gateway->send_game_end(this->game_end(), player_id);

				++player_id;
			}
		}

		void CGameServer::broadcast_action_place(common::TIndex current_player_id, const common::CCard& placed_card) const
		{
			for (const auto& pair : this->players())
			{
				const std::shared_ptr<IPlayerGateway>& p_gateway = pair.second.gateway();
				assert(p_gateway);

				p_gateway->send_action_place_update(current_player_id, placed_card);
			}
		}

		void CGameServer::broadcast_action_drop(common::TIndex current_player_id, const common::CCard& droped_card) const
		{
			for (const auto& pair : this->players())
			{
				const std::shared_ptr<IPlayerGateway>& p_gateway = pair.second.gateway();
				assert(p_gateway);

				p_gateway->send_action_drop_update(current_player_id, droped_card);
			}
		}

		void CGameServer::broadcast_action_speak(common::TIndex current_player_id, common::TIndex target_player_id, common::TValue value) const
		{
			for (const auto& pair : this->players())
			{
				const std::shared_ptr<IPlayerGateway>& p_gateway = pair.second.gateway();
				assert(p_gateway);

				p_gateway->send_action_speak_update(current_player_id, target_player_id, value);
			}
		}

		void CGameServer::broadcast_action_speak(common::TIndex current_player_id, common::TIndex target_player_id, const common::CardColor& color) const
		{
			for (const auto& pair : this->players())
			{
				const std::shared_ptr<IPlayerGateway>& p_gateway = pair.second.gateway();
				assert(p_gateway);

				p_gateway->send_action_speak_update(current_player_id, target_player_id, color);
			}
		}

		void CGameServer::broadcast_bonus_given(common::TIndex current_player_id, const common::CardColor& color) const
		{
			for (const auto& pair : this->players())
			{
				const std::shared_ptr<IPlayerGateway>& p_gateway = pair.second.gateway();
				assert(p_gateway);

				p_gateway->send_bonus_given_update(current_player_id, color);
			}
		}

		void CGameServer::broadcast_error_token_given(common::TIndex current_player_id, common::TCount error_token_count) const
		{
			for (const auto& pair : this->players())
			{
				const std::shared_ptr<IPlayerGateway>& p_gateway = pair.second.gateway();
				assert(p_gateway);

				p_gateway->send_error_token_given_update(current_player_id, error_token_count);
			}
		}

		void CGameServer::place(const common::CCard& card_to_place)
		{
			assert(this->is_a_valid_place(card_to_place));

			this->completed_color(this->game_state().board().place(card_to_place));

			this->call_on_player_place(card_to_place, this->completed_color());
		}

		void CGameServer::give_bonus(const common::CardColor& completed_color)
		{
			this->call_on_giving_bonus_starts(completed_color);

			// Add a speak token
			if (this->game_state().can_drop())
			{
				this->game_state().add_speak_token();
			}

			this->call_on_giving_bonus_ends(completed_color);
		}

		void CGameServer::trash(const common::CCard& card_to_trash)
		{
			this->game_state().trash().trash(card_to_trash);

			this->call_on_player_drop(card_to_trash);
		}

		void CGameServer::draw_from_stack(common::TIndex player_id)
		{
			const common::CCard new_card = this->game_state().stack().draw();
			this->game_state().player_hand().insert(player_id, new_card);
			this->game_state().card_information().new_card(player_id);

			this->call_on_player_draw(new_card);
		}

		common::TIndex CGameServer::get_available_peer_id() const
		{
			if (!this->m_shuffle_peers)
			{
				if (this->players().size() < std::size_t(this->m_player_count))
					return common::TIndex(this->players().size());
				else
					THROW_ERROR(EServerFull);
			}
			else
			{
				std::vector<common::TIndex> available_player_id;
				for (common::TIndex id = 0; id < this->m_player_count; ++id)
				{
					if (!this->players().contains(id))
						available_player_id.push_back(id);
				}
				
				if (available_player_id.empty())
					THROW_ERROR(EServerFull);

				if (available_player_id.size() == 1)
					return available_player_id.at(0);
				else
				{
					std::random_device device;
					std::mt19937 random_generator(device());

					std::uniform_int_distribution<common::TIndex> index_choice(0, available_player_id.size() - 1);
					const common::TIndex random_index = index_choice(random_generator);
					return available_player_id.at(random_index);
				}
			}
		}
	}
}