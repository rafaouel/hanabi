#include "pch.h"
#include "tcpplayer.h"

#include "exception.h"
#include "packetcoder.h"
#include "packetparser.h"


namespace hanabi
{
	namespace server_side
	{
		CTCPPlayer::CTCPPlayer(boost::asio::io_context& io_context, boost::asio::ip::tcp::acceptor& acceptor) :
			m_socket(io_context, acceptor),
			m_name("")
		{

		}

		CTCPPlayer::~CTCPPlayer() noexcept
		{
			this->m_socket.close();
		}

		void CTCPPlayer::virtual_say_hello(const std::string& message)
		{
			// Create the payload
			boost::json::object content;
			content["message"] = boost::json::value_from(message);

			// Send the hello packet
			network::CPacketCoder coder(network::PacketType::HELLO);
			coder.data(content);
			const network::CPacket packet = coder.encode();

			this->send(packet);
		}

		std::shared_ptr<server_side::CPeerInfo> CTCPPlayer::virtual_retrieve_peer_info(common::TIndex player_id, const common::TVersionArray& server_version)
		{
			boost::json::object content;
			content["playerId"] = boost::json::value_from(player_id);
			content["serverVersion"] = boost::json::value_from(common::to_string(server_version));

			// Send request
			network::CPacketCoder coder(network::PacketType::PEER_INFO_REQUEST);
			coder.data(content);
			const network::CPacket packet = coder.encode();

			this->send(packet);

			// Wait for response

			const network::CPacket response_packet = this->receive();

			network::CPacketParser parser(network::PacketType::PEER_INFO_RESPONSE);
			parser.decode(response_packet);

			std::shared_ptr<CPeerInfo> p_info(new CPeerInfo);
			p_info->update(parser.data());

			// Add server side info
			p_info->address(this->m_socket.socket().remote_endpoint().address());
			p_info->id(player_id);

			return p_info;
		}

		void CTCPPlayer::virtual_send_peer_info_update(const server_side::CPeerInfo& peer_info, common::TIndex player_id)
		{
			network::CPacketCoder coder(network::PacketType::PEER_INFO_UPDATE);
			coder.data(peer_info.pack(player_id));

			const network::CPacket packet = coder.encode();

			this->send(packet);
		}

		void CTCPPlayer::virtual_send_game_option(const CGameOption& game_option, common::TIndex player_id)
		{
			network::CPacketCoder coder(network::PacketType::GAME_OPTION_UPDATE);
			coder.data(game_option.pack(player_id));	

			const network::CPacket packet = coder.encode();

			this->send(packet);
		}

		void CTCPPlayer::virtual_send_game_update(const CGameState& game_state, common::TIndex player_id)
		{
			network::CPacketCoder coder(network::PacketType::GAME_STATE_UPDATE);
			coder.data(game_state.pack(player_id));
			
			const network::CPacket packet = coder.encode();

			this->send(packet);
		}

		void CTCPPlayer::virtual_send_game_end(const CGameEnd& game_end, common::TIndex player_id)
		{
			network::CPacketCoder coder(network::PacketType::GAME_END_UPDATE);

			coder.data(game_end.pack(player_id));
			
			const network::CPacket packet = coder.encode();

			this->send(packet);
		}

		std::shared_ptr<common::IAction> CTCPPlayer::virtual_retrieve_action()
		{
			// Send request
			network::CPacketCoder coder(network::PacketType::ACTION_REQUEST);
			const network::CPacket packet = coder.encode();
			
			this->send(packet);

			// Wait for response

			const network::CPacket response_packet = this->receive();

			network::CPacketParser parser(network::PacketType::ACTION_RESPONSE);
			parser.decode(response_packet);
			
			std::shared_ptr<common::IAction> p_action = common::create_action(parser.data());

			assert(p_action);
			return p_action;
		}

		void CTCPPlayer::virtual_send_action_place_update(common::TIndex source_player_id, const common::CCard& placed_card)
		{
			boost::json::object content;

			content["sourcePlayerId"] = boost::json::value_from(source_player_id);

			boost::json::object card_object;
			card_object["value"] = boost::json::value_from(placed_card.value());
			card_object["color"] = boost::json::value_from(common::card_color_to_string(placed_card.color()));

			content["card"] = card_object;

			network::CPacketCoder coder(network::PacketType::ACTION_PLACE_UPDATE);
			coder.data(content);
			const network::CPacket packet = coder.encode();

			this->send(packet);
		}

		void CTCPPlayer::virtual_send_action_drop_update(common::TIndex source_player_id, const common::CCard& droped_card)
		{
			boost::json::object content;
			content["sourcePlayerId"] = boost::json::value_from(source_player_id);

			boost::json::object card_object;
			card_object["value"] = boost::json::value_from(droped_card.value());
			card_object["color"] = boost::json::value_from(common::card_color_to_string(droped_card.color()));

			content["card"] = card_object;

			network::CPacketCoder coder(network::PacketType::ACTION_DROP_UPDATE);
			coder.data(content);
			const network::CPacket packet = coder.encode();

			this->send(packet);
		}

		void CTCPPlayer::virtual_send_action_speak_update(common::TIndex source_player_id, common::TIndex target_player_id, common::TValue value)
		{
			boost::json::object content;

			content["sourcePlayerId"] = boost::json::value_from(source_player_id);
			content["targetPlayerId"] = boost::json::value_from(target_player_id);
			content["value"] = boost::json::value_from(value);

			network::CPacketCoder coder(network::PacketType::ACTION_SPEAK_UPDATE);
			coder.data(content);
			const network::CPacket packet = coder.encode();

			this->send(packet);
		}

		void CTCPPlayer::virtual_send_action_speak_update(common::TIndex source_player_id, common::TIndex target_player_id, common::CardColor color)
		{
			boost::json::object content;

			content["sourcePlayerId"] = boost::json::value_from(source_player_id);
			content["targetPlayerId"] = boost::json::value_from(target_player_id);
			content["color"] = boost::json::value_from(common::card_color_to_string(color));

			network::CPacketCoder coder(network::PacketType::ACTION_SPEAK_UPDATE);
			coder.data(content);
			const network::CPacket packet = coder.encode();

			this->send(packet);
		}

		void CTCPPlayer::virtual_send_ok()
		{
			network::CPacketCoder coder(network::PacketType::OK);
			const network::CPacket packet = coder.encode();

			this->send(packet);
		}

		void CTCPPlayer::virtual_send_not_ok(const std::string& reason)
		{
			boost::json::object content;

			content["reason"] = boost::json::value_from(reason);

			network::CPacketCoder coder(network::PacketType::NOT_OK);
			coder.data(content);

			const network::CPacket packet = coder.encode();

			this->send(packet);
		}

		void CTCPPlayer::virtual_send_bonus_given_update(common::TIndex current_player_id, const common::CardColor& color)
		{
			boost::json::object content;

			content["playerId"] = boost::json::value_from(current_player_id);
			content["color"] = boost::json::value_from(common::card_color_to_string(color));

			network::CPacketCoder coder(network::PacketType::BONUS_EVENT);
			coder.data(content);

			const network::CPacket packet = coder.encode();

			this->send(packet);
		}

		void CTCPPlayer::virtual_send_error_token_given_update(common::TIndex current_player_id, common::TCount error_token_count)
		{
			boost::json::object content;

			content["playerId"] = boost::json::value_from(current_player_id);
			content["errorTokenCount"] = boost::json::value_from(error_token_count);

			network::CPacketCoder coder(network::PacketType::ERROR_EVENT);
			coder.data(content);

			const network::CPacket packet = coder.encode();

			this->send(packet);
		}

		void CTCPPlayer::virtual_init()
		{
			if (!this->m_socket.socket().is_open())
				this->m_socket.init();
		}

		void CTCPPlayer::virtual_close()
		{
			if (this->m_socket.socket().is_open())
				this->m_socket.close();
		}

		const std::string& CTCPPlayer::virtual_get_name() const
		{
			return this->m_name;
		}

		const boost::asio::ip::address& CTCPPlayer::virtual_get_ip() const
		{
			return this->m_ip;
		}

		void CTCPPlayer::send(const network::CPacket& packet)
		{
			this->m_socket.send(packet);
		}

		network::CPacket CTCPPlayer::receive()
		{
			return this->m_socket.read();
		}
	}
}

