#ifndef VERSION_H_D2F38691_1BE9_4895_8DC3_44A4572A33DA
#define VERSION_H_D2F38691_1BE9_4895_8DC3_44A4572A33DA
#pragma once

constexpr auto HANABI_LIB_VERSION = 200901;

#endif