#ifndef SOCKET_H_963910F3_B7F5_4AB2_9D1E_5768E0F2FCC9
#define SOCKET_H_963910F3_B7F5_4AB2_9D1E_5768E0F2FCC9
#pragma once

#include "packet.h"


namespace hanabi
{
	namespace network
	{
		class ISocket
		{
		public:
			ISocket(boost::asio::io_service& io_context);
			virtual ~ISocket() noexcept;


			void init();
			void close();

			void send(const network::CPacket& packet);
			network::CPacket read();

			boost::asio::ip::tcp::socket& socket() noexcept;
			const boost::asio::ip::tcp::socket& socket() const noexcept;

		private:
			virtual void virtual_init() = 0;

			boost::asio::ip::tcp::socket m_socket;
		};
	}
}

#endif