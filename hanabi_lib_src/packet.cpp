#include "pch.h"
#include "packet.h"

namespace hanabi
{
	namespace network
	{
		CPacket::CPacket() :
			m_body_length(0),
			m_data()
		{

		}

		CPacket::~CPacket() noexcept
		{

		}

		const char* CPacket::data() const
		{
			return this->m_data;
		}

		char* CPacket::data()
		{
			return this->m_data;
		}

		std::size_t CPacket::length() const
		{
			return CPacket::header_length + this->m_body_length;
		}

		const char* CPacket::body() const
		{
			return this->m_data + CPacket::header_length;
		}

		char* CPacket::body()
		{
			return this->m_data + CPacket::header_length;
		}

		std::size_t CPacket::body_length() const
		{
			return this->m_body_length;
		}

		void CPacket::body_length(std::size_t new_length)
		{
			this->m_body_length = new_length;
			if (this->m_body_length > CPacket::max_body_length)
				this->m_body_length = CPacket::max_body_length;
		}

		bool CPacket::decode_header()
		{
			// TODO Update this function to throw an exception instead of returning true/false
			char header[CPacket::header_length + 1] = "";
			std::strncat(header, this->m_data, CPacket::header_length);
			this->m_body_length = std::atoi(header);
			if (this->m_body_length > CPacket::max_body_length)
			{
				this->m_body_length = 0;
				return false;
			}
			return true;
		}

		void CPacket::encode_header()
		{
			char header[CPacket::header_length + 1] = "";
			std::sprintf(header, "%4d", static_cast<int>(this->m_body_length));
			std::memcpy(this->m_data, header, CPacket::header_length);
		}

		PacketType packet_type(const CPacket& packet)
		{
			static constexpr std::size_t packet_type_length = 2;
			char packet_type_buffer[packet_type_length + 1] = "";
			std::memcpy(packet_type_buffer, packet.body(), packet_type_length);
			PacketType packet_type = PacketType(std::atoi(packet_type_buffer));
			return packet_type;
		}

		CPacket build_packet(const std::string& content)
		{
			const std::size_t length = content.size();
			CPacket packet;
			packet.body_length(length);

			std::memcpy(packet.body(), content.c_str(), length);

			packet.encode_header();

			return packet;
		}
	}
}