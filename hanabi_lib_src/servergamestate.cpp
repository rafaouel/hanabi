#include "pch.h"
#include "servergamestate.h"

#include "exception.h"

namespace hanabi
{
	namespace server_side
	{
		CGameState::CGameState(const CGameOption& option, const CGameStart& start) :
			m_game_option(option),
			m_game_start(start),
			m_speak_token_count(option.start_speak_token_count()),
			m_error_token_count(option.start_error_token_count()),
			m_player_turn_count(0),
			m_game_turn_count(0),
			m_current_player_id(option.start_player_id()),
			m_player_turn_timestamp(boost::posix_time::not_a_date_time),
			m_board(option),
			m_trash(option),
			m_stack(option),
			m_player_hand(option),
			m_card_information(option)
		{

		}

		CGameState::~CGameState() noexcept
		{

		}

		const CGameOption& CGameState::game_option() const noexcept
		{
			return this->m_game_option;
		}

		const CGameStart& CGameState::game_start() const noexcept
		{
			return this->m_game_start;
		}

		CGameStart& CGameState::game_start() noexcept
		{
			return this->m_game_start;
		}

		common::TCount CGameState::speak_token_count() const noexcept
		{
			return this->m_speak_token_count;
		}

		void CGameState::speak_token_count(common::TCount new_count)
		{
			if (new_count > this->game_option().max_speak_token_count())
				THROW_ERROR_1(EOutOfRangeError, new_count);
			this->m_speak_token_count = new_count;
		}

		bool CGameState::can_speak() const noexcept
		{
			return this->m_speak_token_count > 0;
		}

		bool CGameState::can_drop() const noexcept
		{
			return this->m_speak_token_count < this->game_option().max_speak_token_count();
		}

		void CGameState::add_speak_token()
		{
			if (this->m_speak_token_count < this->game_option().max_speak_token_count())
			{
				++this->m_speak_token_count;
			}
			else
			{
				THROW_ERROR_1(EOutOfRangeError, std::int64_t(this->m_speak_token_count));
			}
		}

		void CGameState::remove_speak_token()
		{
			if (this->m_speak_token_count > 0)
			{
				--this->m_speak_token_count;
			}
			else
			{
				THROW_ERROR_1(EOutOfRangeError, std::int64_t(this->m_speak_token_count));
			}
		}

		common::TCount CGameState::error_token_count() const noexcept
		{
			return this->m_error_token_count;
		}

		void CGameState::error_token_count(common::TCount new_count)
		{
			if (new_count > this->game_option().max_error_token_count())
				THROW_ERROR_1(EOutOfRangeError, new_count);

			this->m_error_token_count = new_count;
		}

		bool CGameState::is_game_over() const noexcept
		{
			return this->error_token_count() >= this->game_option().max_error_token_count();
		}

		void CGameState::add_error_token()
		{
			if (this->error_token_count() < this->game_option().max_error_token_count())
			{
				++this->m_error_token_count;
			}
		}

		void CGameState::remove_error_token()
		{
			if (this->error_token_count() > 0)
			{
				--this->m_error_token_count;
			}
		}

		common::TCount CGameState::player_turn_count() const noexcept
		{
			return this->m_player_turn_count;
		}

		void CGameState::reset_player_turn_count()
		{
			this->m_player_turn_count = 0;
			this->capture_player_turn_timestamp();
		}

		void CGameState::increase_player_turn_count() noexcept
		{
			++this->m_player_turn_count;
			this->capture_player_turn_timestamp();
		}

		common::TCount CGameState::game_turn_count() const noexcept
		{
			return this->m_game_turn_count;
		}

		void CGameState::reset_game_turn_count()
		{
			this->m_game_turn_count = 0;
		}

		void CGameState::increase_game_turn_count() noexcept
		{
			++this->m_game_turn_count;
		}

		common::TIndex CGameState::current_player_id() const noexcept
		{
			return this->m_current_player_id;
		}

		void CGameState::current_player_id(common::TIndex id)
		{
			assert(id < this->game_option().player_count());

			this->m_current_player_id = id;
		}

		void CGameState::move_current_player()
		{
			this->m_current_player_id = (this->m_current_player_id + 1) % this->game_option().player_count();
		}

		const boost::posix_time::ptime& CGameState::player_turn_timestamp() const noexcept
		{
			return this->m_player_turn_timestamp;
		}

		void CGameState::capture_player_turn_timestamp()
		{
			this->m_player_turn_timestamp = boost::posix_time::microsec_clock::universal_time();
		}

		const CBoard& CGameState::board() const noexcept
		{
			return this->m_board;
		}

		CBoard& CGameState::board() noexcept
		{
			return this->m_board;
		}

		const CTrash& CGameState::trash() const noexcept
		{
			return this->m_trash;
		}

		CTrash& CGameState::trash() noexcept
		{
			return this->m_trash;
		}

		const CStack& CGameState::stack() const noexcept
		{
			return this->m_stack;
		}

		CStack& CGameState::stack() noexcept
		{
			return this->m_stack;
		}

		const CPlayerHand& CGameState::player_hand() const noexcept
		{
			return this->m_player_hand;
		}

		CPlayerHand& CGameState::player_hand() noexcept
		{
			return this->m_player_hand;
		}

		const CCardInformation& CGameState::card_information() const noexcept
		{
			return this->m_card_information;
		}

		CCardInformation& CGameState::card_information() noexcept
		{
			return this->m_card_information;
		}

		boost::json::value CGameState::serialize_to_json() const
		{
			// Create JSON object from game state
			boost::json::object game_state;

			game_state["gameUuid"] = boost::json::value_from(boost::uuids::to_string(this->game_start().game_uuid()));
			game_state["speakTokenCount"] = boost::json::value_from(this->speak_token_count());
			game_state["errorTokenCount"] = boost::json::value_from(this->error_token_count());
			game_state["playerTurnCount"] = boost::json::value_from(this->player_turn_count());
			game_state["gameTurnCount"] = boost::json::value_from(this->game_turn_count());
			game_state["currentPlayerId"] = boost::json::value_from(this->current_player_id());
			game_state["playerTurnTimestamp"] = boost::json::value_from(boost::posix_time::to_iso_extended_string(this->player_turn_timestamp()) + "Z");

			// The board
			game_state["board"] = this->board().to_json();

			// The trash
			game_state["trash"] = this->trash().to_json();

			// The stack
			game_state["stack"] = this->stack().to_json();

			// The hands of the players
			game_state["playerHand"] = this->player_hand().to_json();

			// The informations the players exchanged
			game_state["playerCardInfo"] = this->card_information().to_json();

			return game_state;
		}

		void CGameState::parse_from_json(const boost::json::value& /*value*/)
		{

		}

		boost::json::value CGameState::virtual_pack(common::TIndex target_player_id) const
		{
			// Create JSON object from game state
			boost::json::object game_state;

			game_state["gameUuid"] = boost::json::value_from(boost::uuids::to_string(this->game_start().game_uuid()));
			game_state["speakTokenCount"] = boost::json::value_from(this->speak_token_count());
			game_state["errorTokenCount"] = boost::json::value_from(this->error_token_count());
			game_state["playerTurnCount"] = boost::json::value_from(this->player_turn_count());
			game_state["gameTurnCount"] = boost::json::value_from(this->game_turn_count());
			game_state["currentPlayerId"] = boost::json::value_from(this->current_player_id());
			game_state["playerTurnTimestamp"] = boost::json::value_from(boost::posix_time::to_iso_extended_string(this->player_turn_timestamp()) + "Z");

			// The board
			game_state["board"] = this->board().pack(target_player_id);

			// The trash
			game_state["trash"] = this->trash().pack(target_player_id);

			// The stack
			game_state["stack"] = this->stack().pack(target_player_id);

			// The hand of the players
			game_state["playerHand"] = this->player_hand().pack(target_player_id);

			// The informations the players exchanged
			game_state["playerCardInfo"] = this->card_information().pack(target_player_id);

			return game_state;
		}
	}
}