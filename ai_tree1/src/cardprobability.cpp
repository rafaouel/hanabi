#include "pch.h"
#include "cardprobability.h"




bool is_in_board(const hanabi::client_side::CGameOption& game_option, const hanabi::client_side::CGameState& game_state, hanabi::common::TValue value, hanabi::common::CardColor color)
{
	// Get the board value associated with the given color
	const hanabi::common::TValue board_value = game_state.board().board().at(color);

	// Get the direction of this color
	const bool is_ascending = game_option.color_option().at(color).is_ascending();

	if (is_ascending)
		return board_value > 0 && board_value >= value;
	else
		return board_value > 0 && board_value <= value;
}

hanabi::common::TCount count_in_trash(const hanabi::client_side::CGameState& game_state, hanabi::common::TValue value, hanabi::common::CardColor color)
{
	hanabi::common::TCount count = 0;

	for (const hanabi::common::CCard& card : game_state.trash().stack())
	{
		if (card.value() == value && card.color() == color)
			++count;
	}

	return count;
}

bool is_card_critical(const hanabi::client_side::CGameOption& game_option, const hanabi::client_side::CGameState& game_state, hanabi::common::TValue value, hanabi::common::CardColor color)
{
	assert(game_option.color_option().contains(color));
	assert(game_option.color_option().at(color).distribution().contains(value));

	// A critical card should not be useless, lets first check that
	if (is_card_useless(game_option, game_state, value, color))
		return false;

	// The card is not useless...
	// Get the number of card of that type in the game option distribution
	const hanabi::common::TCount distribution_count = game_option.color_option().at(color).distribution().at(value);

	// Count the number of copy already trashed
	hanabi::common::TCount trash_count = 0;
	for (const hanabi::common::CCard& card : game_state.trash().stack())
	{
		const hanabi::common::TValue card_value = card.value();
		if (card_value == value)
		{
			const hanabi::common::CardColor card_color = card.color();
			if (card_color == color)
			{
				++trash_count;
			}
		}
	}

	return (distribution_count == trash_count + 1);
}

bool is_card_placeable(const hanabi::client_side::CGameOption& game_option, const hanabi::client_side::CGameState& game_state, hanabi::common::TValue value, hanabi::common::CardColor color)
{
	assert(game_option.color_option().contains(color));
	assert(game_state.board().board().contains(color));

	if (game_option.color_option().at(color).is_ascending())
		return (game_state.board().board().at(color) + 1 == value);
	else
		return (game_state.board().board().at(color) - 1 == value);
}

bool is_card_useless(const hanabi::client_side::CGameOption& game_option, const hanabi::client_side::CGameState& game_state, hanabi::common::TValue value, hanabi::common::CardColor color)
{
	// If this card is already placed on the board -> useless
	if (is_in_board(game_option, game_state, value, color))
	{
		return true;
	}
	
	// If this card cannot been place because all copy of a card under it have been trashed
	const hanabi::common::CColorOption& color_option = game_option.color_option().at(color);
	const bool is_ascending = color_option.is_ascending();
	const hanabi::common::CColorOption::TValueDistribution distribution = color_option.distribution();
	const hanabi::common::TValue start = (is_ascending ? distribution.cbegin()->first : distribution.crbegin()->first);
	const hanabi::common::TValue increment = (is_ascending ? 1 : -1);

	for (hanabi::common::TValue test_value = start; test_value != value; test_value += increment)
	{
		const hanabi::common::TCount trash_count = count_in_trash(game_state, test_value, color);
		const hanabi::common::TCount distribution_count = distribution.at(test_value);
		if (trash_count >= distribution_count)
		{
			return true;
		}
	}

	return false;
}

CardProbability::CardProbability(hanabi::common::TValue value, hanabi::common::CardColor color, double proba, const hanabi::client_side::CGameOption& game_option, const hanabi::client_side::CGameState& game_state) :
	m_value(value),
	m_color(color),
	m_probability(proba),
	m_is_critical(false),
	m_is_placeable(false),
	m_is_useless(false)
{
	this->compute_attributs(game_option, game_state);
}

CardProbability::~CardProbability() noexcept
{

}

hanabi::common::TValue CardProbability::value() const noexcept
{
	return this->m_value;
}

hanabi::common::CardColor CardProbability::color() const noexcept
{
	return this->m_color;
}

double CardProbability::probability() const noexcept
{
	return this->m_probability;
}

bool CardProbability::is_critical() const noexcept
{
	return this->m_is_critical;
}

bool CardProbability::is_placeable() const noexcept
{
	return this->m_is_placeable;
}

bool CardProbability::is_useless() const noexcept
{
	return this->m_is_useless;
}

void CardProbability::probability(double prob)
{
	assert(prob >= 0.);
	assert(prob <= 1.);
	this->m_probability = prob;
}

bool CardProbability::is_equal(const CardProbability& card_probability) const
{
	return this->value() == card_probability.value() && this->color() == card_probability.color();
}

void CardProbability::compute_attributs(const hanabi::client_side::CGameOption& game_option, const hanabi::client_side::CGameState& game_state)
{
	/////////////
	// Is useless
	this->m_is_useless = is_card_useless(game_option, game_state, this->value(), this->color());

	//////////////
	// Is critical
	// 'is_critical' depends on 'is_useless': if a card is useless it cannot be critical
	if (this->m_is_useless)
		this->m_is_critical = false;
	else
		this->m_is_critical = is_card_critical(game_option, game_state, this->value(), this->color());

	///////////////
	// Is placeable
	this->m_is_placeable = is_card_placeable(game_option, game_state, this->value(), this->color());
}
