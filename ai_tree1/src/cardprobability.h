#ifndef CARD_PROBABILITY_H_6C2D2403_34DF_4FF5_934D_6021969EB76D
#define CARD_PROBABILITY_H_6C2D2403_34DF_4FF5_934D_6021969EB76D
#pragma once


bool is_in_board(const hanabi::client_side::CGameOption& game_option, const hanabi::client_side::CGameState& game_state, hanabi::common::TValue value, hanabi::common::CardColor color);
hanabi::common::TCount count_in_trash(const hanabi::client_side::CGameState& game_state, hanabi::common::TValue value, hanabi::common::CardColor color);

bool is_card_critical(const hanabi::client_side::CGameOption& game_option, const hanabi::client_side::CGameState& game_state, hanabi::common::TValue value, hanabi::common::CardColor color);
bool is_card_placeable(const hanabi::client_side::CGameOption& game_option, const hanabi::client_side::CGameState& game_state, hanabi::common::TValue value, hanabi::common::CardColor color);
bool is_card_useless(const hanabi::client_side::CGameOption& game_option, const hanabi::client_side::CGameState& game_state, hanabi::common::TValue value, hanabi::common::CardColor color);

class CardProbability
{
public:
	CardProbability(hanabi::common::TValue value, hanabi::common::CardColor color, double proba, const hanabi::client_side::CGameOption& game_option, const hanabi::client_side::CGameState& game_state);
	virtual ~CardProbability() noexcept;


	hanabi::common::TValue value() const noexcept;
	hanabi::common::CardColor color() const noexcept;
	double probability() const noexcept;
	bool is_critical() const noexcept;
	bool is_placeable() const noexcept;
	bool is_useless() const noexcept;

	void probability(double prob);

	bool is_equal(const CardProbability& card_probability) const;

private:
	void compute_attributs(const hanabi::client_side::CGameOption& game_option, const hanabi::client_side::CGameState& game_state);

	hanabi::common::TValue m_value;
	hanabi::common::CardColor m_color;
	double m_probability;
	bool m_is_critical;
	bool m_is_placeable;
	bool m_is_useless;
};

#endif