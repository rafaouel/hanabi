#include "pch.h"
#include "aiparameters.h"


CAIParameters::CAIParameters() :
	m_wait(false),
	m_waiting_duration(1500),
	m_placeable_threshold_min_error(0.65),
	m_placeable_threshold_max_error(0.80),
	m_is_critical_threshold(0.90),
	m_is_useless_threshold(0.90),
	m_is_not_critical_threshold(0.05),
	m_is_not_useless_threshold(0.05),
	m_critical_respeak_probability(0.0),
	m_placeable_respeak_probability(0.45),
	m_random_action_drop_weight(10),
	m_random_action_place_weight(1),
	m_random_action_speak_weight(80),
	m_max_number_of_displayed_card(10),
	m_critical_speak_slot_range(3)
{

}

CAIParameters::~CAIParameters() noexcept
{
}

bool CAIParameters::wait() const noexcept
{
	return this->m_wait;
}

void CAIParameters::wait(bool wait)
{
	this->m_wait = wait;
}

std::size_t CAIParameters::waiting_duration() const noexcept
{
	return this->m_waiting_duration;
}

void CAIParameters::waiting_duration(std::size_t duration)
{
	this->m_waiting_duration = duration;
}

double CAIParameters::is_placeable_threshold(double error_ratio) const noexcept
{
	assert(error_ratio >= 0.);
	assert(error_ratio <= 1.);
	assert(this->m_placeable_threshold_min_error >= 0.);
	assert(this->m_placeable_threshold_min_error <= 1.);
	assert(this->m_placeable_threshold_max_error >= 0.);
	assert(this->m_placeable_threshold_max_error <= 1.);

	// error_ratio == 0. => placeable_threshold = threshold_min_error
	// error_ratio == 1. => placeable_threshold = threshold_max_error 
	return this->m_placeable_threshold_min_error + error_ratio * (this->m_placeable_threshold_max_error - this->m_placeable_threshold_min_error);
}

void CAIParameters::is_placeable_threshold(double threshold_min_error, double threshold_max_error)
{
	assert(threshold_min_error >= 0.);
	assert(threshold_min_error <= 1.);
	assert(threshold_max_error >= 0.);
	assert(threshold_max_error <= 1.);

	this->m_placeable_threshold_min_error = threshold_min_error;
	this->m_placeable_threshold_max_error = threshold_max_error;
}

double CAIParameters::is_critical_threshold() const noexcept
{
	assert(this->m_is_critical_threshold >= 0.);
	assert(this->m_is_critical_threshold <= 1.);
	return this->m_is_critical_threshold;
}

void CAIParameters::is_critical_threshold(double threshold)
{
	assert(threshold >= 0.);
	assert(threshold <= 1.);
	this->m_is_critical_threshold = threshold;
}

double CAIParameters::is_not_critical_threshold() const noexcept
{
	assert(this->m_is_not_critical_threshold >= 0.);
	assert(this->m_is_not_critical_threshold <= 1.);
	return this->m_is_not_critical_threshold;
}

void CAIParameters::is_not_critical_threshold(double threshold)
{
	assert(threshold >= 0.);
	assert(threshold <= 1.);
	this->m_is_not_critical_threshold = threshold;
}

double CAIParameters::is_useless_threshold() const noexcept
{
	assert(this->m_is_useless_threshold >= 0.);
	assert(this->m_is_useless_threshold <= 1.);
	return this->m_is_useless_threshold;
}

void CAIParameters::is_useless_threshold(double threshold)
{
	assert(threshold >= 0.);
	assert(threshold <= 1.);
	this->m_is_useless_threshold = threshold;
}

double CAIParameters::is_not_useless_threshold() const noexcept
{
	assert(this->m_is_not_useless_threshold >= 0.);
	assert(this->m_is_not_useless_threshold <= 1.);
	return this->m_is_not_useless_threshold;
}

void CAIParameters::is_not_useless_threshold(double threshold)
{
	assert(threshold >= 0.);
	assert(threshold <= 1.);
	this->m_is_not_useless_threshold = threshold;
}

double CAIParameters::critical_respeak_probability() const noexcept
{
	assert(this->m_critical_respeak_probability >= 0.);
	assert(this->m_critical_respeak_probability <= 1.);
	return this->m_critical_respeak_probability;
}

void CAIParameters::critical_respeak_probability(double probability)
{
	assert(probability >= 0.);
	assert(probability <= 1.);
	this->m_critical_respeak_probability = probability;
}

double CAIParameters::placeable_respeak_probability() const noexcept
{
	assert(this->m_placeable_respeak_probability >= 0.);
	assert(this->m_placeable_respeak_probability <= 1.);
	return this->m_placeable_respeak_probability;
}

void CAIParameters::placeable_respeak_probability(double probability)
{
	assert(probability >= 0.);
	assert(probability <= 1.);
	this->m_placeable_respeak_probability = probability;
}

std::size_t CAIParameters::random_action_drop_weight() const noexcept
{
	return this->m_random_action_drop_weight;
}

void CAIParameters::random_action_drop_weight(std::size_t weight)
{
	this->m_random_action_drop_weight = weight;
}

std::size_t CAIParameters::random_action_place_weight() const noexcept
{
	return this->m_random_action_place_weight;
}

void CAIParameters::random_action_place_weight(std::size_t weight)
{
	this->m_random_action_place_weight = weight;
}

std::size_t CAIParameters::random_action_speak_weight() const noexcept
{
	return this->m_random_action_speak_weight;
}

void CAIParameters::random_action_speak_weight(std::size_t weight)
{
	this->m_random_action_speak_weight = weight;
}

int CAIParameters::max_number_of_displayed_card() const noexcept
{
	assert(this->m_max_number_of_displayed_card > 0);
	return this->m_max_number_of_displayed_card;
}

void CAIParameters::max_number_of_displayed_card(int number_of_card)
{
	assert(number_of_card > 0);
	this->m_max_number_of_displayed_card = number_of_card;
}

std::size_t CAIParameters::critical_speak_slot_range() const noexcept
{
	return this->m_critical_speak_slot_range;
}

void CAIParameters::critical_speak_slot_range(std::size_t slot_count)
{
	this->m_critical_speak_slot_range = slot_count;
}
