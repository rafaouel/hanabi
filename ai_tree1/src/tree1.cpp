#include "pch.h"
#include "tree1.h"

#include "slotprobability.h"


CTree1AI::CTree1AI(boost::asio::io_service& io_service, const boost::asio::ip::address& server_address, const boost::asio::ip::port_type& server_port, const std::shared_ptr<std::mt19937>& p_random_generator) :
	IGameClient(io_service, server_address, server_port),
	m_p_random_generator(p_random_generator),
	m_parameters(),
	m_hand_probability()
{

}

CTree1AI::~CTree1AI() noexcept
{

}

hanabi::common::TVersionArray CTree1AI::version_array() const noexcept
{
	return hanabi::common::to_version(HANABI_LIB_VERSION);
}

const std::shared_ptr<std::mt19937>& CTree1AI::random_generator() const noexcept
{
	return this->m_p_random_generator;
}

void CTree1AI::on_receiving_hello(const std::string& server_hello_message) const
{
	std::cout << "The server says '" << server_hello_message << "'" << std::endl;
}

void CTree1AI::on_receiving_game_end_update(const hanabi::client_side::CGameEnd& game_end, const hanabi::client_side::CGameState& last_game_state, const hanabi::client_side::TPeerInfoMap& peer_info)
{
	std::cout << "game end update received" << std::endl;
}

void CTree1AI::on_receiving_game_state_update(const hanabi::client_side::CGameState& game_state, hanabi::common::TIndex my_id, const hanabi::client_side::TPeerInfoMap& peer_info)
{
	std::cout << "game state update received" << std::endl;
}

void CTree1AI::on_receiving_game_option_update(const hanabi::client_side::CGameOption& game_option)
{
	std::cout << "game option update received" << std::endl;
}

void CTree1AI::on_receiving_action_speak_value_update(hanabi::common::TIndex source_player_id, hanabi::common::TIndex target_player_id, const hanabi::client_side::TPeerInfoMap& peer_info, hanabi::common::TValue value_info) const
{
	std::cout << "player action update received (speak on value)" << std::endl;
}

void CTree1AI::on_receiving_action_speak_color_update(hanabi::common::TIndex source_player_id, hanabi::common::TIndex target_player_id, const hanabi::client_side::TPeerInfoMap& peer_info, hanabi::common::CardColor color_info) const
{
	std::cout << "player action update received (speak on color)" << std::endl;
}

void CTree1AI::on_receiving_action_drop_update(hanabi::common::TIndex source_player_id, const hanabi::client_side::TPeerInfoMap& peer_info, const hanabi::common::CCard& card) const
{
	std::cout << "player action update received (drop)" << std::endl;
}

void CTree1AI::on_receiving_action_place_update(hanabi::common::TIndex source_player_id, const hanabi::client_side::TPeerInfoMap& peer_info, const hanabi::common::CCard& card) const
{
	std::cout << "player action update received (place)" << std::endl;
}

void CTree1AI::on_receiving_peer_info_request(const std::string& server_version) const
{
	std::cout << "server version is '" << server_version << "'" << std::endl;
}

void CTree1AI::on_receiving_peer_info_update(hanabi::common::TIndex new_peer_id, const hanabi::client_side::CPeerInfo& new_peer, const hanabi::client_side::TPeerInfoMap& peer_info) const
{
	std::cout << "peer info update received" << std::endl;
}

void CTree1AI::on_receiving_action_accepted() const
{
	std::cout << "action accepted" << std::endl;
}

void CTree1AI::on_receiving_action_rejected(const std::string& reason) const
{
	std::cout << "action rejected" << std::endl;
}

void CTree1AI::on_receiving_bonus_event(hanabi::common::TIndex current_player_id, const hanabi::common::CardColor& color) const
{
	std::cout << "bonus :)" << std::endl;
}

void CTree1AI::on_receiving_error_event(hanabi::common::TIndex current_player_id, hanabi::common::TCount error_token_count) const
{
	std::cout << "error :(" << std::endl;
}

std::shared_ptr<hanabi::common::IAction> CTree1AI::process_action_request(const hanabi::client_side::CGameState& game_state, const hanabi::client_side::TPeerInfoMap& peer_info, hanabi::common::TIndex my_id)
{
	std::cout << std::endl;
	std::cout << "It's my turn !" << std::endl;

	this->m_hand_probability.compute_probabilities(this->game_state(), this->my_id(), this->m_parameters);
	
	// Display the probabilities
	int slot_index = 0;
	for (const auto& slot : this->m_hand_probability.slots())
	{
		std::cout << std::endl;

		this->display_slot(slot, slot_index);

		++slot_index;
	}

	if (this->m_parameters.wait())
		std::this_thread::sleep_for(std::chrono::milliseconds(this->m_parameters.waiting_duration()));


	// Choose the action to do
	std::cout << std::endl;
	std::cout << "Selecting action..." << std::endl;
	std::cout << std::endl;

	std::shared_ptr<hanabi::common::IAction> p_action;

	// Look for critical cards in other player hands
	p_action = this->speak_on_critical(this->m_hand_probability.slots());
	if (p_action)
		return p_action;
	std::cout << std::endl;

	// Place card from our hand
	p_action = this->place_placeable(this->m_hand_probability.slots());
	if (p_action)
		return p_action;
	std::cout << std::endl;

	// Drop useless card from our hand
	p_action = this->drop_useless(this->m_hand_probability.slots());
	if (p_action)
		return p_action;
	std::cout << std::endl;

	// Drop copy card from our hand
	p_action = this->drop_copy(this->m_hand_probability.slots());
	if (p_action)
		return p_action;
	std::cout << std::endl;

	// Look for placeable card in other player hands
	p_action = this->speak_on_placeable(this->m_hand_probability.slots());
	if (p_action)
		return p_action;
	std::cout << std::endl;

	// Drop card without info from our hand
	p_action = this->drop_no_info(this->m_hand_probability.slots());
	if (p_action)
		return p_action;
	std::cout << std::endl;

	// Drop the least critical card in our hand
	p_action = this->drop_least_critical(this->m_hand_probability.slots());
	if (p_action)
		return p_action;
	std::cout << std::endl;

	// All previous actions have been rejected !!!
	// PANIC !!!
	// I choose a random action
	std::cout << "I don't know what to do, I am in panic !!! I am going to play at random" << std::endl;
	std::cout << "Allowed actions: " << std::endl;
	std::vector<std::string> action_vector;
	if (this->is_allowed_to_drop())
	{
		std::cout << " - drop" << std::endl;
		for (std::size_t i = 0; i < this->m_parameters.random_action_drop_weight(); ++i)
			action_vector.push_back("drop");
	}
	if (this->is_allowed_to_place())
	{
		std::cout << " - place" << std::endl;
		for (std::size_t i = 0; i < this->m_parameters.random_action_place_weight(); ++i)
			action_vector.push_back("place");
	}
	if (this->is_allowed_to_speak())
	{
		std::cout << " - speak" << std::endl;
		for (std::size_t i = 0; i < this->m_parameters.random_action_speak_weight(); ++i)
			action_vector.push_back("speak");
	}
	
	assert(action_vector.size() > 0);

	// Choose one action randomly
	std::uniform_int_distribution<int> action_choice(0, int(action_vector.size() - 1));
	const int action_index = action_choice(*this->random_generator());
	const std::string& action = action_vector.at(action_index);

	if (action.compare("drop") == 0)
	{
		// The chosen action is 'drop'

		// Choose the card to drop
		const int hand_index = 0;


		const auto& slot = this->m_hand_probability.slots().at(hand_index);
		std::cout << std::endl;
		std::cout << "I have chosen to drop card with index #" << hand_index << std::endl;
		this->display_slot(slot, hand_index);

		return std::shared_ptr<hanabi::common::CDrop>(new hanabi::common::CDrop(hand_index));
	}
	else if (action.compare("place") == 0)
	{
		// The chosen action is 'place'

		// Choose the card to place
		const int hand_index = 0;

		const auto& slot = this->m_hand_probability.slots().at(hand_index);
		std::cout << std::endl;
		std::cout << "I have chosen to place card with index #" << hand_index << std::endl;
		this->display_slot(slot, hand_index);

		return std::shared_ptr<hanabi::common::CPlace>(new hanabi::common::CPlace(hand_index));
	}
	else if (action.compare("speak") == 0)
	{
		// The chosen action is 'speak'

		// Choose the target player
		std::vector<hanabi::common::TIndex> targets;
		for (const auto& pair : this->game_state().other_hands().other_hands())
			targets.push_back(pair.first);
		std::uniform_int_distribution<int> player_choice(0, int(targets.size() - 1));
		const hanabi::common::TIndex target_player_id = targets.at(player_choice(*this->random_generator()));

		// Choose the kind of information to give
		std::bernoulli_distribution bool_generator;
		const bool is_value_info = bool_generator(*this->random_generator());

		if (is_value_info)
		{
			// The chosen speak type is value

			// Choose the value
			std::uniform_int_distribution<hanabi::common::TValue> value_choice(this->game_state().game_option().min_card_value(), this->game_state().game_option().max_card_value());
			const hanabi::common::TValue value_info = value_choice(*this->random_generator());

			std::shared_ptr<hanabi::common::CSpeak> p_action(new hanabi::common::CSpeak(target_player_id));
			p_action->value_info(value_info);
			return p_action;
		}
		else
		{
			// The chosen speak type is color

			// Choose the color
			std::vector<hanabi::common::CardColor> colors;
			for (const auto& pair : this->game_state().game_option().color_option())
			{
				if (pair.first != hanabi::common::CardColor::black && pair.first != hanabi::common::CardColor::multicolor)
					colors.push_back(pair.first);
			}
			std::uniform_int_distribution<int> color_choice(0, int(colors.size() - 1));
			const int color_index = color_choice(*this->random_generator());
			const hanabi::common::CardColor color_info = colors.at(color_index);

			std::shared_ptr<hanabi::common::CSpeak> p_action(new hanabi::common::CSpeak(target_player_id));
			p_action->color_info(color_info);
			return p_action;
		}
	}
	else
	{
		assert(false);
		return std::shared_ptr<hanabi::common::IAction>();
	}
}

hanabi::client_side::CPeerInfoResponse CTree1AI::process_peer_info_request(hanabi::common::TIndex my_id, const std::string server_verison)
{
	const hanabi::common::TTagList tags = { "tree1_ai" };
	return hanabi::client_side::CPeerInfoResponse("AI_tree1", hanabi::common::to_version(HANABI_LIB_VERSION), hanabi::common::PeerType::AI, compiled_with_debug_option, tags);
}

void CTree1AI::display_slot(const SlotProbability& slot, std::size_t slot_index) const
{
	std::cout << "#" << slot_index;

	std::cout << " 'criticality': " << slot.criticality() * 100. << "%";
	std::cout << " 'placeability': " << slot.placeability() * 100. << "%";
	std::cout << " 'uselessness': " << slot.uselessness() * 100. << "%";
	std::cout << std::endl;

	int card_count = 0;
	for (const auto& card_proba : slot.probabilities())
	{
		std::cout << card_proba.probability() * 100. << "% " << card_proba.value() << "-" << hanabi::common::card_color_to_string(card_proba.color());

		if (card_proba.is_critical())
			std::cout << " 'critical'";
		if (card_proba.is_placeable())
			std::cout << " 'placeable'";
		if (card_proba.is_useless())
			std::cout << " 'useless'";

		std::cout << std::endl;

		++card_count;

		if (card_count >= this->m_parameters.max_number_of_displayed_card())
			break;
	}
}

bool CTree1AI::is_allowed_to_drop() const
{
	return this->game_state().speak_token_count() < this->game_state().game_option().max_speak_token_count();
}

bool CTree1AI::is_allowed_to_place() const
{
	return this->game_state().information_collection().information().at(this->my_id()).size() > 0;
}

bool CTree1AI::is_allowed_to_speak() const
{
	return this->game_state().speak_token_count() > 0;
}

std::shared_ptr<hanabi::common::IAction> CTree1AI::place_placeable(const std::vector<SlotProbability>& slots) const
{
	std::cout << "Place placeable" << std::endl;
	if (this->is_allowed_to_place())
	{
		bool find_placeable = false;
		double max_placeability = 0.;
		std::size_t max_index = 0;
		std::size_t index = 0;
		std::size_t count = 0;
		for (const auto& slot : slots)
		{
			if (slot.is_placeable())
			{
				find_placeable = true;
				++count;
				const double placeability = slot.placeability();
				std::cout << "hand_index " << index << " 'placeable' (placeability " << placeability * 100. << "%)" << std::endl;
				if (placeability > max_placeability)
				{
					//std::cout << "This is the best chose for now" << std::endl;
					max_placeability = placeability;
					max_index = index;
				}
			}
			++index;
		}
		if (find_placeable)
		{
			std::cout << count << " cards detected" << std::endl;
			const std::size_t hand_index = max_index;

			const auto& slot = slots.at(hand_index);
			std::cout << std::endl;
			std::cout << "Chosen action: place card with index #" << hand_index << std::endl;
			this->display_slot(slot, hand_index);

			return std::shared_ptr<hanabi::common::CPlace>(new hanabi::common::CPlace(hand_index));
		}
		else
		{
			std::cout << "No card detected" << std::endl;
		}
	}
	return std::shared_ptr<hanabi::common::IAction>();
}

std::shared_ptr<hanabi::common::IAction> CTree1AI::drop_useless(const std::vector<SlotProbability>& slots) const
{
	std::cout << "Drop useless..." << std::endl;
	if (this->is_allowed_to_drop())
	{
		bool find_useless = false;
		double max_uselessness = 0.;
		std::size_t max_index = 0;
		std::size_t index = 0;
		std::size_t count = 0;
		for (const auto& slot : slots)
		{
			if (slot.is_useless())
			{
				find_useless = true;
				++count;
				const double uselessness = slot.uselessness();
				std::cout << "hand_index " << index << " 'useless' (uselessness " << uselessness * 100. << "%)" << std::endl;
				if (uselessness > max_uselessness)
				{
					//std::cout << "This is the best chose for now" << std::endl;
					max_uselessness = uselessness;
					max_index = index;
				}
			}
			++index;
		}
		if (find_useless)
		{
			std::cout << count << " cards detected" << std::endl;
			const std::size_t hand_index = max_index;

			const auto& slot = slots.at(hand_index);
			std::cout << std::endl;
			std::cout << "Chosen action: drop card with index #" << hand_index << std::endl;
			this->display_slot(slot, hand_index);

			return std::shared_ptr<hanabi::common::CDrop>(new hanabi::common::CDrop(hand_index));
		}
		else
		{
			std::cout << "No card detected" << std::endl;
		}
	}
	else
	{
		std::cout << "Action not allowed" << std::endl;
	}

	return std::shared_ptr<hanabi::common::IAction>();
}

std::shared_ptr<hanabi::common::IAction> CTree1AI::drop_least_critical(const std::vector<SlotProbability>& slots) const
{
	std::cout << "Drop least critical..." << std::endl;
	if (this->is_allowed_to_drop())
	{
		bool find_not_critical = false;
		double min_criticality = 1.;
		double max_uselessness = 0.;
		std::size_t min_index = 0;
		std::size_t index = 0;
		std::size_t count = 0;
		for (const auto& slot : slots)
		{
			if (slot.is_not_critical())
			{
				find_not_critical = true;
				++count;
				const double criticality = slot.criticality();
				const double uselessness = slot.uselessness();
				std::cout << "hand_index " << index << " 'not critical' (criticality " << criticality * 100. << "% uselessness " << uselessness * 100. << "%)" << std::endl;

				if (criticality <= min_criticality)
				{
					min_criticality = criticality;
					min_index = index;
				}
				else if (std::abs(criticality - min_criticality) < 1.e-3)
				{
					// The criticality is equal to the current minimum
					// Let's compare the uselessness...
					if (uselessness > max_uselessness)
					{
						max_uselessness = uselessness;
						min_index = index;
					}
				}
			}
			++index;
		}
		if (find_not_critical)
		{
			std::cout << count << " cards detected" << std::endl;
			const std::size_t hand_index = min_index;

			const auto& slot = slots.at(hand_index);
			std::cout << std::endl;
			std::cout << "Chosen action: drop card with index #" << hand_index << std::endl;
			this->display_slot(slot, hand_index);

			return std::shared_ptr<hanabi::common::CDrop>(new hanabi::common::CDrop(hand_index));
		}
		else
		{
			std::cout << "No card detected" << std::endl;
		}
	}
	else
	{
		std::cout << "Action not allowed" << std::endl;
	}

	return std::shared_ptr<hanabi::common::IAction>();
}

std::shared_ptr<hanabi::common::IAction> CTree1AI::drop_no_info(const std::vector<SlotProbability>& slots) const
{
	std::cout << "Drop if no info..." << std::endl;
	if (this->is_allowed_to_drop())
	{
		// Loop on our hand infos
		hanabi::common::TIndex hand_index = 0;
		for (const auto& card_info : this->game_state().information_collection().information(this->my_id()))
		{
			const bool has_positive_value_info = card_info.positive_value() > 0;
			const bool has_positive_color_info = !card_info.positive_color().empty();

			if (!has_positive_value_info && !has_positive_color_info)
			{
				// There is no info about this card
				std::cout << "There is no info about the card #" << hand_index << " in my hand" << std::endl;
				std::cout << "Chosen action: drop card with index #" << hand_index << std::endl;
				const auto& slot = slots.at(hand_index);
				this->display_slot(slot, hand_index);
				std::shared_ptr<hanabi::common::CDrop> p_action(new hanabi::common::CDrop(hand_index));
				return p_action;
			}

			++hand_index;
		}
		std::cout << "No card detected" << std::endl;
	}
	else
	{
		std::cout << "Action not allowed" << std::endl;
	}

	return std::shared_ptr<hanabi::common::IAction>();
}

std::shared_ptr<hanabi::common::IAction> CTree1AI::speak_on_critical(const std::vector<SlotProbability>& slots) const
{
	std::cout << "Speak on critical..." << std::endl;
	if (this->is_allowed_to_speak())
	{
		// Loop on the other players, starting by our id
		for (hanabi::common::TIndex raw_player_id = 0; raw_player_id < this->game_state().game_option().player_count(); ++raw_player_id)
		{
			// The player id is corrected to count from our id
			const hanabi::common::TIndex player_id = (raw_player_id + this->my_id()) % this->game_state().game_option().player_count();

			// Skip our player id
			if (player_id == this->my_id())
				continue;

			std::cout << "Watching player #" << player_id << std::endl;

			const auto& other_hand = this->game_state().other_hands().hand(player_id);

			// Loop on the cards in the hand of other players
			std::size_t previous_no_info_card_count = 0;
			int hand_index = 0;
			for (const hanabi::common::CCard& card : other_hand)
			{
				const bool has_positive_value_info = this->game_state().information_collection().information(player_id).at(hand_index).positive_value() > 0;
				const bool has_positive_color_info = !this->game_state().information_collection().information(player_id).at(hand_index).positive_color().empty();

				if (is_card_critical(this->game_state().game_option(), this->game_state(), card.value(), card.color()))
				{
					std::cout << "Card #" << hand_index << " (" << card.value() << "-" << card.color() << ") is critical" << std::endl;
					std::cout << "Previous no info card count=" << previous_no_info_card_count << " (threshold=" << this->m_parameters.critical_speak_slot_range() << ")" << std::endl;

					// If there is no hurry to speak on this critical card
					if (previous_no_info_card_count > this->m_parameters.critical_speak_slot_range())
					{
						std::cout << "No hurry to speak on this critical card" << std::endl;
						++hand_index;
						continue;
					}

					// If there is no info about this card
					if (!has_positive_value_info && !has_positive_color_info)
					{
						// => give info on value
						std::cout << "There is no info => speak on value" << std::endl;
						std::cout << "Player #" << player_id << " has a critical card (" << card.value() << "-" << hanabi::common::card_color_to_string(card.color()) << ") at index #" << hand_index << " => speak on value" << std::endl;
						std::shared_ptr<hanabi::common::CSpeak> p_action(new hanabi::common::CSpeak(player_id));
						p_action->value_info(card.value());
						return p_action;
					}
					else
					{
						// There is already an info about this card
						// Respeak ?

						// Is there an info about value ?
						if (!has_positive_value_info)
						{
							std::cout << "No value info given... ";
							// Roll a dice for repseak
							std::uniform_int_distribution<std::size_t> dice(0, 100);
							const std::size_t dice_value = dice(*this->random_generator());
							const double percent = 100. * this->m_parameters.critical_respeak_probability();
							const bool respeak = dice_value < static_cast<int>(percent);
							std::cout << "Value respeak roll=" << dice_value << " (threshold=" << percent << ")" << std::endl;

							if (respeak)
							{
								// => give info on color
								std::cout << "Player #" << player_id << " has a critical card (" << card.value() << "-" << hanabi::common::card_color_to_string(card.color()) << ") at index #" << hand_index << " => respeak on value" << std::endl;
								std::shared_ptr<hanabi::common::CSpeak> p_action(new hanabi::common::CSpeak(player_id));
								p_action->value_info(card.value());
								return p_action;
							}
						}
						else
						{
							std::cout << "Value info already given" << std::endl;
						}

						// TODO Those lines will not work with multicolor or black
						// Is there an info about color ?
						if (!has_positive_color_info)
						{
							std::cout << "No color info given... ";
							// Roll a dice for repseak
							std::uniform_int_distribution<std::size_t> dice(0, 100);
							const std::size_t dice_value = dice(*this->random_generator());
							const double percent = 100. * this->m_parameters.critical_respeak_probability();
							const bool respeak = dice_value < static_cast<int>(percent);
							std::cout << "Color respeak roll=" << dice_value << " (threshold=" << percent << ")" << std::endl;

							if (respeak)
							{
								// => give info on color
								std::cout << "Player #" << player_id << " has a critical card (" << card.value() << "-" << hanabi::common::card_color_to_string(card.color()) << ") at index #" << hand_index << " => respeak on color" << std::endl;
								std::shared_ptr<hanabi::common::CSpeak> p_action(new hanabi::common::CSpeak(player_id));
								p_action->color_info(card.color());
								return p_action;
							}
						}
						else
						{
							std::cout << "Color info already given" << std::endl;
						}
					}
				}
				else
				{
					// The card is not a critical one...

					// If there is no info about this card
					if (!has_positive_value_info && !has_positive_color_info)
						++previous_no_info_card_count;
					else
						previous_no_info_card_count = 0;
				}
				++hand_index;
			}
		}
		std::cout << "No card detected" << std::endl;
	}
	else
	{
		std::cout << "Action not allowed" << std::endl;
	}
	return std::shared_ptr<hanabi::common::IAction>();
}

std::shared_ptr<hanabi::common::IAction> CTree1AI::speak_on_placeable(const std::vector<SlotProbability>& slots) const
{
	std::cout << "Speak on placeable..." << std::endl;
	if (this->is_allowed_to_speak())
	{
		// Loop on the other players, starting by our id
		for (hanabi::common::TIndex raw_player_id = 0; raw_player_id < this->game_state().game_option().player_count(); ++raw_player_id)
		{
			const hanabi::common::TIndex player_id = (raw_player_id + this->my_id()) % this->game_state().game_option().player_count();

			// Skip our player id
			if (player_id == this->my_id())
				continue;

			std::cout << "Watching player #" << player_id << std::endl;

			const auto& other_hand = this->game_state().other_hands().hand(player_id);

			// Loop on the cards in the hand
			hanabi::common::TIndex hand_index = 0;
			for (const hanabi::common::CCard& card : other_hand)
			{
				if (is_card_placeable(this->game_state().game_option(), this->game_state(), card.value(), card.color()))
				{
					std::cout << "Card #" << hand_index << " (" << card.value() << "-" << card.color() << ") is placeable" << std::endl;

					// Check if we have the same card in our hand (at least one positive info that leads to this card)
					// TODO Do not speak on a card we could have in the hand

					const bool has_positive_value_info = this->game_state().information_collection().information(player_id).at(hand_index).positive_value() > 0;
					const bool has_positive_color_info = !this->game_state().information_collection().information(player_id).at(hand_index).positive_color().empty();

					// If there is no info about this card
					if (!has_positive_value_info && !has_positive_color_info)
					{
						std::cout << "Player #" << player_id << " has a placeable card (" << card.value() << "-" << hanabi::common::card_color_to_string(card.color()) << ") at index #" << hand_index << std::endl;
						std::cout << "Checking for copy with info..." << std::endl;

						bool is_copy_with_info = false;

						for (hanabi::common::TIndex id = 0; id < this->game_state().game_option().player_count(); ++id)
						{
							if (!is_copy_with_info)
							{
								if (id != this->my_id())
								{
									// Avoid speaking on this card if an other player already carry a copy with informations
									
									// For the players other than me
									hanabi::common::TIndex index = 0;
									for (const hanabi::common::CCard& other_card : this->game_state().other_hands().hand(id))
									{
										// To not detect the same card
										if (player_id != id || hand_index != index)
										{
											// If this is the same card
											if (card.value() == other_card.value() && card.color() == other_card.color())
											{
												bool other_has_positive_value_info = this->game_state().information_collection().information(id).at(index).positive_value() > 0;
												bool other_has_positive_color_info = !this->game_state().information_collection().information(id).at(index).positive_color().empty();

												if (other_has_positive_value_info || other_has_positive_color_info)
												{
													is_copy_with_info = true;
													std::cout << "There is a copy with info in the hand of player #" << id << " at hand index " << index << std::endl;
													break;
												}
											}
										}
									}
								}
								else
								{
									// Avoid speaking on this card if we cound have it in the hand

									// For me...
									for (hanabi::common::TIndex index = 0; index < this->game_state().information_collection().information(id).size(); ++index)
									{
										// Is there info on the card in our hand ?
										bool my_card_has_positive_value_info = this->game_state().information_collection().information(id).at(index).positive_value() > 0;
										bool my_card_has_positive_color_info = !this->game_state().information_collection().information(id).at(index).positive_color().empty();

										if (my_card_has_positive_value_info || my_card_has_positive_color_info)
										{
											for (const auto& prob : slots.at(index).probabilities())
											{
												if (prob.value() == card.value() && prob.color() == card.color())
												{
													is_copy_with_info = true;
													std::cout << "I could have a copy in my hand at index " << index << std::endl;
													break;
												}
											}

											if (is_copy_with_info)
												break;
										}
									}
								}
							}
						}

						if (is_copy_with_info)
						{
							continue;
						}
						else
						{
							std::cout << "There is no copy with info => speak on value" << std::endl;
							std::shared_ptr<hanabi::common::CSpeak> p_action(new hanabi::common::CSpeak(player_id));
							p_action->value_info(card.value());
							return p_action;
						}
					}
					else
					{
						// There is already an info about this card
						// Respeak ?

						if (!has_positive_value_info)
						{
							std::cout << "No value info given... ";
							// Roll a dice for respeak on value
							std::uniform_int_distribution<std::size_t> dice(0, 100);
							const std::size_t dice_value = dice(*this->random_generator());
							const double percent = 100. * this->m_parameters.placeable_respeak_probability();
							const bool respeak = dice_value < static_cast<int>(percent);
							std::cout << "Value respeak roll=" << dice_value << " (threshold=" << percent << ")" << std::endl;

							if (respeak)
							{
								std::cout << "Player #" << player_id << " has a placeable card (" << card.value() << "-" << hanabi::common::card_color_to_string(card.color()) << ") at index #" << hand_index << " => respeak on value" << std::endl;
								std::shared_ptr<hanabi::common::CSpeak> p_action(new hanabi::common::CSpeak(player_id));
								p_action->value_info(card.value());
								return p_action;
							}
						}
						else
						{
							std::cout << "Value info already given" << std::endl;
						}

						// TODO Those lines will not work with multicolor or black
						// Is there an info about color ?
						if (!has_positive_color_info)
						{
							std::cout << "No color info given... ";
							// Roll a dice for respeak
							std::uniform_int_distribution<std::size_t> dice(0, 100);
							const std::size_t dice_value = dice(*this->random_generator());
							const double percent = 100. * this->m_parameters.placeable_respeak_probability();
							const bool respeak = dice_value < static_cast<int>(percent);
							std::cout << "Color respeak roll=" << dice_value << " (threshold=" << percent << ")" << std::endl;

							if (respeak)
							{
								// => give info on color

								std::cout << "Player #" << player_id << " has a placeable card (" << card.value() << "-" << hanabi::common::card_color_to_string(card.color()) << ") at index #" << hand_index << " => respeak on color" << std::endl;
								std::shared_ptr<hanabi::common::CSpeak> p_action(new hanabi::common::CSpeak(player_id));
								p_action->color_info(card.color());
								return p_action;
							}
						}
						else
						{
							std::cout << "Color info already given" << std::endl;
						}
					}
				}
				++hand_index;
			}
		}
		std::cout << "No card detected" << std::endl;
	}
	else
	{
		std::cout << "Action not allowed" << std::endl;
	}
	
	return std::shared_ptr<hanabi::common::IAction>();
}

std::shared_ptr<hanabi::common::IAction> CTree1AI::drop_copy(const std::vector<SlotProbability>& slots) const
{
	std::cout << "Drop copy..." << std::endl;
	if (this->is_allowed_to_drop())
	{
		for (std::size_t ref_index = 0; ref_index < slots.size(); ++ref_index)
		{
			const auto& ref_slot = slots.at(ref_index);
			if (ref_slot.probabilities().size() == 1)
			{
				// We know the color and the value of the card
				const auto& ref_card = ref_slot.probabilities().front();

				std::cout << "Reference is card #" << ref_index << " (" << ref_card.value() << "-" << hanabi::common::card_color_to_string(ref_card.color()) << ")" << std::endl;

				for (std::size_t test_index = ref_index; test_index < slots.size(); ++test_index)
				{
					if (test_index != ref_index)
					{
						// To not compare the card with itself

						const auto& test_slot = slots.at(test_index);

						if (test_slot.probabilities().size() == 1)
						{
							const auto& test_card = test_slot.probabilities().front();

							std::cout << "Test is card #" << test_index << " (" << test_card.value() << "-" << hanabi::common::card_color_to_string(test_card.color()) << ")" << std::endl;

							if (ref_card.is_equal(test_card))
							{
								std::cout << "> The cards are the same" << std::endl;
								return std::shared_ptr<hanabi::common::CDrop>(new hanabi::common::CDrop(test_index));
							}
							else
							{
								std::cout << "> The cards are different" << std::endl;
							}
						}
					}
				}
			}
		}
		std::cout << "No card detected" << std::endl;
	}
	else
	{
		std::cout << "Action not allowed" << std::endl;
	}

	return std::shared_ptr<hanabi::common::IAction>();
}