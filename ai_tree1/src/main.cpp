#include "pch.h"
#include "tree1.h"

using namespace hanabi;


int main(int argc, const char* argv[])
{
	try
	{
		BOOST_LOG_TRIVIAL(trace) << "Program starts";

		const std::string default_host = "ironknee.ovh";
		std::string host = default_host;

		const boost::asio::ip::port_type default_port = 32000;
		boost::asio::ip::port_type port = default_port;

		boost::program_options::options_description description("Allowed options");
		description.add_options()
			("help,h", "produce help message")
			("host", boost::program_options::value<std::string>(&host)->default_value(default_host), "IP address of the server")
			("port", boost::program_options::value<boost::asio::ip::port_type>(&port)->default_value(default_port), "Port of the server")
			;

		boost::program_options::variables_map variables_map;
		boost::program_options::store(boost::program_options::parse_command_line(argc, argv, description), variables_map);
		boost::program_options::notify(variables_map);

		if (variables_map.count("help"))
		{
			std::cout << description << std::endl;
			return 1;
		}

		// Version to log
		BOOST_LOG_TRIVIAL(info) << "Versions:";
		BOOST_LOG_TRIVIAL(info) << " - boost: " << common::to_string(common::to_version(BOOST_VERSION));
		BOOST_LOG_TRIVIAL(info) << " - Hanabi lib: " << common::to_string(common::to_version(HANABI_LIB_VERSION));

		// Command line to log
		std::ostringstream oss;
		for (int i = 0; i < argc; ++i)
		{
			if (i != 0)
				oss << " ";
			oss << argv[i];
		}
		BOOST_LOG_TRIVIAL(debug) << "Command line: " << oss.str();

		// Arguments to log
		BOOST_LOG_TRIVIAL(debug) << "Arguments: ";
		BOOST_LOG_TRIVIAL(debug) << " - host=" << host << " (default=" << default_host << ")";
		BOOST_LOG_TRIVIAL(debug) << " - port=" << port << " (default=" << default_port << ")";

		// Create client
		BOOST_LOG_TRIVIAL(info) << "Creating client...";
		BOOST_LOG_TRIVIAL(debug) << "Server : host=" << host << " port=" << port;

		boost::asio::io_service io_service;
		boost::asio::ip::tcp::resolver resolver(io_service);
		const boost::asio::ip::address host_address = resolver.resolve(host, "").begin()->endpoint().address();

		std::random_device random_device;
		std::shared_ptr<std::mt19937> p_random_generator = std::make_shared<std::mt19937>(random_device());

		CTree1AI ai(io_service, host_address, port, p_random_generator);
		ai.set_callback_on_receiving_action_accepted(std::bind(&CTree1AI::on_receiving_action_accepted, &ai));
		ai.set_callback_on_receiving_action_drop_update(std::bind(&CTree1AI::on_receiving_action_drop_update, &ai, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
		ai.set_callback_on_receiving_action_place_update(std::bind(&CTree1AI::on_receiving_action_place_update, &ai, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
		ai.set_callback_on_receiving_action_rejected(std::bind(&CTree1AI::on_receiving_action_rejected, &ai, std::placeholders::_1));
		ai.set_callback_on_receiving_action_speak_color_update(std::bind(&CTree1AI::on_receiving_action_speak_color_update, &ai, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));
		ai.set_callback_on_receiving_action_speak_value_update(std::bind(&CTree1AI::on_receiving_action_speak_value_update, &ai, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));
		ai.set_callback_on_receiving_bonus_event(std::bind(&CTree1AI::on_receiving_bonus_event, &ai, std::placeholders::_1, std::placeholders::_2));
		ai.set_callback_on_receiving_error_event(std::bind(&CTree1AI::on_receiving_error_event, &ai, std::placeholders::_1, std::placeholders::_2));
		ai.set_callback_on_receiving_game_end_update(std::bind(&CTree1AI::on_receiving_game_end_update, &ai, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
		ai.set_callback_on_receiving_game_option_update(std::bind(&CTree1AI::on_receiving_game_option_update, &ai, std::placeholders::_1));
		ai.set_callback_on_receiving_game_state_update(std::bind(&CTree1AI::on_receiving_game_state_update, &ai, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
		ai.set_callback_on_receiving_hello(std::bind(&CTree1AI::on_receiving_hello, &ai, std::placeholders::_1));
		ai.set_callback_on_receiving_peer_info_request(std::bind(&CTree1AI::on_receiving_peer_info_request, &ai, std::placeholders::_1));
		ai.set_callback_on_receiving_peer_info_update(std::bind(&CTree1AI::on_receiving_peer_info_update, &ai, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));

		ai.run();

		BOOST_LOG_TRIVIAL(trace) << "Program ends normally";
		return 0;
	}
	catch (std::exception& e)
	{
		BOOST_LOG_TRIVIAL(fatal) << "A standard exception has been catch.";
		BOOST_LOG_TRIVIAL(fatal) << e.what();
		return 1;
	}
	catch (...)
	{
		BOOST_LOG_TRIVIAL(fatal) << "An unknown error occured.";
		return 1;
	}
}