#include "pch.h"
#include "slotprobability.h"


SlotProbability::SlotProbability(const CAIParameters& params, double error_ratio) :
	m_propabilities(),
	m_is_critical_threshold(params.is_critical_threshold()),
	m_is_not_critical_threshold(params.is_not_critical_threshold()),
	m_is_placeable_threshold(params.is_placeable_threshold(error_ratio)),
	m_is_useless_threshold(params.is_useless_threshold()),
	m_is_not_useless_threshold(params.is_not_useless_threshold())
{

}

SlotProbability::SlotProbability(const SlotProbability& slot):
	m_propabilities(slot.probabilities()),
	m_is_critical_threshold(slot.is_critical_threshold()),
	m_is_not_critical_threshold(slot.is_not_critical_threshold()),
	m_is_placeable_threshold(slot.is_placeable_threshold()),
	m_is_useless_threshold(slot.is_useless_threshold()),
	m_is_not_useless_threshold(slot.is_not_useless_threshold())
{

}

SlotProbability::~SlotProbability() noexcept
{

}

SlotProbability& SlotProbability::operator=(const SlotProbability& slot)
{
	this->m_propabilities = slot.probabilities();
	this->m_is_critical_threshold = slot.is_critical_threshold();
	this->m_is_not_critical_threshold = slot.is_not_critical_threshold();
	this->m_is_placeable_threshold = slot.is_placeable_threshold();
	this->m_is_useless_threshold = slot.is_useless_threshold();
	this->m_is_not_useless_threshold = slot.is_not_useless_threshold();
	return *this;
}

const SlotProbability::TProbabilities& SlotProbability::probabilities() const noexcept
{
	return this->m_propabilities;
}

double SlotProbability::is_critical_threshold() const
{
	assert(this->m_is_critical_threshold >= 0.);
	assert(this->m_is_critical_threshold <= 1.);
	return this->m_is_critical_threshold;
}

double SlotProbability::is_not_critical_threshold() const
{
	assert(this->m_is_not_critical_threshold >= 0.);
	assert(this->m_is_not_critical_threshold <= 1.);
	return this->m_is_not_critical_threshold;
}

double SlotProbability::is_placeable_threshold() const
{
	assert(this->m_is_placeable_threshold >= 0.);
	assert(this->m_is_placeable_threshold <= 1.);
	return this->m_is_placeable_threshold;
}

double SlotProbability::is_useless_threshold() const
{
	assert(this->m_is_useless_threshold >= 0.);
	assert(this->m_is_useless_threshold <= 1.);
	return this->m_is_useless_threshold;
}

double SlotProbability::is_not_useless_threshold() const
{
	assert(this->m_is_not_useless_threshold >= 0.);
	assert(this->m_is_not_useless_threshold <= 1.);
	return this->m_is_not_useless_threshold;
}

bool SlotProbability::is_critical() const
{
	return this->criticality() >= this->is_critical_threshold();
}

bool SlotProbability::is_not_critical() const
{
	return this->criticality() <= this->is_not_critical_threshold();
}

bool SlotProbability::is_placeable() const
{
	return this->placeability() >= this->is_placeable_threshold();
}

bool SlotProbability::is_useless() const
{
	return this->uselessness() >= this->is_useless_threshold();
}

bool SlotProbability::is_not_useless() const
{
	return this->uselessness() <= this->is_useless_threshold();
}

double SlotProbability::criticality() const
{
	double cumul = 0.;

	for (const auto& card_proba : this->m_propabilities)
	{
		if (card_proba.is_critical())
		{
			cumul += card_proba.probability();
		}
	}

	assert(cumul >= 0.);
	assert(cumul <= 1.);
	return cumul;
}

double SlotProbability::placeability() const
{
	double cumul = 0.;

	for (const auto& card_proba : this->m_propabilities)
	{
		if (card_proba.is_placeable())
		{
			cumul += card_proba.probability();
		}
	}

	assert(cumul >= 0.);
	assert(cumul <= 1.);
	return cumul;
}

double SlotProbability::uselessness() const
{
	double cumul = 0.;

	for (const auto& card_proba : this->m_propabilities)
	{
		if (card_proba.is_useless())
		{
			cumul += card_proba.probability();
		}
	}

	assert(cumul >= 0.);
	assert(cumul <= 1.);
	return cumul;
}

void SlotProbability::compute_probabilities(const hanabi::client_side::CGameState& game_state, hanabi::common::TIndex my_player_id, hanabi::common::TIndex hand_index, const std::map<std::size_t, std::pair<hanabi::common::TValue, hanabi::common::CardColor>>& sure_in_my_hand)
{
	const hanabi::client_side::CGameOption& game_option = game_state.game_option();
	const hanabi::common::CCardRawInformation& card_info = game_state.information_collection().information(my_player_id)[hand_index];

	// List of the possible colors
	std::vector<hanabi::common::CardColor> possible_color;

	// Loop on the color present in the deck
	for (const std::pair<hanabi::common::CardColor, hanabi::common::CColorOption>& color_option : game_option.color_option())
	{
		// We only keep the colors left after:
		// - we removed the colors that reacts with negative color info
		// - we removed the colors that doesn't reacts with positive color info

		bool match_negative = false;
		for (const hanabi::common::CardColor negative_color : card_info.negative_color())
		{
			if (color_option.second.reacts_to().find(negative_color) != color_option.second.reacts_to().cend())
			{
				match_negative = true;
				break;
			}
		}
		if (match_negative)
			continue;

		// If we are here, the color doesn't match any negative color call

		bool match_positive = false;
		for (const hanabi::common::CardColor positive_color : card_info.positive_color())
		{
			if (color_option.second.reacts_to().find(positive_color) == color_option.second.reacts_to().cend())
			{
				match_positive = true;
				break;
			}
		}
		if (match_positive)
			continue;

		// If we are here, the color doesn't match any negative color call and match positive color call
		possible_color.push_back(color_option.first);
	}

	// Loop on the colors found earlier
	for (const auto& color : possible_color)
	{
		// Get the min/max value for this color
		const auto& distribution = game_option.color_option().at(color).distribution();
		const hanabi::common::TValue min_value = distribution.cbegin()->first;
		const hanabi::common::TValue max_value = distribution.crbegin()->first;

		for (hanabi::common::TValue value = min_value; value <= max_value; ++value)
		{
			// Test against positive value call
			if (card_info.positive_value() > 0 && value != card_info.positive_value())
				continue;

			// Is the candidate value not in negative value ?
			if (card_info.negative_value().find(value) != card_info.negative_value().cend())
				continue;

			// The pair (color, value) is a candidate

			// Get the count number of this card according to the distribution
			const hanabi::common::TCount distribution_count = distribution.at(value);
			hanabi::common::TCount left_count = distribution_count;

			// Check against the board
			if (is_in_board(game_option, game_state, value, color))
				--left_count;

			// Check against the trash
			const hanabi::common::TCount trash_count = count_in_trash(game_state, value, color);
			left_count -= trash_count;

			// Check against the other player hands
			hanabi::common::TCount players_hands_count = 0;
			for (const auto& player_hand : game_state.other_hands().other_hands())
			{
				for (const hanabi::common::CCard& player_card : player_hand.second)
				{
					if (player_card.value() == value && player_card.color() == color)
						++players_hands_count;
				}
			}
			left_count -= players_hands_count;

			// Check against the cards that are in my hand for sure
			for (const auto& in_my_hand : sure_in_my_hand)
			{
				const std::size_t slot_index = in_my_hand.first;
				if (slot_index != hand_index)
				{
					const auto& card = in_my_hand.second;
					if (card.first == value && card.second == color)
						--left_count;
				}
			}

			// If this is still a possiblity...
			if (left_count > 0)
			{
				// Create the probability
				const double probability = static_cast<double>(left_count);
				CardProbability prob(value, color, probability, game_option, game_state);
				this->m_propabilities.push_back(prob);
			}
		}
	}

	// We need to normalize the result
	double sum = 0.;
	for (auto& card_probability : this->m_propabilities)
		sum += card_probability.probability();
	for (auto& card_probability : this->m_propabilities)
		card_probability.probability(card_probability.probability() / sum);

	// Sort according to probability value
	this->m_propabilities.sort([](const CardProbability& left, const CardProbability& right) { return left.probability() > right.probability(); });
}
