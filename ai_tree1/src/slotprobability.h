#ifndef SLOTPROBABILITY_H_BDAF5C9E_869F_4B6B_9E7B_B98C639CA0F1
#define SLOTPROBABILITY_H_BDAF5C9E_869F_4B6B_9E7B_B98C639CA0F1
#pragma once

#include "aiparameters.h"
#include "cardprobability.h"


class SlotProbability
{
public:
	typedef std::list<CardProbability> TProbabilities;


	SlotProbability(const CAIParameters& params, double error_ratio);
	SlotProbability(const SlotProbability& slot);
	virtual ~SlotProbability() noexcept;

	SlotProbability& operator=(const SlotProbability& slot);


	const TProbabilities& probabilities() const noexcept;

	double is_critical_threshold() const;
	double is_not_critical_threshold() const;
	double is_placeable_threshold() const;
	double is_useless_threshold() const;
	double is_not_useless_threshold() const;

	bool is_critical() const;
	bool is_not_critical() const;
	bool is_placeable() const;
	bool is_useless() const;
	bool is_not_useless() const;
	
	double criticality() const;
	double placeability() const;
	double uselessness() const;

	void compute_probabilities(const hanabi::client_side::CGameState& game_state, hanabi::common::TIndex my_player_id, hanabi::common::TIndex hand_index, const std::map<std::size_t, std::pair<hanabi::common::TValue, hanabi::common::CardColor>>& sure_in_my_hand);

private:
	TProbabilities m_propabilities;

	double m_is_critical_threshold;
	double m_is_not_critical_threshold;
	double m_is_placeable_threshold;
	double m_is_useless_threshold;
	double m_is_not_useless_threshold;
};

#endif