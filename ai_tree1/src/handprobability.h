#ifndef HAND_PROBABILITY_H_F5D85707_B12F_42C8_9AA3_3B34E42B76FF
#define HAND_PROBABILITY_H_F5D85707_B12F_42C8_9AA3_3B34E42B76FF
#pragma once

#include "aiparameters.h"
#include "slotprobability.h"


class CHandProbability
{
public:
	typedef std::vector<SlotProbability> TSlots;

	CHandProbability();
	virtual ~CHandProbability() noexcept;


	const TSlots& slots() const;

	void compute_probabilities(const hanabi::client_side::CGameState& game_state, hanabi::common::TIndex my_id, const CAIParameters& ai_params);

private:
	bool is_probability_count_changed(const std::vector<std::size_t> lhs, const std::vector<std::size_t> rhs) const;

	const std::size_t m_max_iteration;
	TSlots m_slots;
};

#endif