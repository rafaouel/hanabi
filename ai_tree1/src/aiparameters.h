#ifndef AIPARAMETERS_H_73A63262_D078_4C06_8FD4_62F7C80ACBE0
#define AIPARAMETERS_H_73A63262_D078_4C06_8FD4_62F7C80ACBE0
#pragma once

/// <summary>
/// AI Parameters class
/// wait: [bool] true if the AI wait before computing its action
/// waiting_duration: [std::size_t] Number of ms the AI wait before computing its action. Not used if 'wait' is false
/// placeable_threshold: [double] If the placeability is greater or equal than this value, is_placeable will be true (and if the placeability is lower or equal than this value, is_not_placeable will be true
/// is_critical_threshold: [double] If the criticality is greater or equal than this value, is_critial will be true
/// is_not_critical_threshold: [double] If the criticality is lower or equal than this value, is_not_critical will be true
/// is_useless_threshold: [double] If the uselessness is greater or equal than this value, is_useless will be true
/// is_not_useless_threshold: [double] If the uselessness is lower or equal than this value, is_not_useless will be true
/// critical_respeak_probability: [double] If a dice is lower or equal than this value, the AI will respeak on a critical card
/// placeable_respeak_probability: [double] If a dice is lower or equal than this value, the AI will respeak on a placeable card
/// random_action_drop_weight: [std::size_t] Number of drop action inserted in the hat before choosing the action at random
/// random_action_place_weight: [std::size_t] Number of place action inserted in the hat before choosing the action at random
/// random_action_speak_weigth: [std::size_t] Number of speak action inserted in the hat befire choosing the action at random
/// max_number_of_displayed_card: [std::size_t] Number of card displayed when listing probabilities
/// critical_speak_slot_range: [std::size_t] Maximal number of slot without positive info could be on the left of a critical card before speaking about it
/// </summary>
class CAIParameters
{
public:
	CAIParameters();
	virtual ~CAIParameters() noexcept;


	bool wait() const noexcept;
	void wait(bool wait);

	std::size_t waiting_duration() const noexcept;
	void waiting_duration(std::size_t duration);

	double is_placeable_threshold(double error_ratio) const noexcept;
	void is_placeable_threshold(double threshold_min_error, double threshold_max_error);

	double is_critical_threshold() const noexcept;
	void is_critical_threshold(double threshold);

	double is_not_critical_threshold() const noexcept;
	void is_not_critical_threshold(double threshold);

	double is_useless_threshold() const noexcept;
	void is_useless_threshold(double threshold);

	double is_not_useless_threshold() const noexcept;
	void is_not_useless_threshold(double threshold);

	double critical_respeak_probability() const noexcept;
	void critical_respeak_probability(double probability);

	double placeable_respeak_probability() const noexcept;
	void placeable_respeak_probability(double probability);

	std::size_t random_action_drop_weight() const noexcept;
	void random_action_drop_weight(std::size_t weight);

	std::size_t random_action_place_weight() const noexcept;
	void random_action_place_weight(std::size_t weight);

	std::size_t random_action_speak_weight() const noexcept;
	void random_action_speak_weight(std::size_t weight);

	int max_number_of_displayed_card() const noexcept;
	void max_number_of_displayed_card(int number_of_card);

	std::size_t critical_speak_slot_range() const noexcept;
	void critical_speak_slot_range(std::size_t slot_count);


private:
	bool m_wait;
	std::size_t m_waiting_duration;
	double m_placeable_threshold_min_error;
	double m_placeable_threshold_max_error;
	double m_is_critical_threshold;
	double m_is_useless_threshold;
	double m_is_not_critical_threshold;
	double m_is_not_useless_threshold;
	double m_critical_respeak_probability;
	double m_placeable_respeak_probability;
	std::size_t m_random_action_drop_weight;
	std::size_t m_random_action_place_weight;
	std::size_t m_random_action_speak_weight;
	int m_max_number_of_displayed_card;
	std::size_t m_critical_speak_slot_range;
};


#endif