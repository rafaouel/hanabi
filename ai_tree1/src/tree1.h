#ifndef RANDOMAI_H_142460DD_88E2_4443_9DD6_B521560CDD55
#define RANDOMAI_H_142460DD_88E2_4443_9DD6_B521560CDD55
#pragma once

#include "aiparameters.h"
#include "handprobability.h"
//#include "slotprobability.h"


class CTree1AI : public hanabi::client_side::IGameClient
{
public:
	CTree1AI(boost::asio::io_service& io_service, const boost::asio::ip::address& server_address, const boost::asio::ip::port_type& server_port, const std::shared_ptr<std::mt19937>& p_random_generator);
	virtual ~CTree1AI() noexcept;


	hanabi::common::TVersionArray version_array() const noexcept;

	const std::shared_ptr<std::mt19937>& random_generator() const noexcept;

	void on_receiving_hello(const std::string& server_hello_message) const;
	void on_receiving_game_end_update(const hanabi::client_side::CGameEnd& game_end, const hanabi::client_side::CGameState& last_game_state, const hanabi::client_side::TPeerInfoMap& peer_info) ;
	void on_receiving_game_state_update(const hanabi::client_side::CGameState& game_state, hanabi::common::TIndex my_id, const hanabi::client_side::TPeerInfoMap& peer_info);
	void on_receiving_game_option_update(const hanabi::client_side::CGameOption& game_option);
	void on_receiving_action_speak_value_update(hanabi::common::TIndex source_player_id, hanabi::common::TIndex target_player_id, const hanabi::client_side::TPeerInfoMap& peer_info, hanabi::common::TValue value_info) const;
	void on_receiving_action_speak_color_update(hanabi::common::TIndex source_player_id, hanabi::common::TIndex target_player_id, const hanabi::client_side::TPeerInfoMap& peer_info, hanabi::common::CardColor color_info) const;
	void on_receiving_action_drop_update(hanabi::common::TIndex source_player_id, const hanabi::client_side::TPeerInfoMap& peer_info, const hanabi::common::CCard& card) const;
	void on_receiving_action_place_update(hanabi::common::TIndex source_player_id, const hanabi::client_side::TPeerInfoMap& peer_info, const hanabi::common::CCard& card) const;
	void on_receiving_peer_info_request(const std::string& server_version) const;
	void on_receiving_peer_info_update(hanabi::common::TIndex new_peer_id, const hanabi::client_side::CPeerInfo& new_peer, const hanabi::client_side::TPeerInfoMap& peer_info) const;
	void on_receiving_action_accepted() const;
	void on_receiving_action_rejected(const std::string& reason) const;
	void on_receiving_bonus_event(hanabi::common::TIndex current_player_id, const hanabi::common::CardColor& color) const;
	void on_receiving_error_event(hanabi::common::TIndex current_player_id, hanabi::common::TCount error_token_count) const;

private:
	virtual std::shared_ptr<hanabi::common::IAction> process_action_request(const hanabi::client_side::CGameState& game_state, const hanabi::client_side::TPeerInfoMap& peer_info, hanabi::common::TIndex my_id);
	virtual hanabi::client_side::CPeerInfoResponse process_peer_info_request(hanabi::common::TIndex my_id, const std::string server_verison);

	void display_slot(const SlotProbability& slot, std::size_t slot_index) const;

	bool is_allowed_to_drop() const;
	bool is_allowed_to_place() const;
	bool is_allowed_to_speak() const;

	std::shared_ptr<hanabi::common::IAction> place_placeable(const std::vector<SlotProbability>& slots) const;
	std::shared_ptr<hanabi::common::IAction> drop_useless(const std::vector<SlotProbability>& slots) const;
	std::shared_ptr<hanabi::common::IAction> drop_least_critical(const std::vector<SlotProbability>& slots) const;
	std::shared_ptr<hanabi::common::IAction> drop_no_info(const std::vector<SlotProbability>& slots) const;
	std::shared_ptr<hanabi::common::IAction> speak_on_critical(const std::vector<SlotProbability>& slots) const;
	std::shared_ptr<hanabi::common::IAction> speak_on_placeable(const std::vector<SlotProbability>& slots) const;
	std::shared_ptr<hanabi::common::IAction> drop_copy(const std::vector<SlotProbability>& slots) const;

	const std::shared_ptr<std::mt19937> m_p_random_generator;

	CAIParameters m_parameters;
	CHandProbability m_hand_probability;
};

#endif 