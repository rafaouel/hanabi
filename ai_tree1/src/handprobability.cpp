#include "pch.h"
#include "handprobability.h"

CHandProbability::CHandProbability() :
	m_max_iteration(10),
	m_slots()
{

}

CHandProbability::~CHandProbability() noexcept
{

}

const CHandProbability::TSlots& CHandProbability::slots() const
{
	return this->m_slots;
}

void CHandProbability::compute_probabilities(const hanabi::client_side::CGameState& game_state, hanabi::common::TIndex my_id, const CAIParameters& ai_params)
{
	std::cout << "Start probability computation..." << std::endl;

	const double error_ratio = static_cast<double>(game_state.error_token_count()) / static_cast<double>(game_state.game_option().max_error_token_count() - 1);
	const std::size_t slot_number = game_state.information_collection().information(my_id).size();
	std::vector<std::size_t> slot_probability_size(slot_number);
	TSlots slots;
	std::vector<std::size_t> slot_probability_size_save(slot_number);
	this->m_slots.clear();
	std::map<std::size_t, std::pair<hanabi::common::TValue, hanabi::common::CardColor>> sure_in_my_hand;

	// Prepare for computationn
	for (std::size_t slot_index = 0; slot_index < slot_number; ++slot_index)
	{
		slot_probability_size.at(slot_index) = 0;
		slot_probability_size_save.at(slot_index) = 0;
	}

	std::size_t iteration_count = 0;
	for (std::size_t iteration_count = 0; iteration_count < this->m_max_iteration; ++iteration_count)
	{
		std::cout << "Iteration #" << iteration_count << std::endl;

		// Compute slots probabilities
		std::cout << "Computing slots... ";
		for (std::size_t slot_index = 0; slot_index < slot_number; ++slot_index)
		{
			std::cout << "#" << slot_index << " ";

			SlotProbability slot(ai_params, error_ratio);

			slot.compute_probabilities(game_state, my_id, slot_index, sure_in_my_hand);

			slots.push_back(slot);

			// Record the size of the probability
			slot_probability_size.at(slot_index) = slot.probabilities().size();
		}
		std::cout << std::endl;

		// Build the list of the card we are sure is in our hand
		for (std::size_t slot_index = 0; slot_index < slot_number; ++slot_index)
		{
			if (slots.at(slot_index).probabilities().size() == 1)
			{
				const auto& card = slots.at(slot_index).probabilities().front();
				sure_in_my_hand[slot_index] = std::pair(card.value(), card.color());
			}
		}

		if (this->is_probability_count_changed(slot_probability_size, slot_probability_size_save))
		{
			std::cout << "Not converged" << std::endl;

			// Save...
			this->m_slots = slots;
			slots.clear();
			slot_probability_size_save = slot_probability_size;

			// ... and re-run
			continue;
		}
		else
		{
			std::cout << "Converged" << std::endl;
			this->m_slots = slots;
			return;
		}
	}

	if (iteration_count >= this->m_max_iteration)
		throw std::exception("Max probability computation count reached");
}

bool CHandProbability::is_probability_count_changed(const std::vector<std::size_t> lhs, const std::vector<std::size_t> rhs) const
{
	assert(lhs.size() == rhs.size());

	const std::size_t slot_number = lhs.size();

	for (std::size_t slot_index = 0; slot_index < slot_number; ++slot_index)
	{
		if (lhs.at(slot_index) != rhs.at(slot_index))
			return true;
	}

	return false;
}


