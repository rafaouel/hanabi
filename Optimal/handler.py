import errors
from multiprocessing import context
import apigateway
from http.server import BaseHTTPRequestHandler
import gamedata.gameoption as gameoption
import json
from urllib.parse import urlparse, parse_qs
import os
import solver


class Handler(BaseHTTPRequestHandler):

    def __send_cors_headers(self):
        self.send_header("Access-Control-Allow-Origin", "*")
        self.send_header("Access-Control-Allow-Methods", "GET,POST,OPTIONS")
        self.send_header("Access-Control-Allow-Headers", "x-api-key,Content-Type")

    def do_OPTIONS(self):
        self.send_response(200)
        self.__send_cors_headers()
        self.end_headers()

    def do_GET(self):
        try:
            parsed = urlparse(self.path)
            path = parsed.path

            if path == "/api/v1/liveness-probe":
                self.__liveness_probe()
                return

            if path == "/api/v1/readiness-probe":
                self.__readiness_probe()
                return

            if path == "/api/v1/startup-probe":
                self.__startup_probe()
                return

            if path == "/api/v1/submit":
                query_string = parse_qs(parsed.query)
                game_option_identifier_string = "gameOptionId"
                show_actions_identifier_string = "showActions"

                # game option id should be there
                if not game_option_identifier_string in query_string:
                    raise errors.ClientError(400, "'{}' not found in query string".format(game_option_identifier_string))
                
                game_option_id = query_string[game_option_identifier_string]
                if len(game_option_id) != 1:
                    raise errors.ClientError(400, "'{}' should be sent once".format(game_option_identifier_string))
                game_option_id = game_option_id[0]

                self.__submit(game_option_id)
                return

            self.__path_not_found()
            return

        except errors.ClientError as error:
            self.send_response(error.status_code())
            self.__send_cors_headers()
            self.send_header('Content-type', 'application/json')
            self.end_headers()
            body = {
                "message": error.message(),
                "statusCode": error.status_code()
                }
            str_body = json.dumps(body)
            self.wfile.write(bytes(str_body, "utf-8"))
            return

        except Exception as error:
            self.send_response(500)
            self.__send_cors_headers()
            self.send_header('Content-type', 'application/json')
            self.end_headers()
            body = {
                "message": str(error),
                "statusCode": 500
                }
            str_body = json.dumps(body)
            self.wfile.write(bytes(str_body, "utf-8"))
            return

        except:
            self.send_response(500)
            self.__send_cors_headers()
            self.end_headers()
            return


    def __path_not_found(self):
        self.send_response(404)
        self.__send_cors_headers()
        self.end_headers()
        return


    def __liveness_probe(self):
        self.send_response(200)
        self.__send_cors_headers()
        self.end_headers()
        return


    def __readiness_probe(self):
                
        returned_status_code = 503
        api_status = "ko"

        # Create the API Gateway
        context = apigateway.ApiGatewayContext()
        gateway = context.get_api_gateway()

        is_api_ready = gateway.readiness_probe()
        if is_api_ready:
            returned_status_code = 200
            api_status = "ok"

        body = {
            "apiStatus": api_status
            }

        self.send_response(returned_status_code)
        self.__send_cors_headers()
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        str_body = json.dumps(body)
        self.wfile.write(bytes(str_body, "utf-8"))
        return


    def __startup_probe(self):
        self.send_response(200)
        self.__send_cors_headers()
        self.end_headers()
        return


    def __submit(self, game_option_id):

        try:        
            
            print_debug = False
            if "PRINT_DETAILS" in os.environ:
                print_debug = True

            # Create the API Gateway
            context = apigateway.ApiGatewayContext()
            gateway = context.get_api_gateway()

            # Get a Game Option
            result = gateway.get_game_options(id=game_option_id)

            if len(result["data"]) != 1:
                raise errors.ClientError(404, "Unable to find the Game Option '{}'".format(game_option_id))

            game_option_data = result["data"][0]
            game_option = gameoption.GameOption(game_option_data)

            # Create the solver
            game_solver = solver.Solver(game_option)

            (score, player_turn_count, action_list) = game_solver.solve(print_details=print_debug)
            initial_max_score = game_option.max_score()

            if print_debug:
                print("The solver found a solution for the game '{}' in {} turns, scoring {}/{}\n".format(game_option.game_option_id(), player_turn_count, score, initial_max_score))

            body = {
                "playerTurnCount": player_turn_count,
                "gameTurnCount": (player_turn_count // game_option.player_count()),
                "score": score,
                "initialMaxScore": initial_max_score,
                "actionArray": action_list
            }            

            self.send_response(200)
            self.__send_cors_headers()
            self.send_header('Content-type', 'application/json')
            self.end_headers()
            str_body = json.dumps(body)
            self.wfile.write(bytes(str_body, "utf-8"))
            return
        
        except errors.ClientError as error:
            self.send_response(error.status_code())
            self.__send_cors_headers()
            self.send_header('Content-type', 'application/json')
            self.end_headers()
            body = {
                "message": error.message(),
                "statusCode": error.status_code()
                }
            str_body = json.dumps(body)
            self.wfile.write(bytes(str_body, "utf-8"))
            return

        except Exception as error:
            self.send_response(500)
            self.__send_cors_headers()
            self.send_header('Content-type', 'application/json')
            self.end_headers()
            body = {
                "message": str(error),
                "statusCode": 500
                }
            str_body = json.dumps(body)
            self.wfile.write(bytes(str_body, "utf-8"))
            return

        except:
            self.send_response(500)
            self.__send_cors_headers()
            self.end_headers()
            return
