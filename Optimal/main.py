optimal_version = "2.2.2"


if __name__ == "__main__":

    try:
        import os
        import http.server

        import apigateway
        import handler
        
        if not "API_HOST" in os.environ:
            raise Exception("Please define 'API_HOST'")
        host = os.environ["API_HOST"]

        if not "API_PORT" in os.environ:
            raise Exception("Please define 'API_PORT'")
        port = os.environ["API_PORT"]

        if not "API_LOGIN" in os.environ:
            raise Exception("Please define 'API_LOGIN'")
        login = os.environ["API_LOGIN"]

        if not "API_PASSWORD" in os.environ:
            raise Exception("Please define 'API_PASSWORD'")
        password = os.environ["API_PASSWORD"]

        if not "MY_API_PORT" in os.environ:
            raise Exception("Please define 'MY_API_PORT'")
        my_api_port = os.environ["MY_API_PORT"]

        context = apigateway.ApiGatewayContext()
        context.init(host, port, login, password)

        httpd = http.server.HTTPServer(("0.0.0.0", int(my_api_port)), handler.Handler)

        print("Optimal version =", optimal_version)
        print("The server is reachable at http://0.0.0.0:{}".format(my_api_port))

        httpd.serve_forever()

        
    except Exception as error:
        print(error)

    except:
        print("Unknown error")
