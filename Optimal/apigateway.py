import errors

import datetime
import requests


class BearerAuth:
    def __init__(self, token):
        self.__token = token

    def __call__(self, r):
        r.headers["authorization"] = "Bearer " + self.__token
        return r


class ApiGatewayContext:
    
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(ApiGatewayContext, cls).__new__(cls)
        return cls.instance

    def init(self, host, port, login, password):
        self.__api_gateway = ApiGateway(host, port, login, password)

    def get_api_gateway(self):
        return self.__api_gateway



class ApiGateway:
    def __init__(self, host, port, login, password):
        self.__host = host
        self.__port = port
        self.__login = login
        self.__password = password
        self.__access_token = None
        self.__access_token_expiration_date = datetime.datetime.min
        self.__refresh_token = None
        self.__refresh_token_expiration_date = datetime.datetime.min
        self.__expiration_threshold = datetime.timedelta(days=0, seconds=300)
        self.__date_format = "%Y-%m-%dT%H:%M:%S.%f%z"
        self.__timeout = 1

    def str_2_datetime(self, str):
        return datetime.datetime.strptime(str, self.__date_format)

    def __is_expired(self, date):
        now = datetime.datetime.now(datetime.timezone.utc)
        return date < now + self.__expiration_threshold

    def __get_access_token(self):

        # Is there an access token ?
        if self.__access_token:
            # There is an access token
            # Is it expired ?
            if not self.__is_expired(self.__access_token_expiration_date):
                # The acces token is not expired
                return self.__access_token
        
        # There is no access token or it is expired...
        # Is there a refresh token ?
        if self.__refresh_token:
            # There is a refresh token
            # Is it expired ?
            if not self.__is_expired(self.__refresh_token_expiration_date):
                # The refresh token is not expired...

                # Ask for a pair of token
                url = "http://{}:{}/api/v1/tokens/".format(self.__host, self.__port)
                response = requests.put(url, auth=BearerAuth(self.__refresh_token), timeout=self.__timeout)

                if response.status_code == 201:
                    json = response.json()
                    self.__access_token = json["accessToken"]
                    self.__access_token_expiration_date = self.str_2_datetime(json["accessTokenExpirationDate"])
                    self.__refresh_token = json["refreshToken"]
                    self.__refresh_token_expiration_date = self.str_2_datetime(json["refreshTokenExpirationDate"])
                    return self.__access_token
                else:
                    raise errors.ServerError(500, "Unable to retrieve an access token (refreshing)")

        # There is no refresh token or it is expired...
        # Ask for a pair of token
        url = "http://{}:{}/api/v1/tokens/".format(self.__host, self.__port)
        headers = {"Accept":"*/*"}
        response = requests.post(url, auth=(self.__login, self.__password), headers=headers, timeout=self.__timeout)

        if response.status_code == 201:
            json = response.json()
            self.__access_token = json["accessToken"]
            self.__access_token_expiration_date = self.str_2_datetime(json["accessTokenExpirationDate"])
            self.__refresh_token = json["refreshToken"]
            self.__refresh_token_expiration_date = self.str_2_datetime(json["refreshTokenExpirationDate"])
            return self.__access_token
        else:
            raise errors.ServerError(500, "Unable to retrieve an access token (from stratch)")


    def check_access_token(self):

        if self.__access_token == None:
            raise Exception("No access token available")

        now = datetime.datetime.now(datetime.timezone.utc)

        if self.__is_expired(self.__access_token_expiration_date):
            raise Exception("Expired access token")

        url = "http://{}:{}/api/v1/tokens/".format(self.__host, self.__port)

        response = requests.get(url, auth=("Bearer {}".format(self.__access_token)), timeout=self.__timeout)

        return response.ok


    def liveness_probe(self):

        url = "http://{}:{}/api/v1/healthcheck/liveness-probe/".format(self.__host, self.__port)

        response = requests.get(url, timeout=self.__timeout)

        return response.ok

    def readiness_probe(self):

        url = "http://{}:{}/api/v1/healthcheck/readiness-probe/".format(self.__host, self.__port)

        response = requests.get(url, timeout=self.__timeout)

        return response.ok


    def get_game_options(self, id = None, page = 0):

        url = "http://{}:{}/api/v2/hanabi/gameOption".format(self.__host, self.__port)

        url += "?page={}".format(page)

        if id:
            url += "&gameOptionId={}".format(id)

        response = requests.get(url, auth=BearerAuth(self.__get_access_token()), timeout=self.__timeout)

        if not response.ok:
            raise Exception("Unable to retrieve game option")

        json = response.json()

        return json


if __name__ == "__main__":

    try:
        host = "localhost"
        port = "3000"
        login = "game_server"
        password = "IAmthEmAstEr"

        # Gateway creation
        api_gateway_context = ApiGatewayContext()
        api_gateway_context.init(host, port, login, password)
        gateway = api_gateway_context.get_api_gateway()

        # Liveness probe
        print("liveness:", "ok" if gateway.liveness_probe() else "ko")

        # Count Game Options
        print(gateway.get_game_options()["metadata"]["documentCount"])

        # And a second time (should reuse the token)
        print(gateway.get_game_options()["metadata"]["documentCount"])

        # Get the game option associated with the first item of the list
        game_option_id_list = []
        page = 0
        result = gateway.get_game_options(page=page)
        game_option = result["data"][0]
        print("game option id: {}".format(game_option["gameOptionId"]))

    except Exception as error:
        print(error)
    except:
        print("Unknow error")