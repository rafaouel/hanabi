

class ClientError(Exception):
    def __init__(self, status_code, message):
        Exception.__init__(self, message)
        self.__status_code = status_code
        self.__message = message

    def status_code(self):
        return self.__status_code

    def message(self):
        return self.__message


class ServerError(Exception):
    def __init__(self, status_code, message):
        Exception.__init__(self, message)
        self.__status_code = status_code
        self.__message = message

    def status_code(self):
        return self.__status_code

    def message(self):
        return self.__message