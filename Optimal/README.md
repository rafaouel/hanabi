# OPTIMAL

This project is responsible for finding an optimal solution to a game knowing everything about the game:
 - The stack
 - The hand of the players
It should be able to determine if a game is winable and in the best conditions how many turns are needed to do so.

# Docker image

## Building

To build the docker image:

``` bash
# building image
docker build -f .\Dockerfile -t registry.gitlab.com/rafaouel/hanabi/optimal:<version> .
```

## Pushing

To push the image to the registry:

``` bash
# Login
docker login registry.gitlab.com/rafaouel/hanabi
# Enter the username then a project access token

# Push image
docker push registry.gitlab.com/rafaouel/hanabi/optimal:<version>
```

## Running

``` bash
docker run --rm -d --name optimal -p 3001:3001 -e API_HOST=localhost -e API_PORT=3000 -e API_LOGIN=Optimal -e API_PASSWORD=Optimal -e MY_API_PORT=3001 registry.gitlab.com/rafaouel/hanabi/optimal:<version>
```