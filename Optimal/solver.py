from pickle import FALSE
import gamedata.cards as gamedata

class Solver:

    def __init__(self, game_option):
        self.__game_option = game_option
        self.__board = None
        self.__trash = None
        self.__hands = []
        self.__stack = None


    def solve(self, print_details):

        if print_details:
            print()
            print("Solving game '{}'...".format(self.__game_option.game_option_id()))

        # Initialize the game
        self.__init_game()
        initial_max_score = self.__game_option.max_score()
        current_score = self.__board.current_score()
        current_max_score = self.__trash.current_max_score()
        speak_token_count = self.__game_option.start_speak_token_count()
        # No need for counting error because the player will never place an invalid card
        max_speak_token_count = self.__game_option.max_speak_token_count()

        # Run the solver
        action_list = []
        player_turn_count = 0
        end_of_the_game = False
        current_player_id = self.__game_option.start_player_id()
        while not end_of_the_game:

            if print_details:
                print()
                print("turn #{}".format(player_turn_count))
                print("score={cur}/{max}/{init}".format(cur=current_score, max=current_max_score, init=initial_max_score))
                print("speak={cur}/{max}".format(cur=speak_token_count, max=max_speak_token_count))
                print("player #{} playing...".format(current_player_id))

            cards_to_place = {}
            cards_to_drop = {}

            for index in range(self.__hands[current_player_id].count()):
                # For each card in the hand
                card = self.__hands[current_player_id].get(index)

                # Is it a card to place ?
                is_valid = self.__board.is_valid(card.value(), card.color())
                if is_valid:
                    cards_to_place[index] = card

                # Is it a card to drop ?
                is_critical = self.__trash.is_critical(card.value(), card.color())
                is_usefull = self.__board.is_usefull(card.value(), card.color())
                if not is_critical or not is_usefull:
                    cards_to_drop[index] = card

            if print_details:
                print("placeable={}/{}".format(len(cards_to_place), self.__hands[current_player_id].count()))
                print("dropable={}/{}".format(len(cards_to_drop), self.__hands[current_player_id].count()))

            # The current player is able to place a card ?
            if len(cards_to_place) > 0:
                # => Place the highest card

                highest_card_index = 0
                highest_value = self.__game_option.min_card_value() - 1
                for index, card in cards_to_place.items():
                    value = card.value()

                    if not self.__game_option.color_options()[card.color()].is_ascending():
                        value = self.__game_option.max_card_value() - value + 1

                    if value > highest_value:
                        highest_card_index = index
                        highest_value = value

                card_to_place = cards_to_place[highest_card_index]

                action_list.append({
                    "currentPlayerId" : current_player_id,
                    "actionType": "PLACE",
                    "handIndex": highest_card_index,
                    "card": { "value": card_to_place.value(), "color": card_to_place.color()}
                    })

                if print_details:
                    print("> placing {}-{}".format(card_to_place.value(), card_to_place.color()))

                # Remove the card from the hand
                self.__hands[current_player_id].remove(highest_card_index)
                # Put the card in the board
                self.__board.insert(card_to_place)
                # Draw a new card (if possible)
                if self.__stack.count() > 0:
                    drawn_card = self.__stack.draw_a_card()
                    if drawn_card != None:
                        self.__hands[current_player_id].insert(drawn_card)
                # Check for a bonus
                bonus_threshold = 2 # If there are more than 'bonus_threshold' speak token missing, the completion of a color give a bonus, otherwise not token is claimed
                not_too_much_speak = self.__speak_count < self.__game_option.max_speak_token_count() - bonus_threshold
                complete_a_color = False
                if self.__game_option.color_options()[card_to_place.color()].is_ascending():
                    complete_a_color = self.__game_option.max_card_value() == card_to_place.value()
                else:
                    complete_a_color = self.__game_option.min_card_value() == card_to_place.value()
                if not_too_much_speak and complete_a_color:
                    # Add a speak token
                    self.__speak_count += 1

            else:
                # The player is not able to place a card
                # The player is able to drop a card ?
                if len(cards_to_drop) > 0 and speak_token_count < self.__game_option.max_speak_token_count():
                    # => Drop the lowest card

                    lowest_card_index = 0
                    lowest_value = self.__game_option.max_card_value() + 1
                    for index, card in cards_to_drop.items():
                        value = card.value()

                        if not self.__game_option.color_options()[card.color()].is_ascending():
                            value = self.__game_option.max_card_value() - value + 1

                        if value < lowest_value:
                            lowest_card_index = index
                            lowest_value = value

                    card_to_drop = cards_to_drop[lowest_card_index]

                    action_list.append({
                        "currentPlayerId" : current_player_id,
                        "actionType": "DROP",
                        "handIndex": lowest_card_index,
                        "card": { "value": card_to_drop.value(), "color": card_to_drop.color()}
                        })

                    if print_details:
                        print("> droping {}-{}".format(card_to_drop.value(), card_to_drop.color()))

                    # Remove the card from the hand
                    self.__hands[current_player_id].remove(lowest_card_index)
                    # Put the card in the trash
                    self.__trash.insert(card_to_drop)
                    # Draw a new card (if possible)
                    if self.__stack.count() > 0:
                        drawn_card = self.__stack.draw_a_card()
                        if drawn_card != None:
                            self.__hands[current_player_id].insert(drawn_card)
                    # Increase the speak token count
                    speak_token_count += 1

                else:
                    # The player is not able to drop a card
                    
                    if speak_token_count > 0:
                        # The player is able to speak

                        speak_token_count -= 1

                        action_list.append({
                            "currentPlayerId" : current_player_id,
                            "actionType": "SPEAK"
                            })

                        if print_details:
                            print("> speaking")

                    else:
                        # The current player cannot place because there is no valid card...
                        # he cannot drop without decreasing the max score...
                        # he cannot speak because there is no speak token left...
                        # => One of the best solution is to drop the highest card

                        highest_card_index = 0
                        highest_value = 0
                        for index in range(self.__hands[current_player_id].count()):
                            # For each card in the hand
                            card = self.__hands[current_player_id].get(index)

                            value = card.value()

                            if not self.__game_option.color_options()[card.color()].is_ascending():
                                value = self.__game_option.max_card_value() - value + 1



                            if value > highest_value:
                                highest_value = value
                                highest_card_index = index

                        card_to_drop = self.__hands[current_player_id].get(highest_card_index)

                        action_list.append({
                            "currentPlayerId" : current_player_id,
                            "actionType": "DROP",
                            "handIndex": highest_card_index,
                            "card": { "value": card_to_drop.value(), "color": card_to_drop.color()}
                            })

                        if print_details:
                            print("> droping {}-{}".format(card_to_drop.value(), card_to_drop.color()))

                        # Remove the card from the hand
                        self.__hands[current_player_id].remove(highest_card_index)
                        # Put the card in the trash
                        self.__trash.insert(card_to_drop)
                        # Draw a new card (if possible)
                        if self.__stack.count() > 0:
                            drawn_card = self.__stack.draw_a_card()
                            if drawn_card != None:
                                self.__hands[current_player_id].insert(drawn_card)
                        # Increase the speak token count
                        speak_token_count += 1
                        
                
            # End of the game condition ?
            current_score = self.__board.current_score()
            current_max_score = self.__trash.current_max_score()

            # Test for victory condition
            if current_score == current_max_score:
                end_of_the_game = True
                continue

            # This is useless to test for lose conditions because the player will never place an invalid card

            # Change the current player
            current_player_id += 1
            current_player_id %= self.__game_option.player_count()

            # Increase the turn count
            player_turn_count += 1

        return current_score, player_turn_count, action_list


    def __init_game(self):
        
        # Create the components
        self.__stack = gamedata.Stack(self.__game_option.stack().card_list())
        self.__board = gamedata.Board(self.__game_option)
        self.__trash = gamedata.Trash(self.__game_option)
        self.__hands = []
        for _ in range(self.__game_option.player_count()):
            self.__hands.append(gamedata.Hand(self.__game_option))

        # Deal the cards
        for card_number in range(self.__game_option.player_hand_max_size()):
            for player_id in range(self.__game_option.player_count()):
                card = self.__stack.draw_a_card()
                if card != None:
                    self.__hands[player_id].insert(card)

        # Initialize the token counts
        self.__speak_count = self.__game_option.start_speak_token_count()
