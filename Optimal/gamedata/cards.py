class Card:
    def __init__(self, value, color):
        self.__value = value
        self.__color = color

    def color(self):
        return self.__color

    def value(self):
        return self.__value


class Stack:
    def __init__(self, card_list):
        self.__card_list = card_list[:]

    def count(self):
        return len(self.__card_list)

    def draw_a_card(self):
        if len(self.__card_list) == 0:
            return None

        card = self.__card_list.pop(0)

        return card

    def get(self, index):
        assert index < self.count()
        return self.__card_list[index]

    def card_list(self):
        return self.__card_list

class Trash:
    def __init__(self, game_option):
        self.__card_list = []
        self.__game_option = game_option

    def insert(self, card):
        assert card != None

        self.__card_list.append(card)

    def count(self):
        return len(self.__card_list)

    def find(self, value, color):
        count = 0
        for card in self.__card_list:
            if card.color() == color and card.value() == value:
                count += 1
        return count

    def current_max_score(self):
        max_score = 0

        # For each color...
        color_options = self.__game_option.color_options()
        for color, options in color_options.items():
            
            # Get the distribution
            distribution = options.distribution()
            begin_value = self.__game_option.min_card_value()
            end_value = self.__game_option.max_card_value()
            step_value = 1
            if not options.is_ascending():
                begin_value = self.__game_option.max_card_value()
                end_value = self.__game_option.min_card_value()
                step_value = -1
            for value in range(begin_value, end_value + step_value, step_value):
                                
                theorical_count = distribution.count(value)
                
                trash_count = self.find(value, color)
                if trash_count < theorical_count:
                    # If there is less card than in the distribution, the max score increase
                    max_score += 1
                else:
                    # If all card of this color and value are in the trash, the score cannot be increased
                    break

        return max_score

    def is_critical(self, value, color):
        color_options = self.__game_option.color_options()
        
        assert color in color_options
        
        initial_count = color_options[color].distribution().count(value)

        return (initial_count - self.find(value, color)) == 1


class Board:
    def __init__(self, game_option):
        self.__colors = {}
        self.__game_option = game_option
        for color in self.__game_option.color_options().keys():
            if self.__game_option.color_options()[color].is_ascending():
                self.__colors[color] = self.__game_option.min_card_value() - 1
            else:
                self.__colors[color] = self.__game_option.max_card_value() + 1

    def is_valid(self, value, color):
        assert color in self.__colors

        board_value = self.__colors[color]

        if self.__game_option.color_options()[color].is_ascending():
            return board_value + 1 == value
        else:
            return board_value - 1 == value

    def is_usefull(self, value, color):
        assert color in self.__colors

        board_value = self.__colors[color]

        if self.__game_option.color_options()[color].is_ascending():
            return value > board_value
        else:
            return value < board_value


    def insert(self, card):
        assert self.is_valid(card.value(), card.color())
        assert card != None

        self.__colors[card.color()] = card.value()

    def current_score(self):
        score = 0

        for color, value in self.__colors.items():
            
            if self.__game_option.color_options()[color].is_ascending():
                score += value
            else:
                score += self.__game_option.max_card_value() - value + 1

        return score
        

class Hand:
    def __init__(self, game_option):
        self.__card_list = []
        self.__game_option = game_option

    def insert(self, card):
        assert len(self.__card_list) < self.__game_option.player_hand_max_size()
        assert card != None

        self.__card_list.append(card)

    def remove(self, index):
        assert index >= 0
        assert index < self.__game_option.player_hand_max_size()
        assert index < len(self.__card_list)
            
        self.__card_list.pop(index)

    def get(self, index):
        assert index >= 0 
        assert index < self.__game_option.player_hand_max_size()
        assert index < len(self.__card_list)

        return self.__card_list[index]

    def count(self):
        return len(self.__card_list)
