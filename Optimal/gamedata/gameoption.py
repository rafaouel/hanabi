import gamedata.cards as cards


class ColorDistribution:
    def __init__(self):
        self.__distribution = {}

    def insert(self, value, count):
        if value in self.__distribution:
            raise KeyError("Key already exists")
        self.__distribution[value] = count

    def count(self, value):
        if not value in self.__distribution:
            raise KeyError("Unknown key")
        return self.__distribution[value]

    def initial_max_score(self):
        return len(self.__distribution.keys())


class ColorOption:
    def __init__(self, is_ascending, distribution):
        self.__is_ascending = is_ascending
        self.__distribution = distribution

    def initial_max_score(self):
        return self.__distribution.initial_max_score()

    def distribution(self):
        return self.__distribution

    def is_ascending(self):
        return self.__is_ascending


class GameOption:

    def __init__(self, game_option_data):
        self.__game_option_id = game_option_data["gameOptionId"]
        self.__player_count = game_option_data["playerCount"]
        self.__player_hand_max_size = game_option_data["playerHandMaxSize"]
        self.__start_player_id = game_option_data["startPlayerId"]
        self.__max_speak_token_count = game_option_data["maxSpeakTokenCount"]
        self.__start_speak_token_count = game_option_data["startSpeakTokenCount"]
        self.__min_card_value = game_option_data["minCardValue"]
        self.__max_card_value = game_option_data["maxCardValue"]
        self.__max_score = game_option_data["maxScore"]
        self.__color_options = {}
        for color_option in game_option_data["colorOption"]:
            is_ascending = color_option["isAscending"]
            distribution = ColorDistribution()
            for distribution_data in color_option["distribution"]:
                distribution.insert(distribution_data["value"], distribution_data["count"])
            self.__color_options[color_option["color"]] = ColorOption(is_ascending, distribution)
        card_list = [cards.Card(raw_card["value"], raw_card["color"]) for raw_card in game_option_data["stack"]]
        self.__stack = cards.Stack(card_list)

    def game_option_id(self):
        return self.__game_option_id
        
    def color_options(self):
        return self.__color_options

    def min_card_value(self):
        return self.__min_card_value

    def max_card_value(self):
        return self.__max_card_value

    def player_count(self):
        return self.__player_count

    def player_hand_max_size(self):
        return self.__player_hand_max_size

    def stack(self):
        return self.__stack

    def start_speak_token_count(self):
        return self.__start_speak_token_count

    def max_speak_token_count(self):
        return self.__max_speak_token_count

    def start_player_id(self):
        return self.__start_player_id

    def max_score(self):
        return self.__max_score