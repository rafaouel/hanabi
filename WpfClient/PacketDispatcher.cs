﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfClient
{
    internal class PacketDispatcher
    {
        public PacketDispatcher()
        {
            this.handlers = new Dictionary<int, EventHandler<string>>();
        }

        public void AddPacketHandler(int packet_type, EventHandler<string> handler)
        {
            this.handlers.Add(packet_type, handler);
        }

        public void Dispatch(int packet_type, string packet_content)
        {
            if (!this.handlers.ContainsKey(packet_type))
            {
                // TODO handle error    
            }
            this.handlers[packet_type].Invoke(this, packet_content);
        }

        private Dictionary<int, EventHandler<string>> handlers;
    }
}
