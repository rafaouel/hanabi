﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace WpfClient
{
    internal class Client
    {
        private TcpClient m_tcp_client;
        private PacketDispatcher m_dispatcher;
        private readonly int m_header_length = 6;

        public Client(PacketDispatcher dispatcher)
        {
            this.m_dispatcher = dispatcher;
        }

        public void Connect(IPEndPoint ip_endpoint)
        {
            try
            {
                this.m_tcp_client = new TcpClient();
                this.m_tcp_client.Connect(ip_endpoint);

                _ = this.ReceiveDataAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private async Task<bool> Read(byte[] buffer, int count)
        {
            // This function read until count bytes have been red
            NetworkStream stream = this.m_tcp_client.GetStream();

            int cumul_received_count = 0;
            int keep_reading = count;
            while (count != cumul_received_count)
            {
                int received_count = await stream.ReadAsync(buffer, cumul_received_count, keep_reading);
                if (received_count == 0)
                {
                    return true;
                }
                cumul_received_count += received_count;
                keep_reading -= received_count;
            }

            return false;
        }

        private async Task ReceiveDataAsync()
        {
            try
            {
                NetworkStream stream = this.m_tcp_client.GetStream();

                byte[] header_buffer = new byte[this.m_header_length];

                while (true)
                {
                    // Read the header
                    {
                        bool job_done = await this.Read(header_buffer, header_buffer.Length);
                        if (job_done)
                            break;
                    }

                    // Parse header
                    string header = Encoding.ASCII.GetString(header_buffer);
                    int body_length = Int32.Parse(header);

                    // Read the body
                    byte[] body_buffer = new byte[body_length + 10];
                    {
                        bool job_done = await this.Read(body_buffer, body_length);
                        if (job_done)
                            break;
                    }


                    // Parse body
                    string data = Encoding.ASCII.GetString(body_buffer, 0, body_length);

                    if (data.Length > 0)
                    {
                        // Split the type from the payload
                        StringReader reader = new StringReader(data.Replace(";", "\n"));

                        // Get the type
                        int packet_type = Int32.Parse(reader.ReadLine());

                        // Get the payload
                        string packet_content = reader.ReadLine();

                        this.m_dispatcher.Dispatch(packet_type, packet_content);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public void Send<T>(ref T packet) where T : Packet
        {
            try
            {
                NetworkStream stream = this.m_tcp_client.GetStream();

                // Create the payload
                JsonSerializerOptions options = new JsonSerializerOptions();
                options.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;
                string payload = packet.packetType.ToString() + ";" + JsonSerializer.Serialize<T>(packet, options);
                byte[] body_buffer = Encoding.ASCII.GetBytes(payload);

                // Create the header
                byte[] header_buffer = Encoding.ASCII.GetBytes(body_buffer.Length.ToString());
                Array.Resize<byte>(ref header_buffer, this.m_header_length);

                // Send the header
                stream.Write(header_buffer, 0, header_buffer.Length);

                // Send the body
                stream.Write(body_buffer, 0, body_buffer.Length);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
