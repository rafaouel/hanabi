﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WpfClient
{
    public class ColorOptionData : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private string colorName;
        private bool isAscending;
        private List<string> reactsTo;
        private Dictionary<int, int> distribution;

        public ColorOptionData()
        {
            this.reactsTo = new List<string>();
            this.distribution = new Dictionary<int, int>();
        }

        public string ColorName
        {
            get
            {
                return this.colorName;
            }
            set
            {
                if (this.colorName != value)
                {
                    this.colorName = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged("ColorOptionStr");
                }
            }
        }

        public bool IsAscending
        {
            get
            {
                return this.isAscending;
            }
            set
            {
                if (this.isAscending != value)
                {
                    this.isAscending = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged("ColorOptionStr");
                }
            }
        }

        public List<string> ReactsTo
        {
            get
            {
                return this.reactsTo;
            }
            set
            {
                if (this.reactsTo != value)
                {
                    this.reactsTo = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged("ColorOptionStr");
                }
            }
        }

        public Dictionary<int, int> Distribution
        { 
            get 
            { 
                return this.distribution;
            }
            set
            {
                if (this.distribution != value)
                {
                    this.distribution = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged("ColorOptionStr");
                }
            }
        }

        public string ColorOptionStr
        {
            get
            {
                string result = $"{this.colorName}: ";

                if (this.isAscending)
                    result += "Ascending";
                else
                    result += "Descending";

                // The reaction list
                result += ", Reacts to ";
                if (this.ReactsTo.Count == 0)
                    result += "nothing";
                else
                {
                    if (this.ReactsTo.Count > 1)
                        result += "[";

                    bool first = true;
                    foreach (string reaction in this.ReactsTo)
                    {
                        if (first)
                            first = false;
                        else
                            result += ", ";
                        result += reaction;
                    }

                    if (this.ReactsTo.Count > 1)
                        result += "]";
                }

                // The distributionn
                result += ", Distribution (";
                {
                    bool first = true;
                    foreach (KeyValuePair<int, int> pair in this.distribution)
                    {
                        if (first)
                            first = false;
                        else
                            result += " ";
                        result += $"{pair.Key}x{pair.Value}";
                    }
                }
                result += ")";

                return result;
            }
        }
    }

    public class CardData : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void NotifySpeakTokenCountChanged()
        {
            this.NotifyPropertyChanged("DropVisibility");
            this.NotifyPropertyChanged("SpeakVisibility");
        }

        public void NotifyCurrentPlayerIdChanged()
        {

        }

        public void NotifyEnableDropChanged()
        {
            this.NotifyPropertyChanged("EnableDrop");
        }

        public void NotifyEnablePlaceChanged()
        {
            this.NotifyPropertyChanged("EnablePlace");
        }

        public void NotifyEnableSpeakChanged()
        {
            this.NotifyPropertyChanged("EnableSpeak");
        }

        public void NotifyGameStartedChanged()
        {

        }

        public void NotifyGameIsOnChanged()
        {

        }

        private string color;
        private int value;
        private List<string> positiveColorInfo;
        private List<string> negativeColorInfo;
        private List<int> positiveValueInfo;
        private List<int> negativeValueInfo;
        private bool hidden;
        private bool disabled;
        private readonly PlayerData parent;

        public CardData(PlayerData parent)
        {
            this.color = String.Empty;
            this.value = 0;
            this.positiveValueInfo = new List<int>();
            this.negativeValueInfo = new List<int>();
            this.positiveColorInfo = new List<string>();
            this.negativeColorInfo = new List<string>();
            this.hidden = true;
            this.disabled = true;
            this.parent = parent;

            this.NotifyPropertyChanged(String.Empty);
        }

        public bool Hidden
        {
            get
            {
                return this.hidden;
            }
            set
            {
                if (this.hidden != value)
                {
                    this.hidden = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged("CardStr");
                }
            }
        }

        public bool Disabled
        {
            get
            {
                return this.disabled;
            }
            set
            {
                if (this.disabled != value)
                {
                    this.disabled = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged(String.Empty);
                }
            }
        }

        public int Value
        {
            get
            {
                return this.value;
            }
            set
            {
                if (this.value != value)
                {
                    this.value = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged("CardStr");
                }
            }
        }

        public string Color
        {
            get
            {
                return this.color;
            }
            set
            {
                if (this.color != value)
                {
                    this.color = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged("CardStr");
                }
            }
        }

        public List<int> PositiveValueInfo
        {
            set
            {
                this.positiveValueInfo = value;
                this.NotifyPropertyChanged("PositiveValueInfoStr");
            }
        }

        public List<int> NegativeValueInfo
        {
            set
            {
                this.negativeValueInfo = value;
                this.NotifyPropertyChanged("NegativeValueInfoStr");
            }
        }

        public List<string> PositiveColorInfo
        {
            set
            {
                this.positiveColorInfo = value;
                this.NotifyPropertyChanged("PositiveColorInfoStr");
            }
        }

        public List<string> NegativeColorInfo
        {
            set
            {
                this.negativeColorInfo = value;
                this.NotifyPropertyChanged("NegativeColorInfoStr");
            }
        }

        public string CardStr
        {
            get
            {
                if (this.disabled)
                    return String.Empty;
                if (this.hidden)
                    return "?";
                return $"{this.value} - {this.color}";
            }
        }

        public string PositiveColorInfoStr
        {
            get
            {
                string result = String.Empty;

                if (this.positiveColorInfo.Count > 0)
                {
                    bool first = true;
                    foreach (string color in this.positiveColorInfo)
                    {
                        if (first)
                            first = false;
                        else
                            result += ", ";
                        result += color[0];
                    }
                }

                return result;
            }
        }

        public string NegativeColorInfoStr
        {
            get
            {
                string result = String.Empty;

                if (this.negativeColorInfo.Count > 0)
                {
                    bool first = true;
                    foreach (string color in this.negativeColorInfo)
                    {
                        if (first)
                            first = false;
                        else
                            result += ", ";
                        result += color[0];
                    }
                }

                return result;
            }
        }

        public string PositiveValueInfoStr
        {
            get
            {
                string result = String.Empty;

                if (this.positiveValueInfo.Count > 0)
                {
                    bool first = true;
                    foreach (int value in this.positiveValueInfo)
                    {
                        if (first)
                            first = false;
                        else
                            result += ", ";
                        result += $"{value}";
                    }
                }

                return result;
            }
        }

        public string NegativeValueInfoStr
        {
            get
            {
                string result = String.Empty;

                if (this.negativeValueInfo.Count > 0)
                {
                    bool first = true;
                    foreach (int value in this.negativeValueInfo)
                    {
                        if (first)
                            first = false;
                        else
                            result += ", ";
                        result += $"{value}";
                    }
                }

                return result;
            }
        }

        public System.Windows.Visibility Visibility
        {
            get
            {
                return this.disabled ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        public System.Windows.Visibility DropVisibility
        {
            get
            {
                return (this.parent.Me) ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public System.Windows.Visibility PlaceVisibility
        {
            get
            {
                return (this.parent.Me) ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public System.Windows.Visibility SpeakVisibility
        {
            get
            {
                return (this.parent.Parent.AllowedToSpeak && !this.parent.Me) ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public bool EnableDrop
        {
            get
            {
                return this.parent.EnableDrop;
            }
        }

        public bool EnablePlace
        {
            get
            {
                return this.parent.EnablePlace;
            }
        }

        public bool EnableSpeak
        {
            get
            {
                return this.parent.EnableSpeak;
            }
        }
    }

    public class PlayerData : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void NotifySpeakTokenCountChanged()
        {
            foreach (CardData card in this.hand)
            {
                card.NotifySpeakTokenCountChanged();
            }
            this.NotifyPropertyChanged("Visibility");
            this.NotifyPropertyChanged("SpeakVisibility");
        }

        public void NotifyCurrentPlayerIdChanged()
        {
            foreach (CardData card in this.hand)
            { 
                card.NotifyCurrentPlayerIdChanged();
            }
                
            this.NotifyPropertyChanged("Playing");
            this.NotifyPropertyChanged("NameStr");
        }

        public void NotifyEnableDropChanged()
        {
            foreach (CardData card in this.hand)
            {
                card.NotifyEnableDropChanged();
            }
            this.NotifyPropertyChanged("EnableDrop");
        }

        public void NotifyEnablePlaceChanged()
        {
            foreach (CardData card in this.hand)
            {
                card.NotifyEnablePlaceChanged();
            }
            this.NotifyPropertyChanged("EnablePlace");
        }

        public void NotifyEnableSpeakChanged()
        {
            foreach (CardData card in this.hand)
            {
                card.NotifyEnableSpeakChanged();
            }
            this.NotifyPropertyChanged("EnableSpeak");
        }

        public void NotifyGameStartedChanged()
        {
            foreach (CardData card in this.hand)
            {
                card.NotifyGameStartedChanged();
            }
            this.NotifyPropertyChanged("NameStr");
        }

        public void NotifyGameIsOnChanged()
        {
            foreach (CardData card in this.hand)
            {
                card.NotifyGameIsOnChanged();
            }
        }

        private readonly int id;
        private string name;
        private string type;
        private List<CardData> hand;
        private bool disabled;
        private bool me;
        private readonly DataModel parent;

        public PlayerData(int max_hand_size, DataModel parent, int id)
        {
            this.id = id;
            this.name = "";
            this.type = "unknown";
            this.hand = new List<CardData>();
            for (int i = 0; i < max_hand_size; i++)
                this.hand.Add(new CardData(this));
            this.disabled = true;
            this.me = false;
            this.parent = parent;

            this.NotifyPropertyChanged(String.Empty);
        }

        public DataModel Parent
        { 
            get 
            { 
                return this.parent; 
            }
        }

        public bool Me
        {
            get { return this.me; }
            set
            {
                if (this.me != value)
                {
                    this.me = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged("NameStr");
                }
            }
        }

        public bool Disabled
        {
            get
            {
                return this.disabled;
            }
            set
            {
                if (this.disabled != value)
                {
                    this.disabled = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged(String.Empty);
                }
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
                this.NotifyPropertyChanged();
                this.NotifyPropertyChanged("NameStr");
            }
        }

        public string Type
        {
            get
            {
                return this.type;
            }
            set
            {
                this.type = value;
                this.NotifyPropertyChanged();
                this.NotifyPropertyChanged("NameStr");
            }
        }

        public List<CardData> Hand
        {
            get
            {
                return this.hand;
            }
            set
            {
                this.hand = value;
                this.NotifyPropertyChanged();
            }
        }

        public bool Playing
        {
            get
            {
                return this.parent.CurrentPlayerId == this.id;
            }
        }

        public string NameStr
        {
            get
            {
                string result = String.Empty;

                if (this.name.Length > 0)
                {
                    string prefix = String.Empty;
                    string sufix = String.Empty;

                    if (this.Playing && this.parent.GameStarted)
                    {
                        prefix = ">>> ";
                        sufix = "is playing";
                    }

                    if (this.me)
                    {
                        prefix += $"[me] ";
                    }

                    result = $"{prefix}{this.name} ({this.type}) {sufix}";
                }

                return result;
            }
        }

        public System.Windows.Visibility Visibility
        {
            get
            {
                if (this.disabled)
                    return Visibility.Collapsed;

                return Visibility.Visible;
            }
        }

        public System.Windows.Visibility SpeakVisibility
        {
            get
            {
                if (this.me)
                    return Visibility.Hidden;
                return Visibility.Visible;
            }
        }

        public System.Windows.Visibility BlueVisibility
        {
            get
            {
                return this.parent.CallableColors.Contains("blue") ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public System.Windows.Visibility WhiteVisibility
        {
            get
            {
                return this.parent.CallableColors.Contains("white") ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public System.Windows.Visibility RedVisibility
        {
            get
            {
                return this.parent.CallableColors.Contains("red") ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public System.Windows.Visibility YellowVisibility
        {
            get
            {
                return this.parent.CallableColors.Contains("yellow") ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public System.Windows.Visibility GreenVisibility
        {
            get
            {
                return this.parent.CallableColors.Contains("green") ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public System.Windows.Visibility OneVisibility
        {
            get
            {
                int value = 1;
                if (this.parent.MinCardValue <= value && value <= this.parent.MaxCardValue)
                    return Visibility.Visible;
                return Visibility.Collapsed;
            }
        }

        public System.Windows.Visibility TwoVisibility
        {
            get
            {
                int value = 2;
                if (this.parent.MinCardValue <= value && value <= this.parent.MaxCardValue)
                    return Visibility.Visible;
                return Visibility.Collapsed;
            }
        }

        public System.Windows.Visibility ThreeVisibility
        {
            get
            {
                int value = 3;
                if (this.parent.MinCardValue <= value && value <= this.parent.MaxCardValue)
                    return Visibility.Visible;
                return Visibility.Collapsed;
            }
        }

        public System.Windows.Visibility FourVisibility
        {
            get
            {
                int value = 4;
                if (this.parent.MinCardValue <= value && value <= this.parent.MaxCardValue)
                    return Visibility.Visible;
                return Visibility.Collapsed;
            }
        }

        public System.Windows.Visibility FiveVisibility
        {
            get
            {
                int value = 5;
                if (this.parent.MinCardValue <= value && value <= this.parent.MaxCardValue)
                    return Visibility.Visible;
                return Visibility.Collapsed;
            }
        }

        public bool IsMyTurn
        {
            get
            {
                return this.parent.IsMyTurn;
            }
        }

        public bool EnableDrop
        {
            get
            {
                return this.parent.EnableDrop;
            }
        }

        public bool EnablePlace
        {
            get
            {
                return this.parent.EnablePlace;
            }
        }

        public bool EnableSpeak
        {
            get
            {
                return this.parent.EnableSpeak;
            }
        }
    }

    public class DataModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void NotifySpeakTokenCountChanged()
        {
            foreach (KeyValuePair<int, PlayerData> pair in this.PlayerData)
            {
                pair.Value.NotifySpeakTokenCountChanged();
            }
            this.NotifyPropertyChanged("EnableDrop");
            this.NotifyPropertyChanged("EnableSpeak");
        }

        public void NotifyCurrentPlayerIdChanged()
        {
            foreach (KeyValuePair<int, PlayerData> pair in this.PlayerData)
            {
                pair.Value.NotifyCurrentPlayerIdChanged();
            }
            this.NotifyEnableDropChanged();
            this.NotifyEnablePlaceChanged();
            this.NotifyEnableSpeakChanged();
        }

        public void NotifyGameStartedChanged()
        {
            foreach (KeyValuePair<int, PlayerData> pair in this.PlayerData)
            {
                pair.Value.NotifyGameStartedChanged();
            }
        }

        public void NotifyGameIsOnChanged()
        {
            foreach (KeyValuePair<int, PlayerData> pair in this.PlayerData)
                pair.Value.NotifyGameIsOnChanged();
        }

        public void NotifyEnableDropChanged()
        {
            foreach (KeyValuePair<int, PlayerData> pair in this.PlayerData)
            {
                pair.Value.NotifyEnableDropChanged();
            }
        }

        public void NotifyEnablePlaceChanged()
        {
            foreach (KeyValuePair<int, PlayerData> pair in this.PlayerData)
            {
                pair.Value.NotifyEnablePlaceChanged();
            }
        }

        public void NotifyEnableSpeakChanged()
        {
            foreach (KeyValuePair<int, PlayerData> pair in this.PlayerData)
            {
                pair.Value.NotifyEnableSpeakChanged();
            }
        }

        public DataModel(int ui_player_slot, int max_hand_size)
        {
            this.callableColors = new List<string>();
            this.board = new Dictionary<string, int>();
            this.trash = new Dictionary<string, List<int>>();
            this.playerData = new Dictionary<int, PlayerData>();
            this.gameStarted = false;
            this.actionHistory = new List<string>();
            this.gameIsOn = false;
            this.ColorOptions = new List<ColorOptionData>();
            
            // Initialize the slots
            for (int i = 0; i < ui_player_slot; i++)
            {
                this.playerData[i] = new PlayerData(max_hand_size, this, i);
            }
            
            NotifyPropertyChanged(String.Empty);
        }

        private int stackCount;
        private int initialMaxScore;
        private int currentMaxScore;
        private int currentScore;
        private int errorTokenCount;
        private int startErrorTokenCount;
        private int maxErrorTokenCount;
        private int speakTokenCount;
        private int startSpeakTokenCount;
        private int maxSpeakTokenCount;
        private int playerTurnCount;
        private int gameTurnCount;
        private string gameOptionId;
        private string gameUuid;
        private List<string> callableColors;
        private int minCardValue;
        private int maxCardValue;
        private int playerCount;
        private Dictionary<string, int> board;
        private Dictionary<string, List<int>> trash;
        private Dictionary<int, PlayerData> playerData;
        private int currentPlayerId;
        private bool gameStarted;
        private bool gameIsOn;
        private List<string> actionHistory;
        private List<ColorOptionData> colorOptions;

        public bool AllowedToSpeak
        {
            get
            {
                return this.speakTokenCount > 0;
            }
        }

        public bool AllowedToDrop
        {
            get
            {
                return this.speakTokenCount < this.maxSpeakTokenCount;
            }
        }

        public int StackCount       
        {
            get
            {
                return this.stackCount;
            }
            set
            {
                if (this.stackCount != value)
                {
                    this.stackCount = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged("StackCountStr");
                }
            }
        }

        public int CurrentScore
        {
            get
            {
                return this.currentScore;
            }
            set
            {
                if (this.currentScore != value)
                {
                    this.currentScore = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged("ScoreStr");
                }
            }
        }

        public int CurrentMaxScore
        {
            get
            {
                return this.currentMaxScore;
            }
            set
            {
                if (this.currentMaxScore != value)
                {
                    this.currentMaxScore = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged("ScoreStr");
                }
            }
        }

        public int InitialMaxScore
        {
            get
            {
                return this.initialMaxScore;
            }
            set
            {
                if (this.initialMaxScore != value)
                {
                    this.initialMaxScore = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged("ScoreStr");
                }
            }
        }

        public int ErrorTokenCount
        {
            get
            {
                return this.errorTokenCount;
            }
            set
            {
                if (this.errorTokenCount != value)
                {
                    this.errorTokenCount = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged("ErrorTokenCountStr");
                }
            }
        }

        public int StartErrorTokenCount
        {
            get
            {
                return this.startErrorTokenCount;
            }
            set
            {
                if (this.startErrorTokenCount != value)
                {
                    this.startErrorTokenCount = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged("GameOptionErrorStr");
                }
            }
        }

        public int MaxErrorTokenCount
        {
            get
            {
                return this.maxErrorTokenCount;
            }
            set
            {
                if (this.maxErrorTokenCount != value)
                {
                    this.maxErrorTokenCount = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged("ErrorTokenCountStr");
                    this.NotifyPropertyChanged("GameOptionErrorStr");
                }
            }
        }

        public int SpeakTokenCount
        {
            get
            {
                return this.speakTokenCount;
            }
            set
            {
                if (this.speakTokenCount != value)
                {
                    this.speakTokenCount = value;
                    this.NotifyPropertyChanged();
                    this.NotifySpeakTokenCountChanged();
                    this.NotifyPropertyChanged("SpeakTokenCountStr");
                }
            }
        }

        public int StartSpeakTokenCount
        {
            get
            {
                return this.startSpeakTokenCount;
            }
            set
            {
                if (this.startSpeakTokenCount != value)
                {
                    this.startSpeakTokenCount = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged("GameOptionSpeakStr");
                }
            }
        }

        public int MaxSpeakTokenCount
        {
            get
            {
                return this.maxSpeakTokenCount;
            }
            set
            {
                if (this.maxSpeakTokenCount != value)
                {
                    this.maxSpeakTokenCount = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged("SpeakTokenCountStr");
                    this.NotifyPropertyChanged("GameOptionSpeakStr");
                }
            }
        }

        public int PlayerTurnCount
        {
            get
            {
                return this.playerTurnCount;
            }
            set
            {
                if (this.playerTurnCount != value)
                {
                    this.playerTurnCount = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged("TurnCountStr");
                }
            }
        }

        public int GameTurnCount
        {
            get
            {
                return this.gameTurnCount;
            }
            set
            {
                if (this.gameTurnCount != value)
                {
                    this.gameTurnCount = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged("TurnCountStr");
                }
            }
        }

        public string GameOptionId
        {
            get
            {
                return this.gameOptionId;
            }
            set
            {
                if (this.gameOptionId != value)
                {
                    this.gameOptionId = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        public string GameUuid
        {
            get
            {
                return this.gameUuid;
            }
            set
            {
                if (this.gameUuid != value)
                {
                    this.gameUuid = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged("GameTitle");
                }
            }
        }

        public List<string> CallableColors
        {
            get
            {
                return this.callableColors;
            }
            set
            {
                this.callableColors = value;
                this.NotifyPropertyChanged();
                this.NotifyPropertyChanged("CallableColorsStr");
            }
        }

        public int MinCardValue
        {
            get
            {
                return this.minCardValue;
            }
            set
            {
                if (this.minCardValue != value)
                {
                    this.minCardValue = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        public int MaxCardValue
        {
            get
            {
                return this.maxCardValue;
            }
            set
            {
                if (this.maxCardValue != value)
                {
                    this.maxCardValue = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        public int PlayerCount
        {
            get
            {
                return this.playerCount;
            }
            set
            {
                if (this.playerCount != value)
                {
                    this.playerCount = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        public Dictionary<string, int> Board
        {
            get
            {
                return this.board;
            }
            set
            {
                this.board = value;
                this.NotifyPropertyChanged();
                this.NotifyPropertyChanged("BoardListBox");
            }
        }

        public Dictionary<string, List<int>> Trash
        {
            get
            {
                return this.trash;
            }
            set
            {
                this.trash = value;
                this.NotifyPropertyChanged();
                this.NotifyPropertyChanged("TrashListBox");
            }
        }

        public Dictionary<int, PlayerData> PlayerData
        {
            get
            {
                return this.playerData;
            }
            set
            {
                this.playerData = value;
                this.NotifyPropertyChanged();
            }
        }

        public int CurrentPlayerId
        {
            get
            {
                return this.currentPlayerId;
            }
            set
            {
                if (this.currentPlayerId != value)
                {
                    this.currentPlayerId = value;
                    this.NotifyPropertyChanged();
                    this.NotifyCurrentPlayerIdChanged();
                }
            }
        }

        public bool GameStarted
        {
            get
            {
                return this.gameStarted;
            }
            set
            {
                if (this.gameStarted != value)
                {
                    this.gameStarted = value;
                    this.NotifyPropertyChanged();
                    this.NotifyGameStartedChanged();
                    this.NotifyEnableDropChanged();
                    this.NotifyEnablePlaceChanged();
                    this.NotifyEnableSpeakChanged();
                }
            }
        }

        public bool GameIsOn
        {
            get
            {
                return this.gameIsOn;
            }
            set
            {
                if (this.gameIsOn != value)
                {
                    this.gameIsOn = value;
                    this.NotifyPropertyChanged();
                    this.NotifyGameIsOnChanged();
                    this.NotifyEnableDropChanged();
                    this.NotifyEnablePlaceChanged();
                    this.NotifyEnableSpeakChanged();
                }
            }
        }

        public void PushSpeakValueActionHistory(int sourcePlayerId, int targetPlayerId, int value)
        {
            string sourceName = this.PlayerData[sourcePlayerId].Name;
            string targetName = this.PlayerData[targetPlayerId].Name;
            this.actionHistory.Insert(0, $"{this.playerTurnCount}-{this.gameTurnCount}: '{sourceName}' speak to '{targetName}' with 'value is {value}'");

            this.NotifyPropertyChanged("ActionHistoryListBox");
        }

        public void PushSpeakColorActionHistory(int sourcePlayerId, int targetPlayerId, string color)
        {
            string sourceName = this.PlayerData[sourcePlayerId].Name;
            string targetName = this.PlayerData[targetPlayerId].Name;
            this.actionHistory.Insert(0, $"{this.playerTurnCount}-{this.gameTurnCount}: '{sourceName}' speak to '{targetName}' with 'value is {color}'");

            this.NotifyPropertyChanged("ActionHistoryListBox");
        }

        public void PushDropActionHistory(int sourcePlayerId, int handIndex, Card card)
        {
            string sourceName = this.PlayerData[sourcePlayerId].Name;
            this.actionHistory.Insert(0, $"{this.playerTurnCount}-{this.gameTurnCount}: '{sourceName}' drop '{card}'");

            this.NotifyPropertyChanged("ActionHistoryListBox");
        }

        public void PushPlaceActionHistory(int sourcePlayerId, int handIndex, Card card)
        {
            string sourceName = this.PlayerData[sourcePlayerId].Name;
            this.actionHistory.Insert(0, $"{this.playerTurnCount}-{this.gameTurnCount}: '{sourceName}' place '{card}'");

            this.NotifyPropertyChanged("ActionHistoryListBox");
        }

        public void PushBonusActionHistory(int playerId, string color)
        {
            string sourceName = this.PlayerData[playerId].Name;
            this.actionHistory.Insert(0, $"'{sourceName}' finished the firework '{color}', the team get a bonus for that");

            this.NotifyPropertyChanged("ActionHistoryListBox");
        }

        public void PushErrorActionHistory(int playerId, int newErrorTokenCount)
        {
            string sourceName = this.PlayerData[playerId].Name;
            this.actionHistory.Insert(0, $"'{sourceName}' made an error, the error count increased to '{newErrorTokenCount}'");

            this.NotifyPropertyChanged("ActionHistoryListBox");
        }

        public void PushGameEndActionHistory(bool victory, DateTime gameStartTimestamp, DateTime gameEndTimestamp, List<PlayerStatistics> statistics)
        {
            double duration = (gameEndTimestamp - gameStartTimestamp).TotalMilliseconds / 1000;
            if (victory)
                this.actionHistory.Insert(0, $"Congratulations !! You won this game in {duration:0.000}s");
            else
                this.actionHistory.Insert(0, $"Unfortunately, you lose this game after {duration:0.000}s");

            if (statistics != null)
            {
                foreach (PlayerStatistics stats in statistics)
                {
                    string playerName = this.PlayerData[stats.playerId].Name;
                    string format = "{0,12}    {1,2:D} ({2:F3}s)    {3,2:D} ({4:F3}s)    {5,2:D} ({6:F3}s)    {7:F3}s";
                    string output = String.Format(format, playerName, stats.dropCount, stats.dropDuration, stats.placeCount, stats.placeDuration, stats.speakCount, stats.speakDuration, stats.totalDuration);
                    this.actionHistory.Insert(0, output);
                }
                this.actionHistory.Insert(0, $"Player name  drop             place            speak            total");
            }

            this.NotifyPropertyChanged("ActionHistoryListBox");
        }

        public void PushHelloMessage(string message)
        { 
            this.actionHistory.Insert(0, message); 

            this.NotifyPropertyChanged("ActionHistoryListBox");
        }

        public List<ColorOptionData> ColorOptions
        {
            get
            {
                return this.colorOptions;
            }
            set
            {
                if (this.colorOptions != value)
                {
                    this.colorOptions = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged("ColorOptionsStr");
                }
            }
        }

        public string StackCountStr
        {
            get
            {
                return $"{this.stackCount} cards remaining";
            }
        }

        public string ScoreStr
        {
            get
            {
                return $"{this.currentScore}/{this.currentMaxScore}/{this.initialMaxScore}";
            }
        }

        public string ErrorTokenCountStr
        {
            get
            {
                return $"{this.errorTokenCount}/{this.maxErrorTokenCount}";
            }
        }

        public string SpeakTokenCountStr
        {
            get
            {
                return $"{this.speakTokenCount}/{this.maxSpeakTokenCount}";
            }
        }

        public string TurnCountStr
        {
            get
            {
                return $"{this.playerTurnCount}-{this.gameTurnCount}";
            }
        }

        public string GameOptionErrorStr
        {
            get
            {
                return $"{this.startErrorTokenCount}-{this.maxErrorTokenCount}";
            }
        }

        public string GameOptionSpeakStr
        {
            get
            {
                return $"{this.startSpeakTokenCount}-{this.maxSpeakTokenCount}";
            }
        }

        public string GameTitle
        {
            get
            {
                if (this.gameUuid == null)
                    return "Game";
                else
                    return $"Game {this.gameUuid}";
            }
        }

        public string CallableColorsStr
        {
            get
            {
                string result = "";
                bool first = true;
                foreach (var color in this.callableColors)
                {
                    if (first)
                        first = false;
                    else
                        result += ", ";
                    result += color;
                }
                return result;
            }
        }

        public List<string> BoardListBox
        {
            get
            {
                List<string> result = new List<string>();
                
                foreach (KeyValuePair<string, int> pair in this.board)
                {
                    string colorName = pair.Key[0].ToString().ToUpper() + pair.Key.Substring(1);

                    if (pair.Value > 0)
                    {
                        ColorOptionData option = this.colorOptions.Find((colorOption) => { return colorOption.ColorName == pair.Key; });
                        string values = "";
                        int start = 0;
                        int increment = 0;
                        if (option.IsAscending)
                        {
                            start = option.Distribution.Keys.Min();
                            increment = 1;
                        }
                        else
                        {
                            start = option.Distribution.Keys.Max();
                            increment = -1;
                        }
                        bool first = true;
                        for (int i = start; i != pair.Value + increment; i += increment)
                        {
                            if (first)
                                first = false;
                            else
                                values += " ";
                            values += i.ToString();
                        }

                        result.Add($"{colorName} {values}");
                    }
                    else
                        result.Add($"{colorName} - ");
                }

                return result;
            }
        }

        public List<string> TrashListBox
        {
            get
            {
                List<string> result = new List<string>();

                foreach (KeyValuePair<string, List<int>> pair in this.trash)
                {
                    string colorName = pair.Key[0].ToString().ToUpper() + pair.Key.Substring(1);
                    string colorData = "";

                    if (pair.Value.Count == 0)
                        colorData = " -";
                    
                    foreach (int cardValue in pair.Value)
                    {
                        colorData += $" {cardValue}";
                    }

                    result.Add($"{colorName} {colorData}");
                }

                return result;
            }
        }

        public bool IsMyTurn
        {
            get
            {
                return this.PlayerData[this.CurrentPlayerId].Me;
            }
        }

        public bool EnableSpeak
        {
            get
            {
                return this.gameIsOn && this.gameStarted && this.IsMyTurn && this.AllowedToSpeak;
            }
        }

        public bool EnableDrop
        {
            get
            {
                return this.gameIsOn && this.gameStarted && this.IsMyTurn && this.AllowedToDrop;
            }
        }

        public bool EnablePlace
        {
            get
            {
                return this.gameIsOn && this.gameStarted && this.IsMyTurn;
            }
        }

        public List<string> ActionHistoryListBox
        {
            get
            {
                return new List<string>(this.actionHistory);
            }
        }

        public List<string> ColorOptionsStr
        {
            get
            {
                List<string> result = new List<string>();

                foreach (ColorOptionData colorOptionData in this.ColorOptions)
                {
                    result.Add(colorOptionData.ColorOptionStr);
                }

                return result;
            }
        }
    }
}
