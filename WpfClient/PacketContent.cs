﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace WpfClient
{
    public class Packet
    {
        public Packet(int packetType)
        {
            this.packetType = packetType;
        }

        [JsonIgnore]
        public int packetType { get; private set; }
    }

    // From the hanabi library
    // 0 - HELLO,
    // 1 - OK,
    // 2 - NOT_OK,
    // 3 - PEER_INFO_REQUEST,
    // 4 - PEER_INFO_RESPONSE,
    // 5 - PEER_INFO_UPDATE,
    // 6 - GAME_OPTION_UPDATE,
    // 7 - GAME_STATE_UPDATE,
    // 8 - GAME_END_UPDATE,
    // 9 - ACTION_REQUEST,
    // 10 - ACTION_RESPONSE,
    // 11 - ACTION_SPEAK_UPDATE,
    // 12 - ACTION_DROP_UPDATE,
    // 13 - ACTION_PLACE_UPDATE,
    // 14 - BONUS_EVENT,
    // 15 - ERROR_EVENT

    public class HelloPacket : Packet
    {
        public HelloPacket() :
            base(0)
        {

        }

        public string message { get; set; }
    }

    public class OkPacket : Packet
    {
        public OkPacket() :
            base(1)
        {

        }
    }

    public class NotOkPacket : Packet
    {
        public NotOkPacket() :
            base(2)
        {

        }

        public string reason { get; set; }
    }

    public class PeerInfoRequestPacket : Packet
    {
        public PeerInfoRequestPacket() :
            base(3)
        {

        }

        public int playerId { get; set; }
        public string serverVersion { get; set; }
    }

    public class PeerInfoResponsePacket : Packet
    {
        public PeerInfoResponsePacket() :
            base(4)
        {
            
        }

        public string name { get; set; }
        public string version { get; set; }
        public List<int> versionArray { get; set; }
        public string type { get; set; }
        public bool debug { get; set; }
        public List<string> tags { get; set; }
    }

    public class PeerInfoUpdatePacket : Packet
    {
        public PeerInfoUpdatePacket() :
            base(5)
        {

        }

        public string name { get; set; }
        public int playerId { get; set; }
        public string type { get; set; }
    }

    public class Distribution
    {
        public int value { get; set; }
        public int count { get; set; }
    }

    public class ColorOption
    {
        public string color { get; set; }
        public bool isAscending { get; set; }
        public List<string> reactsTo { get; set; }
        public List<Distribution> distribution { get; set; }
    }

    public class GameOptionUpdatePacket : Packet
    {
        public GameOptionUpdatePacket() :
            base(6)
        {

        }

        public string gameOptionId { get; set; }
        public DateTime timestamp { get; set; }
        public int playerCount { get; set; }
        public int playerHandMaxSize { get; set; }
        public int startPlayerId { get; set; }
        public int maxErrorTokenCount { get; set; }
        public int maxSpeakTokenCount { get; set; }
        public int startErrorTokenCount { get; set; }
        public int startSpeakTokenCount { get; set; }
        public int minCardValue { get; set; }
        public int maxCardValue { get; set; }
        public List<string> callableColors { get; set; }
        public List<ColorOption> colorOption { get; set; }
        public int stackSize { get; set; }
    }

    public class BoardInfo
    {
        public string color { get; set; }
        public int value { get; set; }
    }

    public class Board
    {
        public int score { get; set; }
        public List<BoardInfo> board { get; set; }
    }

    public class Card
    {
        public override string ToString()
        {
            return this.value.ToString() + "-" + this.color;
        }

        public string color { get; set; }
        public int value { get; set; }
    }

    public class Trash
    {
        public List<Card> trash { get; set; }
        public int currentMaxScore { get; set; }
    }

    public class CardStack
    {
        public int stackCount { get; set; }
    }

    public class PlayerHand
    {
        public int playerId { get; set; }
        public List<Card> hand { get; set; }
    }

    public class PlayerCardInfo
    {
        public int positiveValue { get; set; }
        public List<int> negativeValue { get; set; }
        public List<string> positiveColor { get; set; }
        public List<string> negativeColor { get; set; }
    }

    public class GameStateUpdatePacket : Packet
    {
        public GameStateUpdatePacket() :
            base(7)
        {

        }
        
        public string gameUuid { get; set; }
        public int speakTokenCount { get; set; }
        public int errorTokenCount { get; set; }
        public int playerTurnCount { get; set; }
        public int gameTurnCount { get; set; }
        public int currentPlayerId { get; set; }
        public DateTime playerTurnTimestamp { get; set; }
        public Board board { get; set; }
        public Trash trash { get; set; }
        public CardStack stack { get; set; }
        public List<PlayerHand> playerHand { get; set; }
        public List<List<PlayerCardInfo>> playerCardInfo { get; set; }         
    }

    public class PlayerStatistics
    {
        public int playerId { get; set; }
        public int dropCount { get; set; }
        public int placeCount { get; set; }
        public int speakCount { get; set; }
        public double dropDuration { get; set; }
        public double placeDuration { get; set; }
        public double speakDuration { get; set; }
        public double totalDuration { get; set; }
    }

    public class GameEndUpdatePacket : Packet
    {
        public GameEndUpdatePacket() :
            base(8)
        {
            
        }

        public string gameUuid { get; set; }
        public bool victory { get; set; }
        public DateTime gameStartTimestamp { get; set; }
        public DateTime gameEndTimestamp { get; set; }
        public int elapsedPlayerTurnCount { get; set; }
        public int elapsedGameTurnCount { get; set; }
        public int score { get; set; }
        public int maxScore { get; set; }
        public int errorTokenCount { get; set; }
        public int maxErrorTokenCount { get; set; }
        public List<PlayerStatistics> statistics { get; set; }
    }

    public class ActionRequestPacket : Packet
    {
        public ActionRequestPacket(): 
            base(9)
        {

        }
    }

    public class CardInfo
    {
        public int? value { get; set; }
        public string color { get; set; }
    }

    public class ActionResponsePacket : Packet
    {
        public ActionResponsePacket():
            base(10)
        {

        }

        public string actionType { get; set; }
        public int targetPlayerId { get; set; }
        public CardInfo cardInfo { get; set; }
        public int? handIndex { get; set; }

    }

    public class ActionSpeakUpdatePacket : Packet
    {
        public ActionSpeakUpdatePacket():
            base(11)
        {

        }

        public int sourcePlayerId { get; set; }
        public int targetPlayerId { get; set; }
        public int? value { get; set; }
        public string color { get; set; }
    }

    public class ActionDropUpdatePacket : Packet
    {
        public ActionDropUpdatePacket():
            base(12)
        {

        }

        public int sourcePlayerId { get; set; }
        public int handIndex { get; set; }
        public Card card {  get; set; }
    }

    public class ActionPlaceUpdatePacket : Packet 
    {
        public ActionPlaceUpdatePacket():
            base(13)
        {

        }

        public int sourcePlayerId { get; set; }
        public int handIndex { get; set; }
        public Card card { get; set; }
    }

    public class BonusEventPacket : Packet
    {
        public BonusEventPacket() :
            base(14)
        {

        }

        public int playerId { get; set; }
        public string color { get; set; }
    }

    public class ErrorEventPacket : Packet
    {
        public ErrorEventPacket():
            base(15)
        {

        }

        public int playerId { get; set; }
        public int errorTokenCount { get; set; }
    }
}
