﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace WpfClient
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Client client;
        private DataModel model;
        private int ui_player_slot = 5;
        private int ui_max_hand_size = 5;
        private GameOptions game_options;


        public MainWindow()
        {
            InitializeComponent();

#if DEBUG
            this.TextBoxPlayerName.Text = "Debug";
            this.TextBoxHost.Text = "127.0.0.1";
            this.TextBoxPort.Text = "32000";
#else
            this.TextBoxPlayerName.Text = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Split('\\').Last();
            this.TextBoxHost.Text = "ironknee.ovh";
            this.TextBoxPort.Text = "32000";
#endif

            this.InitiateGame();

        }

        private void InitiateGame()
        {
            // Build data model
            this.model = new DataModel(this.ui_player_slot, this.ui_max_hand_size);

            // Set the data context
            this.DataContext = this.model;
            this.GroupBoxPlayer0.DataContext = this.model.PlayerData[0];
            this.GroupBoxSpeakToPlayer0.DataContext = this.model.PlayerData[0];
            this.GroupBoxPlayer0Card0.DataContext = this.model.PlayerData[0].Hand[0];
            this.GroupBoxPlayer0Card1.DataContext = this.model.PlayerData[0].Hand[1];
            this.GroupBoxPlayer0Card2.DataContext = this.model.PlayerData[0].Hand[2];
            this.GroupBoxPlayer0Card3.DataContext = this.model.PlayerData[0].Hand[3];
            this.GroupBoxPlayer0Card4.DataContext = this.model.PlayerData[0].Hand[4];
            this.GroupBoxPlayer1.DataContext = this.model.PlayerData[1];
            this.GroupBoxSpeakToPlayer1.DataContext = this.model.PlayerData[1];
            this.GroupBoxPlayer1Card0.DataContext = this.model.PlayerData[1].Hand[0];
            this.GroupBoxPlayer1Card1.DataContext = this.model.PlayerData[1].Hand[1];
            this.GroupBoxPlayer1Card2.DataContext = this.model.PlayerData[1].Hand[2];
            this.GroupBoxPlayer1Card3.DataContext = this.model.PlayerData[1].Hand[3];
            this.GroupBoxPlayer1Card4.DataContext = this.model.PlayerData[1].Hand[4];
            this.GroupBoxPlayer2.DataContext = this.model.PlayerData[2];
            this.GroupBoxSpeakToPlayer2.DataContext = this.model.PlayerData[2];
            this.GroupBoxPlayer2Card0.DataContext = this.model.PlayerData[2].Hand[0];
            this.GroupBoxPlayer2Card1.DataContext = this.model.PlayerData[2].Hand[1];
            this.GroupBoxPlayer2Card2.DataContext = this.model.PlayerData[2].Hand[2];
            this.GroupBoxPlayer2Card3.DataContext = this.model.PlayerData[2].Hand[3];
            this.GroupBoxPlayer2Card4.DataContext = this.model.PlayerData[2].Hand[4];
            this.GroupBoxPlayer3.DataContext = this.model.PlayerData[3];
            this.GroupBoxSpeakToPlayer3.DataContext = this.model.PlayerData[3];
            this.GroupBoxPlayer3Card0.DataContext = this.model.PlayerData[3].Hand[0];
            this.GroupBoxPlayer3Card1.DataContext = this.model.PlayerData[3].Hand[1];
            this.GroupBoxPlayer3Card2.DataContext = this.model.PlayerData[3].Hand[2];
            this.GroupBoxPlayer3Card3.DataContext = this.model.PlayerData[3].Hand[3];
            this.GroupBoxPlayer3Card4.DataContext = this.model.PlayerData[3].Hand[4];
            this.GroupBoxPlayer4.DataContext = this.model.PlayerData[4];
            this.GroupBoxSpeakToPlayer4.DataContext = this.model.PlayerData[4];
            this.GroupBoxPlayer4Card0.DataContext = this.model.PlayerData[4].Hand[0];
            this.GroupBoxPlayer4Card1.DataContext = this.model.PlayerData[4].Hand[1];
            this.GroupBoxPlayer4Card2.DataContext = this.model.PlayerData[4].Hand[2];
            this.GroupBoxPlayer4Card3.DataContext = this.model.PlayerData[4].Hand[3];
            this.GroupBoxPlayer4Card4.DataContext = this.model.PlayerData[4].Hand[4];

            // Build the dispatcher
            PacketDispatcher dispatcher = new PacketDispatcher();
            dispatcher.AddPacketHandler(0, OnReceiveHello);
            dispatcher.AddPacketHandler(1, OnReceiveOk);
            dispatcher.AddPacketHandler(2, OnReceiveNotOk);
            dispatcher.AddPacketHandler(3, OnReceivePeerInfoRequest);
            dispatcher.AddPacketHandler(5, OnReceivePeerInfoUpdate);
            dispatcher.AddPacketHandler(6, OnReceiveGameOptionUpdate);
            dispatcher.AddPacketHandler(7, OnReceiveGameStateUpdate);
            dispatcher.AddPacketHandler(8, OnReceiveGameEndUpdate);
            dispatcher.AddPacketHandler(9, OnReceiveActionRequest);
            dispatcher.AddPacketHandler(11, OnReceiveActionSpeakUpdate);
            dispatcher.AddPacketHandler(12, OnReceiveActionDropUpdate);
            dispatcher.AddPacketHandler(13, OnReceiveActionPlaceUpdate);
            dispatcher.AddPacketHandler(14, OnReceiveBonusEvent);
            dispatcher.AddPacketHandler(15, OnReceiveErrorEvent);

            // Build the client
            this.client = new Client(dispatcher);
        }

        private void OnReceiveHello(object sender, string packet_content)
        {
            try
            {
                HelloPacket content = JsonSerializer.Deserialize<HelloPacket>(packet_content);
                //Console.WriteLine($"The server says: '{content.message}'");

                this.model.PushHelloMessage(content.message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void OnReceiveOk(object sender, string packet_content)
        {
            try
            {
                OkPacket content = JsonSerializer.Deserialize<OkPacket>(packet_content);
                //Console.WriteLine("server -> OK");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void OnReceiveNotOk(object sender, string packet_content)
        {
            try
            { 
            NotOkPacket content = JsonSerializer.Deserialize<NotOkPacket>(packet_content);
            Console.WriteLine($"server -> KO (reason={content.reason}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void OnReceivePeerInfoRequest(object sender, string packet_content)
        {
            try
            { 
                // The info from the server
                PeerInfoRequestPacket content = JsonSerializer.Deserialize<PeerInfoRequestPacket>(packet_content);
                //Console.WriteLine($"playerId={content.playerId}");
                //Console.WriteLine($"serverVersion={content.serverVersion}");

                this.model.PlayerData[content.playerId].Me = true;

                // Send the response
                PeerInfoResponsePacket response = new PeerInfoResponsePacket();
                response.name = this.TextBoxPlayerName.Text.ToString();
                response.version = "2.7.1";
                response.versionArray = new List<int> { 2, 7, 1 };
                response.debug = false;
                response.type = "human";
                response.tags = new List<string>();
                this.client.Send<PeerInfoResponsePacket>(ref response);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void OnReceivePeerInfoUpdate(object sender, string packet_content)
        {
            try
            { 
            PeerInfoUpdatePacket content = JsonSerializer.Deserialize<PeerInfoUpdatePacket>(packet_content);
            Console.WriteLine($"'{content.name}' connected to the server and using playerId '{content.playerId}'");

            this.model.PlayerData[content.playerId].Name = content.name;
            this.model.PlayerData[content.playerId].Type = content.type;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void OnReceiveGameOptionUpdate(object sender, string packet_content)
        {
            try
            { 
                GameOptionUpdatePacket content = JsonSerializer.Deserialize<GameOptionUpdatePacket>(packet_content);
            
                this.model.CurrentMaxScore = this.model.InitialMaxScore;
                this.model.CurrentScore = 0;
                this.model.MaxErrorTokenCount = content.maxErrorTokenCount;
                this.model.StartErrorTokenCount = content.startErrorTokenCount;
                this.model.ErrorTokenCount = content.startErrorTokenCount;
                this.model.MaxSpeakTokenCount = content.maxSpeakTokenCount;
                this.model.StartSpeakTokenCount = content.startSpeakTokenCount;
                this.model.SpeakTokenCount = content.startSpeakTokenCount;
                this.model.PlayerTurnCount = 0;
                this.model.GameTurnCount = 0;
                this.model.GameOptionId = content.gameOptionId;
                this.model.CallableColors = content.callableColors;
                this.model.MinCardValue = content.minCardValue;
                this.model.MaxCardValue = content.maxCardValue;
            
                this.model.PlayerCount = content.playerCount;
                // Enable only the players that will play
                foreach (KeyValuePair<int, PlayerData> pair in this.model.PlayerData)
                {
                    if (pair.Key < this.model.PlayerCount)
                    {
                        pair.Value.Disabled = false;

                        for (int i = 0; i < pair.Value.Hand.Count; ++i)
                        {
                            if (i < content.playerHandMaxSize)
                            {
                                pair.Value.Hand[i].Disabled = false;
                            }
                        }
                    }
                }

                Dictionary<string, int> board = new Dictionary<string, int>();
                Dictionary<string, List<int>> trash = new Dictionary<string, List<int>>();
                this.model.InitialMaxScore = 0;
                foreach (ColorOption colorOption in content.colorOption)
                {
                    board.Add(colorOption.color, 0);
                    trash.Add(colorOption.color, new List<int>());
                    this.model.InitialMaxScore += colorOption.distribution.Count;
                }
                this.model.Board = board;
                this.model.Trash = trash;

                // Add the color Options
                List<ColorOptionData> colorOptions = new List<ColorOptionData>();
                foreach (ColorOption option in content.colorOption)
                {
                    ColorOptionData colorOptionData = new ColorOptionData();
                    colorOptionData.ColorName = option.color;
                    colorOptionData.IsAscending = option.isAscending;
                    colorOptionData.ReactsTo = new List<string>(option.reactsTo);
                    colorOptionData.Distribution = new Dictionary<int, int>();
                    foreach (Distribution distribution in option.distribution)
                        colorOptionData.Distribution.Add(distribution.value, distribution.count);
                    colorOptions.Add(colorOptionData);
                }
                this.model.ColorOptions = colorOptions;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void OnReceiveGameStateUpdate(object sender, string packet_content)
        {
            try
            {
                GameStateUpdatePacket content = JsonSerializer.Deserialize<GameStateUpdatePacket>(packet_content);
                this.model.StackCount = content.stack.stackCount;
                this.model.CurrentMaxScore = content.trash.currentMaxScore;
                this.model.CurrentScore = content.board.score;
                this.model.ErrorTokenCount = content.errorTokenCount;
                this.model.SpeakTokenCount = content.speakTokenCount;
                this.model.PlayerTurnCount = content.playerTurnCount;
                this.model.GameTurnCount = content.gameTurnCount;
                this.model.GameUuid = content.gameUuid;
                this.model.CurrentPlayerId = content.currentPlayerId;
                this.model.GameStarted = true;

                // Card infos
                // Loop on playerId
                for (int playerId = 0; playerId < content.playerCardInfo.Count; ++playerId)
                {
                    // Disable unused card slot
                    for (int i = content.playerCardInfo[playerId].Count; i < this.ui_max_hand_size; ++i)
                        this.model.PlayerData[playerId].Hand[i].Disabled = true;

                    // Loop on card info (for the playerId)
                    for (int index = 0; index < content.playerCardInfo[playerId].Count; ++index)
                    {
                        List<int> positiveValueInfo = new List<int>();
                        if (content.playerCardInfo[playerId][index].positiveValue > 0)
                            positiveValueInfo.Add(content.playerCardInfo[playerId][index].positiveValue);
                        this.model.PlayerData[playerId].Hand[index].PositiveValueInfo = positiveValueInfo;

                        this.model.PlayerData[playerId].Hand[index].NegativeValueInfo = new List<int>(content.playerCardInfo[playerId][index].negativeValue);
                        this.model.PlayerData[playerId].Hand[index].PositiveColorInfo = new List<string>(content.playerCardInfo[playerId][index].positiveColor);
                        this.model.PlayerData[playerId].Hand[index].NegativeColorInfo = new List<string>(content.playerCardInfo[playerId][index].negativeColor);
                    }
                }

                // Cards
                // Loop on player hands 
                foreach (PlayerHand hand in content.playerHand)
                {
                    int playerId = hand.playerId;
                    for (int i = 0; i < hand.hand.Count; ++i)
                    {
                        this.model.PlayerData[playerId].Hand[i].Hidden = false;
                        this.model.PlayerData[playerId].Hand[i].Value = hand.hand[i].value;
                        this.model.PlayerData[playerId].Hand[i].Color = hand.hand[i].color;
                    }
                }

                Dictionary<string, int> board = new Dictionary<string, int>();
                Dictionary<string, List<int>> trash = new Dictionary<string, List<int>>();
                foreach (BoardInfo pair in content.board.board)
                {
                    board.Add(pair.color, pair.value);
                    trash.Add(pair.color, new List<int>());
                }
                this.model.Board = board;

                foreach (Card card in content.trash.trash)
                {
                    trash[card.color].Add(card.value);
                }
                this.model.Trash = trash;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void OnReceiveGameEndUpdate(object sender, string packet_content)
        {
            try
            {
                GameEndUpdatePacket content = JsonSerializer.Deserialize<GameEndUpdatePacket>(packet_content);
                
                this.model.PushGameEndActionHistory(content.victory, content.gameStartTimestamp, content.gameEndTimestamp, content.statistics);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void OnReceiveActionRequest(object sender, string packet_content)
        {
            try
            {
                ActionRequestPacket content = JsonSerializer.Deserialize<ActionRequestPacket>(packet_content);
                Console.WriteLine("Action requested");
                this.model.GameIsOn = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
}

        private void OnReceiveActionSpeakUpdate(object sender, string packet_content)
        {
            try
            {
                ActionSpeakUpdatePacket content = JsonSerializer.Deserialize<ActionSpeakUpdatePacket>(packet_content);

                string info = "";
                if (content.value != null)
                {
                    info = $"value is {content.value}";
                    this.model.PushSpeakValueActionHistory(content.sourcePlayerId, content.targetPlayerId, content.value.Value);
                }
                else if (content.color != null && content.color.Length > 0)
                {
                    this.model.PushSpeakColorActionHistory(content.sourcePlayerId, content.targetPlayerId, content.color);
                    info = $"color is {content.color}";
                }
                else
                    Console.WriteLine("Unknown card info");

                //Console.WriteLine($"Player {content.sourcePlayerId} speaked to player {content.targetPlayerId} saying '{info}'");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void OnReceiveActionDropUpdate(object sender, string packet_content)
        {
            try
            {
                ActionDropUpdatePacket content = JsonSerializer.Deserialize<ActionDropUpdatePacket>(packet_content);

                this.model.PushDropActionHistory(content.sourcePlayerId, content.handIndex, content.card);

                Console.WriteLine($"Player {content.sourcePlayerId} droped {content.card}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void OnReceiveActionPlaceUpdate(object sender, string packet_content)
        {
            try
            {
                ActionPlaceUpdatePacket content = JsonSerializer.Deserialize<ActionPlaceUpdatePacket>(packet_content);

                this.model.PushPlaceActionHistory(content.sourcePlayerId, content.handIndex, content.card);

                Console.WriteLine($"Player {content.sourcePlayerId} placed {content.card}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void OnReceiveBonusEvent(object sender, string packet_content)
        {
            try
            {
                BonusEventPacket content = JsonSerializer.Deserialize<BonusEventPacket>(packet_content);

                this.model.PushBonusActionHistory(content.playerId, content.color);

                Console.WriteLine($"Player {content.playerId} finished {content.color} and get a bonus for that");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void OnReceiveErrorEvent(object sender, string packet_content)
        {
            try
            {
                ErrorEventPacket content = JsonSerializer.Deserialize<ErrorEventPacket>(packet_content);

                this.model.PushErrorActionHistory(content.playerId, content.errorTokenCount);

                Console.WriteLine($"Player {content.playerId} made an error, the error count is now {content.errorTokenCount}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }


        private void Button_Connect_Click(object sender, RoutedEventArgs e)
        {
            if (this.TextBoxPlayerName.Text.Length == 0)
            {
                MessageBox.Show(this, "Please define a player name", "Error", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                return;
            }

            try
            {
                this.InitiateGame();

                // Connect to the server
                IPAddress ip_address = Dns.GetHostAddresses(this.TextBoxHost.Text)[0];
                int port = Int32.Parse(this.TextBoxPort.Text);
                IPEndPoint ip_endpoint = new IPEndPoint(ip_address, port);
                this.client.Connect(ip_endpoint);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Unable to initiate TCP connnection: " + ex.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                return;
            }
        }

        private void Button_Default_Click(object sender, RoutedEventArgs e)
        {
#if DEBUG
            this.TextBoxHost.Text = "127.0.0.1";
            this.TextBoxPort.Text = "32000";
#else
            this.TextBoxHost.Text = "ironknee.ovh";
            this.TextBoxPort.Text = "32000";
#endif
        }

        private void Button_Drop_Card0(object sender, RoutedEventArgs e)
        {
            ActionResponsePacket response = new ActionResponsePacket();
            response.actionType = "drop";
            response.handIndex = 0;
            this.client.Send<ActionResponsePacket>(ref response);
            this.model.GameIsOn = false;
        }

        private void Button_Drop_Card1(object sender, RoutedEventArgs e)
        {
            ActionResponsePacket response = new ActionResponsePacket();
            response.actionType = "drop";
            response.handIndex = 1;
            this.client.Send<ActionResponsePacket>(ref response);
            this.model.GameIsOn = false;
        }

        private void Button_Drop_Card2(object sender, RoutedEventArgs e)
        {
            ActionResponsePacket response = new ActionResponsePacket();
            response.actionType = "drop";
            response.handIndex = 2;
            this.client.Send<ActionResponsePacket>(ref response);
            this.model.GameIsOn = false;
        }

        private void Button_Drop_Card3(object sender, RoutedEventArgs e)
        {
            ActionResponsePacket response = new ActionResponsePacket();
            response.actionType = "drop";
            response.handIndex = 3;
            this.client.Send<ActionResponsePacket>(ref response);
            this.model.GameIsOn = false;
        }

        private void Button_Drop_Card4(object sender, RoutedEventArgs e)
        {
            ActionResponsePacket response = new ActionResponsePacket();
            response.actionType = "drop";
            response.handIndex = 4;
            this.client.Send<ActionResponsePacket>(ref response);
            this.model.GameIsOn = false;
        }

        private void Button_Place_Card0(object sender, RoutedEventArgs e)
        {
            ActionResponsePacket response = new ActionResponsePacket();
            response.actionType = "place";
            response.handIndex = 0;
            this.client.Send<ActionResponsePacket>(ref response);
            this.model.GameIsOn = false;
        }

        private void Button_Place_Card1(object sender, RoutedEventArgs e)
        {
            ActionResponsePacket response = new ActionResponsePacket();
            response.actionType = "place";
            response.handIndex = 1;
            this.client.Send<ActionResponsePacket>(ref response);
            this.model.GameIsOn = false;
        }

        private void Button_Place_Card2(object sender, RoutedEventArgs e)
        {
            ActionResponsePacket response = new ActionResponsePacket();
            response.actionType = "place";
            response.handIndex = 2;
            this.client.Send<ActionResponsePacket>(ref response);
            this.model.GameIsOn = false;
        }

        private void Button_Place_Card3(object sender, RoutedEventArgs e)
        {
            ActionResponsePacket response = new ActionResponsePacket();
            response.actionType = "place";
            response.handIndex = 3;
            this.client.Send<ActionResponsePacket>(ref response);
            this.model.GameIsOn = false;
        }

        private void Button_Place_Card4(object sender, RoutedEventArgs e)
        {
            ActionResponsePacket response = new ActionResponsePacket();
            response.actionType = "place";
            response.handIndex = 4;
            this.client.Send<ActionResponsePacket>(ref response);
            this.model.GameIsOn = false;
        }

        private void SpeakValue(int targetPlayerId, int value)
        {
            ActionResponsePacket response = new ActionResponsePacket();
            response.actionType = "speak";
            response.cardInfo = new CardInfo();
            response.cardInfo.value = value;
            response.targetPlayerId = targetPlayerId;
            this.client.Send<ActionResponsePacket>(ref response);
            this.model.GameIsOn = false;
        }

        private void SpeakColor(int targetPlayerId, string color)
        {
            ActionResponsePacket response = new ActionResponsePacket();
            response.actionType = "speak";
            response.cardInfo = new CardInfo();
            response.cardInfo.color = color;
            response.targetPlayerId = targetPlayerId;
            this.client.Send<ActionResponsePacket>(ref response);
            this.model.GameIsOn = false;
        }

        private void Button_Speak_To_Player0_1(object sender, RoutedEventArgs e)
        {
            this.SpeakValue(0, 1);
        }

        private void Button_Speak_To_Player0_2(object sender, RoutedEventArgs e)
        {
            this.SpeakValue(0, 2);
        }

        private void Button_Speak_To_Player0_3(object sender, RoutedEventArgs e)
        {
            this.SpeakValue(0, 3);
        }

        private void Button_Speak_To_Player0_4(object sender, RoutedEventArgs e)
        {
            this.SpeakValue(0, 4);
        }

        private void Button_Speak_To_Player0_5(object sender, RoutedEventArgs e)
        {
            this.SpeakValue(0, 5);
        }

        private void Button_Speak_To_Player0_Blue(object sender, RoutedEventArgs e)
        {
            this.SpeakColor(0, "blue");
        }

        private void Button_Speak_To_Player0_White(object sender, RoutedEventArgs e)
        {
            this.SpeakColor(0, "white");
        }

        private void Button_Speak_To_Player0_Red(object sender, RoutedEventArgs e)
        {
            this.SpeakColor(0, "red");
        }

        private void Button_Speak_To_Player0_Yellow(object sender, RoutedEventArgs e)
        {
            this.SpeakColor(0, "yellow");
        }

        private void Button_Speak_To_Player0_Green(object sender, RoutedEventArgs e)
        {
            this.SpeakColor(0, "green");
        }

        private void Button_Speak_To_Player1_1(object sender, RoutedEventArgs e)
        {
            this.SpeakValue(1, 1);
        }

        private void Button_Speak_To_Player1_2(object sender, RoutedEventArgs e)
        {
            this.SpeakValue(1, 2);
        }

        private void Button_Speak_To_Player1_3(object sender, RoutedEventArgs e)
        {
            this.SpeakValue(1, 3);
        }

        private void Button_Speak_To_Player1_4(object sender, RoutedEventArgs e)
        {
            this.SpeakValue(1, 4);
        }

        private void Button_Speak_To_Player1_5(object sender, RoutedEventArgs e)
        {
            this.SpeakValue(1, 5);
        }

        private void Button_Speak_To_Player1_Blue(object sender, RoutedEventArgs e)
        {
            this.SpeakColor(1, "blue");
        }

        private void Button_Speak_To_Player1_White(object sender, RoutedEventArgs e)
        {
            this.SpeakColor(1, "white");
        }

        private void Button_Speak_To_Player1_Red(object sender, RoutedEventArgs e)
        {
            this.SpeakColor(1, "red");
        }

        private void Button_Speak_To_Player1_Yellow(object sender, RoutedEventArgs e)
        {
            this.SpeakColor(1, "yellow");
        }

        private void Button_Speak_To_Player1_Green(object sender, RoutedEventArgs e)
        {
            this.SpeakColor(1, "green");
        }

        private void Button_Speak_To_Player2_1(object sender, RoutedEventArgs e)
        {
            this.SpeakValue(2, 1);
        }

        private void Button_Speak_To_Player2_2(object sender, RoutedEventArgs e)
        {
            this.SpeakValue(2, 2);
        }

        private void Button_Speak_To_Player2_3(object sender, RoutedEventArgs e)
        {
            this.SpeakValue(2, 3);
        }

        private void Button_Speak_To_Player2_4(object sender, RoutedEventArgs e)
        {
            this.SpeakValue(2, 4);
        }

        private void Button_Speak_To_Player2_5(object sender, RoutedEventArgs e)
        {
            this.SpeakValue(2, 5);
        }

        private void Button_Speak_To_Player2_Blue(object sender, RoutedEventArgs e)
        {
            this.SpeakColor(2, "blue");
        }

        private void Button_Speak_To_Player2_White(object sender, RoutedEventArgs e)
        {
            this.SpeakColor(2, "white");
        }

        private void Button_Speak_To_Player2_Red(object sender, RoutedEventArgs e)
        {
            this.SpeakColor(2, "red");
        }

        private void Button_Speak_To_Player2_Yellow(object sender, RoutedEventArgs e)
        {
            this.SpeakColor(2, "yellow");
        }

        private void Button_Speak_To_Player2_Green(object sender, RoutedEventArgs e)
        {
            this.SpeakColor(2, "green");
        }

        private void Button_Speak_To_Player3_1(object sender, RoutedEventArgs e)
        {
            this.SpeakValue(3, 1);
        }

        private void Button_Speak_To_Player3_2(object sender, RoutedEventArgs e)
        {
            this.SpeakValue(3, 2);
        }

        private void Button_Speak_To_Player3_3(object sender, RoutedEventArgs e)
        {
            this.SpeakValue(3, 3);
        }

        private void Button_Speak_To_Player3_4(object sender, RoutedEventArgs e)
        {
            this.SpeakValue(3, 4);
        }

        private void Button_Speak_To_Player3_5(object sender, RoutedEventArgs e)
        {
            this.SpeakValue(3, 5);
        }

        private void Button_Speak_To_Player3_Blue(object sender, RoutedEventArgs e)
        {
            this.SpeakColor(3, "blue");
        }

        private void Button_Speak_To_Player3_White(object sender, RoutedEventArgs e)
        {
            this.SpeakColor(3, "white");
        }

        private void Button_Speak_To_Player3_Red(object sender, RoutedEventArgs e)
        {
            this.SpeakColor(3, "red");
        }

        private void Button_Speak_To_Player3_Yellow(object sender, RoutedEventArgs e)
        {
            this.SpeakColor(3, "yellow");
        }

        private void Button_Speak_To_Player3_Green(object sender, RoutedEventArgs e)
        {
            this.SpeakColor(3, "green");
        }

        private void Button_Speak_To_Player4_1(object sender, RoutedEventArgs e)
        {
            this.SpeakValue(4, 1);
        }

        private void Button_Speak_To_Player4_2(object sender, RoutedEventArgs e)
        {
            this.SpeakValue(4, 2);
        }

        private void Button_Speak_To_Player4_3(object sender, RoutedEventArgs e)
        {
            this.SpeakValue(4, 3);
        }

        private void Button_Speak_To_Player4_4(object sender, RoutedEventArgs e)
        {
            this.SpeakValue(4, 4);
        }

        private void Button_Speak_To_Player4_5(object sender, RoutedEventArgs e)
        {
            this.SpeakValue(4, 5);
        }

        private void Button_Speak_To_Player4_Blue(object sender, RoutedEventArgs e)
        {
            this.SpeakColor(4, "blue");
        }

        private void Button_Speak_To_Player4_White(object sender, RoutedEventArgs e)
        {
            this.SpeakColor(4, "white");
        }

        private void Button_Speak_To_Player4_Red(object sender, RoutedEventArgs e)
        {
            this.SpeakColor(4, "red");
        }

        private void Button_Speak_To_Player4_Yellow(object sender, RoutedEventArgs e)
        {
            this.SpeakColor(4, "yellow");
        }

        private void Button_Speak_To_Player4_Green(object sender, RoutedEventArgs e)
        {
            this.SpeakColor(4, "green");
        }

        private void ButtonGameOptions_Click(object sender, RoutedEventArgs e)
        {
            this.game_options = new GameOptions(this.model);
            this.game_options.Owner = this;
            this.game_options.ShowDialog();
        }
    }
}
