# Hanabi Viewer

## Build image

To build the image, run this command from the viewer directory:

```
# Build the docker image
docker build -f .\Dockerfile -t registry.gitlab.com/rafaouel/hanabi/hanabi_viewer:<viewer_version> .
```

To push the images to the registry :
```
# Login
docker login registry.gitlab.com/rafaouel/hanabi
# Enter the username then a project access token

# Push image
docker push registry.gitlab.com/rafaouel/hanabi/hanabi_viewer:<viewer_version>
```

## Run image

### Docker

To run the prod image :

```
docker run -it --rm -d --name viewer -p 1337:80 registry.gitlab.com/rafaouel/hanabi/hanabi_viewer:<viewer_version>
```

And go to http://localhost:1337

### Kubernetes

To run the prod image :

Apply the deployment manifest: 

``` yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    workload.user.cattle.io/workloadselector: apps.deployment-hanabi-viewer
  name: viewer
  namespace: hanabi
spec:
  replicas: 1
  selector:
    matchLabels:
      workload.user.cattle.io/workloadselector: apps.deployment-hanabi-viewer
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      labels:
        workload.user.cattle.io/workloadselector: apps.deployment-hanabi-viewer
      namespace: hanabi
    spec:
      containers:
        - image: registry.gitlab.com/rafaouel/hanabi/hanabi_viewer:<viewer_version>
          imagePullPolicy: IfNotPresent
          name: container-0
          ports:
            - containerPort: 80
              name: 80tcp
              protocol: TCP
          readinessProbe:
            failureThreshold: 3
            httpGet:
              path: /
              port: 80
              scheme: HTTP
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 1        
```

Apply the service manifest:

``` yaml
apiVersion: v1
kind: Service
metadata:
  name: viewer
  namespace: hanabi
spec:
  ports:
    - name: 80tcp
      port: 80
      protocol: TCP
      targetPort: 80
  selector:
    workload.user.cattle.io/workloadselector: apps.deployment-hanabi-viewer
  type: ClusterIP
```

Apply the ingress manifest:

``` yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
  name: viewer
  namespace: hanabi
spec:
  ingressClassName: nginx
  rules:
    - host: viewer.hanabi.lucifer.k8
      http:
        paths:
          - backend:
              service:
                name: viewer
                port:
                  number: 80
            path: /
            pathType: Prefix
```
