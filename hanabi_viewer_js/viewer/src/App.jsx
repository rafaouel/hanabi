import * as React from 'react';

import Box from '@mui/material/Box';

import * as API from './tools/api_gateway.js';
import * as Cache from './tools/cache.js';
import * as Optimal from './tools/optimal_gateway.js';

import Application from './components/application.jsx';
import SignInPage from './components/signinpage.jsx';


export default function App() {

    // Authentication
    const [isAuthenticated, setIsAuthenticated] = React.useState(false);

    // API options
    //const [apiURL] = React.useState(import.meta.env.VITE_API_URL);
    const [apiHost] = React.useState(window.DYNAMIC_PROPERTIES.API_HOST);
    const [apiPort] = React.useState(window.DYNAMIC_PROPERTIES.API_PORT);
    //const [optimalURL] = React.useState(import.meta.env.VITE_OPTIMAL_URL);
    const [optimalHost] = React.useState(window.DYNAMIC_PROPERTIES.OPTIMAL_HOST);
    const [optimalPort] = React.useState(window.DYNAMIC_PROPERTIES.OPTIMAL_PORT);

    // Objects to serve the application
    const [apiGateway, setApiGateway] = React.useState(null);
    const [dataCache, setDataCache] = React.useState(null);
    const [optimalGateway, setOptimalGateway] = React.useState(null);
    const [optimalCache, setOptimalCache] = React.useState(null);


    // Create the api gateway and the associated cache
    React.useEffect(() => {

        // Create the api gateway
        const apiUrl = `http://${apiHost}:${apiPort}/`;
        const gateway = new API.APIGateway(apiUrl);
        setApiGateway(gateway);

        // Create the data cache
        const cache = new Cache.GameDataCache(gateway);
        setDataCache(cache);

    }, [apiHost, apiPort]);

    // Create the optimal api gateway and the associated cache
    React.useEffect(() => {

        // Create the gateway
        const optimalUrl = `http://${optimalHost}:${optimalPort}/`;
        const gateway = new Optimal.OptimalGateway(optimalUrl);
        setOptimalGateway(gateway);

        // Create the cache
        const cache = new Cache.OptimalCache(gateway);
        setOptimalCache(cache);

    }, [optimalHost, optimalPort]);


    // Authentication
    const signIn = async (_provider, formData) => {

        // Get the credentials
        const username = formData?.get('username');
        const password = formData?.get('password');

        // Try to connect to the API
        const connected = await apiGateway.connect(username, password);

        if (connected) {

            setIsAuthenticated(true);
            return;
        }
        else {

            setIsAuthenticated(false);
            return new Promise((resolve) => {
                resolve({
                    type: "CredentialsSignin",
                    error: "Invalid credentials"
                });
            });
        }
    }
    
    return (
        <Box sx={{ width: '100%' }}>

            {isAuthenticated 
                ? <Application
                    apiGateway={apiGateway}
                    dataCache={dataCache}
                    optimalCache={optimalCache}
                />
                : <SignInPage
                    signIn={signIn}
                />
            }
        </Box>        
    );
}