import * as React from 'react';
import PropTypes from 'prop-types';

import TextField from '@mui/material/TextField';
import { AppProvider } from '@toolpad/core/AppProvider';
import { SignInPage } from '@toolpad/core/SignInPage';
import { useTheme } from '@mui/material/styles';

const providers = [{ id: 'credentials', name: 'username and password' }];

function CustomEmailField() {
    return (
        <TextField
            id="input-with-icon-textfield"
            label="Username"
            name="username"
            type="name"
            size="small"
            required
            fullWidth
            variant="outlined"
        />
    );
}

export default function SignIn(props) {

    const {
        signIn
    } = props;

    const theme = useTheme();
    return (
        <AppProvider theme={theme}>
            <SignInPage
                signIn={signIn}
                providers={providers}
                slots={{
                    emailField: CustomEmailField
                }}
            />
        </AppProvider>
    );
}

SignIn.propTypes = {
    signIn: PropTypes.func.isRequired,
};