import * as React from 'react';
import PropTypes from 'prop-types';

import Button from '@mui/material/Button';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Slider from '@mui/material/Slider';
import Stack from '@mui/material/Stack';
import Switch from '@mui/material/Switch';
import Typography from '@mui/material/Typography';

import GameStateDetails from './gamestate_details.jsx';
import PlayerActionDetails from './playeraction_details.jsx';


const valueText = (value) => {
    return `${value}`;
}


export default function GameDetails(props) {

    const {
        open,
        onClose,
        gameUUID,
        dataCache,
        api,
    } = props;

    const [maxPlayerTurnCount, setMaxPlayerTurnCount] = React.useState(0);
    const [playerTurnCount, setPlayerTurnCount] = React.useState(0);
    const [leftGameState, setLeftGameState] = React.useState([]);
    const [playerAction, setPlayerAction] = React.useState([]);
    const [rightGameState, setRightGameState] = React.useState([]);

    // Live mode
    const [isLiveModeOn, setIsLiveModeOn] = React.useState(false);
    const [liveModeDelay, setLiveModeDelay] = React.useState(2000);
    const [isLiveModeAllowed, setIsLiveModeAllowed] = React.useState(false);

    const getMaxPlayerTurnCount = async () => {
        const overview = await api.getGameSummary({ gameUUID });
        if (overview !== undefined) {
            return overview.data[0].playerTurnCount - 1;
        }
        else {
            return 0;
        }
    }

    React.useEffect(() => {
        
        getMaxPlayerTurnCount().then(count => {
            setMaxPlayerTurnCount(count);
        });

    }, [gameUUID]);

    // Initial loading of the infos
    React.useEffect(() => {
        loadGameTurnInfo(gameUUID, 0);
    }, []);

    // Set the auto-refresh associated to the live mode
    React.useEffect(() => {
        const intervalId = setInterval(async () => {

            if (isLiveModeOn) {
                // Update the max player turn count
                getMaxPlayerTurnCount().then(count => {

                    // Stick the player turn count to the max
                    setPlayerTurnCount(count);
                    setMaxPlayerTurnCount(count);

                    // Load the data
                    loadGameTurnInfo(gameUUID, playerTurnCount);
                });
            }

        }, liveModeDelay);

        return () => { clearInterval(intervalId); }

    }, [isLiveModeOn, liveModeDelay, gameUUID, playerTurnCount, maxPlayerTurnCount]);

    // To disable the live mode if the game has ended
    React.useEffect(() => {
        try {
            dataCache.gameEnd(gameUUID).then(end => {
                if (end === undefined) {
                    setIsLiveModeAllowed(true);
                    return;
                }
                setIsLiveModeAllowed(false);
                setIsLiveModeOn(false);
            });
        }
        catch (_err) {
            setIsLiveModeAllowed(true);
        }
    }, [isLiveModeOn, isLiveModeAllowed, gameUUID, dataCache]);

    const onToggleLiveMode = () => {
        setIsLiveModeOn(!isLiveModeOn && isLiveModeAllowed);
    }

    const handleClose = () => {
        onClose();
    }

    // Convert trash format from a list of card to a 'board like' map
    const convertTrashFormat = (cardList, colorList) => {

        let trashBoard = {};
        colorList.forEach(color => {
            trashBoard[color] = [];
        });

        for (const card of cardList) {
            trashBoard[card.color].push(card.value);
        }

        return trashBoard;
    }

    // Convert a game state into front end data
    const loadGameStateData = (gameState, playerList, gameOption) => {

        const gameStateTree = [];
        let cpt = 0;

        const createNode = (id, label) => {
            return { id, label };
        }

        gameStateTree.push(createNode("score", `Score ${gameState.board.score}/${gameState.trash.currentMaxScore}/${gameOption.maxScore}`));
        gameStateTree.push(createNode("errorTokenCount", `Error ${gameState.errorTokenCount}/${gameOption.maxErrorTokenCount}`));
        gameStateTree.push(createNode("speakTokenCount", `Speak ${gameState.speakTokenCount}/${gameOption.maxSpeakTokenCount}`));

        let trash = {
            id: 'trash',
            label: `Trash (${gameState.trash.trash.length} card(s))`,
            children: []
        }
        const colorList = gameState.board.board.map(item => { return item.color; });
        const trashMap = convertTrashFormat(gameState.trash.trash, colorList);
        for (const color of Object.keys(trashMap)) {
            trash.children.push(createNode(`trash-${color}`, color + ": " + trashMap[color].join(", ")));
        }
        gameStateTree.push(trash);

        let board = {
            id: 'board',
            label: `Board (${gameState.board.score} card(s))`,
            children: [],
        }
        for (const pair of gameState.board.board) {
            const color = pair.color;
            const value = pair.value;
            const text = (color + ": ");
            board.children.push(createNode(`board-${color}`, text + value));
        }
        gameStateTree.push(board);

        for (let playerId = 0; playerId < gameState.playerCardInfo.length; ++playerId) {

            let playerHand = {
                id: `playerHand-${playerId}`,
                label: `Hand of '${playerList[playerId].name}' (${gameState.playerCardInfo[playerId].length} card(s))`,
                children: []
            }

            for (let cardId = 0; cardId < gameState.playerCardInfo[playerId].length; ++cardId) {

                // Cards
                const card = gameState.playerHand[playerId][cardId];

                let cardData = {
                    id: `playerHand-${playerId}-${cardId}`,
                    label: cardId + ": " + card.value + " " + card.color,
                    children: []
                }

                // Infos
                const cardInfo = gameState.playerCardInfo[playerId][cardId];
                if (cardInfo.positiveValue !== 0) {
                    cardData.children.push(createNode(`playerHand-${playerId}-${cardId}-info-value+`, "value+ " + cardInfo.positiveValue.toString()));
                }
                if (cardInfo.negativeValue.length > 0) {
                    cardData.children.push(createNode(`playerHand-${playerId}-${cardId}-info-value-`, "value- " + cardInfo.negativeValue.join(", ")));
                }
                if (cardInfo.positiveColor.length > 0) {
                    cardData.children.push(createNode(`playerHand-${playerId}-${cardId}-info-color+`, "color+ " + cardInfo.positiveColor.join(", ")));
                }
                if (cardInfo.negativeColor.length > 0) {
                    cardData.children.push(createNode(`playerHand-${playerId}-${cardId}-info-color-`, "color- " + cardInfo.negativeColor.join(", ")));
                }

                playerHand.children.push(cardData);
            }

            gameStateTree.push(playerHand);
        }

        let stack = {
            id: 'stack',
            label: `Stack (${gameState.stack.length} card(s))`,
            children: []
        }
        cpt = 0;
        for (const card of gameState.stack) {
            stack.children.push(createNode("stack-card" + cpt, card.value + " " + card.color));
            cpt++;
        }
        gameStateTree.push(stack);

        return gameStateTree;
    }

    // Convert all the data about a turn into front end data
    const loadGameTurnInfo = async (gameUUID, playerTurnCount) => {

        // Get info about players
        const playerList = await dataCache.peerInfo(gameUUID, false);

        // Get game option
        const gameStart = await dataCache.gameStart(gameUUID, true);
        const gameOption = await dataCache.gameOption(gameStart.gameOptionId, true);

        // Load left game state
        const leftGameStateData = await dataCache.gameState(gameUUID, playerTurnCount);
        if (leftGameStateData !== undefined) {
            setLeftGameState(loadGameStateData(leftGameStateData, playerList, gameOption));
        }
        else {
            setLeftGameState([]);
        }

        // Load player action
        const playerAction = await dataCache.playerAction(gameUUID, playerTurnCount);
        if (playerAction !== undefined) {

            const createNode = (id, label) => {
                return { id, label };
            }

            let playerActionData = [
                createNode("currentPlayer", `Turn of '${playerList[playerAction.currentPlayerId].name}' (id=${playerAction.currentPlayerId})`),
            ];
            if (playerAction.actionType === 'speak') {
                playerActionData.push(createNode("actionType", `Action is '${playerAction.actionType}' to '${playerList[playerAction.targetPlayerId].name}' (id=${playerAction.targetPlayerId})`));
                let info = ""
                if (playerAction.cardInfo.color !== undefined) {
                    info = `color is ${playerAction.cardInfo.color}`;
                }
                else if (playerAction.cardInfo.value !== undefined) {
                    info = `value is ${playerAction.cardInfo.value}`;
                }
                else {
                    info = "Error";
                }
                playerActionData.push(createNode("cardInfo", `Card info is '${info}'`));
            }
            else if (playerAction.actionType === 'drop') {
                const card = leftGameStateData.playerHand[playerAction.currentPlayerId][playerAction.handIndex];
                const cardText = `${card.value} ${card.color}`;
                playerActionData.push(createNode("actionType", `Action is '${playerAction.actionType}' on ${cardText} (hand index=${playerAction.handIndex})`));
            }
            else if (playerAction.actionType === 'place') {
                const card = leftGameStateData.playerHand[playerAction.currentPlayerId][playerAction.handIndex];
                const cardText = `${card.value} ${card.color}`;
                playerActionData.push(createNode("actionType", `Action is '${playerAction.actionType}' on ${cardText} (hand index=${playerAction.handIndex})`));
            }
            else {
                // Error
            }
            playerActionData.push({
                id: "roundtrip",
                label: `Roundtrip (duration ${playerAction.roundtripDuration}s)`,
                children: [
                    createNode("requestTimestamp", `Request ${playerAction.requestTimestamp}`),
                    createNode("responseTimestamp", `Response ${playerAction.responseTimestamp}`),
                ]
            });
            
            setPlayerAction(playerActionData);
        }
        else {
            setPlayerAction([]);
        }

        // Load right game state
        const rightGameStateData = await dataCache.gameState(gameUUID, playerTurnCount + 1);
        if (rightGameStateData !== undefined) {
            setRightGameState(loadGameStateData(rightGameStateData, playerList, gameOption));
        }
        else {
            setRightGameState([]);
        }
    }

    const onSliderTurnCountChange = (_event, newValue) => {

        setPlayerTurnCount(newValue);

        // Load the game turn infos
        loadGameTurnInfo(gameUUID, newValue);
    }

    return (
        <Dialog
            onClose={handleClose}
            open={open}
            fullScreen
            maxWidth="xl"
        >

            <Stack direction="row">
                <Button
                    onClick={handleClose}
                    variant="contained"
                    sx={{ml:2, mt:2, mb:2} }
                >Close</Button>
                <DialogTitle>Game details - {gameUUID}</DialogTitle>
            </Stack>

            <Stack
                direction="column"
                sx={{ml:3, mr:3}}
            >

                <Stack direction="column" spacing={1}>

                    <Typography align="center" gutterBottom>Player turn #{playerTurnCount}</Typography>

                    <FormGroup>
                        <FormControlLabel
                            disabled={!isLiveModeAllowed}
                            control={
                                <Switch
                                    checked={isLiveModeOn && isLiveModeAllowed}
                                    onChange={onToggleLiveMode}
                                    inputProps={{ 'aria-label': 'controlled' }}
                                />}
                            label="Live mode"
                            />
                    </FormGroup>

                    <Slider
                        aria-label="Turn count"
                        value={playerTurnCount}
                        getAriaValueText={valueText}
                        valueLabelDisplay="auto"
                        shiftStep={5}
                        step={1}
                        marks
                        min={0}
                        max={maxPlayerTurnCount}
                        onChange={onSliderTurnCountChange}
                        disabled={isLiveModeOn && isLiveModeAllowed}
                        />
                </Stack>

                <Stack id="stack-game" direction="row" spacing={1}>
                    <GameStateDetails
                        title="Left game state"
                        gameUuid={gameUUID}
                        dataCache={dataCache}
                        playerTurnCount={playerTurnCount}
                    />

                    
                    <PlayerActionDetails
                        name="Player action"
                        data={playerAction}
                    />

                    <GameStateDetails
                        title="Right game state"
                        gameUuid={gameUUID}
                        dataCache={dataCache}
                        playerTurnCount={playerTurnCount + 1}
                    />
                </Stack>
            </Stack>
        </Dialog>
    );
}

GameDetails.propTypes = {
    open: PropTypes.bool.isRequired,
    gameUUID: PropTypes.string.isRequired,
    onClose: PropTypes.func.isRequired,
    dataCache: PropTypes.object.isRequired,
    api: PropTypes.object.isRequired,
};