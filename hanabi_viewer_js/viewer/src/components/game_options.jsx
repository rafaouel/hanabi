import * as React from 'react';
import PropTypes from 'prop-types';

import {
    Box,
    Button,
    Stack,
} from '@mui/material';

import { DataGrid } from '@mui/x-data-grid';

import { RichTreeView } from '@mui/x-tree-view/RichTreeView';

import EditTags from './edit_tags.jsx';


export default function GameOptions(props) {

    const {
        apiGateway,
        dataCache,
        optimalCache,
    } = props;

    // Selected game Option
    const [selectedGameOption, setSelectedGameOption] = React.useState(null);

    // Tag edition
    const [openEditTags, setOpenEditTags] = React.useState(false);
    const [currentTags, setCurrentTags] = React.useState([]);

    const handleOpenEditTags = () => {
        setOpenEditTags(true);
    }

    const handleCloseEditTags = async (newTags) => {

        setOpenEditTags(false);

        // If tags have been changed
        if (newTags) {
            if (selectedGameOption) {
                const hasChanged = await apiGateway.gameOptionReplaceTags(selectedGameOption.gameOptionId, newTags);
                setNeedUpdate(true);
            }
        }
    }

    // Game option data
    const [needUpdate, setNeedUpdate] = React.useState(true);

    const gameOptionsColumns = [
        {
            field: 'id',
            headerName: 'id',
            width: 225,
            align: 'center',
            description: 'Seed of the game',
            hide: true,
        },
        {
            field: 'gameOptionId',
            headerName: 'Game Option Id',
            width: 225,
            align: 'center',
            description: 'Seed of the game',
        },
        {
            field: 'playerCount',
            headerName: 'Player',
            width: 60,
            align: 'center',
            description: 'Number of player in the game'
        },
        {
            field: 'reserved',
            headerName: "Reserved",
            width: 80,
            type: 'boolean',
            align: 'center',
            description: "'false' if the game option has never been played"
        },
        {
            field: 'optimalScore',
            headerName: 'AI score',
            description: "Score realized by 'Optimal' AI",
            width: 75,
            align: 'center',
            valueGetter: (value, row) => {
                if (!row.optimalScore) {
                    return null;
                }
                return row.optimalScore;
            },
            valueFormatter: (value, row) => {
                if (!row.optimalScore || !row.maxScore) {
                    return "?";
                }
                return `${row.optimalScore}/${row.maxScore}`;
            },
            sortable: false,        // At least for the moment
        },
        {
            field: 'isMaxScore',
            headerName: "Perfect",
            width: 80,
            type: 'boolean',
            align: 'center',
            description: "'true' if the AI succeeded in reaching the max score",
            valueGetter: (value, row) => {
                if (!row.optimalScore || !row.maxScore) {
                    return null;
                }
                return row.optimalScore == row.maxScore;
            },
            sortable: false,
        },
        {
            field: 'optimalTurnCount',
            headerName: 'AI turn',
            description: "Player turn count realized by 'Optimal' AI",
            width: 120,
            align: 'center',
            valueGetter: (value, row) => {
                if (!row.optimalPlayerTurnCount || !row.optimalGameTurnCount) {
                    return null;
                }
                return row.optimalPlayerTurnCount;
            },
            valueFormatter: (value, row) => {
                if (!row.optimalPlayerTurnCount || !row.optimalGameTurnCount) {
                    return "?";
                }
                return `${row.optimalPlayerTurnCount} - ${row.optimalGameTurnCount}`;
            },
            sortable: false,    // At least for the moment
        },
        {
            field: 'timestamp',
            headerName: 'Creation',
            type: Date,
            width: 200,
            align: 'center',
            description: 'Game creation timestamp'
        },
        {
            field: 'tags',
            headerName: 'Tags',
            width: 300,
            description: 'Game list of tags',
            sortable: false,
        }
    ];

    // Pagination
    const [paginationModel, setPaginationModel] = React.useState({
        page: 0,
        pageSize: 25,
    });
    const [totalRowCount, setTotalRowCount] = React.useState(0);
    const rowCountRef = React.useRef(totalRowCount || 0);
    const rowCount = React.useMemo(() => {
        if (totalRowCount !== undefined) {
            rowCountRef.current = totalRowCount;
        }
        return rowCountRef.current;
    }, [totalRowCount]);

    // Sorting
    const [sortModel, setSortModel] = React.useState([{
        "field": "timestamp",
        "sort": "desc"
    }]);

    // Filtering
    const [filterModel, setFilterModel] = React.useState({});


    const [isGameOptionsLoading, setIsGameOptionsLoading] = React.useState(false);

    // Data
    const [gameOptionsRows, setGameOptionsRows] = React.useState([]);
    const [gameOptionTree, setGameOptionTree] = React.useState([]);

    const expandedItems = ["tags"];

    // Load on first access
    React.useEffect(() => {
        loadGameOption(apiGateway, dataCache, optimalCache);
        setNeedUpdate(false);
    }, [needUpdate, apiGateway, dataCache, optimalCache, paginationModel, filterModel, sortModel]);


    const loadGameOption = async (apiGateway, dataCache, optimalCache) => {

        setIsGameOptionsLoading(true);

        let page = [];

        // Query parameters
        let queryParams = {
            // Pagination
            ...paginationModel,
        };

        // Sorting
        queryParams["sort_by"] = sortModel.map(item => {
            return `${item.field}[${item.sort}]`
        }).join(",");

        // TODO Add filtering
        console.log(`filterModel ${JSON.stringify(filterModel)}`);

        const gameOptionResponse = await apiGateway.getGameOption(queryParams);

        if (gameOptionResponse) {
            setTotalRowCount(gameOptionResponse.metadata.documentCount);
            for (const gameOption of gameOptionResponse.data) {

                let row = {
                    id: gameOption.gameOptionId,
                    gameOptionId: gameOption.gameOptionId,
                    reserved: gameOption.reserved,
                    tags: gameOption.tags,
                    timestamp: gameOption.timestamp,
                    maxScore: gameOption.maxScore,
                    playerCount: gameOption.playerCount,
                }

                // Call optimal for the score
                const optimalData = await optimalCache.submit(gameOption.gameOptionId);
                if (optimalData) {
                    row.optimalScore = optimalData.score;
                    row.optimalPlayerTurnCount = optimalData.playerTurnCount;
                    row.optimalGameTurnCount = optimalData.gameTurnCount;
                }

                page.push(row);
            }
            setGameOptionsRows(page);
        }

        setIsGameOptionsLoading(false);
    }

    // Load the tree
    React.useEffect(() => {

        if (selectedGameOption) {

            let tree = [];

            const createSimpleNode = (id, label) => {
                return { id, label };
            }

            tree.push(createSimpleNode("gameOptionId", `Game Option Id: ${selectedGameOption.gameOptionId}`));
            tree.push(createSimpleNode("timestamp", `Creation timestamp: ${selectedGameOption.timestamp}`));
            tree.push(createSimpleNode("reserved", `Reserved: ${selectedGameOption.reserved}`));
            tree.push(createSimpleNode("playerCount", `Player count: ${selectedGameOption.playerCount}`));
            tree.push(createSimpleNode("playerHandMaxSize", `Player hand max size: ${selectedGameOption.playerHandMaxSize}`));
            tree.push(createSimpleNode("startPlayerId", `Start player id: ${selectedGameOption.startPlayerId}`));
            tree.push(createSimpleNode("maxErrorTokenCount", `Max error token count: ${selectedGameOption.maxErrorTokenCount}`));
            tree.push(createSimpleNode("maxSpeakTokenCount", `Max speak token count: ${selectedGameOption.maxSpeakTokenCount}`));
            tree.push(createSimpleNode("startErrorTokenCount", `Start error token count: ${selectedGameOption.startErrorTokenCount}`));
            tree.push(createSimpleNode("startSpeakTokenCount", `Start speak token count: ${selectedGameOption.startSpeakTokenCount}`));
            tree.push(createSimpleNode("maxScore", `Max score: ${selectedGameOption.maxScore}`));
            tree.push(createSimpleNode("minCardValue", `Min card value: ${selectedGameOption.minCardValue}`));
            tree.push(createSimpleNode("maxCardValue", `Max card value: ${selectedGameOption.maxCardValue}`));

            // Tags
            const tags = {
                id: "tags",
                label: `Tags (${selectedGameOption.tags !== undefined ? selectedGameOption.tags.length : 0})`,
                children: selectedGameOption.tags !== undefined ? selectedGameOption.tags.map((tag, index) => {
                    return {
                        id: `tag-${index}`,
                        label: tag,
                    }
                }) : [],
            }
            tree.push(tags);

            // Callable colors
            const callableColors = {
                id: "callableColors",
                label: `Callable colors (${selectedGameOption.callableColors.length})`,
                children: selectedGameOption.callableColors.map((color, index) => {
                    return {
                        id: `callableColor-${index}`,
                        label: color
                    }
                }),
            }
            tree.push(callableColors);

            // Color option
            const colorOption = {
                id: "colorOption",
                label: `Color option (${selectedGameOption.colorOption.length})`,
                children: selectedGameOption.colorOption.map((colorOption, index) => {
                    let colorOptionData = [];

                    colorOptionData.push(createSimpleNode(`${colorOption.color}-isAscending`, `Is ascending: ${colorOption.isAscending}`));

                    colorOptionData.push({
                        id: `${colorOption.color}-reactsTo`,
                        label: `Reacts to: ${colorOption.reactsTo.join(", ")}`
                    });

                    colorOptionData.push({
                        id: `${colorOption.color}-distribution`,
                        label: `Distribution: ${colorOption.distribution.map((distributionData) => {
                            return `${distributionData.value}x${distributionData.count}`
                        }).join(", ")}`
                    });

                    return {
                        id: `colorOption-${index}`,
                        label: `${colorOption.color}`,
                        children: colorOptionData,
                    }
                })
            }
            tree.push(colorOption);

            // Stack
            const stack = {
                id: "stack",
                label: `Stack (${selectedGameOption.stack.length})`,
                children: selectedGameOption.stack.map((card, index) => {
                    return {
                        id: `card-${index}`,
                        label: `${card.value} - ${card.color}`,
                    }
                }),
            }
            tree.push(stack);

            setGameOptionTree(tree);
        }
        else {
            setGameOptionTree([]);
        }

    }, [selectedGameOption, needUpdate]);

    // Load the tags when a game option is selected
    React.useEffect(() => {

        if (selectedGameOption)
            setCurrentTags(selectedGameOption.tags || []);
        else
            setCurrentTags([]);

    }, [selectedGameOption]);

    const onGameOptionsSelectionChange = async (ids) => {
        if (ids.length == 1) {

            const gameOptionId = ids[0];

            const loadedGameOption = await dataCache.gameOption(gameOptionId, false);
            setSelectedGameOption(loadedGameOption);
        }
    }

    return (
        <Stack direction="row" spacing={1}>

            <EditTags
                initialTags={currentTags}
                open={openEditTags}
                onClose={handleCloseEditTags}
                />

            <DataGrid
                autoHeight
                initialState={{
                    columns: {
                        columnVisibilityModel: {
                            id: false,
                        }
                    }
                }}
                rowHeight={25}
                rows={gameOptionsRows}
                rowCount={rowCount}
                columns={gameOptionsColumns}
                pageSizeOption={[25]}
                paginationModel={paginationModel}
                paginationMode="server"
                sortMode="server"
                filterMode="server"
                loading={isGameOptionsLoading}
                onRowSelectionModelChange={onGameOptionsSelectionChange}
                onPaginationModelChange={setPaginationModel}
                onFilterModelChange={setFilterModel}
                onSortModelChange={setSortModel}
            />

            <Box width={"25%"}>
                <Stack direction="row" spacing={2}>
                    <Button
                        variant="contained"
                        onClick={handleOpenEditTags}
                    >Edit tags</Button>
                </Stack>

                {gameOptionTree.length > 0 && <RichTreeView
                    items={gameOptionTree}
                    defaultExpandedItems={expandedItems}
                />}
            </Box>
            
        </Stack>
    );
}

GameOptions.propTypes = {
    apiGateway: PropTypes.object.isRequired,
    dataCache: PropTypes.object.isRequired,
    optimalCache: PropTypes.object.isRequired,
};

