import * as React from 'react';
import PropTypes from 'prop-types';

import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import { Gauge } from '@mui/x-charts/Gauge';
import LinearProgress from '@mui/material/LinearProgress';
import Paper from '@mui/material/Paper';
import { RichTreeView } from '@mui/x-tree-view/RichTreeView';
import Stack from '@mui/material/Stack';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';
import Typography from '@mui/material/Typography';


export default function Statistics(props) {

    const {
        open,
        onClose,
        gameUUIDList,
        dataCache,
    } = props;

    const handleClose = () => {
        onClose();
    }

    const victoryRatioRound = 3;
    const scoreRound = 1;
    const errorCountRound = 2;
    const turnCountRound = 2;

    const round = (value, decimalNumber) => {
        const num = Math.pow(10, decimalNumber);
        return Math.round(value * num) / num;
    }

    const [isLoading, setIsLoading] = React.useState(false);
    const [loadingContent, setLoadingContent] = React.useState("");
    const [loadingCount, setLoadingCount] = React.useState(0);
    const [loadingMaxCount, setLoadingMaxCount] = React.useState(0);

    const [statisticsTree, setStatisticsTree] = React.useState([]);
    const [statisticsRows, setStatisticsRows] = React.useState([]);

    React.useEffect(() => {

        const load = async () => {
            const statTree = await loadStatisticsTree();
            await loadStatisticsRows(statTree);
        };

        load();

    }, [gameUUIDList, dataCache]);

    const loadStatisticsTree = async () => {

        try {

            setIsLoading(true);
            setLoadingContent("Loading statistics data...");

            let statTree = [];

            const createNode = (id, label) => { return { id, label }; }

            let count = 0;
            setLoadingCount(count);
            setLoadingMaxCount(gameUUIDList.length);
            for (let gameUUID of gameUUIDList) {

                count += 1;
                setLoadingCount(count);

                const gameEnd = await dataCache.gameEnd(gameUUID);

                setLoadingContent(`Loading statistics data (game UUID: ${gameUUID})...`);

                if (gameEnd) {
                    let node = createNode(gameUUID, gameUUID);
                    node.children = [];
                    node.data = {
                        victory: gameEnd.victory,
                        score: gameEnd.score,
                        maxScore: gameEnd.maxScore,
                        errorCount: gameEnd.errorTokenCount,
                        playerTurnCount: gameEnd.elapsedPlayerTurnCount,
                        gameTurnCount: gameEnd.elapsedGameTurnCount,
                        serie: [
                        ]
                    };

                    for (let playerTurnCount = 0; playerTurnCount <= node.data.playerTurnCount; ++playerTurnCount) {

                        const gameState = await dataCache.gameState(gameUUID, playerTurnCount);

                        node.data.serie.push({
                            playerTurnCount,
                            score: gameState.board.score,
                            maxScore: gameState.trash.currentMaxScore,
                        });
                    }

                    // Victory
                    const disabled = false;

                    node.children.push(createNode(gameUUID + "Victory", gameEnd.victory ? "Victory" : "Defeat"));
                    node.children.push(createNode(gameUUID + "Score", `Score=${node.data.score}/${node.data.maxScore}`));
                    node.children.push(createNode(gameUUID + "Error", `Error=${node.data.errorCount}`));
                    node.children.push(createNode(gameUUID + "TurnCount", `Turn count=${node.data.playerTurnCount}/${node.data.gameTurnCount}`));

                    node.disabled = disabled;
                    statTree.push(node);
                }
                else {
                    const node = createNode(gameUUID, gameUUID);
                    node.disabled = true;
                    statTree.push(node);
                }
            }

            setStatisticsTree(statTree);
            setIsLoading(false);
            setLoadingContent("");
            return statTree;
            
        }
        catch (err) {
            setIsLoading(false);
            setLoadingContent("");
            return undefined;
        }
    }

    const loadStatisticsRows = async (statTree) => {

        try {
            setIsLoading(true);
            setLoadingContent("Computing statistics...");

            // Computation
            let victoryRatio = 0;
            let victoryCount = 0;
            let defeatCount = 0;
            let gameCount = 0;
            let meanScore = 0.;
            let meanMaxScore = 0.;
            let meanVictoryScore = 0.;
            let meanVictoryMaxScore = 0.;
            let meanDefeatScore = 0.;
            let meanDefeatMaxScore = 0.;
            let meanErrorCount = 0.;
            let meanVictoryErrorCount = 0.;
            let meanPlayerTurnCount = 0.;
            let meanGameTurnCount = 0.;
            let meanVictoryPlayerTurnCount = 0.;
            let meanVictoryGameTurnCount = 0.;
            let meanDefeatPlayerTurnCount = 0.;
            let meanDefeatGameTurnCount = 0.;
            let maxScore = 0;


            for (const node of statTree) {
                if (!node.disabled) {

                    if (node.data.victory) {
                        // In case of victory
                        victoryCount += 1
                        meanVictoryScore += node.data.score;
                        meanVictoryMaxScore += node.data.maxScore;
                        meanVictoryErrorCount += node.data.errorCount;
                        meanVictoryPlayerTurnCount += node.data.playerTurnCount;
                        meanVictoryGameTurnCount += node.data.gameTurnCount;
                    }
                    else {
                        // In case of defeat
                        defeatCount += 1;
                        meanDefeatScore += node.data.score;
                        meanDefeatMaxScore += node.data.maxScore;
                        meanDefeatPlayerTurnCount += node.data.playerTurnCount;
                        meanDefeatGameTurnCount += node.data.gameTurnCount;
                    }

                    if (node.data.maxScore > maxScore)
                        maxScore = node.data.maxScore;

                    meanScore += node.data.score;
                    meanMaxScore += node.data.maxScore;
                    meanErrorCount += node.data.errorCount;
                    meanPlayerTurnCount += node.data.playerTurnCount;
                    meanGameTurnCount += node.data.gameTurnCount;
                }
            }
            gameCount = victoryCount + defeatCount;
            if (gameCount > 0) {
                victoryRatio = round(victoryCount / gameCount, victoryRatioRound);
                meanScore = round(meanScore / gameCount, scoreRound);
                meanMaxScore = round(meanMaxScore / gameCount, scoreRound);
                meanErrorCount = round(meanErrorCount / gameCount, errorCountRound);
                meanPlayerTurnCount = round(meanPlayerTurnCount / gameCount, turnCountRound);
                meanGameTurnCount = round(meanGameTurnCount / gameCount, turnCountRound);
            }
            if (defeatCount > 0) {
                meanDefeatScore = round(meanDefeatScore / defeatCount, scoreRound);
                meanDefeatMaxScore = round(meanDefeatMaxScore / defeatCount, scoreRound);
                meanDefeatPlayerTurnCount = round(meanDefeatPlayerTurnCount / defeatCount, turnCountRound);
                meanDefeatGameTurnCount = round(meanDefeatGameTurnCount / defeatCount, turnCountRound);
            }
            if (victoryCount > 0) {
                meanVictoryScore = round(meanVictoryScore / victoryCount, scoreRound);
                meanVictoryMaxScore = round(meanVictoryMaxScore / victoryCount, scoreRound);
                meanVictoryErrorCount = round(meanVictoryErrorCount / victoryCount, errorCountRound);
                meanVictoryPlayerTurnCount = round(meanVictoryPlayerTurnCount / victoryCount, turnCountRound);
                meanVictoryGameTurnCount = round(meanVictoryGameTurnCount / victoryCount, turnCountRound);
            }

            // Display
            let statRows = [];

            const createStatRow = (id, title, value) => {
                return { id, title, value };
            }

            statRows.push(createStatRow("gameCount", "Game count (Victory - Defeat)", `${gameCount} (${victoryCount} - ${defeatCount})`));

            if (gameCount > 0) {
                statRows.push(createStatRow("victoryRatio", "Victory ratio", victoryRatio));
                statRows.push(createStatRow("meanScore", "Mean score / Mean max score / Max score", `${meanScore} / ${meanMaxScore} / ${maxScore}`));
                statRows.push(createStatRow("meanVictoryScore", "-> Victories only", `${meanVictoryScore} / ${meanVictoryMaxScore}`));
                statRows.push(createStatRow("meanDefeatScore", "-> Defeat only", `${meanDefeatScore} / ${meanDefeatMaxScore}`));
                statRows.push(createStatRow("meanErrorCount", "Mean error count", meanErrorCount));
                statRows.push(createStatRow("meanVictoryErrorCount", "-> Victories only", meanVictoryErrorCount));
                statRows.push(createStatRow("meanTurnCount", "Mean player turn count / mean game turn count", `${meanPlayerTurnCount} / ${meanGameTurnCount}`));
                statRows.push(createStatRow("meanVictoryTurnCount", "-> Victories only", `${meanVictoryPlayerTurnCount} / ${meanVictoryGameTurnCount}`));
                statRows.push(createStatRow("meanDefeatTurnCount", "-> Defeat only", `${meanDefeatPlayerTurnCount} / ${meanDefeatGameTurnCount}`));
            }

            setStatisticsRows(statRows);

            setIsLoading(false);
            setLoadingContent("");
        }
        catch (err) {
            setIsLoading(false);
            setLoadingContent("");
        }
    }

    const isItemDisabled = (item) => !!item.disabled;

    return (
        <Dialog onClose={handleClose} open={open}
            fullScreen
            maxWidth="xl">
            <Stack direction="row">
                <Button
                    onClick={handleClose}
                    variant="contained"
                    sx={{ ml: 2, mt: 2, mb: 2 }}
                >Close</Button>

                <DialogTitle>Statistics</DialogTitle>

                {isLoading && loadingContent.length > 0 &&
                    <Box alignItems="center"
                        justifyContent="center"
                        sx={{mt: 2}}>
                        <Typography variant="caption">{loadingContent}</Typography>
                        <Box alignItems="center">
                            <LinearProgress variant="determinate" value={(100 * loadingCount) / loadingMaxCount } />
                        </Box>
                    </Box>
                }
            </Stack>
            {isLoading ||
                <Stack direction="row" spacing={1}>
                    <Box minWidth={500}>
                        <RichTreeView items={statisticsTree} isItemDisabled={isItemDisabled} />
                    </Box>

                    <Stack direction="column" spacing={1 }>
                        <TableContainer component={Paper}>
                            <Table sx={{ minwidth: 200 }} size="small" aria-label="gameend-table">
                                <TableBody>
                                    {statisticsRows.map((row) => (
                                        <TableRow key={row.id}>
                                            <TableCell component="th" scope="row">{row.title}</TableCell>
                                            <TableCell align="right">{row.value}</TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </Stack>
                </Stack>
            }
        </Dialog>
    );
}

Statistics.propTypes = {
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    gameUUIDList: PropTypes.array.isRequired,
    dataCache: PropTypes.object.isRequired,
}