import * as React from 'react';
import PropTypes from 'prop-types';

import Games from './games.jsx';
import GameOptions from './game_options.jsx';

import Box from '@mui/material/Box';
import Stack from '@mui/material/Stack';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import Typography from '@mui/material/Typography';


export default function Application(props) {

    const {
        apiGateway,
        dataCache,
        optimalCache,
    } = props

    // Application info
    const [appVersion] = React.useState(import.meta.env.VITE_VIEWER_VERSION);
    const [appConfig] = React.useState(import.meta.env.VITE_VIEWER_CONFIG);
    const [apiHost] = React.useState(window.DYNAMIC_PROPERTIES.API_HOST);
    const [apiPort] = React.useState(window.DYNAMIC_PROPERTIES.API_PORT);
    const [optimalHost] = React.useState(window.DYNAMIC_PROPERTIES.OPTIMAL_HOST);
    const [optimalPort] = React.useState(window.DYNAMIC_PROPERTIES.OPTIMAL_PORT);

    // Tabs
    const [selectedTab, setSelectedTab] = React.useState("1");

    const handleTabChange = (event, newValue) => {
        setSelectedTab(newValue);
    }


    return (
        <Box sx={{ width: '100%' }}>
            <Stack direction="column" spacing={1}>

                <Typography variant="h2" align="center">Hanabi viewer</Typography>

                <TabContext value={selectedTab}>
                    <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                        <TabList onChange={handleTabChange} aria-label="Main page content">
                            <Tab label="Games" value="1" />
                            <Tab label="Game options" value="2" />
                            <Tab label="Help" value="3" />
                        </TabList>
                    </Box>
                    <TabPanel value="1">
                        <Games
                            apiGateway={apiGateway}
                            dataCache={dataCache}
                            optimalCache={optimalCache}
                            refreshDelay={1000}
                        />
                    </TabPanel>

                    <TabPanel value="2">
                        <Stack direction="column" spacing={1}>
                            <GameOptions
                                apiGateway={apiGateway}
                                dataCache={dataCache}
                                optimalCache={optimalCache}
                            />
                        </Stack>
                    </TabPanel>

                    <TabPanel value="3">
                        <Typography align="center">Viewer version: v{appVersion} - {appConfig}</Typography>
                        <Typography align="center">Hanabi api url: http://{apiHost}:{apiPort}</Typography>
                        <Typography align="center">Optimal api url: http://{optimalHost}:{optimalPort}</Typography>
                    </TabPanel>
                </TabContext>
            </Stack>
        </Box>
    );

}

Application.propTypes = {
    apiGateway: PropTypes.object.isRequired,
    dataCache: PropTypes.object.isRequired,
    optimalCache: PropTypes.object.isRequired,
};