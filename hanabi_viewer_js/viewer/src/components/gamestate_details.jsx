import * as React from 'react';
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';


export default function GameStateDetails(props) {

    const {
        gameUuid,
        playerTurnCount,
        dataCache
    } = props;

    // Dashboard
    const [currentScore, setCurrentScore] = React.useState("");
    const [currentMaxScore, setCurrentMaxScore] = React.useState("");
    const [maxScore, setMaxScore] = React.useState("");
    const [errorCount, setErrorCount] = React.useState("");
    const [maxErrorCount, setMaxErrorCount] = React.useState("");
    const [speakCount, setSpeakCount] = React.useState("");
    const [maxSpeakCount, setMaxSpeakCount] = React.useState("");
    const [stackCount, setStackCount] = React.useState(0);
    const [stackTop, setStackTop] = React.useState("");

    // Board and trash
    const [colorData, setColorData] = React.useState([]);

    // Player hands
    const [hands, setHands] = React.useState([]);

    React.useEffect(() => {
        loadData(dataCache, gameUuid, playerTurnCount);
    }, [dataCache, gameUuid, playerTurnCount]);

    const loadData = async (dataCache, gameUuid, playerTurnCount) => {
        //console.log(`Loading data for player turn count = ${playerTurnCount}...`);

        const gameState = await dataCache.gameState(gameUuid, playerTurnCount);
        const gameStart = await dataCache.gameStart(gameUuid);
        const gameOption = await dataCache.gameOption(gameStart.gameOptionId);
        const peerInfos = await dataCache.peerInfo(gameUuid);

        // Dashboard
        setCurrentScore(gameState.board.score);
        setCurrentMaxScore(gameState.trash.currentMaxScore);
        setMaxScore(gameOption.maxScore);

        setErrorCount(gameState.errorTokenCount);
        setMaxErrorCount(gameOption.maxErrorTokenCount);

        setSpeakCount(gameState.speakTokenCount);
        setMaxSpeakCount(gameOption.maxSpeakTokenCount);

        setStackCount(gameState.stack.length);
        if (gameState.stack.length > 0) {
            const card = gameState.stack[0];
            setStackTop(`${card.value}-${card.color}`);
        }

        // Board and trash
        let colors = []
        const createColorData = (name, board, trash, ascending, minValue, maxValue) => {
            let boardValue = "";
            for (const entry of board) {
                if (entry.color == name && entry.value > 0) {
                    if (ascending) {
                        for (let value = minValue; value <= entry.value; ++value) {
                            boardValue += `${value} `;
                        }
                    }
                    else {
                        for (let value = maxValue; value >= entry.value; --value) {
                            boardValue += `${value} `;
                        }
                    }
                }
            }

            let trashValue = "";
            for (const card of trash) {
                if (card.color == name) {
                    trashValue += `${card.value} `;
                }
            }

            return { name, boardValue, trashValue };
        }
        for (const color of gameOption.colorOption) {
            const board = gameState.board.board;
            const trash = gameState.trash.trash;
            const values = color.distribution.map((item) => { return item.value; });
            const minValue = Math.min(...values);
            const maxValue = Math.max(...values);
            colors.push(createColorData(color.color, board, trash, color.isAscending, minValue, maxValue));
        }
        setColorData(colors);

        // Player hands
        let playerHands = [];
        const createHandInfo = (name, type, cards, positiveValueInfo, negativeValueInfo, positiveColorInfo, negativeColorInfo) => {
            return { name, type, cards, positiveValueInfo, negativeValueInfo, positiveColorInfo, negativeColorInfo};
        }
        for (let playerIndex = 0; playerIndex < gameState.playerHand.length; ++playerIndex) {
            
            const name = (playerIndex == gameState.currentPlayerId ? ">>> " : "") + peerInfos[playerIndex].name;
            const type = peerInfos[playerIndex].type;

            const hand = gameState.playerHand[playerIndex];
            let cards = [];
            for (let handIndex = 0; handIndex < hand.length; ++handIndex) {
                const card = gameState.playerHand[playerIndex][handIndex];
                cards.push(card);
            }

            const infos = gameState.playerCardInfo[playerIndex];
            let positiveValueInfo = [];
            let negativeValueInfo = [];
            let positiveColorInfo = [];
            let negativeColorInfo = [];
            for (let handIndex = 0; handIndex < infos.length; ++handIndex) {
                const posValue = (infos[handIndex].positiveValue > 0) ? [infos[handIndex].positiveValue] : [];
                positiveValueInfo.push(posValue);

                const negValue = infos[handIndex].negativeValue.map((item, index) => {
                    return ((index == 0) ? "" : ", ") + item;
                });
                negativeValueInfo.push(negValue);

                const posColor = infos[handIndex].positiveColor.map((item, index) => {
                    return ((index == 0) ? "" : ", ") + item.substring(0, 1);
                });
                positiveColorInfo.push(posColor);

                const negColor = infos[handIndex].negativeColor.map((item, index) => {
                    return ((index == 0) ? "" : ", ") + item.substring(0, 1);
                });
                negativeColorInfo.push(negColor);
            }
            
            playerHands.push(createHandInfo(name, type, cards, positiveValueInfo, negativeValueInfo, positiveColorInfo, negativeColorInfo));
        }
        setHands(playerHands);
    }

    const boardWidth = 30;
    const infoWidth = 70;

    return (
        <Box sx={{ width: 700 }}>
            <TableContainer component={Paper}>
                <Table size="small">
                    <TableBody>
                        <TableRow>
                            <TableCell sx={{ width: 10 }} align="right">Score</TableCell>
                            <TableCell sx={{ width: 10 }} align="left">{currentScore}/{currentMaxScore}/{maxScore}</TableCell>

                            <TableCell sx={{ width: 10 }} align="right">Error</TableCell>
                            <TableCell sx={{ width: 10 }} align="left">{errorCount}/{maxErrorCount}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell sx={{ width: 10 }} align="right">Stack</TableCell>
                            {(stackCount > 0) ?
                                <TableCell sx={{ minWidth: 150 }} align="left">{stackCount} card remaining [next: {stackTop}]</TableCell> :
                                <TableCell sx={{ minWidth: 150 }} align="left">empty</TableCell>
                            }

                            <TableCell sx={{ width: 10 }} align="right">Speak</TableCell>
                            <TableCell sx={{ width: 10 }} align="left">{speakCount}/{maxSpeakCount}</TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </TableContainer>

            <TableContainer component={Paper}>
                <Table size="small">
                    <TableHead>
                        <TableRow>
                            <TableCell sx={{ width: boardWidth }} align="right"></TableCell>
                            <TableCell sx={{ width: boardWidth }} align="center">Board</TableCell>
                            <TableCell sx={{ width: boardWidth }} align="center">Trash</TableCell>
                        </TableRow>
                    </TableHead>

                    <TableBody>
                        {colorData.map((row) => (
                            <TableRow key={row.name}>
                                <TableCell sx={{ width: boardWidth }} align="right">{row.name}</TableCell>
                                <TableCell sx={{ width: boardWidth }} align="center">{row.boardValue}</TableCell>
                                <TableCell sx={{ width: boardWidth }} align="center">{row.trashValue}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>

            <TableContainer component={Paper}>
                <Table size="small">

                    {hands.map((hand, playerIndex) => (
                        <TableBody key={playerIndex}>
                            <TableRow key="{playerIndex}-card">
                                <TableCell sx={{ width: infoWidth }} align="right">{hand.name}</TableCell>
                                {hand.cards.map((card, handIndex) => (
                                    <TableCell sx={{ width: infoWidth }} key={handIndex} align="center">{card.value}-{card.color}</TableCell>
                                ))}
                            </TableRow>
                            <TableRow key={20*(playerIndex+1)}>
                                <TableCell sx={{ width: infoWidth }} align="right">Value +</TableCell>
                                {hand.positiveValueInfo.map((info, handIndex) => (
                                    <TableCell sx={{ width: infoWidth }} key={handIndex} align="center">{info}</TableCell>
                                ))}
                            </TableRow>
                            <TableRow key={30*(playerIndex+1)}>
                                <TableCell sx={{ width: infoWidth }} align="right">Value -</TableCell>
                                {hand.negativeValueInfo.map((info, handIndex) => (
                                    <TableCell sx={{ width: infoWidth }} key={handIndex} align="center">{info}</TableCell>
                                ))}
                            </TableRow>
                            <TableRow key={40*(playerIndex+1)}>
                                <TableCell sx={{ width: infoWidth }} align="right">Color +</TableCell>
                                {hand.positiveColorInfo.map((info, handIndex) => (
                                    <TableCell sx={{ width: infoWidth }} key={handIndex} align="center">{info}</TableCell>
                                ))}
                            </TableRow>
                            <TableRow key={50*(playerIndex+1)}>
                                <TableCell sx={{ width: infoWidth }} align="right">Color -</TableCell>
                                {hand.negativeColorInfo.map((info, handIndex) => (
                                    <TableCell sx={{ width: infoWidth }} key={handIndex} align="center">{info}</TableCell>
                                ))}
                            </TableRow>
                        </TableBody>
                    ))}
                </Table>
            </TableContainer>

        </Box>
    );
}

GameStateDetails.propTypes = {
    gameUuid: PropTypes.string.isRequired,
    playerTurnCount: PropTypes.number.isRequired,
    dataCache: PropTypes.object.isRequired,
};