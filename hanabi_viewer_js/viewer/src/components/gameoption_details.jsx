import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import { RichTreeView } from '@mui/x-tree-view/RichTreeView';


export default function GameOptionDetails(props) {

    const {
        open,
        onClose,
        data
    } = props;

    const expandedItems = ["tags", "colorOptions"];

    const handleClose = () => {
        onClose();
    }

    return (
        <Dialog onClose={handleClose} open={open}>
            <DialogTitle>Game option</DialogTitle>
            <Box sx={{ minHeight: 600, minWidth: 400 }}>
                <RichTreeView
                    items={data}
                    defaultExpandedItems={expandedItems}
                />
            </Box>
        </Dialog>
    );
}

GameOptionDetails.propTypes = {
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    data: PropTypes.array.isRequired
};