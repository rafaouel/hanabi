import PropTypes from 'prop-types';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import TableContainer from '@mui/material/TableContainer';
import Table from '@mui/material/Table';
import TableRow from '@mui/material/TableRow';
import TableCell from '@mui/material/TableCell';
import TableBody from '@mui/material/TableBody';
import Paper from '@mui/material/Paper';


export default function GameStats(props) {

    const {
        open,
        onClose,
        gameUUID,
        rows
    } = props;

    const handleClose = () => {
        onClose();
    }

    return (
        <Dialog onClose={handleClose} open={open}>
            <DialogTitle>Game statistics - {gameUUID}</DialogTitle>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 500 }} size="small" aria-label="game-stats-table">
                    <TableBody>
                        {rows.map((row) => (
                            <TableRow key={row.playerId} >
                                <TableCell component="th" scope="row" align="center">{row.playerId}</TableCell>
                                <TableCell align="center">{row.playerName}</TableCell>
                                <TableCell align="center">{row.dropCount}</TableCell>
                                <TableCell align="center">{row.placeCount}</TableCell>
                                <TableCell align="center">{row.speakCount}</TableCell>
                                <TableCell align="center">{row.elapsedTime}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Dialog>
    );
}

GameStats.propTypes = {
    open: PropTypes.bool.isRequired,
    gameUUID: PropTypes.string.isRequired,
    onClose: PropTypes.func.isRequired,
    rows: PropTypes.array.isRequired
};
