import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import { RichTreeView } from '@mui/x-tree-view/RichTreeView';
import Typography from '@mui/material/Typography';


export default function PlayerActionDetails(props) {

    const {
        name,
        data
    } = props;

    const expandedItems = [];

    return (
        <Box sx={{minWidth:380, minHeight: 700}}>
            <Typography align="center" gutterBottom>{name}</Typography>
            <RichTreeView
                items={data }
                defaultExpandedItems={expandedItems}
            />
        </Box>
    );
}

PlayerActionDetails.propTypes = {
    name: PropTypes.string.isRequired,
    data: PropTypes.array.isRequired
}