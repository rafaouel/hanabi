import * as React from 'react';
import PropTypes from 'prop-types';


import {
    Box,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    IconButton,
    List,
    ListItem,
    ListItemText,
    Stack,
    TextField,
    Tooltip
} from '@mui/material';

import {
    Add,
    Delete,
    RemoveCircle
} from '@mui/icons-material';


export default function EditTags(props) {

    const {
        initialTags = [],
        open,
        onClose,
    } = props;


    // Close the dialog and tell the caller about the tags the user selected
    const handleSaveTags = () => {
        onClose(tags);
    }


    // Close the dialog returning nothing
    const handleCloseDialog = () => {
        onClose();
    }


    // List of the tags that we can modify
    const [tags, setTags] = React.useState(initialTags);

    const addTag = () => {
        setTags([...tags, newTag]);
        setNewTag("");
    }

    const removeTag = (index) => {
        setTags(tags.filter((_, i) => i !== index));
    }

    const removeAllTags = () => {
        setTags([]);
        setNewTag("");
    }

    React.useEffect(() => {
        setTags(initialTags);
    }, [initialTags]);


    // The tag the user can write
    const [newTag, setNewTag] = React.useState("");

    const changeNewTag = (event) => {
        setNewTag(event.target.value);
    }

    return (
        <Dialog
            onClose={handleCloseDialog}
            open={open}
            maxWidth={"sm"}
        >
            <DialogTitle>Edit tags</DialogTitle>
            <DialogActions>
                <Stack
                    direction="row"
                    spacing={1}
                    justifyContent="center"
                    alignItems="center"
                >

                    <TextField
                        label="New item"
                        variant="filled"
                        onChange={changeNewTag}
                        margin='dense'
                        size='small'
                        value={newTag} />

                    <Tooltip title="Add tag" arrow disableInteractive={newTag.length !== 0}>
                        <span>
                            <IconButton onClick={addTag} disabled={newTag.length === 0}>
                                <Add />
                            </IconButton>
                        </span>
                    </Tooltip>

                    <Tooltip title="Remove all" arrow disableInteractive={tags.length !== 0}>
                        <span>
                            <IconButton onClick={removeAllTags} disabled={tags.length === 0}>
                                <RemoveCircle />
                            </IconButton>
                        </span>
                    </Tooltip>
                </Stack>
            </DialogActions>

            <DialogContent sx={{ height: '30vh', maxHeight: '30vh', overflow: 'auto' }}>    
                <List>
                    {tags.map((item, index) => (
                        <ListItem
                            key={index}
                            secondaryAction={
                                <IconButton onClick={() => removeTag(index)}>
                                    <Delete />
                                </IconButton>
                            }
                        >
                            <ListItemText primary={item} />
                        </ListItem>
                    ))}
                </List>
            </DialogContent>

            <DialogActions>
                <Button variant="contained" onClick={handleSaveTags}>Save</Button>
                <Button variant="contained" onClick={handleCloseDialog}>Cancel</Button>
            </DialogActions>
            <Box
                sx={{ ml: 2, mr: 2, mb: 2 }}
            >

                <Stack
                    direction="row"
                    spacing={1}
                    justifyContent="center"
                >
                </Stack>

            </Box>

        </Dialog>
    );
}

EditTags.propTypes = {
    initialTags: PropTypes.arrayOf(PropTypes.string),
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
};

