import PropTypes from 'prop-types';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';


export default function PeerInfoDetails(props) {

    const {
        open,
        onClose,
        rows
    } = props;

    const handleClose = () => {
        onClose();
    }

    return (
        <Dialog onClose={handleClose} open={open}>
            <DialogTitle>Peer info</DialogTitle>
            <TableContainer component={Paper}>
                <Table sx={{ minwidth: 500 }} size="small" aria-label="peerinfo-table">
                    <TableBody>
                        {rows.map((row) => (
                            <TableRow key={row.playerId}>
                                <TableCell component="th" scope="row">{row.playerId}</TableCell>
                                <TableCell align="right">{row.playerName}</TableCell>
                                <TableCell align="right">{row.type}</TableCell>
                                <TableCell align="right">{row.ip}</TableCell>
                                <TableCell align="right">{row.clientVersion}</TableCell>
                                <TableCell align="right">{row.tags}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Dialog>
    );
}

PeerInfoDetails.propTypes = {
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    rows: PropTypes.array.isRequired
}