import * as React from 'react';
import PropTypes from 'prop-types';

import EditTags from './edit_tags.jsx';
import GameDetails from './game_details.jsx';
import GameEndDetails from './gameend_details.jsx';
import GameOptionDetails from './gameoption_details.jsx';
import GameStats from './game_stats.jsx';
import PeerInfoDetails from './peerinfo_details.jsx';
import Statistics from './statistics.jsx';
import StatusMessage from './status_message.jsx';

import {
    Button,
    CircularProgress,
    FormControlLabel,
    Stack,
    Switch
} from '@mui/material';

import { DataGrid } from '@mui/x-data-grid';


export default function Games(props) {

    const {
        apiGateway,
        dataCache,
        optimalCache,
        refreshDelay,
    } = props;


    const gameUUIDSelectionColumns = [
        {
            field: 'state',
            headerName: 'State',
            width: 75,
            align: 'center',
            resizable: false
        },
        {
            field: 'playerNumber',
            headerName: 'Players',
            width: 75,
            align: 'center',
            resizable: false,
            valueGetter: (value, row) => {
                return `${row.playerCount}/${row.maxPlayerCount}`;
            }
        },
        {
            field: 'turnCount',
            headerName: 'Turn',
            width: 75,
            align: 'center',
            sortable: false,
            resizable: false,
            valueGetter: (value, row) => {
                if (row.playerTurnCount && row.gameTurnCount) {
                    return `${row.playerTurnCount} - ${row.gameTurnCount}`;
                }
                return "";
            }
        },
        {
            field: 'errorCount',
            headerName: 'Error',
            width: 50,
            align: 'center',
            sortable: false,
            resizable: false,
            valueGetter: (value, row) => {
                if (row.currentErrorCount) {
                    return `${row.currentErrorCount}/${row.maxErrorCount}`;
                }
                return `0/${row.maxErrorCount}`;
            }
        },
        {
            field: 'score',
            headerName: 'Score',
            description: 'Current score/Current max score/Max score',
            width: 80,
            align: 'center',
            sortable: false,
            resizable: false,
            valueGetter: (value, row) => {
                if (row.currentMaxScore) {
                    return `${row.currentScore}/${row.currentMaxScore}/${row.maxScore}`;
                }
                return `0/0/${row.maxScore}`;
            }
        },
        {
            field: 'optimalScore',
            headerName: 'AI score',
            description: "Score realized by 'Optimal' AI",
            width: 75,
            align: 'center',
            sortable: true,
            resizable: false,
            valueGetter: (value, row) => {
                if (row.optimalScore !== undefined) {
                    return `${row.optimalScore}/${row.maxScore}`;
                }
                else {
                    return undefined;
                }
            }
        },
        {
            field: 'optimalTurnCount',
            headerName: 'AI turn',
            description: "Player turn count realized by 'Optimal' AI",
            width: 75,
            align: 'center',
            sortable: false,
            resizable: false,
        },
        {
            field: 'id',
            headerName: 'Game UUID',
            width: 300,
            align: 'center',
            description: 'Unique game identifier'
        },
        {
            field: 'gameOptionId',
            headerName: 'Game Option Id',
            width: 225,
            align: 'center',
            description: 'Seed of the game',
        },
        {
            field: 'tags',
            headerName: 'Tags',
            width: 275,
            description: 'Game list of tags'
        },
        {
            field: 'gameDuration',
            headerName: 'Duration (s)',
            type: Number,
            width: 100,
            align: 'center',
            description: 'Game duration, in seconds'
        },
        {
            field: 'startTimestamp',
            headerName: 'Start',
            type: Date,
            width: 200,
            align: 'center',
            description: 'Game start timestamp'
        },
        {
            field: 'endTimestamp',
            headerName: 'End',
            type: Date,
            width: 200,
            align: 'center',
            description: 'Game end timestamp'
        },
        {
            field: 'creationTimestamp',
            headerName: 'Creation',
            type: Date,
            width: 200,
            align: 'center',
            description: 'Game creation timestamp'
        },
    ];

    // Settings
    const [autoRefreshDelay] = React.useState(refreshDelay | 1000);
    const [isAutoRefresh, setIsAutoRefresh] = React.useState(false);
    const [isLoading, setIsLoading] = React.useState(false);

    // Game UUID selection
    const [paginationModel, setPaginationModel] = React.useState({
        page: 0,
        pageSize: 25,
    });
    const [totalRowCount, setTotalRowCount] = React.useState(0);
    const [isGameUUIDSelectionLoading, setIsGameUUIDSelectionLoading] = React.useState(false);
    const [gameUUIDSelectionRows, setGameUUIDSelectionRows] = React.useState([]);
    const [selectedGameUUID, setSelectedGameUUID] = React.useState([]);
    const [isGameUUIDSelectionEmpty, setIsGameUUIDSelectionEmpty] = React.useState(true);
    const [isOnlyOneGameSelected, setIsOnlyOneGameSelected] = React.useState(false);
    const rowCountRef = React.useRef(totalRowCount || 0);
    const rowCount = React.useMemo(() => {
        if (totalRowCount !== undefined) {
            rowCountRef.current = totalRowCount;
        }
        return rowCountRef.current;
    }, [totalRowCount]);

    // Game statistics
    const [gameStatistics, setGameStatistics] = React.useState([]);
    const [displayGameStatistics, setDisplayGameStatistics] = React.useState(false);

    // Peer info
    const [peerInfoRows, setPeerInfoRows] = React.useState([]);
    const [displayPeerInfo, setDisplayPeerInfo] = React.useState(false);

    // Game end
    const [gameEndRows, setGameEndRows] = React.useState([]);
    const [displayGameEnd, setDisplayGameEnd] = React.useState(false);

    // Game option
    const [gameOptionData, setGameOptionData] = React.useState([]);
    const [displayGameOption, setDisplayGameOption] = React.useState(false);

    // Game details
    const [displayGameDetails, setDisplayGameDetails] = React.useState(false);

    // Statistics
    const [displayStatistics, setDisplayStatistics] = React.useState(false);


    // Status message
    const [displayStatusMessage, setDisplayStatusMessage] = React.useState(false);
    const [statusTitle, setStatusTitle] = React.useState("");
    const [statusMessage, setStatusMessage] = React.useState("");
    const [statusSeverity, setStatusSeverity] = React.useState("info");

    // Load on first access
    React.useEffect(() => {
        loadGameUUIDSelection(apiGateway, dataCache, optimalCache);
    }, [apiGateway, dataCache, optimalCache, paginationModel]);

    // Autoload the list of the game UUID
    React.useEffect(() => {
        const intervalId = setInterval(async () => {

            if (isAutoRefresh) {
                // Load the data
                if (apiGateway != null && dataCache != null) {
                    await loadGameUUIDSelection(apiGateway, dataCache, optimalCache);
                }
            }

        }, autoRefreshDelay);

        return () => { clearInterval(intervalId); }
    }, [isAutoRefresh, autoRefreshDelay, apiGateway, dataCache, optimalCache, paginationModel]);

    const onClickButtonRefreshGameUUIDSelection = async () => {

        await loadGameUUIDSelection(apiGateway, dataCache, optimalCache);
    }

    const onClickButtonRemoveOrphan = async () => {
        setIsLoading(true);

        try {
            let count = 0;

            // List all game UUID from game starts
            const gameStartUUID = await apiGateway.listGameStartGameUUID();

            // List all peer info
            const peerInfoUUID = await apiGateway.listPeerInfoGameUUID();
            peerInfoUUID.filter(async (UUID) => {
                if (!gameStartUUID.includes(UUID)) {
                    await apiGateway.deletePeerInfo(UUID);
                    count += 1;
                }
            });

            // List all game state
            const gameStateUUID = await apiGateway.listGameStateGameUUID();
            gameStateUUID.filter(async (UUID) => {
                if (!gameStartUUID.includes(UUID)) {
                    await apiGateway.deleteGameState(UUID);
                    count += 1;
                }
            });

            // List all player action
            const playerActionUUID = await apiGateway.listPlayerActionGameUUID();
            playerActionUUID.filter(async (UUID) => {
                if (!gameStartUUID.includes(UUID)) {
                    await apiGateway.deletePlayerAction(UUID);
                    count += 1;
                }
            });

            // List all game end
            const gameEndUUID = await apiGateway.listGameEndGameUUID();
            gameEndUUID.filter(async (UUID) => {
                if (!gameStartUUID.includes(UUID)) {
                    await apiGateway.deleteGameEnd(UUID);
                    count += 1;
                }
            });

            setStatusSeverity("success");
            setStatusTitle("Success")
            setStatusMessage(`Orphan removed: ${count}`)
            setDisplayStatusMessage(true);
        }
        catch (err) {
            setStatusSeverity("error");
            setStatusTitle("Error")
            setStatusMessage(`Error while removing orphans (${err.message})`)
            setDisplayStatusMessage(true);
        }

        setIsLoading(false);
    }

    const loadGameUUIDSelection = async (api, dataCache, optimalCache) => {

        setIsGameUUIDSelectionLoading(true);

        // Load game overview
        const summaryData = await api.getGameSummary({ page: paginationModel.page, limit: paginationModel.pageSize });

        let selection = [];

        setTotalRowCount(summaryData.metadata.documentCount);
        if (summaryData.metadata.documentCount > 0) {
            for (const gameSummary of summaryData.data) {

                // Initialize the row
                let row = { id: gameSummary.gameUuid };

                // Load game summary
                if (gameSummary) {
                    row.playerTurnCount = gameSummary.playerTurnCount;
                    row.gameTurnCount = gameSummary.gameTurnCount;
                    row.maxPlayerCount = gameSummary.maxPlayerCount;
                    row.currentScore = gameSummary.currentScore;
                    row.currentMaxScore = gameSummary.currentMaxScore;
                    row.currentErrorCount = gameSummary.errorTokenCount;
                    row.creationTimestamp = gameSummary.gameCreation;
                    row.maxScore = gameSummary.maxScore;
                    row.tags = gameSummary.tags;
                    row.maxErrorCount = gameSummary.maxErrorTokenCount;
                    row.playerCount = gameSummary.playerCount;
                    row.gameOptionId = gameSummary.gameOptionId;

                    // State of the game
                    if (row.playerCount == row.maxPlayerCount) {
                        row.state = "Running";
                    }
                    else {
                        row.state = "Waiting";
                    }

                    row.endTimestamp = gameSummary.gameEndTimestamp;
                    row.startTimestamp = gameSummary.gameStartTimestamp;
                    row.gameDuration = gameSummary.duration;
                    row.victory = gameSummary.victory;
                    if (row.victory !== undefined) {
                        row.state = row.victory ? "Victory" : "Defeat";
                    }

                    // Call optimal for the score
                    const optimalData = await optimalCache.submit(gameSummary.gameOptionId);
                    if (optimalData) {
                        row.optimalScore = optimalData.score;
                        row.optimalTurnCount = optimalData.playerTurnCount;
                    }
                }

                selection.push(row);
            }
        }

        setGameUUIDSelectionRows(selection);
        setIsGameUUIDSelectionLoading(false);
    }


    const onClickButtonShowStatistics = async () => {
        setIsLoading(true);

        const gameUUID = selectedGameUUID[0];

        // Number of player turn already played
        const summaryData = await apiGateway.getGameSummary({ gameUUID });
        if (summaryData && summaryData.metadata.documentCount == 1) {
            const gameOverview = summaryData.data[0];
            const elapsedPlayerTurnCount = gameOverview.playerTurnCount;

            // Load peer info
            const peerInfoList = await dataCache.peerInfo(gameUUID);

            // Load game statistics
            if (elapsedPlayerTurnCount > 0) {
                const createGameStatisticsRow = (playerId, playerName, dropCount, placeCount, speakCount, elapsedTime) => {
                    return { playerId, playerName, dropCount, placeCount, speakCount, elapsedTime };
                }
                const gameStatisticsRows = [createGameStatisticsRow("Player id", "Player name", "# Drop", "# Place", "# Speak", "Elapsed time")];
                const playerStatistics = {};
                for (let playerTurn = 0; playerTurn < elapsedPlayerTurnCount; playerTurn++) {
                    const playerAction = await dataCache.playerAction(gameUUID, playerTurn);
                    const data = {
                        actionType: playerAction.actionType,
                        roundtripDuration: playerAction.roundtripDuration,
                        currentPlayerId: playerAction.currentPlayerId,
                        gameTurnCount: playerAction.gameTurnCount,
                        handIndex: playerAction.handIndex,
                        playerTurnCount: playerAction.playerTurnCount,
                        requestTimestamp: playerAction.requestTimestamp,
                        responseTimestamp: playerAction.responseTimestamp,
                        targetPlayerId: playerAction.targetPlayerId,
                        cardInfo: playerAction.cardInfo,
                    };
                    if (Object.getOwnPropertyNames(playerStatistics).includes(playerAction.currentPlayerId.toString())) {
                        playerStatistics[playerAction.currentPlayerId].push(data);
                    }
                    else {
                        playerStatistics[playerAction.currentPlayerId] = [data];
                    }
                }
                for (const peerInfo of peerInfoList) {
                    const statistics = playerStatistics[peerInfo.playerId];
                    if (statistics === undefined) {
                        continue;
                    }
                    let elapsedTime = 0;
                    let dropCount = 0;
                    let placeCount = 0;
                    let speakCount = 0;
                    statistics.forEach((elem) => {
                        elapsedTime += elem.roundtripDuration;
                        if (elem.actionType == "drop") {
                            dropCount += 1;
                        }
                        else if (elem.actionType == "place") {
                            placeCount += 1;
                        }
                        else {
                            speakCount += 1;
                        }
                    });
                    gameStatisticsRows.push(createGameStatisticsRow(peerInfo.playerId, peerInfo.name, dropCount, placeCount, speakCount, elapsedTime.toFixed(3)));
                }
                setGameStatistics(gameStatisticsRows);
            }
            else {
                setGameStatistics([]);
            }
        }
        else {
            setGameStatistics([]);
        }

        setIsLoading(false);

        setDisplayGameStatistics(true);
    }

    const onClickButtonShowGameEnd = async () => {

        setIsLoading(true);

        const gameUUID = selectedGameUUID[0];

        // Load Game end data
        const createGameEndRow = (title, value) => {
            return { title, value };
        }

        const gameEnd = await dataCache.gameEnd(gameUUID);

        if (gameEnd !== undefined) {
            const gameEndRows = [
                createGameEndRow("Victory", gameEnd.victory ? "true" : "false"),
                createGameEndRow("Turn", `${gameEnd.elapsedPlayerTurnCount}-${gameEnd.elapsedGameTurnCount}`),
                createGameEndRow("Score", `${gameEnd.score}/${gameEnd.maxScore}`),
                createGameEndRow("Errors", `${gameEnd.errorTokenCount}/${gameEnd.maxErrorTokenCount}`),
                createGameEndRow("Game start", gameEnd.gameStartTimestamp),
                createGameEndRow("Game end", gameEnd.gameEndTimestamp),
                createGameEndRow("Game duration (s)", gameEnd.gameDuration),
            ];
            setGameEndRows(gameEndRows);
        }
        else {
            setGameEndRows([]);
        }

        setIsLoading(false);
        setDisplayGameEnd(true);
    }

    const onClickButtonShowPeerInfo = async () => {
        // Load peer info
        setIsLoading(true);

        const gameUUID = selectedGameUUID[0];

        const createPeerInfoRow = (playerId, playerName, type, ip, clientVersion, tags) => {
            return { playerId, playerName, type, ip, clientVersion, tags };
        }

        const peerInfoList = await dataCache.peerInfo(gameUUID);
        if (peerInfoList.length != 0) {
            const peerInfoRows = [createPeerInfoRow("Player id", "Player name", "type", "IP", "Client version", "Tags")];
            for (const peer of peerInfoList) {
                const tags = peer.tags ? peer.tags.join(", ") : "";
                const config = peer.debug ? "Debug" : "Release";
                const version = `v${peer.version} ${config}`;
                peerInfoRows.push(createPeerInfoRow(peer.playerId, peer.name, peer.type, peer.ip, version, tags));
            }
            setPeerInfoRows(peerInfoRows);
        }
        else {
            setPeerInfoRows([]);
        }

        setIsLoading(false);
        setDisplayPeerInfo(true);
    }

    const onClickButtonShowGameOption = async () => {
        // Load game option
        setIsLoading(true);

        setGameOptionData([]);

        // Get game start
        const gameUUID = selectedGameUUID[0];
        const gameStart = await dataCache.gameStart(gameUUID);
        if (gameStart !== undefined) {

            const gameOption = await dataCache.gameOption(gameStart.gameOptionId);
            if (gameOption !== undefined) {

                const gameOptionTree = [];
                let cpt = 0;

                const createNode = (id, label) => {
                    return { id, label }
                };

                gameOptionTree.push(createNode("gameOptionId", "gameOptionId=" + gameOption.gameOptionId));

                const tags = (gameOption.tags ? gameOption.tags : []);
                let tagsNode = {
                    id: 'tags',
                    label: `tags (${tags.length})`,
                    children: []
                }
                for (const tag of tags) {
                    tagsNode.children.push(createNode(tag, tag));
                }
                gameOptionTree.push(tagsNode);

                gameOptionTree.push(createNode("timestamp", "timestamp=" + gameOption.timestamp));
                gameOptionTree.push(createNode("playerCount", "player count=" + gameOption.playerCount));
                gameOptionTree.push(createNode("playerHandMaxSize", "player hand max size=" + gameOption.playerHandMaxSize));
                gameOptionTree.push(createNode("startPlayerId", "start player id=" + gameOption.startPlayerId));
                gameOptionTree.push(createNode("maxErrorTokenCount", "max error token count=" + gameOption.maxErrorTokenCount));
                gameOptionTree.push(createNode("maxSpeakTokenCount", "max speak token count=" + gameOption.maxSpeakTokenCount));
                gameOptionTree.push(createNode("startErrorTokenCount", "start error token count=" + gameOption.startErrorTokenCount));
                gameOptionTree.push(createNode("startSpeakTokenCount", "start speak token count=" + gameOption.startSpeakTokenCount));
                gameOptionTree.push(createNode("maxScore", `max score=${gameOption.maxScore}`));
                gameOptionTree.push(createNode("minCardValue", "min card value=" + gameOption.minCardValue));
                gameOptionTree.push(createNode("maxCardValue", "max card value=" + gameOption.maxCardValue));

                let callableColorsNode = {
                    id: 'callableColors',
                    label: `callable colors (${gameOption.callableColors.length})`,
                    children: [],
                };
                for (const color of gameOption.callableColors) {
                    callableColorsNode.children.push(createNode(`callableColors-${color}`, color));
                }
                gameOptionTree.push(callableColorsNode);

                let colorOptions = {
                    id: 'colorOptions',
                    label: 'color options',
                    children: []
                };
                for (const colorOption of gameOption.colorOption) {
                    let color = {
                        id: colorOption.color,
                        label: colorOption.color,
                        children: []
                    };
                    color.children.push(createNode(`${colorOption.color}-isAscending`, `is ascending=${colorOption.isAscending}`));

                    let reactionList = {
                        id: `${colorOption.color}-Reaction`,
                        label: "reacts to",
                        children: []
                    }
                    for (const reaction of colorOption.reactsTo) {
                        reactionList.children.push(createNode(`${colorOption.color}-reaction-${reaction}`, reaction));
                    }
                    color.children.push(reactionList);

                    let distribution = {
                        id: `${colorOption.color}-Distribution`,
                        label: "distribution",
                        children: []
                    };
                    for (const pair of colorOption.distribution) {
                        distribution.children.push(createNode(`${colorOption.color}-distribution-${pair.value}`, `${pair.value}x${pair.count}`));
                    }
                    color.children.push(distribution);
                    colorOptions.children.push(color);
                }
                gameOptionTree.push(colorOptions);

                let stack = {
                    id: 'stack',
                    label: 'stack',
                    children: []
                };
                cpt = 0;
                for (const card of gameOption.stack) {
                    stack.children.push(createNode("card" + cpt, card.value + " " + card.color));
                    cpt++;
                }
                gameOptionTree.push(stack);
                setGameOptionData(gameOptionTree);
            }
        }

        setIsLoading(false);
        setDisplayGameOption(true);
    }

    const onClickButtonGameUUIDDelete = async () => {

        setIsLoading(true);

        // Are you sure ?
        const confirmed = true;
        if (confirmed) {
            // Get the UUID of the selected rows
            let newGameUUIDSelection = gameUUIDSelectionRows;
            const selectedRows = selectedGameUUID;
            if (selectedRows.length > 0) {
                for (const gameUUID of selectedRows) {

                    // Send a request to the API to delete the entries
                    await apiGateway.deleteGame(gameUUID);

                    newGameUUIDSelection = newGameUUIDSelection.filter((item) => {
                        return item.id != gameUUID;
                    });
                }
                setGameUUIDSelectionRows(newGameUUIDSelection);
            }
        }

        setIsLoading(false);
    }

    const onClickButtonShowGameDetails = async () => {

        setDisplayGameDetails(true);
    }

    const onGameUUIDSelectionChange = (ids) => {
        setSelectedGameUUID(ids);
        setIsGameUUIDSelectionEmpty(ids.length == 0);
        setIsOnlyOneGameSelected(ids.length == 1);
    }

    const [currentTags, setCurrentTags] = React.useState([]);
    const [openEditTags, setOpenEditTags] = React.useState(false);

    const handleCloseEditTags = async (newTags) => {

        setOpenEditTags(false);

        if (newTags) {
            // The tags have been updated
            await apiGateway.gameStartReplaceTags(selectedGameUUID[0], newTags);
        }
    }

    // Load the tags when a game is selected
    React.useEffect(() => {

        if (isOnlyOneGameSelected) {
            const selectedGameData = gameUUIDSelectionRows.filter((item) => item.id == selectedGameUUID[0]);
            setCurrentTags(selectedGameData[0]?.tags || []);
        }
        else
            setCurrentTags([]);

    }, [selectedGameUUID, isOnlyOneGameSelected, gameUUIDSelectionRows]);

    const onClickButtonEditTags = () => {
        setOpenEditTags(true);
    }

    return (
        <Stack direction="column" spacing={1}>

            <EditTags
                initialTags={currentTags}
                open={openEditTags}
                onClose={handleCloseEditTags}
            />

            {displayGameStatistics &&
            <GameStats
                open={displayGameStatistics}
                onClose={() => { setDisplayGameStatistics(false); } }
                gameUUID={selectedGameUUID[0]}
                rows={gameStatistics}
            />
            }

            {displayPeerInfo &&
            <PeerInfoDetails
                open={displayPeerInfo}
                onClose={() => { setDisplayPeerInfo(false); } }
                rows={peerInfoRows}
            />
            }

            {displayGameEnd &&
            <GameEndDetails
                open={displayGameEnd}
                onClose={() => { setDisplayGameEnd(false); }}
                rows={gameEndRows}
            />
            }

            {displayGameOption &&
            <GameOptionDetails
                open={displayGameOption}
                onClose={() => { setDisplayGameOption(false); }}
                data={gameOptionData}
            />
            }

            {displayGameDetails &&
            <GameDetails
                open={displayGameDetails}
                onClose={() => { setDisplayGameDetails(false); }}
                gameUUID={selectedGameUUID[0]}
                dataCache={dataCache}
                api={apiGateway}
            />
            }

            {displayStatistics &&
            <Statistics 
                open={displayStatistics}
                onClose={() => { setDisplayStatistics(false); }}
                gameUUIDList={selectedGameUUID}
                dataCache={dataCache }
            />
            }

            <Stack direction="row" spacing={1}>
                <Button
                    variant="contained"
                    onClick={onClickButtonRemoveOrphan}
                >Remove orphans</Button>
                <Button
                    variant="contained"
                    onClick={onClickButtonRefreshGameUUIDSelection}
                    disabled={isAutoRefresh}
                >Refresh</Button>
                <FormControlLabel control={<Switch checked={isAutoRefresh} onChange={() => { setIsAutoRefresh(!isAutoRefresh); }} inputProps={{ 'aria-label': 'controlled' }} />} label="Auto refresh" />
            </Stack>

            <DataGrid
                autoHeight
                rowHeight={25}
                rows={gameUUIDSelectionRows}
                rowCount={rowCount}
                columns={gameUUIDSelectionColumns}
                pageSizeOption={[25]}
                paginationModel={paginationModel}
                paginationMode="server"
                onPaginationModelChange={setPaginationModel}
                checkboxSelection
                loading={isGameUUIDSelectionLoading}
                onRowSelectionModelChange={onGameUUIDSelectionChange}
            />

            <Stack direction="column" spacing={1}>
                <Stack direction="row" alignItems="center" spacing={1}>
                    <Button
                        variant="contained"
                        onClick={onClickButtonGameUUIDDelete}
                        disabled={isGameUUIDSelectionEmpty}
                    >Delete</Button>
                    <Button
                        variant="contained"
                        onClick={() => { setDisplayStatistics(true); }}
                        disabled={isGameUUIDSelectionEmpty}
                    >Compute statistics</Button>
                </Stack>
                <Stack direction="row" alignItems="center" spacing={1}>
                    <Button
                        variant="contained"
                        onClick={onClickButtonShowPeerInfo}
                        disabled={!isOnlyOneGameSelected}
                    >Peer info</Button>
                    <Button
                        variant="contained"
                        onClick={onClickButtonShowGameOption}
                        disabled={!isOnlyOneGameSelected}
                    >Game option</Button>
                    <Button
                        variant="contained"
                        onClick={onClickButtonShowStatistics}
                        disabled={!isOnlyOneGameSelected}
                    >Player statistics</Button>
                    <Button
                        variant="contained"
                        onClick={onClickButtonShowGameEnd}
                        disabled={!isOnlyOneGameSelected}
                    >Game end</Button>
                    <Button
                        variant="contained"
                        onClick={onClickButtonShowGameDetails}
                        disabled={!isOnlyOneGameSelected}
                    >Details</Button>
                    <Button 
                        variant="contained"
                        onClick={onClickButtonEditTags}
                        disabled={!isOnlyOneGameSelected}
                    >Edit tags</Button>
                    {isLoading && <CircularProgress />}

                </Stack>
            </Stack>

            {displayStatusMessage &&
            <StatusMessage
                open={displayStatusMessage}
                setOpen={setDisplayStatusMessage}
                title={statusTitle}
                message={statusMessage}
                severity={statusSeverity}
            />
            }
        </Stack>
    );
}

Games.propTypes = {
    apiGateway: PropTypes.object.isRequired,
    dataCache: PropTypes.object.isRequired,
    optimalCache: PropTypes.object.isRequired,
    refreshDelay: PropTypes.number,
};