
export class GameDataCache {
    #apiGateway
    #gameOptionMap
    #gameStartMap
    #gameEndMap
    #peerInfoMap
    #playerActionMap
    #gameStateMap
    #overviewMap

    constructor(apiGateway) {
        this.#apiGateway = apiGateway;
        this.#gameOptionMap = {};
        this.#gameStartMap = {};
        this.#gameEndMap = {};
        this.#peerInfoMap = {};
        this.#playerActionMap = {};
        this.#gameStateMap = {};
        this.#overviewMap = {};
    }

    async overview(gameUUID, useCache = true) {
        if (!useCache || !Object.getOwnPropertyNames(this.#overviewMap).includes(gameUUID)) {
            // The data are not in cache
            const overview = await this.#apiGateway.getGameSummary({ gameUUID });
            if (overview === undefined) {
                return undefined;
            }
            this.#overviewMap[gameUUID] = overview;
        }

        return this.#overviewMap[gameUUID];
    }

    async gameOption(id, useCache = true) {
        if (!useCache || !Object.getOwnPropertyNames(this.#gameOptionMap).includes(id)) {
            // The data are not in cache
            const gameOptionData = await this.#apiGateway.getGameOption({ gameOptionId: id });   
            let gameOption = undefined;
            if (gameOptionData && gameOptionData.metadata.documentCount == 1) {
                gameOption = gameOptionData.data[0];
            }
            if (gameOption === undefined) {
                return undefined;
            }
            this.#gameOptionMap[id] = gameOption;
        }

        return this.#gameOptionMap[id];
    }

    async gameStart(gameUUID, useCache = true) {
        if (!useCache || !Object.getOwnPropertyNames(this.#gameStartMap).includes(gameUUID)) {
            // The data are not in cache
            const gameStart = await this.#apiGateway.getGameStart(gameUUID);
            if (gameStart === undefined) {
                return undefined;
            }
            this.#gameStartMap[gameUUID] = gameStart;
        }

        return this.#gameStartMap[gameUUID];
    }

    async gameEnd(gameUUID, useCache = true) {
        if (!useCache || !Object.getOwnPropertyNames(this.#gameEndMap).includes(gameUUID)) {
            // The date are not in cache
            const gameEnd = await this.#apiGateway.getGameEnd(gameUUID);
            if (gameEnd === undefined) {
                return undefined;
            }
            this.#gameEndMap[gameUUID] = gameEnd;
        }

        return this.#gameEndMap[gameUUID];
    }

    async peerInfo(gameUUID, useCache = true) {
        if (!useCache || !Object.getOwnPropertyNames(this.#peerInfoMap).includes(gameUUID)) {
            // The game UUID data are not in cache
            const peerInfoList = await this.#apiGateway.getPeerInfo(gameUUID);
            this.#peerInfoMap[gameUUID] = peerInfoList;
        }

        return this.#peerInfoMap[gameUUID];
    }

    async playerAction(gameUUID, playerTurn, useCache = true) {
        if (!useCache || !Object.getOwnPropertyNames(this.#playerActionMap).includes(gameUUID)) {
            // No Game UUID data are loaded
            this.#playerActionMap[gameUUID] = {};
        }
        
        if (!useCache || !Object.getOwnPropertyNames(this.#playerActionMap[gameUUID]).includes(playerTurn.toString())) {
            // The data are not in cache
            const playerAction = await this.#apiGateway.getPlayerAction(gameUUID, playerTurn);
            if (playerAction === undefined) {
                return undefined;
            }
            this.#playerActionMap[gameUUID][playerTurn] = playerAction;
        }

        return this.#playerActionMap[gameUUID][playerTurn];
    }

    async gameState(gameUUID, playerTurn, useCache = true) {
        if (!useCache || !Object.getOwnPropertyNames(this.#gameStateMap).includes(gameUUID)) {
            // No Game UUID data are loaded
            this.#gameStateMap[gameUUID] = {};
        }

        if (!useCache || !Object.getOwnPropertyNames(this.#gameStateMap[gameUUID]).includes(playerTurn.toString())) {
            // The data are not in cache
            const gameState = await this.#apiGateway.getGameState(gameUUID, playerTurn);
            if (gameState === undefined) {
                return undefined;
            }
            this.#gameStateMap[gameUUID][playerTurn] = gameState;
        }

        return this.#gameStateMap[gameUUID][playerTurn];
    }
}


export class OptimalCache {
    #optimalGateway
    #resultMap

    constructor(optimalGateway) {
        this.#optimalGateway = optimalGateway;
        this.#resultMap = {};
    }

    async submit(gameOptionId, useCache = true) {
        if (!useCache || !Object.getOwnPropertyNames(this.#resultMap).includes(gameOptionId)) {
            // The data are not in cache
            const result = await this.#optimalGateway.submit(gameOptionId, 5000);
            if (result === undefined) {
                return undefined;
            }
            this.#resultMap[gameOptionId] = result;
        }

        return this.#resultMap[gameOptionId];
    }
}