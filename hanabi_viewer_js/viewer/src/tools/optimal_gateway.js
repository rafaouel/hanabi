
export class OptimalGateway {

    #apiUrl = "";

    constructor(apiUrl) {
        this.#apiUrl = apiUrl;
    }

    // Healthchecks
    async readinessProbe() {
        try {
            const target = "api/v1/readiness-probe/";
            const options = {};
            const response = await fetch(this.#apiUrl + target, options);

            return response.ok;
        }
        catch {
            return false;
        }
    }

    async livenessProbe() {
        try {
            const target = "api/v1/liveness-probe/";
            const options = {};
            const response = await fetch(this.#apiUrl + target, options);

            return response.ok;
        }
        catch (err) {
            return false;
        }
    }

    async submit(gameOptionId, timeout = 1000) {

        const target = "api/v1/submit";
        const query = "?gameOptionId=" + gameOptionId;
        const url = this.#apiUrl + target + query;

        const headers = new Headers();
        headers.append("Accept", "application/json");

        const options = {
            headers,
            signal: AbortSignal.timeout(timeout),
        }

        const response = await fetch(url, options);

        if (!response.ok)
            throw new Error(`Error while retrieving data (status=${response.status})`);

        const jsonData = await response.json();
        return jsonData;
    }
}