
export class APIGateway {

    #apiUrl = "";
    #apiLogin = "";
    #apiPassword = "";
    #accessToken = "";
    #accessTokenExpiration = new Date(-8640000000000000);
    #refreshToken = "";
    #refreshTokenExpiration = new Date(-8640000000000000);
    #expirationMargin = 30 * 1000;

    constructor(apiUrl) {
        this.#apiUrl = apiUrl;
        this.#accessToken = "";
        this.#accessTokenExpiration = new Date(-8640000000000000);
        this.#refreshToken = "";
        this.#refreshTokenExpiration = new Date(-8640000000000000);
    }

    // Healthchecks
    async readinessProbe() {
        try {
            const target = "api/v1/healthcheck/readiness-probe/";
            const options = { };
            const response = await fetch(this.#apiUrl + target, options);

            return response.ok;
        }
        catch {
            return false;
        }
    }

    async livenessProbe() {
        try {
            const target = "api/v1/healthcheck/liveness-probe/";
            const options = {};
            const response = await fetch(this.#apiUrl + target, options);

            return response.ok;
        }
        catch {
            return false;
        }
    }

    // Connection to API
    async connect(apiLogin, apiPasword, timeout = 1000) {

        this.#apiLogin = apiLogin;
        this.#apiPassword = apiPasword;

        try {

            await this.retrieveTokens(timeout);

            return true;
        }
        catch (err) {
            return false;
        }
    }

    async #getAccessToken() {

        const now = new Date(Date.now() + this.#expirationMargin);

        if (this.#accessToken.length > 0) {
            // We have an access token

            if (now < this.#accessTokenExpiration) {
                // We have an access token that has not expired

                return this.#accessToken;
            }
        }

        // We do not have an access token
        if (this.#refreshToken.length > 0) {
            // We have a refresh token

            if (now < this.#refreshTokenExpiration) {
                // We have a refresh token that has not expired

                await this.refreshTokens();

                return this.#accessToken;
            }
        }

        // We don't have any token
        await this.retrieveTokens();

        return this.#accessToken;
    }

    async refreshTokens() {
        const target = "api/v1/tokens/";
        let headers = new Headers();
        headers.append('authorization', this.#bearerAuthRefresh());
        const options = {
            headers,
            method: "PUT"
        };
        const response = await fetch(this.#apiUrl + target, options);

        if (!response.ok) {
            throw new Error("Unable to refresh tokens");
        }

        const json = await response.json();
        this.#accessToken = json.accessToken;
        this.#accessTokenExpiration = new Date(json.accessTokenExpirationDate);
        this.#refreshToken = json.refreshToken;
        this.#refreshTokenExpiration = new Date(json.refreshTokenExpirationDate);
    }

    async retrieveTokens(timeout = 1000) {
        const target = "api/v1/tokens/";
        let headers = new Headers();
        headers.append('authorization', this.#basicAuth());
        const options = {
            headers,
            method: "POST",
            signal: AbortSignal.timeout(timeout),
        };
        const response = await fetch(this.#apiUrl + target, options);

        if (!response.ok)
            throw new Error("Unable to retrieve tokens");

        const json = await response.json();
        this.#accessToken = json.accessToken;
        this.#accessTokenExpiration = new Date(json.accessTokenExpirationDate);
        this.#refreshToken = json.refreshToken;
        this.#refreshTokenExpiration = new Date(json.refreshTokenExpirationDate);
    }

    #basicAuth() {
        return "Basic " + btoa(this.#apiLogin + ":" + this.#apiPassword);
    }

    async #bearerAuth() {
        return "Bearer " + await this.#getAccessToken();
    }

    #bearerAuthRefresh() {
        return "Bearer " + this.#refreshToken;
    }

    /////////////////
    // Game Option //
    async getGameOption(param, timeout = 1000) {

        const page = param.page || 0;
        const limit = param.pageSize || 1;
        let target = "api/v2/hanabi/gameOption/?page=" + page.toString() + "&limit=" + limit.toString();

        if (param.gameOptionId)
            target += `&gameOptionId=${param.gameOptionId}`;

        if (param.sort_by)
            target += "&sort_by=" + param.sort_by;
        
        let headers = new Headers();
        headers.append('pragma', 'no-cache');
        headers.append('cache-control', 'no-cache');
        headers.append('authorization', await this.#bearerAuth());
        const options = {
            headers,
            signal: AbortSignal.timeout(timeout),
        };
        const response = await fetch(this.#apiUrl + target, options);

        if (!response.ok)
            throw new Error(`Error while retrieving data (status=${response.status})`);

        const jsonData = await response.json();
        return jsonData;
    }

    async gameOptionReplaceTags(gameOptionId, newTags, timeout = 1000) {
        try {
            const target = "api/v2/hanabi/gameOption/gameOptionId/" + gameOptionId.toString();
            let headers = new Headers();
            headers.append('authorization', await this.#bearerAuth());
            headers.append('Content-Type', 'application/json');
            const body = {
                tags: newTags,
            }
            const options = {
                headers,
                method: 'PUT',
                signal: AbortSignal.timeout(timeout),
                body: JSON.stringify(body),
            };
            const response = await fetch(this.#apiUrl + target, options);
            return response.ok;
        }
        catch (err) {
            return false;
        }
    }

    async deleteGameOption(gameOptionId, timeout = 1000) {
        try {
            const target = "api/v2/hanabi/gameOption/gameUuid/" + gameOptionId.toString();
            let headers = new Headers();
            headers.append('authorization', await this.#bearerAuth());
            const options = {
                headers,
                method: 'DELETE',
                signal: AbortSignal.timeout(timeout),
            };
            const response = await fetch(this.#apiUrl + target, options);
            return response.ok;
        }
        catch (err) {
            return false;
        }
    }

    ////////////////
    // Game Start //
    async getGameStart(gameUUID, timeout = 1000) {
        try {
            const target = "api/v1/hanabi/gameStart/?gameUuid=" + gameUUID.toString();
            let headers = new Headers();
            headers.append('pragma', 'no-cache');
            headers.append('cache-control', 'no-cache');
            headers.append('authorization', await this.#bearerAuth());
            const options = {
                headers,
                signal: AbortSignal.timeout(timeout),
            };
            const response = await fetch(this.#apiUrl + target, options);

            if (!response.ok) {
                return undefined;
            }

            const jsonData = await response.json();
            if (jsonData.metadata.documentCount == 1) {
                return jsonData.data[0];
            }
            return undefined;
        }
        catch {
            return undefined;
        }
    }

    async gameStartReplaceTags(gameUUID, newTags, timeout = 1000) {
        try {
            const target = "api/v1/hanabi/gameStart/gameUuid/" + gameUUID.toString();

            const headers = new Headers();
            headers.append('authorization', await this.#bearerAuth());
            headers.append('content-type', "application/json");

            const body = JSON.stringify({
                tags: newTags
            });

            const options = {
                headers,
                method: "PUT",
                signal: AbortSignal.timeout(timeout),
                body,
            }

            const response = await fetch(this.#apiUrl + target, options);

            return response.ok;
        }
        catch {
            return false;
        }
    }

    async deleteGameStart(gameUUID, timeout = 1000) {
        try {
            const target = "api/v1/hanabi/gameStart/gameUuid/" + gameUUID.toString();
            let headers = new Headers();
            headers.append('authorization', await this.#bearerAuth());
            const options = {
                headers,
                method: 'DELETE',
                signal: AbortSignal.timeout(timeout),
            };
            const response = await fetch(this.#apiUrl + target, options);
            return response.ok;
        }
        catch (err) {
            return false;
        }
    }

    ///////////////
    // Peer Info //
    async getPeerInfo(gameUUID, timeout = 1000) {
        try {
            const target = "api/v1/hanabi/peerInfo/?gameUuid=" + gameUUID.toString();
            let headers = new Headers();
            headers.append('pragma', 'no-cache');
            headers.append('cache-control', 'no-cache');
            headers.append('authorization', await this.#bearerAuth());
            const options = {
                headers: headers,
                signal: AbortSignal.timeout(timeout),
            };
            const response = await fetch(this.#apiUrl + target, options);

            if (!response.ok) {
                return undefined;
            }

            const jsonData = await response.json();
            if (jsonData.metadata.documentCount > 0) {
                return jsonData.data;
            }
        }
        catch {
            return undefined;
        }
    }

    async deletePeerInfo(gameUUID, timeout = 1000) {
        try {
            const target = "api/v1/hanabi/peerInfo/gameUuid/" + gameUUID.toString();
            let headers = new Headers();
            headers.append('authorization', await this.#bearerAuth());
            const options = {
                headers,
                method: 'DELETE',
                signal: AbortSignal.timeout(timeout),
            };
            const response = await fetch(this.#apiUrl + target, options);
            return response.ok;
        }
        catch {
            return false;
        }
    }

    ////////////////
    // Game State //
    async getGameState(gameUUID, playerTurn, timeout = 1000) {
        try {
            const target = "api/v1/hanabi/gameState/?gameUuid=" + gameUUID.toString() + "&playerTurnCount=" + playerTurn.toString();
            let headers = new Headers();
            headers.append('authorization', await this.#bearerAuth());
            const options = {
                headers,
                signal: AbortSignal.timeout(timeout),
            };
            const response = await fetch(this.#apiUrl + target, options);

            if (!response.ok) {
                return undefined;
            }

            const jsonData = await response.json();
            if (jsonData.metadata.documentCount == 1) {
                return jsonData.data[0];
            }
            return undefined;
        }
        catch {
            return undefined;
        }
    }

    async deleteGameState(gameUUID, timeout = 1000) {
        try {
            const target = "api/v1/hanabi/gameState/gameUuid/" + gameUUID.toString();
            let headers = new Headers();
            headers.append('authorization', await this.#bearerAuth());
            const options = {
                headers,
                method: 'DELETE',
                signal: AbortSignal.timeout(timeout),
            };
            const response = await fetch(this.#apiUrl + target, options);
            return response.ok;
        }
        catch {
            return false;
        }
    }

    ///////////////////
    // Player Action //
    async getPlayerAction(gameUUID, playerTurn, timeout = 1000) {
        try {
            const target = "api/v1/hanabi/playerAction/?gameUuid=" + gameUUID.toString() + "&playerTurnCount=" + playerTurn.toString();
            let headers = new Headers();
            headers.append('authorization', await this.#bearerAuth());
            const options = {
                headers,
                signal: AbortSignal.timeout(timeout),
            };
            const response = await fetch(this.#apiUrl + target, options);

            if (!response.ok) {
                return undefined;
            }

            const jsonData = await response.json();
            if (jsonData.metadata.documentCount == 1) {
                return jsonData.data[0];
            }
            return undefined;
        }
        catch {
            return undefined;
        }
    }

    async deletePlayerAction(gameUUID, timeout = 1000) {
        try {
            const target = "api/v1/hanabi/playerAction/gameUuid/" + gameUUID.toString();
            let headers = new Headers();
            headers.append('authorization', await this.#bearerAuth());
            const options = {
                headers,
                method: 'DELETE',
                signal: AbortSignal.timeout(timeout),
            };
            const response = await fetch(this.#apiUrl + target, options);
            return response.ok;
        }
        catch {
            return false;
        }
    }

    //////////////
    // Game End //
    async getGameEnd(gameUUID, timeout = 1000) {
        try {
            const target = "api/v1/hanabi/gameEnd/?gameUuid=" + gameUUID.toString();
            let headers = new Headers();
            headers.append('pragma', 'no-cache');
            headers.append('cache-control', 'no-cache');
            headers.append('authorization', await this.#bearerAuth());
            const options = {
                headers: headers,
                signal: AbortSignal.timeout(timeout),
            };
            const response = await fetch(this.#apiUrl + target, options);

            if (!response.ok) {
                return undefined;
            }

            const jsonData = await response.json();
            if (jsonData.metadata.documentCount == 1) {
                return jsonData.data[0];
            }

            return undefined;
        }
        catch {
            return undefined;
        }
    }

    async deleteGameEnd(gameUUID, timeout = 1000) {
        try {
            const target = "api/v1/hanabi/gameEnd/gameUuid/" + gameUUID.toString();
            let headers = new Headers();
            headers.append('authorization', await this.#bearerAuth());
            const options = {
                headers,
                method: 'DELETE',
                signal: AbortSignal.timeout(timeout),
            };
            const response = await fetch(this.#apiUrl + target, options);
            return response.ok;
        }
        catch {
            return false;
        }
    }

    //////////////////
    // Game Summary //
    async getGameSummary(param, timeout = 1000) {
        try {
            const page = param.page ? param.page : 0;
            const limit = param.limit ? param.limit : 1;
            let target = `api/v2/hanabi/game/overview/?page=${page}&limit=${limit}`;
            if (param.gameUUID) {
                target += `&gameUuid=${param.gameUUID}`
            }
            let headers = new Headers();
            headers.append('pragma', 'no-cache');
            headers.append('cache-control', 'no-cache');
            headers.append('authorization', await this.#bearerAuth());
            const options = {
                headers: headers,
                signal: AbortSignal.timeout(timeout),
            };
            const response = await fetch(this.#apiUrl + target, options);

            if (!response.ok) {
                return undefined;
            }

            const jsonData = await response.json();
            return jsonData;
        }
        catch (err) {
            return undefined;
        }
    }

    async deleteGame(gameUUID, timeout = 1000) {
        await this.deleteGameStart(gameUUID, timeout);
        await this.deletePeerInfo(gameUUID, timeout);
        await this.deleteGameState(gameUUID, timeout);
        await this.deletePlayerAction(gameUUID, timeout);
        await this.deleteGameEnd(gameUUID, timeout);
    }

    async listGameStartGameUUID(timeout = 1000) {
        try {
            const target = "api/v2/hanabi/gameUuid/gameStart/";
            let headers = new Headers();
            headers.append('pragma', 'no-cache');
            headers.append('cache-control', 'no-cache');
            headers.append('authorization', await this.#bearerAuth());
            const options = {
                headers: headers,
                signal: AbortSignal.timeout(timeout),
            };
            const response = await fetch(this.#apiUrl + target, options);

            if (!response.ok) {
                return [];
            }

            return await response.json();
        }
        catch {
            return [];
        }
    }

    async listPeerInfoGameUUID(timeout = 1000) {
        try {
            const target = "api/v2/hanabi/gameUuid/peerInfo/";
            let headers = new Headers();
            headers.append('pragma', 'no-cache');
            headers.append('cache-control', 'no-cache');
            headers.append('authorization', await this.#bearerAuth());
            const options = {
                headers: headers,
                signal: AbortSignal.timeout(timeout),
            };
            const response = await fetch(this.#apiUrl + target, options);

            if (!response.ok) {
                return [];
            }

            return await response.json();
        }
        catch {
            return [];
        }
    }

    async listGameStateGameUUID(timeout = 1000) {
        try {
            const target = "api/v2/hanabi/gameUuid/gameState/";
            let headers = new Headers();
            headers.append('pragma', 'no-cache');
            headers.append('cache-control', 'no-cache');
            headers.append('authorization', await this.#bearerAuth());
            const options = {
                headers: headers,
                signal: AbortSignal.timeout(timeout),
            };
            const response = await fetch(this.#apiUrl + target, options);

            if (!response.ok) {
                return [];
            }

            return await response.json();
        }
        catch {
            return [];
        }
    }

    async listPlayerActionGameUUID(timeout = 1000) {
        try {
            const target = "api/v2/hanabi/gameUuid/playerAction/";
            let headers = new Headers();
            headers.append('pragma', 'no-cache');
            headers.append('cache-control', 'no-cache');
            headers.append('authorization', await this.#bearerAuth());
            const options = {
                headers: headers,
                signal: AbortSignal.timeout(timeout),
            };
            const response = await fetch(this.#apiUrl + target, options);

            if (!response.ok) {
                return [];
            }

            return await response.json();
        }
        catch {
            return [];
        }
    }

    async listGameEndGameUUID(timeout = 1000) {
        try {
            const target = "api/v2/hanabi/gameUuid/gameEnd/";
            let headers = new Headers();
            headers.append('pragma', 'no-cache');
            headers.append('cache-control', 'no-cache');
            headers.append('authorization', await this.#bearerAuth());
            const options = {
                headers: headers,
                signal: AbortSignal.timeout(timeout),
            };
            const response = await fetch(this.#apiUrl + target, options);

            if (!response.ok) {
                return [];
            }

            return await response.json();
        }
        catch {
            return [];
        }
    }
}