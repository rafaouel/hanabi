# Hanabi Server

## Build image

Use the following command to build the hanabi game server image from the 'hanabi_server_linux' directory :

``` bash
docker build -t registry.gitlab.com/rafaouel/hanabi/hanabi_server:<server_version> .
```

To push the images to the registry :
```
# Login
docker login registry.gitlab.com/rafaouel/hanabi
# Enter the username then a project access token

# Push image
docker push registry.gitlab.com/rafaouel/hanabi/hanabi_server:<server_version>
```

## Run image

### Docker

Use the following command to run the game server docker image :

```
docker run -p 32000:32000 hanabi_server:<server_version>
```

You can then join the server at localhost:32000

### Kubernetes

Apply the headless service manifest:
``` yaml
apiVersion: v1
kind: Service
metadata:
  name: hanabi-server
  namespace: hanabi
spec:
  clusterIP: None
  ports:
    - name: game-port
      port: 32000
      protocol: TCP
      targetPort: 32000
  selector:
    workload.user.cattle.io/workloadselector: apps.statefulset-hanabi-hanabi-server
  sessionAffinity: None
  type: ClusterIP
```

Apply the StatefulSet manifest:

``` yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: hanabi-server
  labels:
    workload.user.cattle.io/workloadselector: apps.statefulset-hanabi-hanabi-server
  namespace: hanabi
spec:
  selector:
    matchLabels:
      workload.user.cattle.io/workloadselector: apps.statefulset-hanabi-hanabi-server
  template:
    metadata:
      labels:
        workload.user.cattle.io/workloadselector: apps.statefulset-hanabi-hanabi-server
      namespace: hanabi
    spec:
      containers:
        - args:
            - '--api_host'
            - $(API_HOST)
            - '--api_port'
            - $(API_PORT)
            - '--player_count'
            - $(PLAYER_COUNT)
            - '--tags'
            - $(TAGS)
            - '--save_to_db'
            - '--mode'
            - $(MODE)
          command:
            - ./hanabi_server_linux.out
          env:
            - name: API_HOST
              valueFrom:
                configMapKeyRef:
                  key: api-host
                  name: api-settings
                  optional: false
            - name: API_PORT
              valueFrom:
                configMapKeyRef:
                  key: api-port
                  name: api-settings
                  optional: false
            - name: PLAYER_COUNT
              valueFrom:
                configMapKeyRef:
                  key: player_count
                  name: server-option
                  optional: false
            - name: MODE
              valueFrom:
                configMapKeyRef:
                  key: mode
                  name: server-option
                  optional: false
            - name: TAGS
              valueFrom:
                configMapKeyRef:
                  key: tags
                  name: server-option
                  optional: false
          image: registry.gitlab.com/rafaouel/hanabi/hanabi_server:<server_version>
          imagePullPolicy: IfNotPresent
          name: container-0
          securityContext:
            allowPrivilegeEscalation: false
            privileged: false
            readOnlyRootFilesystem: false
            runAsNonRoot: false
          terminationMessagePath: /dev/termination-log
          terminationMessagePolicy: File
          workingDir: /app
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      schedulerName: default-scheduler
      terminationGracePeriodSeconds: 30
  persistentVolumeClaimRetentionPolicy:
    whenDeleted: Retain
    whenScaled: Retain
  podManagementPolicy: OrderedReady
  replicas: 1
  revisionHistoryLimit: 10
  serviceName: hanabi-server
  updateStrategy:
    rollingUpdate:
      partition: 0
    type: RollingUpdate
```

Apply a NodePort service manifest to expose the game server:

``` yaml
apiVersion: v1
kind: Service
metadata:
  name: hanabi-node-port
  namespace: hanabi
spec:
  ports:
    - name: game-port
      nodePort: 32000
      port: 32000
      protocol: TCP
      targetPort: 32000
  selector:
    workload.user.cattle.io/workloadselector: apps.statefulset-hanabi-hanabi-server
  sessionAffinity: None
  type: NodePort
```