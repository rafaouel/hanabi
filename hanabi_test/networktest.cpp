#include "pch.h"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace hanabi;

namespace network_test
{
	TEST_CLASS(NetworkTest)
	{
	public:

		TEST_METHOD(HelloEndToEnd)
		{
			network::CPacketCoder coder(network::PacketType::HELLO);
			boost::json::object request_content;
			const std::string coder_message("Hello world !");
			const hanabi::common::TIndex coder_player_id = 12;
			request_content["message"] = boost::json::value_from(coder_message);
			request_content["player_id"] = boost::json::value_from(int(coder_player_id));
			coder.data(request_content);

			const network::CPacket packet = coder.encode();

			network::CPacketParser parser(network::PacketType::HELLO);
			parser.decode(packet);
			boost::json::object parsed_object = parser.data().as_object();
			const std::string parsed_message = parsed_object.at("message").as_string().c_str();
			const hanabi::common::TIndex parsed_player_id = parsed_object.at("player_id").as_int64();

			Assert::AreEqual(coder_message, parsed_message);
			Assert::AreEqual(coder_player_id, parsed_player_id);
		}

		TEST_METHOD(PeerInfoRoundtripEndToEnd)
		{
			{
				// Server asks for peer info
				network::CPacketCoder coder(network::PacketType::PEER_INFO_REQUEST);
				boost::json::object content;
				const common::TIndex player_id = 12;
				content["player_id"] = boost::json::value_from(player_id);
				coder.data(content);

				const network::CPacket packet = coder.encode();

				network::CPacketParser parser(network::PacketType::PEER_INFO_REQUEST);
				parser.decode(packet);
				const boost::json::object parsed_response = parser.data().as_object();
				const common::TIndex parsed_player_id = parsed_response.at("player_id").as_int64();

				Assert::AreEqual(player_id, parsed_player_id);
			}

			{
				// Client sends peer info
				network::CPacketCoder coder(network::PacketType::PEER_INFO_RESPONSE);
				boost::json::object content;
				const std::string name("Test t357");
				content["name"] = boost::json::value_from(name);
				coder.data(content);

				const network::CPacket packet = coder.encode();

				network::CPacketParser parser(network::PacketType::PEER_INFO_RESPONSE);
				parser.decode(packet);
				const boost::json::object object = parser.data().as_object();
				const std::string parsed_name = object.at("name").as_string().c_str();
			}
		}

		TEST_METHOD(ActionResponseEndToEndDrop)
		{
			// Check the convertion

			// Data at start
			common::CDrop coder_action(1);

			// Convertion to json
			boost::json::object coder_object = coder_action.to_json().as_object();
			Assert::IsTrue(coder_object.at("action_type").is_string());
			Assert::IsTrue(coder_object.at("hand_index").is_int64());

			// Coder
			network::CPacketCoder coder(network::PacketType::ACTION_RESPONSE);
			coder.data(coder_object);
			const network::CPacket packet = coder.encode();

			// Parser
			network::CPacketParser parser(network::PacketType::ACTION_RESPONSE);
			parser.decode(packet);


			Assert::AreEqual(int(boost::json::kind::object), int(parser.data().kind()));

			const boost::json::object& parser_object = parser.data().as_object();
			
			Assert::AreEqual(coder_object.at("action_type").as_string().c_str(), parser_object.at("action_type").as_string().c_str());
			Assert::AreEqual(coder_object.at("hand_index").as_int64(), parser_object.at("hand_index").as_int64());

			const std::shared_ptr<common::IAction> p_parser_action = common::create_action(parser.data());
			common::CDrop parser_action = *std::static_pointer_cast<common::CDrop, common::IAction>(p_parser_action);

			Assert::AreEqual(coder_action.hand_index(), parser_action.hand_index());
			Assert::AreEqual(int(coder_action.type()), int(parser_action.type()));
		}

		TEST_METHOD(ActionResponseEndToEndPlace)
		{
			// Check the convertion

			// Data at start
			common::CPlace coder_action(1);

			// Convertionn to json
			boost::json::object coder_object = coder_action.to_json().as_object();
			Assert::IsTrue(coder_object.at("action_type").is_string());
			Assert::IsTrue(coder_object.at("hand_index").is_int64());

			// Coder
			network::CPacketCoder coder(network::PacketType::ACTION_RESPONSE);
			coder.data(coder_object);
			const network::CPacket packet = coder.encode();

			// Parser
			network::CPacketParser parser(network::PacketType::ACTION_RESPONSE);
			parser.decode(packet);


			Assert::AreEqual(int(boost::json::kind::object), int(parser.data().kind()));

			const boost::json::object& parser_object = parser.data().as_object();

			Assert::AreEqual(coder_object.at("action_type").as_string().c_str(), parser_object.at("action_type").as_string().c_str());
			Assert::AreEqual(coder_object.at("hand_index").as_int64(), parser_object.at("hand_index").as_int64());

			const std::shared_ptr<common::IAction> p_parser_action = common::create_action(parser.data());
			common::CPlace parser_action = *std::static_pointer_cast<common::CPlace, common::IAction>(p_parser_action);

			Assert::AreEqual(coder_action.hand_index(), parser_action.hand_index());
			Assert::AreEqual(int(coder_action.type()), int(parser_action.type()));
		}

		TEST_METHOD(ActionResponseEndToEndSpeakValue)
		{
			// Check the convertion

			// Data at start
			common::CSpeak coder_action(1);
			coder_action.value_info(1);

			// Convertionn to json
			boost::json::object coder_object = coder_action.to_json().as_object();
			Assert::IsTrue(coder_object.at("action_type").is_string());
			Assert::IsTrue(coder_object.at("target_player_id").is_int64());
			Assert::IsTrue(coder_object.at("card_info").is_object());
			Assert::IsTrue(coder_object.at("card_info").at("value").is_int64());
			Assert::IsTrue(!coder_object.at("card_info").as_object().contains("color"));

			// Coder
			network::CPacketCoder coder(network::PacketType::ACTION_RESPONSE);
			coder.data(coder_object);
			const network::CPacket packet = coder.encode();

			// Parser
			network::CPacketParser parser(network::PacketType::ACTION_RESPONSE);
			parser.decode(packet);

			Assert::AreEqual(int(boost::json::kind::object), int(parser.data().kind()));

			const boost::json::object& parser_object = parser.data().as_object();

			Assert::AreEqual(coder_object.at("action_type").as_string().c_str(), parser_object.at("action_type").as_string().c_str());
			Assert::AreEqual(coder_object.at("target_player_id").as_int64(), parser_object.at("target_player_id").as_int64());
			Assert::IsTrue(parser_object.at("card_info").is_object());
			Assert::IsTrue(parser_object.at("card_info").at("value").is_int64());
			Assert::AreEqual(coder_object.at("card_info").at("value").as_int64(), parser_object.at("card_info").at("value").as_int64());
			Assert::IsTrue(!parser_object.at("card_info").as_object().contains("color"));

			const std::shared_ptr<common::IAction> p_parser_action = common::create_action(parser.data());
			common::CSpeak parser_action = *std::static_pointer_cast<common::CSpeak, common::IAction>(p_parser_action);

			Assert::AreEqual(coder_action.is_color_info(), parser_action.is_color_info());
			Assert::AreEqual(coder_action.is_value_info(), parser_action.is_value_info());
			Assert::AreEqual(coder_action.player_index(), parser_action.player_index());
			Assert::AreEqual(coder_action.value_info(), parser_action.value_info());
			Assert::AreEqual(int(coder_action.type()), int(parser_action.type()));
		}

		TEST_METHOD(ActionResponseEndToEndSpeakColor)
		{
			// Check the convertion

			// Data at start
			common::CSpeak coder_action(1);
			coder_action.color_info(common::CardColor::blue);

			// Convertionn to json
			boost::json::object coder_object = coder_action.to_json().as_object();
			Assert::IsTrue(coder_object.at("action_type").is_string());
			Assert::IsTrue(coder_object.at("target_player_id").is_int64());
			Assert::IsTrue(coder_object.at("card_info").is_object());
			Assert::IsTrue(coder_object.at("card_info").at("color").is_string());
			Assert::IsTrue(!coder_object.at("card_info").as_object().contains("value"));

			// Coder
			network::CPacketCoder coder(network::PacketType::ACTION_RESPONSE);
			coder.data(coder_object);
			const network::CPacket packet = coder.encode();

			// Parser
			network::CPacketParser parser(network::PacketType::ACTION_RESPONSE);
			parser.decode(packet);

			Assert::AreEqual(int(boost::json::kind::object), int(parser.data().kind()));

			const boost::json::object& parser_object = parser.data().as_object();

			Assert::AreEqual(coder_object.at("action_type").as_string().c_str(), parser_object.at("action_type").as_string().c_str());
			Assert::AreEqual(coder_object.at("target_player_id").as_int64(), parser_object.at("target_player_id").as_int64());
			Assert::IsTrue(parser_object.at("card_info").is_object());
			Assert::IsTrue(parser_object.at("card_info").at("color").is_string());
			Assert::AreEqual(coder_object.at("card_info").at("color").as_string().c_str(), parser_object.at("card_info").at("color").as_string().c_str());
			Assert::IsTrue(!parser_object.at("card_info").as_object().contains("value"));

			const std::shared_ptr<common::IAction> p_parser_action = common::create_action(parser.data());
			common::CSpeak parser_action = *std::static_pointer_cast<common::CSpeak, common::IAction>(p_parser_action);

			Assert::AreEqual(coder_action.is_color_info(), parser_action.is_color_info());
			Assert::AreEqual(coder_action.is_value_info(), parser_action.is_value_info());
			Assert::AreEqual(coder_action.player_index(), parser_action.player_index());
			Assert::AreEqual(int(coder_action.color_info()), int(parser_action.color_info()));
			Assert::AreEqual(int(coder_action.type()), int(parser_action.type()));
		}

		TEST_METHOD(ActionUpdateEndToEndDrop)
		{
			// Check the convertion

			// Data at start
			common::CDrop coder_action(1);
			
			// Convertion to json
			boost::json::object coder_object = coder_action.to_json().as_object();
			Assert::IsTrue(coder_object.at("action_type").is_string());
			Assert::IsTrue(coder_object.at("hand_index").is_int64());

			// Coder
			network::CPacketCoder coder(network::PacketType::ACTION_DROP_UPDATE);
			coder.data(coder_object);
			const network::CPacket packet = coder.encode();

			// Parser
			network::CPacketParser parser(network::PacketType::ACTION_DROP_UPDATE);
			parser.decode(packet);

			Assert::AreEqual(int(boost::json::kind::object), int(parser.data().kind()));

			const boost::json::object& parser_object = parser.data().as_object();

			Assert::AreEqual(coder_object.at("action_type").as_string().c_str(), parser_object.at("action_type").as_string().c_str());
			Assert::AreEqual(coder_object.at("hand_index").as_int64(), parser_object.at("hand_index").as_int64());

			const std::shared_ptr<common::IAction> p_parser_action = common::create_action(parser.data());
			common::CDrop parser_action = *std::static_pointer_cast<common::CDrop, common::IAction>(p_parser_action);

			Assert::AreEqual(coder_action.hand_index(), parser_action.hand_index());
			Assert::AreEqual(int(coder_action.type()), int(parser_action.type()));
		}

		TEST_METHOD(ActionUpdateEndToEndPlace)
		{
			// Check the convertion

			// Data at start
			common::CPlace coder_action(1);

			// Convertion to json
			boost::json::object coder_object = coder_action.to_json().as_object();
			Assert::IsTrue(coder_object.at("action_type").is_string());
			Assert::IsTrue(coder_object.at("hand_index").is_int64());

			// Coder
			network::CPacketCoder coder(network::PacketType::ACTION_PLACE_UPDATE);
			coder.data(coder_object);
			const network::CPacket packet = coder.encode();

			// Parser
			network::CPacketParser parser(network::PacketType::ACTION_PLACE_UPDATE);
			parser.decode(packet);

			Assert::AreEqual(int(boost::json::kind::object), int(parser.data().kind()));

			const boost::json::object& parser_object = parser.data().as_object();

			Assert::AreEqual(coder_object.at("action_type").as_string().c_str(), parser_object.at("action_type").as_string().c_str());
			Assert::AreEqual(coder_object.at("hand_index").as_int64(), parser_object.at("hand_index").as_int64());

			const std::shared_ptr<common::IAction> p_parser_action = common::create_action(parser.data());
			common::CPlace parser_action = *std::static_pointer_cast<common::CPlace, common::IAction>(p_parser_action);

			Assert::AreEqual(coder_action.hand_index(), parser_action.hand_index());
			Assert::AreEqual(int(coder_action.type()), int(parser_action.type()));
		}

		TEST_METHOD(ActionUpdateEndToEndSpeakColor)
		{
			// Check the convertion

			// Data at start
			common::CSpeak coder_action(1);
			coder_action.color_info(common::CardColor::blue);

			// Convertionn to json
			boost::json::object coder_object = coder_action.to_json().as_object();
			Assert::IsTrue(coder_object.at("action_type").is_string());
			Assert::IsTrue(coder_object.at("target_player_id").is_int64());
			Assert::IsTrue(coder_object.at("card_info").is_object());
			Assert::IsTrue(coder_object.at("card_info").at("color").is_string());
			Assert::IsTrue(!coder_object.at("card_info").as_object().contains("value"));

			// Coder
			network::CPacketCoder coder(network::PacketType::ACTION_SPEAK_UPDATE);
			coder.data(coder_object);
			const network::CPacket packet = coder.encode();

			// Parser
			network::CPacketParser parser(network::PacketType::ACTION_SPEAK_UPDATE);
			parser.decode(packet);

			Assert::AreEqual(int(boost::json::kind::object), int(parser.data().kind()));

			const boost::json::object& parser_object = parser.data().as_object();

			Assert::AreEqual(coder_object.at("action_type").as_string().c_str(), parser_object.at("action_type").as_string().c_str());
			Assert::AreEqual(coder_object.at("target_player_id").as_int64(), parser_object.at("target_player_id").as_int64());
			Assert::IsTrue(parser_object.at("card_info").is_object());
			Assert::IsTrue(parser_object.at("card_info").at("color").is_string());
			Assert::AreEqual(coder_object.at("card_info").at("color").as_string().c_str(), parser_object.at("card_info").at("color").as_string().c_str());
			Assert::IsTrue(!parser_object.at("card_info").as_object().contains("value"));

			const std::shared_ptr<common::IAction> p_parser_action = common::create_action(parser.data());
			common::CSpeak parser_action = *std::static_pointer_cast<common::CSpeak, common::IAction>(p_parser_action);

			Assert::AreEqual(coder_action.is_color_info(), parser_action.is_color_info());
			Assert::AreEqual(coder_action.is_value_info(), parser_action.is_value_info());
			Assert::AreEqual(coder_action.player_index(), parser_action.player_index());
			Assert::AreEqual(int(coder_action.color_info()), int(parser_action.color_info()));
			Assert::AreEqual(int(coder_action.type()), int(parser_action.type()));
		}

		TEST_METHOD(ActionUpdateEndToEndSpeakValue)
		{
			// Check the convertion

			// Data at start
			common::CSpeak coder_action(1);
			coder_action.value_info(1);

			// Convertionn to json
			boost::json::object coder_object = coder_action.to_json().as_object();
			Assert::IsTrue(coder_object.at("action_type").is_string());
			Assert::IsTrue(coder_object.at("target_player_id").is_int64());
			Assert::IsTrue(coder_object.at("card_info").is_object());
			Assert::IsTrue(coder_object.at("card_info").at("value").is_int64());
			Assert::IsTrue(!coder_object.at("card_info").as_object().contains("color"));

			// Coder
			network::CPacketCoder coder(network::PacketType::ACTION_SPEAK_UPDATE);
			coder.data(coder_object);
			const network::CPacket packet = coder.encode();

			// Parser
			network::CPacketParser parser(network::PacketType::ACTION_SPEAK_UPDATE);
			parser.decode(packet);

			Assert::AreEqual(int(boost::json::kind::object), int(parser.data().kind()));

			const boost::json::object& parser_object = parser.data().as_object();

			Assert::AreEqual(coder_object.at("action_type").as_string().c_str(), parser_object.at("action_type").as_string().c_str());
			Assert::AreEqual(coder_object.at("target_player_id").as_int64(), parser_object.at("target_player_id").as_int64());
			Assert::IsTrue(parser_object.at("card_info").is_object());
			Assert::IsTrue(parser_object.at("card_info").at("value").is_int64());
			Assert::AreEqual(coder_object.at("card_info").at("value").as_int64(), parser_object.at("card_info").at("value").as_int64());
			Assert::IsTrue(!parser_object.at("card_info").as_object().contains("color"));

			const std::shared_ptr<common::IAction> p_parser_action = common::create_action(parser.data());
			common::CSpeak parser_action = *std::static_pointer_cast<common::CSpeak, common::IAction>(p_parser_action);

			Assert::AreEqual(coder_action.is_color_info(), parser_action.is_color_info());
			Assert::AreEqual(coder_action.is_value_info(), parser_action.is_value_info());
			Assert::AreEqual(coder_action.player_index(), parser_action.player_index());
			Assert::AreEqual(coder_action.value_info(), parser_action.value_info());
			Assert::AreEqual(int(coder_action.type()), int(parser_action.type()));
		}

		TEST_METHOD(GameEndEndToEnd)
		{
			// Check the convertion

			network::CPacketCoder coder(network::PacketType::GAME_END_UPDATE);
			boost::json::object coder_object;
			coder_object["victory"] = boost::json::value_from(true);
			coder_object["score"] = boost::json::value_from(int(10));
			coder_object["game_max_score"] = boost::json::value_from(int(3));
			coder_object["turn_count"] = boost::json::value_from(int(5));
			coder_object["error_count"] = boost::json::value_from(int(12));
			coder.data(coder_object);
			coder.encode();

			const network::CPacket packet = coder.encode();

			network::CPacketParser parser(network::PacketType::GAME_END_UPDATE);
			parser.decode(packet);

			Assert::AreEqual(int(boost::json::kind::object), int(parser.data().kind()));

			const boost::json::object& parser_object = parser.data().as_object();

			Assert::AreEqual(coder_object.at("victory").as_bool(), parser_object.at("victory").as_bool());
			Assert::AreEqual(coder_object.at("score").as_int64(), parser_object.at("score").as_int64());
			Assert::AreEqual(coder_object.at("turn_count").as_int64(), parser_object.at("turn_count").as_int64());
			Assert::AreEqual(coder_object.at("error_count").as_int64(), parser_object.at("error_count").as_int64());
		}

		TEST_METHOD(GameInitEndToEnd)
		{
			// Check the convertion
			const server_side::CGameRules server_rules = server_side::create_game_rules_std();

			const std::shared_ptr<std::mt19937> p_random_generator(new std::mt19937);

			const server_side::CGameOption server_option = server_rules.create_game(2, p_random_generator);

			network::CPacketCoder coder(network::PacketType::GAME_OPTION_UPDATE);
			boost::json::object coder_object = server_option.pack(0).as_object();
			coder.data(coder_object);

			network::CPacket packet = coder.encode();

			network::CPacketParser parser(network::PacketType::GAME_OPTION_UPDATE);
			parser.decode(packet);

			Assert::AreEqual(int(boost::json::kind::object), int(parser.data().kind()));

			const boost::json::object& parser_object = parser.data().as_object();

			Assert::AreEqual(coder_object.at("game_uuid").as_string().c_str(), parser_object.at("game_uuid").as_string().c_str());
			Assert::AreEqual(coder_object.at("timestamp").as_string().c_str(), parser_object.at("timestamp").as_string().c_str());
			Assert::AreEqual(coder_object.at("player_count").as_int64(), parser_object.at("player_count").as_int64());
			Assert::AreEqual(coder_object.at("player_hand_max_size").as_int64(), parser_object.at("player_hand_max_size").as_int64());
			Assert::AreEqual(coder_object.at("start_player_id").as_int64(), parser_object.at("start_player_id").as_int64());
			Assert::AreEqual(coder_object.at("max_error_token_count").as_int64(), parser_object.at("max_error_token_count").as_int64());
			Assert::AreEqual(coder_object.at("max_speak_token_count").as_int64(), parser_object.at("max_speak_token_count").as_int64());
			Assert::AreEqual(coder_object.at("start_error_token_count").as_int64(), parser_object.at("start_error_token_count").as_int64());
			Assert::AreEqual(coder_object.at("start_speak_token_count").as_int64(), parser_object.at("start_speak_token_count").as_int64());
			Assert::AreEqual(coder_object.at("min_card_value").as_int64(), parser_object.at("min_card_value").as_int64());
			Assert::AreEqual(coder_object.at("max_card_value").as_int64(), parser_object.at("max_card_value").as_int64());
			Assert::AreEqual(coder_object.at("stack_size").as_int64(), parser_object.at("stack_size").as_int64());
			
			client_side::CGameOption client_option;
			client_option.update(parser_object);

			void color_option(const common::TColorOptionMap & color_option);

			Assert::AreEqual(boost::uuids::to_string(server_option.game_uuid()), boost::uuids::to_string(client_option.game_uuid()));
			Assert::AreEqual(boost::posix_time::to_iso_extended_string(server_option.timestamp()), boost::posix_time::to_iso_extended_string(client_option.timestamp()));
			Assert::AreEqual(server_option.player_count(), client_option.player_count());
			Assert::AreEqual(server_option.player_hand_max_size(), client_option.player_hand_max_size());
			Assert::AreEqual(server_option.start_player_id(), client_option.start_player_id());
			Assert::AreEqual(server_option.max_error_token_count(), client_option.max_error_token_count());
			Assert::AreEqual(server_option.max_speak_token_count(), client_option.max_speak_token_count());
			Assert::AreEqual(server_option.start_error_token_count(), client_option.start_error_token_count());
			Assert::AreEqual(server_option.start_speak_token_count(), client_option.start_speak_token_count());
			Assert::AreEqual(server_option.min_card_value(), client_option.min_card_value());
			Assert::AreEqual(server_option.max_card_value(), client_option.max_card_value());
			Assert::AreEqual(server_option.game_max_score(), client_option.game_max_score());
			Assert::AreEqual(server_option.stack().size(), client_option.stack_size());

			Assert::AreEqual(server_option.color_option().size(), client_option.color_option().size());

			for (const auto& pair : server_option.color_option())
			{
				Assert::IsTrue(client_option.color_option().contains(pair.first));

				const common::CColorOption& option = client_option.color_option().at(pair.first);

				Assert::AreEqual(pair.second.is_ascending(), option.is_ascending());

				Assert::AreEqual(pair.second.distribution().size(), option.distribution().size());

				for (const auto& distri_pair : pair.second.distribution())
				{
					Assert::IsTrue(option.distribution().contains(distri_pair.first));

					Assert::AreEqual(distri_pair.second, option.distribution().at(distri_pair.first));
				}
			}
		}
	};
}
