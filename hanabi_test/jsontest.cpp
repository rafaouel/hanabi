#include "pch.h"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace hanabi;

namespace json_test
{
	TEST_CLASS(JsonTest)
	{
	public:

		TEST_METHOD(JsonParserWithSpaces)
		{
			// Create a json object
			boost::json::object initial_object;
			initial_object["int"] = boost::json::value_from(12);
			initial_object["string"] = boost::json::value_from(std::string("Hello world !"));
			initial_object["array"] = boost::json::value_from(std::vector<int>({ 1, 2, 3, 4 }));
			
			// Send it to a stream
			std::ostringstream oss;
			oss << initial_object;

			// Parse the stream
			const boost::json::object parsed_object = boost::json::parse(oss.str()).as_object();
			const std::string parsed_string(parsed_object.at("string").as_string().c_str());
			Assert::AreEqual(initial_object.at("string").as_string().c_str(), parsed_object.at("string").as_string().c_str());

		}

		TEST_METHOD(JsonStreamParserWithSpaces)
		{
			// Create a json object
			boost::json::object initial_object;
			initial_object["int"] = boost::json::value_from(12);
			initial_object["string"] = boost::json::value_from(std::string("Hello world !"));
			initial_object["array"] = boost::json::value_from(std::vector<int>({ 1, 2, 3, 4 }));

			// Send it to a stream
			std::ostringstream oss;
			oss << initial_object;
			std::istringstream iss(oss.str());

			// Parse the stream
			boost::json::stream_parser parser;
			boost::json::error_code ec;
			std::string line;
			while (std::getline(iss, line))
			{
				parser.write(line, ec);
				Assert::IsTrue(!ec);
			}
			parser.finish(ec);
			Assert::IsTrue(!ec);

			const boost::json::object parsed_object = parser.release().as_object();
			const std::string parsed_string(parsed_object.at("string").as_string().c_str());
			Assert::AreEqual(initial_object.at("string").as_string().c_str(), parsed_object.at("string").as_string().c_str());
		}
		
		TEST_METHOD(SerializeServerGameOption)
		{
			server_side::CGameRules rules = server_side::create_game_rules_std();
			const std::size_t player_count = 2;
			const std::shared_ptr<std::mt19937> p_random_generator(new std::mt19937);
			server_side::CGameOption option = rules.create_game(player_count, p_random_generator);
			const boost::json::value root_value = option.to_json();

			Assert::IsTrue(root_value.is_object());
			const boost::json::object& root_object = root_value.as_object();

			Assert::IsTrue(root_object.contains("game_uuid"));
			Assert::IsTrue(root_object.at("game_uuid").is_string());
			
			Assert::IsTrue(root_object.contains("timestamp"));
			Assert::IsTrue(root_object.at("timestamp").is_string());

			Assert::IsTrue(root_object.contains("player_count"));
			Assert::IsTrue(root_object.at("player_count").is_int64());

			Assert::IsTrue(root_object.contains("player_hand_max_size"));
			Assert::IsTrue(root_object.at("player_hand_max_size").is_int64());
			
			Assert::IsTrue(root_object.contains("start_player_id"));
			Assert::IsTrue(root_object.at("start_player_id").is_int64());

			Assert::IsTrue(root_object.contains("max_error_token_count"));
			Assert::IsTrue(root_object.at("max_error_token_count").is_int64());

			Assert::IsTrue(root_object.contains("max_speak_token_count"));
			Assert::IsTrue(root_object.at("max_speak_token_count").is_int64());

			Assert::IsTrue(root_object.contains("start_error_token_count"));
			Assert::IsTrue(root_object.at("start_error_token_count").is_int64());

			Assert::IsTrue(root_object.contains("start_speak_token_count"));
			Assert::IsTrue(root_object.at("start_speak_token_count").is_int64());

			Assert::IsTrue(root_object.contains("min_card_value"));
			Assert::IsTrue(root_object.at("min_card_value").is_int64());

			Assert::IsTrue(root_object.contains("max_card_value"));
			Assert::IsTrue(root_object.at("max_card_value").is_int64());

			Assert::IsTrue(root_object.contains("color_option"));
			Assert::IsTrue(root_object.at("color_option").is_array());

			for (const auto& item_value : root_object.at("color_option").as_array())
			{
				Assert::IsTrue(item_value.is_object());
				const boost::json::object& item_object = item_value.as_object();

				Assert::IsTrue(item_object.contains("color"));
				Assert::IsTrue(item_object.at("color").is_string());

				Assert::IsTrue(item_object.contains("is_ascending"));
				Assert::IsTrue(item_object.at("is_ascending").is_bool());

				Assert::IsTrue(item_object.contains("distribution"));
				Assert::IsTrue(item_object.at("distribution").is_array());

				for (const auto& distri_value : item_object.at("distribution").as_array())
				{
					Assert::IsTrue(distri_value.is_object());
					const boost::json::object& distri_object = distri_value.as_object();

					Assert::IsTrue(distri_object.contains("value"));
					Assert::IsTrue(distri_object.at("value").is_int64());

					Assert::IsTrue(distri_object.contains("count"));
					Assert::IsTrue(distri_object.at("count").is_int64());
				}
			}

			Assert::IsTrue(root_object.contains("stack"));
			Assert::IsTrue(root_object.at("stack").is_array());

			for (const auto& item_value : root_object.at("stack").as_array())
			{
				Assert::IsTrue(item_value.is_object());
				const boost::json::object& item_object = item_value.as_object();

				Assert::IsTrue(item_object.contains("color"));
				Assert::IsTrue(item_object.at("color").is_string());

				Assert::IsTrue(item_object.contains("value"));
				Assert::IsTrue(item_object.at("value").is_int64());
			}
		}

		TEST_METHOD(EndToEndGameOption)
		{
			server_side::CGameRules rules = server_side::create_game_rules_std();
			const std::size_t player_count = 2;
			const std::shared_ptr<std::mt19937> p_random_generator(new std::mt19937);
			server_side::CGameOption option = rules.create_game(player_count, p_random_generator);
			const boost::json::value root_value = option.to_json();

			server_side::CGameOption option_parsed;
			option_parsed.from_json(root_value);

			Assert::AreEqual(option.player_count(), option_parsed.player_count());
			Assert::AreEqual(option.player_hand_max_size(), option_parsed.player_hand_max_size());
			Assert::AreEqual(option.start_player_id(), option_parsed.start_player_id());
			Assert::AreEqual(option.max_error_token_count(), option_parsed.max_error_token_count());
			Assert::AreEqual(option.max_speak_token_count(), option_parsed.max_speak_token_count());
			Assert::AreEqual(option.start_error_token_count(), option_parsed.start_error_token_count());
			Assert::AreEqual(option.start_speak_token_count(), option_parsed.start_speak_token_count());
			Assert::AreEqual(option.min_card_value(), option_parsed.min_card_value());
			Assert::AreEqual(option.max_card_value(), option_parsed.max_card_value());
			Assert::AreEqual(option.game_max_score(), option_parsed.game_max_score());
			
			Assert::AreEqual(option.color_option().size(), option_parsed.color_option().size());

			for (const auto& pair : option.color_option())
			{
				Assert::IsTrue(option_parsed.color_option().contains(pair.first));

				const auto& parsed_pair = option_parsed.color_option().at(pair.first);

				Assert::AreEqual(pair.second.is_ascending(), parsed_pair.is_ascending());

				Assert::AreEqual(pair.second.distribution().size(), parsed_pair.distribution().size());

				for (const auto& distri : pair.second.distribution())
				{
					Assert::IsTrue(parsed_pair.distribution().contains(distri.first));

					const auto& parsed_distri = parsed_pair.distribution().at(distri.first);

					Assert::AreEqual(distri.second, parsed_distri);
				}
			}

			Assert::AreEqual(option.stack().size(), option_parsed.stack().size());

			for (std::size_t i = 0; i < option.stack().size(); ++i)
			{
				const common::CCard& card = option.stack().at(i);
				const common::CCard& parsed_card = option_parsed.stack().at(i);

				Assert::AreEqual(card.value(), parsed_card.value());
				Assert::AreEqual(common::card_color_to_string(card.color()), common::card_color_to_string(parsed_card.color()));
			}
		}
	};
}
